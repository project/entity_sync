<?php

namespace Drupal\Tests\entity_sync\Unit\Utility;

use Drupal\entity_sync\Utility\EntityValidation;
use Drupal\Tests\entity_sync\TestTrait\DataProviderTrait;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\Validator\ConstraintViolationInterface;

require_once __DIR__ . '/../../TestTrait/DataProviderTrait.php';

/**
 * @coversDefaultClass \Drupal\entity_sync\Utility\EntityValidation
 *
 * @group contrib
 * @group entity_sync
 * @group unit
 */
class EntityValidationTest extends UnitTestCase {

  use DataProviderTrait;

  /**
   * Data provider for successful cases where the violation has a code.
   */
  public function dataProviderSuccessWithCode() {
    $providers = [
      'violationWithCodeDataProvider',
      'allowedViolationsSuccessWithCodeDataProvider',
    ];

    return $this->combineDataProviders($providers);
  }

  /**
   * Data provider for successful cases where the violation does not have a code.
   */
  public function dataProviderSuccessWithoutCode() {
    $providers = [
      'violationWithoutCodeDataProvider',
      'allowedViolationsSuccessWithoutCodeDataProvider',
    ];

    return $this->combineDataProviders($providers);
  }

  /**
   * Data provider for failed cases where the violation has a code.
   */
  public function dataProviderFailureWithCode() {
    $providers = [
      'violationWithCodeDataProvider',
      'allowedViolationsFailureDataProvider',
    ];

    return $this->combineDataProviders($providers);
  }

  /**
   * Data provider for failed cases where the violation does not have a code.
   */
  public function dataProviderFailureWithoutCode() {
    $providers = [
      'violationWithoutCodeDataProvider',
      'allowedViolationsFailureDataProvider',
    ];

    return $this->combineDataProviders($providers);
  }

  /**
   * @covers ::isViolationAllowed
   * @dataProvider dataProviderSuccessWithCode
   */
  public function testIsViolationAllowedWithCodeSuccess(
    array $violation_data,
    array $allowed_violations,
  ) {
    $violation = $this->prophesize(ConstraintViolationInterface::class);
    $violation->getCode()->willReturn($violation_data[1]);

    $allowed = EntityValidation::isViolationAllowed(
      $violation_data[0],
      $violation->reveal(),
      $allowed_violations
    );

    $this->assertEquals(TRUE, $allowed);
  }

  /**
   * @covers ::isViolationAllowed
   * @dataProvider dataProviderSuccessWithoutCode
   */
  public function testIsViolationAllowedWithoutCodeSuccess(
    array $violation_data,
    array $allowed_violations,
  ) {
    $violation = $this->prophesize(ConstraintViolationInterface::class);
    $violation->getCode()->willReturn($violation_data[1]);

    $allowed = EntityValidation::isViolationAllowed(
      $violation_data[0],
      $violation->reveal(),
      $allowed_violations
    );

    $this->assertEquals(TRUE, $allowed);
  }

  /**
   * @covers ::isViolationAllowed
   * @dataProvider dataProviderFailureWithCode
   */
  public function testIsViolationAllowedFailureWithCode(
    array $violation_data,
    array $allowed_violations,
  ) {
    $violation = $this->prophesize(ConstraintViolationInterface::class);
    $violation->getCode()->willReturn($violation_data[1]);

    $allowed = EntityValidation::isViolationAllowed(
      $violation_data[0],
      $violation->reveal(),
      $allowed_violations
    );

    $this->assertEquals(FALSE, $allowed);
  }

  /**
   * @covers ::isViolationAllowed
   * @dataProvider dataProviderFailureWithoutCode
   */
  public function testIsViolationAllowedFailureWithoutCode(
    array $violation_data,
    array $allowed_violations,
  ) {
    $violation = $this->prophesize(ConstraintViolationInterface::class);
    $violation->getCode()->willReturn($violation_data[1]);

    $allowed = EntityValidation::isViolationAllowed(
      $violation_data[0],
      $violation->reveal(),
      $allowed_violations
    );

    $this->assertEquals(FALSE, $allowed);
  }

  /**
   * Violation data for test cases were the violation has an error code.
   *
   * Violations always have a field for our use case, which is extracted from
   * the violation path.
   */
  private function violationWithCodeDataProvider() {
    return [
      // Case: violation with error code.
      [
        // Field on which the violation occurred.
        'field_node',
        // Error code of the violation that occurred.
        'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed',
      ],
    ];
  }

  /**
   * Violation data for test cases were the violation does not have an error code.
   */
  private function violationWithoutCodeDataProvider() {
    return [
      // Case: violation with no error code.
      [
        'field_node',
        NULL,
      ],
      // Case: violation with empty error code.
      [
        'field_node',
        '',
      ],
    ];
  }

  /**
   * Settings data for test cases for allowed violations with error codes.
   */
  private function allowedViolationsSuccessWithCodeDataProvider() {
    return [
      // Single group, single-value field property.
      [
        [
          'field' => 'field_node',
        ],
      ],
      // Single group, multi-value field property.
      [
        [
          'field' => 'field_node|field_term',
        ],
      ],
      // Single group, single-value code property.
      [
        [
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed',
        ],
      ],
      // Single group, multi-value field property.
      [
        [
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed|b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b',
        ],
      ],
      // Single group, multiple properties (field/code) single-value each.
      [
        [
          'field' => 'field_node',
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed',
        ],
      ],
      // Single group, multiple properties (field/code) multi-value each.
      [
        [
          'field' => 'field_node|field_term',
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed|b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b',
        ],
      ],
      // Multiple groups, one failing one succeeding.
      [
        [
          'field' => 'field_term',
        ],
        [
          'field' => 'field_node|field_term',
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed|b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b',
        ],
      ],
    ];
  }

  /**
   * Settings data for test cases for allowed violations without error codes.
   */
  private function allowedViolationsSuccessWithoutCodeDataProvider() {
    return [
      // Single group, single-value field property.
      [
        [
          'field' => 'field_node',
        ],
      ],
      // Single group, multi-value field property.
      [
        [
          'field' => 'field_node|field_term',
        ],
      ],
      // Multiple groups, one failing one succeeding.
      [
        [
          'field' => 'field_node',
        ],
        [
          'field' => 'field_node',
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed|b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b',
        ],
      ],
    ];
  }

  /**
   * Settings data for test cases for all disallowed violations.
   */
  private function allowedViolationsFailureDataProvider() {
    return [
      // No allowed violations.
      [],
      // Single group, single-value field property.
      [
        [
          'field' => 'field_term',
        ],
      ],
      // Single group, multi-value field property.
      [
        [
          'field' => 'field_term|field_content',
        ],
      ],
      // Single group, single-value code property.
      [
        [
          'code' => 'b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b',
        ],
      ],
      // Single group, multi-value field property.
      [
        [
          'code' => 'b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b|69945ac1-2db4-405f-bec7-d2772f73df52',
        ],
      ],
      // Single group, multiple properties (field/code) single-value each.
      // One failing, one suceeding.
      [
        [
          'field' => 'field_term',
          'code' => 'f4ebb66a-201d-473d-a1a1-b5ea2c9583ed',
        ],
      ],
      // Single group, multiple properties (field/code) single-value each.
      // Both failing.
      [
        [
          'field' => 'field_term',
          'code' => 'b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b',
        ],
      ],
      // Single group, multiple properties (field/code) multi-value each.
      // One failing, one suceeding.
      [
        [
          'field' => 'field_node|field_term',
          'code' => 'b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b|69945ac1-2db4-405f-bec7-d2772f73df52',
        ],
      ],
      // Single group, multiple properties (field/code) multi-value each.
      // Both failing.
      [
        [
          'field' => 'field_term|field_content',
          'code' => 'b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b|69945ac1-2db4-405f-bec7-d2772f73df52',
        ],
      ],
      // Multiple groups, all failing.
      [
        [
          'field' => 'field_term',
        ],
        [
          'field' => 'field_node|field_term',
          'code' => 'b1b427ae-9f6f-41b0-aa9b-84511fbb3c5b|69945ac1-2db4-405f-bec7-d2772f73df52',
        ],
      ],
    ];
  }

}
