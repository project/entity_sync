<?php

/**
 * @file
 * Post update functions for the Entity Synchronization module.
 */

/**
 * Imports the operations view if it doesn't already exist.
 */
function entity_sync_post_update_1() {
  \Drupal::service('entity_sync.updater.post_1')->run();
}
