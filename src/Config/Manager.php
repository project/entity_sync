<?php

namespace Drupal\entity_sync\Config;

use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Default implementation of the Entity Sync configuration manager.
 *
 * phpcs:disable
 * @I Rename to `SyncManager` to separate from `ProviderManager`
 *    type     : task
 *    priority : low
 *    labels   : config, structure
 * phpcs:enable
 */
class Manager implements ManagerInterface {

  /**
   * The Entity Synchronization configuration entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * Constructs a new Manager instance.
   *
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The Entity Synchronization configuration entity loader.
   */
  public function __construct(ConfigEntityLoaderInterface $config_entity_loader) {
    $this->configEntityLoader = $config_entity_loader;
  }

  /**
   * {@inheritdoc}
   *
   * phpcs:disable
   * @I Rewrite the `getSyncs` method so that it is more performant
   *    type     : task
   *    priority : low
   *    labels   : config, performance
   *    notes    : We use multiple array reduce/filter functions because it is
   *               easier to follow the logic. However, the function ends up
   *               being too long and that makes the readability benefits
   *               questionable so the performance hit is not justified. Review
   *               implementation.
   * phpcs:enable
   */
  public function getSyncs(array $filters = []) {
    $filters = NestedArray::mergeDeep(
      [
        'status' => NULL,
        'local_entity' => ['type_id' => NULL],
        'operation' => [
          'id' => NULL,
          'status' => NULL,
        ],
      ],
      $filters
    );

    $syncs = $this->configEntityLoader->loadMultipleWithPluginInstantiated();
    if (!$syncs) {
      return [];
    }

    // Filter out any synchronization that do not match the status, if given.
    $syncs = array_filter(
      $syncs,
      function ($sync) use ($filters) {
        if ($filters['status'] === NULL) {
          return TRUE;
        }

        if ($sync->status() !== $filters['status']) {
          return FALSE;
        }

        return TRUE;
      }
    );

    // Filter out any synchronization that do not match the entity type ID
    // filter, if given.
    $syncs = array_filter(
      $syncs,
      function ($sync) use ($filters) {
        if (!$filters['local_entity']['type_id']) {
          return TRUE;
        }

        $type_id = $sync->getLocalEntitySettings()['type_id'] ?? NULL;
        if ($type_id !== $filters['local_entity']['type_id']) {
          return FALSE;
        }

        return TRUE;
      }
    );

    // Filter out any synchronization that do not match the operation ID and
    // status, if given.
    return array_filter(
      $syncs,
      function ($sync) use ($filters) {
        if (!$filters['operation']['id']) {
          return TRUE;
        }

        $operation = $sync->getOperationsSettings()[$filters['operation']['id']] ?? NULL;
        if (!$operation) {
          return FALSE;
        }

        if ($filters['operation']['status'] === NULL) {
          return TRUE;
        }

        if ($operation['status'] !== $filters['operation']['status']) {
          return FALSE;
        }

        return TRUE;
      }
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mergeFieldMappingDefaults(array $config) {
    return NestedArray::mergeDeep(
      $this->fieldMappingDefaults(),
      $config
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mergeExportFieldMappingDefaults(array $config) {
    return NestedArray::mergeDeep(
      $this->exportFieldMappingDefaults(),
      $config
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mergeExportComputedValueDefaults(array $config) {
    return NestedArray::mergeDeep(
      $this->exportComputedValueDefaults(),
      $config
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mergeImportFieldMappingDefaults(array $config) {
    return NestedArray::mergeDeep(
      $this->importFieldMappingDefaults(),
      $config
    );
  }

  /**
   * {@inheritdoc}
   */
  public function mergeImportComputedValueDefaults(array $config) {
    return NestedArray::mergeDeep(
      $this->importComputedValueDefaults(),
      $config
    );
  }

  /**
   * {@inheritdoc}
   */
  public function fieldMappingDefaults() {
    return $this->exportFieldMappingDefaults() +
      $this->importFieldMappingDefaults();
  }

  /**
   * {@inheritdoc}
   */
  public function exportFieldMappingDefaults() {
    return [
      'export' => [
        'status' => TRUE,
        'skip' => FALSE,
        'callback' => [],
        'transformer' => [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function exportComputedValueDefaults() {
    return [
      'export' => [
        'status' => TRUE,
        'callback' => [],
        'transformer' => [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function importFieldMappingDefaults() {
    return [
      'import' => [
        'status' => TRUE,
        'callback' => [],
        'transformer' => [],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function importComputedValueDefaults() {
    return [
      'import' => [
        'status' => TRUE,
        'callback' => [],
        'transformer' => [],
      ],
    ];
  }

}
