<?php

namespace Drupal\entity_sync\Utility;

use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Utility methods that provide functionality related to entity validation.
 */
class EntityValidation {

  /**
   * Returns whether the given violation should be allowed.
   *
   * @param string $field_name
   *   The machine name of the field on which the violation occurred.
   * @param \Symfony\Component\Validator\ConstraintViolationInterface $violation
   *   The violation object.
   * @param array $allowed_violations
   *   The settings that determine which violations are allowed to occur, as
   *   provided by the synchronization configuration.
   *
   * @return bool
   *   `TRUE` if the violation should be allowed, `FALSE` otherwise.
   *
   * @see config/schema/entity_sync.schema.yml
   */
  public static function isViolationAllowed(
    string $field_name,
    ConstraintViolationInterface $violation,
    array $allowed_violations
  ) {
    if (empty($allowed_violations)) {
      return FALSE;
    }

    $expand = fn ($p) => explode('|', $p);

    $value_builders = [
      'field' => fn ($v) => $field_name,
      'code' => fn ($v) => $violation->getCode(),
    ];

    $allow = function (
      $property_key,
      $property_value
    ) use (
      $expand,
      $value_builders
    ) {
      return \in_array(
        $value_builders[$property_key]($property_value),
        $expand($property_value),
        TRUE
      );
    };

    $allowed = FALSE;

    foreach ($allowed_violations as $settings_group) {
      foreach ($settings_group as $property_key => $property_value) {
        $allowed = $allow(
          $property_key,
          $property_value
        );
        // Within a group we have an AND operator. If we have even just one
        // property check that fails, the group does not allow the violation.
        if ($allowed === FALSE) {
          break;
        }
      }

      // Between all groups we have an OR operator. If we have even just one
      // group that allows the violation, the violation is allowed.
      if ($allowed === TRUE) {
        break;
      }
    }

    return $allowed;
  }

}
