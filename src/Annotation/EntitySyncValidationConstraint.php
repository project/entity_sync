<?php

namespace Drupal\entity_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for validation constraint plugins.
 *
 * Plugin namespace: Plugin\EntitySync\ValidationConstraint.
 *
 * @Annotation
 * phpcs:disable
 * @I Support defining applicable field types
 *    type     : improvement
 *    priority : normal
 *    labels   : validation
 * phpcs:enable
 */
class EntitySyncValidationConstraint extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
