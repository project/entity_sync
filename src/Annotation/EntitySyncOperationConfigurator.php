<?php

namespace Drupal\entity_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for the operation configurator plugins.
 *
 * Plugin namespace: Plugin\EntitySync\OperationConfigurator.
 *
 * @Annotation
 */
class EntitySyncOperationConfigurator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The action types supported by the plugin.
   *
   * Action types are what are called `operations` in the original
   * synchronization configuration terminology that is still in use. Not to be
   * confused with the Operation entity.
   *
   * Currently available action types are:
   * - import_list
   * - import_entity
   * - export_list
   * - export_entity
   *
   * See `entity_sync.operation.*` in `entity_sync.schema.yml`.
   *
   * @var string
   */
  public $action_types;

  /**
   * The workflow ID for the operation state field.
   *
   * @var string
   */
  public $workflow_id = 'entity_sync_operation_default';

}
