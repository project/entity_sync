<?php

namespace Drupal\entity_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for the field transformer plugins.
 *
 * Plugin namespace: Plugin\EntitySync\FieldTransformer.
 *
 * @Annotation
 * phpcs:disable
 * @I Support defining applicable field types
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer, validation
 * phpcs:enable
 */
class EntitySyncFieldTransformer extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
