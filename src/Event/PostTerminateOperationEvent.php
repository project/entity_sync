<?php

namespace Drupal\entity_sync\Event;

use Drupal\entity_sync\Entity\SyncInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the operation post-terminate event.
 *
 * Allows subscribers to respond after an operation has been terminated.
 */
class PostTerminateOperationEvent extends Event {

  /**
   * The operation that was terminated.
   *
   * @var string
   */
  protected $operation;

  /**
   * The context of the operation.
   *
   * @var array
   */
  protected $context;

  /**
   * The Synchronization or Operation Type configuration entity.
   *
   * @var \Drupal\entity_sync\Entity\SyncInterface
   */
  protected $sync;

  /**
   * Custom data related to the operation that was terminated.
   *
   * @var array
   */
  protected $data;

  /**
   * Constructs a new PostTerminateOperationEvent object.
   *
   * @param string $operation
   *   The operation that was terminated.
   * @param array $context
   *   The context array.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $data
   *   Custom data related to the operation that was terminated.
   */
  public function __construct(
    $operation,
    array $context,
    SyncInterface $sync,
    array $data = []
  ) {
    $this->operation = $operation;
    $this->context = $context;
    $this->sync = $sync;
    $this->data = $data;
  }

  /**
   * Gets the operation.
   *
   * @return string
   *   The operation.
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Gets the context array.
   *
   * @return array
   *   The context array.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Gets the Synchronization or Operation Type configuration entity.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface
   *   The Synchronization or Operation Type configuration entity.
   */
  public function getSync() {
    return $this->sync;
  }

  /**
   * Gets the custom data.
   *
   * @return array
   *   The custom data associated with the operation.
   */
  public function getData() {
    return $this->data;
  }

}
