<?php

namespace Drupal\entity_sync\Event;

use Drupal\entity_sync\Entity\SyncInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the operation terminate event.
 *
 * Allows subscribers to respond after an operation has been executed.
 */
class TerminateOperationEvent extends Event {

  /**
   * The operation that was executed.
   *
   * @var string
   */
  protected $operation;

  /**
   * The context of the operation.
   *
   * @var array
   */
  protected $context;

  /**
   * The Synchronization or Operation Type configuration entity.
   *
   * @var \Drupal\entity_sync\Entity\SyncInterface
   */
  protected $sync;

  /**
   * Custom data related to the operation that was executed.
   *
   * @var array
   */
  protected $data;

  /**
   * Whether to save the local entity at the end of the terminate event.
   *
   * When an entity is imported, a common requirement is to make changes in a
   * terminate event after the entity has already been saved. For example,
   * update another entity field using the ID of the newly created entity, or to
   * create the shipments for an imported order (which requires the order ID to
   * be available) which then requires storing the shipments in the order field
   * and saving the order.
   *
   * To prevent multiple subscribers unnecessarily saving the local entity
   * multiple times in such cases, we allow them to set this property to
   * TRUE. The import entity manager will save the local entity after all
   * terminate subscribers have run, if requested.
   *
   * @var bool
   */
  protected $saveLocalEntity;

  /**
   * Constructs a new TerminateOperationEvent object.
   *
   * @param string $operation
   *   The operation that was executed.
   * @param array $context
   *   The context array.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $data
   *   Custom data related to the operation that was executed.
   */
  public function __construct(
    $operation,
    array $context,
    SyncInterface $sync,
    array $data = []
  ) {
    $this->operation = $operation;
    $this->context = $context;
    $this->sync = $sync;
    $this->data = $data;

    $this->saveLocalEntity = FALSE;
  }

  /**
   * Gets the operation.
   *
   * @return string
   *   The operation.
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Gets the context array.
   *
   * @return array
   *   The context array.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Gets the Synchronization or Operation Type configuration entity.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface
   *   The Synchronization or Operation Type configuration entity.
   */
  public function getSync() {
    return $this->sync;
  }

  /**
   * Gets the custom data.
   *
   * @return array
   *   The custom data associated with the operation.
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Gets the save local entity flag.
   *
   * @return bool
   *   Returns the flag indicating whether the local entity should be saved
   *   after all terminate subscribers have run.
   */
  public function getSaveLocalEntity() {
    return $this->saveLocalEntity;
  }

  /**
   * Sets the save local entity flag.
   *
   * @param bool $save_local_entity
   *   The flag indicating whether the local entity should be saved after all
   *   terminate subscribers have run.
   */
  public function setSaveLocalEntity($save_local_entity) {
    return $this->saveLocalEntity = $save_local_entity;
  }

}
