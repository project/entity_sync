<?php

namespace Drupal\entity_sync\Event;

use Drupal\entity_sync\Entity\SyncInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the operation queue event.
 *
 * Allows subscribers to act before an operation is queued. Subscribers are
 * also given the opportunity to cancel the queueing.
 */
class QueueOperationEvent extends Event {

  /**
   * The operation that is being executed.
   *
   * @var string
   */
  protected $operation;

  /**
   * The context of the queueing.
   *
   * Not to be confused with the context of the operation at the moment of its
   * execution.
   *
   * @var array
   */
  protected $context;

  /**
   * The context stored in the queue.
   *
   * If queueing the operation is not cancelled, this context will be stored in
   * the queue and made available to the runner/manager when running the
   * operation. Should be careful to not store any data that could cause
   * serialization issues.
   *
   * @var array
   */
  protected $storedContext;

  /**
   * The Synchronization or Operation Type configuration entity.
   *
   * @var \Drupal\entity_sync\Entity\SyncInterface
   */
  protected $sync;

  /**
   * Whether queueing the operation should be cancelled.
   *
   * @var bool
   */
  protected $cancel;

  /**
   * Text messages containing the reason(s) for cancellation.
   *
   * @var array
   */
  protected $cancellationMessages;

  /**
   * Constructs a new QueueOperationEvent object.
   *
   * @param string $operation
   *   The operation that is being queued.
   * @param array $context
   *   The context array.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $stored_context
   *   The stored context array.
   */
  public function __construct(
    string $operation,
    array $context,
    SyncInterface $sync,
    array $stored_context = []
  ) {
    $this->operation = $operation;
    $this->context = $context;
    $this->sync = $sync;
    $this->storedContext = $stored_context;

    $this->cancel = FALSE;
    $this->cancellationMessages = [];
  }

  /**
   * Gets the operation.
   *
   * @return string
   *   The operation.
   */
  public function getOperation() {
    return $this->operation;
  }

  /**
   * Gets the context array.
   *
   * @return array
   *   The context array.
   */
  public function getContext() {
    return $this->context;
  }

  /**
   * Gets the context array.
   *
   * @return array
   *   The context array.
   */
  public function getStoredContext() {
    return $this->storedContext;
  }

  /**
   * Sets the context array.
   *
   * It allows subscribers to add information to the context that may be used by
   * other subscribers that follow, or by the queue manager at the end of the
   * event processing chain.
   *
   * This method will override the whole context array; most commonly the
   * `addContext` method should be used to minimize the risk of mistakenly
   * removing context items that might be required by other subscribers or by
   * the queue manager.
   *
   * @param array $context
   *   The stored context array.
   */
  public function setStoredContext(array $context) {
    $this->storedContext = $context;
  }

  /**
   * Adds items to the context array.
   *
   * It allows subscribers to add information to the context that may be used by
   * other subscribers that follow, or by the queue manager at the end of the
   * event processing chain.
   *
   * If context elements with the same keys already exist, they will be
   * overridden. No deep merge.
   *
   * @param array $context
   *   An associative array containing elements to add to the stored context
   *   array.
   */
  public function addStoredContext(array $context) {
    $this->storedContext = $context + $this->storedContext;
  }

  /**
   * Gets the Synchronization or Operation Type configuration entity.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface
   *   The Synchronization or Operation Type configuration entity.
   */
  public function getSync() {
    return $this->sync;
  }

  /**
   * Sets the operation to be cancelled with the given message.
   *
   * @param string|null $message
   *   The reason for the cancellation; it will be logged as a warning by the
   *   logger service. NULL if the operation should be cancelled without logging
   *   a message.
   */
  public function cancel($message = NULL) {
    $this->cancel = TRUE;

    if ($message === NULL) {
      return;
    }

    $this->cancellationMessages[] = $message;
  }

  /**
   * Gets whether queueing should be cancelled with any messages available.
   *
   * @return array
   *   An array containing the following elements in the given order:
   *   - A boolean that is TRUE if queueing should be cancelled, FALSE
   *     otherwise.
   *   - An array of text messages with the reason(s) that the queueing was
   *     cancelled, if applicable.
   */
  public function getCancellations() {
    return [$this->cancel, $this->cancellationMessages];
  }

}
