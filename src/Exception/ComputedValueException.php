<?php

namespace Drupal\entity_sync\Exception;

/**
 * Exception when a runtime error occurs during computed value calculation.
 */
class ComputedValueException extends \RuntimeException {
}
