<?php

namespace Drupal\entity_sync\Exception;

/**
 * Exception that instructs the import/export field managers to skip fields.
 */
class SkipFieldException extends \RuntimeException {
}
