<?php

namespace Drupal\entity_sync\Configure\Updater;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;

/**
 * Runs the 9003 updates.
 *
 * Installs the Dynamic Entity Reference module.
 */
class Updater9003 {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * Constructs a new Updater9003 object.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $module_installer
   *   The module installer.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    ModuleInstallerInterface $module_installer
  ) {
    $this->moduleHandler = $module_handler;
    $this->moduleInstaller = $module_installer;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    if ($this->moduleHandler->moduleExists('dynamic_entity_reference')) {
      return;
    }

    $this->moduleInstaller->install(['dynamic_entity_reference']);
  }

}
