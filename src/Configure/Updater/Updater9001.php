<?php

namespace Drupal\entity_sync\Configure\Updater;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Runs the 9001 updates.
 */
class Updater9001 {

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdaterManagerInterface
   */
  protected $updateManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Updater9001 object.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityDefinitionUpdateManagerInterface $update_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->updateManager = $update_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    $entity_type_id = 'entity_sync_operation';

    // Do not install if already installed.
    $entity_type = $this->updateManager->getEntityType($entity_type_id);
    if ($entity_type) {
      return;
    }

    $this->updateManager->installEntityType(
      $this->entityTypeManager->getDefinition($entity_type_id)
    );
  }

}
