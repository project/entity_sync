<?php

namespace Drupal\entity_sync\Configure\Updater;

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Runs the 9001 updates.
 */
class PostUpdater1 {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The extension path resolver.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $pathResolver;

  /**
   * Constructs a new PostUpdater1 object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $path_resolver
   *   The extension path resolver.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    ExtensionPathResolver $path_resolver
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
    $this->pathResolver = $path_resolver;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    if (!$this->moduleHandler->moduleExists('views')) {
      return;
    }

    $view_id = 'entity_sync_operations';
    $storage = $this->entityTypeManager->getStorage('view');

    // Nothing to do if the view already exists.
    if ($storage->load($view_id)) {
      return;
    }

    $source = new FileStorage(
      $this->pathResolver->getPath('module', 'entity_sync') . '/config/optional'
    );
    $storage->create($source->read("views.view.{$view_id}"))->save();
  }

}
