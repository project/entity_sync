<?php

namespace Drupal\entity_sync\Configure\Updater;

use Drupal\entity_sync\Entity\Operation;

use Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Runs the 9002 updates.
 */
class Updater9002 {

  /**
   * The entity definition update manager.
   *
   * @var \Drupal\Core\Entity\EntityDefinitionUpdaterManagerInterface
   */
  protected $updateManager;

  /**
   * Constructs a new Updater9002 object.
   *
   * @param \Drupal\Core\Entity\EntityDefinitionUpdateManagerInterface $update_manager
   *   The entity definition update manager.
   */
  public function __construct(
    EntityDefinitionUpdateManagerInterface $update_manager
  ) {
    $this->updateManager = $update_manager;
  }

  /**
   * Runs the updates.
   */
  public function run() {
    $definition = BaseFieldDefinition::create('state')
      ->setLabel(new TranslatableMarkup('State'))
      ->setDescription(new TranslatableMarkup('The state of the operation.'))
      ->setRequired(TRUE)
      ->setSetting(
        'workflow_callback',
        [Operation::class, 'getWorkflowId']
      );

    $this->updateManager->installFieldStorageDefinition(
      'state',
      'entity_sync_operation',
      'entity_sync',
      $definition
    );
  }

}
