<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for clients that can get individual entities.
 */
interface SupportsGetClientInterface {

  /**
   * Gets the remote entity with the given primary ID.
   *
   * @param int|string $id
   *   The ID of the entity to get.
   *
   * @return object
   *   The remote entity object.
   */
  public function importEntity($id);

}
