<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for client factories implemented by providers.
 *
 * The client factory can be used to generate the client based on dynamic
 * parameters. Currently this is supported for importing or exporting a local
 * entity where event subscribers are given the opportunity to provide the
 * client configuration that will be passed on to the factory here. When
 * importing a remote entity or a list of remote entities the parameters are
 * currently loaded from the synchronization configuration.
 *
 * phpcs:disable
 * @I Support dynamically changing client configuration
 *    type     : bug
 *    priority : normal
 *    labels   : client, import
 * phpcs:enable
 */
interface ProviderClientFactoryInterface {

  /**
   * Returns an initialized client for the requested synchronization.
   *
   * @param string|null $sync_id
   *   The ID of the sync that we will be performing operations for.
   * @param array $parameters
   *   An associative array containing parameters that the factory can use to
   *   create the client.
   *
   * @return \Drupal\entity_sync\Client\ClientInterface
   *   The initialized client.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   */
  public function get($sync_id, array $parameters);

}
