<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for export clients that support delete operations.
 *
 * @see \Drupal\entity_sync\Client\ClientFactory
 */
interface SupportsDeleteClientInterface {

  /**
   * Deletes the remote entity with the given primary ID.
   *
   * @param int|string $id
   *   The ID of the remote entity that will be deleted.
   * @param array $options
   *   An associative array of options to be passed to the client. The client
   *   should document the options it supports.
   *
   * @return object
   *   The parsed response.
   */
  public function delete($id, array $options = []);

}
