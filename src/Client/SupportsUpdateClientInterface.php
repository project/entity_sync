<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for clients that can update individual entities.
 */
interface SupportsUpdateClientInterface {

  /**
   * Updates the remote entity with the given primary ID.
   *
   * @param int|string $id
   *   The ID of the remote entity that will be updated.
   * @param array $fields
   *   An associative array containing the fields that will be updated, keyed by
   *   the field name and containing the field value.
   * @param array $options
   *   An associative array of options to be passed to the client. The client
   *   should document the options it supports.
   *
   * @return object
   *   The parsed response.
   */
  public function update($id, array $fields, array $options = []);

}
