<?php

namespace Drupal\entity_sync\Client;

use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;

use Drupal\Component\Utility\NestedArray;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Factory for generating Entity Sync clients.
 *
 * @see \Drupal\entity_sync\Client\ClientInterface
 */
class ClientFactory implements ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * Returns an initialized client for the requested synchronization.
   *
   * The client configuration overrides can be used to dynamically change the
   * client configuration. For example, dynamic parameters can be passed to the
   * client factory.
   *
   * Note that if the default for `$recursive_overrides` ever changes, we may
   * need to change the default in some places where this method is called where
   * a default is defined as well to avoid code complexity. Known callers:
   * - \Drupal\entity_sync\Import\Manager::doImportRemoteList()
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type configuration entity that
   *   we will be performing operations for.
   * @param array $client_config_overrides
   *   An array containing the client configuration overrides. For supported
   *   client configuration settings see
   *   `config/schema/entity_sync.schema::entity_sync.sync.*.remote_resource.client`.
   * @param bool $recursive_overrides
   *   Defaults to FALSE, the behavior is as follows.
   *   - If FALSE the overrides will be applied using PHP's `array_replace`
   *     i.e. values for keys existing in the overrides will replace the
   *     corresponding values on the synchronization configuration.
   *   - If TRUE the overrides will be applied using
   *     `\Drupal\Component\Utility\NestedArray::mergeDeep` i.e. they will be
   *     recursively merged/replaced.
   *
   * @return \Drupal\entity_sync\Client\ClientInterface
   *   The initialized client.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When there is no Synchronization or Operation Type with the given ID.
   *
   * @link https://www.php.net/manual/en/function.array-replace.php
   * @see \Drupal\Component\Utility\NestedArray::mergeDeep
   */
  public function get(
    $sync_id,
    array $client_config_overrides = [],
    bool $recursive_overrides = FALSE
  ) {
    $sync = $this->container
      ->get('entity_sync.config_entity_loader')
      ->load($sync_id);

    // Check that the sync exists.
    if (!$sync) {
      throw new InvalidConfigurationException(
        sprintf(
          'There is no Synchronization or Operation Type with ID "%s"',
          $sync_id
        )
      );
    }

    return $this->getByClientConfig(
      $this->getOverriddenClientConfig(
        $sync,
        $client_config_overrides,
        $recursive_overrides
      ),
      $sync_id
    );
  }

  /**
   * Returns an initialized client for the requested client configuration.
   *
   * @param array $client_config
   *   An associative array containing the client configuration. Supported
   *   elements are:
   *   - type: The type of the client; currently supported types are:
   *     - service
   *     - factory
   *   - service: The Drupal service that provides the client or the client
   *     factory.
   *   - parameters: An associative array containing parameters to pass to pass
   *     to the client factory - when the type is set to `factory`.
   * @param string|null $sync_id
   *   The ID of the Synchronization or Operation Type configuration entity that
   *   we will be performing operations for, if available.
   *
   * @return \Drupal\entity_sync\Client\ClientInterface
   *   The initialized client.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the client configuration is invalid.
   */
  public function getByClientConfig(array $client_config, $sync_id = NULL) {
    if (empty($client_config['type'])) {
      throw new InvalidConfigurationException(
        $this->buildExceptionMessage(
          'The given client configuration does not define the type of client.',
          $client_config,
          $sync_id
        )
      );
    }

    if ($client_config['type'] === 'service') {
      return $this->getByService($client_config, $sync_id);
    }

    if ($client_config['type'] === 'factory') {
      return $this->getByFactory($client_config, $sync_id);
    }

    throw new InvalidConfigurationException(
      $this->buildExceptionMessage(
        'Unknown type provided in the given client configuration.',
        $client_config,
        $sync_id
      )
    );
  }

  /**
   * Returns an initialized client using the given client service.
   *
   * @param array $client_config
   *   An associative array containing the client configuration. For supported
   *   elements see `getByClientConfig`.
   * @param string|null $sync_id
   *   The ID of the Synchronization or Operation Type configuration entity that
   *   we will be performing operations for, if available.
   *
   * @return \Drupal\entity_sync\Client\ClientInterface
   *   The initialized client.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the client configuration is invalid.
   */
  protected function getByService(array $client_config, $sync_id = NULL) {
    // Check if the config defines a service.
    if (empty($client_config['service'])) {
      throw new InvalidConfigurationException(
        $this->buildExceptionMessage(
          'The given client configuration does not define a service that provides the remote resource client.',
          $client_config,
          $sync_id
        )
      );
    }

    // Check that the service implements the ClientInterface.
    $client = $this->container->get($client_config['service']);
    if (!$client instanceof ClientInterface) {
      throw new InvalidConfigurationException(
        $this->buildExceptionMessage(
          'The "%s" service defined as the client for the given client configuration must implement the \Drupal\entity_sync\Client\ClientInterface interface.',
          $client_config,
          $sync_id,
          $client_config['service']
        )
      );
    }

    return $client;
  }

  /**
   * Returns an initialized client using the given factory service.
   *
   * @param array $client_config
   *   An associative array containing the client configuration. For supported
   *   elements see `getByClientConfig`.
   * @param string|null $sync_id
   *   The ID of the Synchronization or Operation Type configuration entity that
   *   we will be performing operations for, if available.
   *
   * @return \Drupal\entity_sync\Client\ClientInterface
   *   The initialized client.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the client configuration is invalid.
   */
  protected function getByFactory(array $client_config, $sync_id = NULL) {
    // Check if the config defines a service.
    if (empty($client_config['service'])) {
      throw new InvalidConfigurationException(
        $this->buildExceptionMessage(
          'The given client configuration does not define a service that provides the remote resource client factory.',
          $client_config,
          $sync_id
        )
      );
    }

    // Check that the service implements the ClientFactoryInterface.
    $factory = $this->container->get($client_config['service']);
    if (!$factory instanceof ProviderClientFactoryInterface) {
      throw new InvalidConfigurationException(
        $this->buildExceptionMessage(
          'The "%s" service defined as the provider client factory for the given client configuration must implement the \Drupal\entity_sync\Client\ProviderClientFactoryInterface interface.',
          $client_config,
          $sync_id,
          $client_config['service']
        )
      );
    }

    return $factory->get(
      $sync_id,
      $client_config['parameters'] ?? []
    );
  }

  /**
   * Prepares the given message so that it contains client config and sync info.
   *
   * @param string $message
   *   The message to prepare.
   * @param array $client_config
   *   An associative array containing the client configuration as defined in
   *   the synchronization schema.
   * @param string|null $sync_id
   *   The ID of the Synchronization or Operation Type configuration entity that
   *   we will be performing operations for, if available.
   * @param mixed $args
   *   Additional arguments to pass to `sprintf` for preparing the message.
   */
  protected function buildExceptionMessage(
    $message,
    array $client_config,
    $sync_id = NULL,
    ...$args
  ) {
    $params = [];

    if ($sync_id) {
      $message .= ' Sync ID: %s.';
      $params[] = $sync_id;
    }
    $message .= ' Client config: %s';
    $params[] = json_encode($client_config);

    return call_user_func_array(
      'sprintf',
      array_merge(
        [$message],
        $args,
        $params
      )
    );
  }

  /**
   * Returns the client configuration with the given overrides applied.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity containing the
   *   original client configuration.
   * @param array $client_config_overrides
   *   An array containing the client configuration overrides. For supported
   *   client configuration settings see
   *   `config/schema/entity_sync.schema::entity_sync.sync.*.remote_resource.client`.
   * @param bool $recursive_overrides
   *   The behavior is as follows.
   *   - If FALSE the overrides will be applied using PHP's `array_replace`
   *     i.e. values for keys existing in the overrides will replace the
   *     corresponding values on the synchronization configuration.
   *   - If TRUE the overrides will be applied using
   *     `\Drupal\Component\Utility\NestedArray::mergeDeep` i.e. they will be
   *     recursively merged/replaced.
   *
   * @return array
   *   The client configuration with the overrides applied as requested.
   */
  protected function getOverriddenClientConfig(
    SyncInterface $sync,
    array $client_config_overrides,
    bool $recursive_overrides
  ) {
    $client_config = $sync->getRemoteResourceSettings()['client'] ?? NULL;
    if (!$client_config_overrides) {
      return $client_config;
    }

    if ($recursive_overrides) {
      return NestedArray::mergeDeepArray([
        $client_config,
        $client_config_overrides,
      ]);
    }

    return array_replace($client_config, $client_config_overrides);
  }

}
