<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for remote resource clients.
 *
 * Remote resource clients will be used to perform operations on the remote
 * resource such as get the list of entities to import or update the remote
 * entity that we are exporting.
 *
 * Currently, clients need to be made available as services and identified in
 * the Sync's configuration so that the Client Factory will know how to generate
 * the client.
 *
 * We have also split the client into multiple interfaces. This is to make it
 * easier for clients to not have to implement all operations if they don't need
 * them. For backwards compatibility though, we keep the parent interface (this
 * file).
 *
 * @see \Drupal\entity_sync\Client\ClientFactory
 */
interface ClientInterface extends
  SupportsGetClientInterface,
  SupportsGetMultipleClientInterface,
  SupportsCreateClientInterface,
  SupportsUpdateClientInterface {
}
