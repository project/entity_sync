<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for clients that can create individual entities.
 */
interface SupportsCreateClientInterface {

  /**
   * Creates a new remote entity with the given fields.
   *
   * @param array $fields
   *   An associative array containing the fields that will be created, keyed by
   *   the field name and containing the field value.
   * @param array $options
   *   An associative array of options to be passed to the client. The client
   *   should document the options it supports.
   *
   * @return object
   *   The parsed response.
   */
  public function create(array $fields, array $options = []);

}
