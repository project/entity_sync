<?php

namespace Drupal\entity_sync\Client;

/**
 * Defines the interface for clients that can get multiple entities.
 */
interface SupportsGetMultipleClientInterface {

  /**
   * Gets the list of remote entities to import.
   *
   * @param array $filters
   *   An associative array of filters that determine which entities will be
   *   imported. Supported conditions are:
   *   - created_start: (Optional) A Unix timestamp that when set, should limit
   *     the remote entities to those created after or at the given timestamp.
   *   - created_end: (Optional) A Unix timestamp that when set, should limit
   *     the remote entities to those created before or at the given timestamp.
   *   - changed_start: (Optional) A Unix timestamp that when set, should limit
   *     the remote entities to those created or updated after or at the given
   *      timestamp.
   *   - changed_end: (Optional) A Unix timestamp that when set, should limit
   *     the remote entities to those created or updated before or at the given
   *     timestamp.
   *   - limit: (Optional) The maximum number of entities to import; leave empty
   *     or set to NULL for no limit. No limit will result in all entities
   *     determined by the filters to be imported, while setting a limit will
   *     result in importing to stop when that limit is reached - which might
   *     happen before importing all incoming entities. Set to 0 for not
   *     importing any entities. If this is a state-managed import and the
   *     remote resource/API client supports browsing results, the state manager
   *     will store the number of entities imported on the last run and the next
   *     run will continue from there.
   *   - offset: (Optional) The offset i.e. the number of entities to skip.
   * @param array $options
   *   An associative array of options. Supported options are:
   *   - limit: (Optional) The maximum number of items to fetch per request.
   *   - offset: (Optional) The offset i.e. how many items to skip, such as when
   *     continuing from a previous import.
   *   - query: (Optional) Additional parameters that will be added to the
   *     request.
   *
   * @return \Iterator|null
   *   An iterator containing the entities to import, or NULL if there are no
   *   entities to import for the given filters.
   * phpcs:disable
   * @I Support paginated results
   *    type     : improvement
   *    priority : high
   *    labels   : import, operation, memory-consumption
   * phpcs:enable
   */
  public function importList(array $filters = [], array $options = []);

}
