<?php

namespace Drupal\entity_sync\EventSubscriber;

use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\LocalEntityMappingEvent;
use Drupal\entity_sync\Import\ManagerInterface;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Builds the default remote ID mapping for import operations.
 */
class DefaultImportLocalEntityMapping implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_MAPPING => ['buildEntityMapping', 0],
    ];
    return $events;
  }

  /**
   * Builds the default local entity mapping.
   *
   * The default local entity mapping is defined in the Synchronization or
   * Operation Type configuration entity.
   *
   * @param \Drupal\entity_sync\Import\Event\LocalEntityMappingEvent $event
   *   The entity mapping event.
   */
  public function buildEntityMapping(LocalEntityMappingEvent $event) {
    $sync = $event->getSync();
    $local_entity = $event->getLocalEntity();

    // Check if the field exists and has a value.
    //
    // phpcs:disable
    // @I Support imports that do not define a remote ID field
    //    type     : bug
    //    priority : high
    //    labels   : import
    //    notes    : We probably need to return unaltered the mapping contained
    //               currently in the event, plus add a check whether the
    //               setting exists as `hasField` would throw an error
    //               otherwise. Custom or contrib code will need to define an
    //               alternative subscriber for providing the mapping, otherwise
    //               there will be no errors but there will be no entity mapping
    //               provided and the operation will be skipped.
    // phpcs:enable
    $id_field_name = $sync->getLocalEntitySettings()['remote_id_field'] ?? NULL;
    if (!$local_entity->hasField($id_field_name)) {
      return [];
    }

    $id_field = $local_entity->get($id_field_name);
    if ($id_field->isEmpty()) {
      return [];
    }

    $event->setEntityMapping([
      'action' => ManagerInterface::ACTION_IMPORT,
      'client' => $sync->getRemoteResourceSettings()['client'] ?? NULL,
      'entity_id' => $id_field->value,
    ]);
  }

}
