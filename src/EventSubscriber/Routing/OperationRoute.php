<?php

namespace Drupal\entity_sync\EventSubscriber\Routing;

use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener for altering the operation entity controller methods.
 */
class OperationRoute implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      RoutingEvents::ALTER => ['onAlterRoutes', -10],
    ];
    return $events;
  }

  /**
   * Alters the operation entity `add_page` route to use a custom method.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route alter event.
   */
  public function onAlterRoutes(RouteBuildEvent $event) {
    $route = $event->getRouteCollection()->get(
      'entity.entity_sync_operation.add_page'
    );
    if (!$route) {
      return;
    }

    $route->setDefault(
      '_controller',
      '\Drupal\entity_sync\Entity\Controller\Operation::addPage'
    );
  }

}
