<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\Event\LocalEntityMappingEvent;
use Drupal\entity_sync\Export\EntityManagerInterface;
// Third-party libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Updates entity mapping to skip operations disabled in configuration.
 */
class ExportLocalEntityMappingSkipDisabledActions implements
  EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // We give it a very low priority so that it is the last subscriber to
      // run.
      Events::LOCAL_ENTITY_MAPPING => ['updateEntityMapping', -1000],
    ];
    return $events;
  }

  /**
   * Changes entity mapping to skip disabled operations.
   *
   * We used to skip the export within the export entity manager after the
   * entity mapping was processed i.e. just not triggering the export but
   * without changing the action in the entity mapping. This was causing issues
   * with terminate subscribers that use the entity mapping action in their
   * logic e.g. the entity mapping was saying that this was a `create` action
   * but the operation was actually skipped and there was no remote entity in
   * the results.
   *
   * Therefore, if the action determined by the entity mapping is disabled in
   * the synchronization configuration, we change the entity mapping to skip the
   * operation. This is also a cleaner solution from the perspective of not
   * having another hidden layer of skipping operations magically; the only ways
   * to skip an operation should be by pre-initiate cancellations and entity
   * mapping skipping.
   *
   * @param \Drupal\entity_sync\Export\Event\LocalEntityMappingEvent $event
   *   The local entity mapping event.
   * phpcs:disable
   * @I Add ability to log entity mapping skip reason
   *    type     : improvement
   *    priotiy  : low
   *    labels   : entity-mapping
   *    notes    : The message could be adding to the entity mapping, and the
   *               export manager would be responsible to log it or not
   *               according to configuration - similarly to pre-initiate
   *               cancellation messages.
   * phpcs:enable
   */
  public function updateEntityMapping(LocalEntityMappingEvent $event) {
    $entity_mapping = $event->getEntityMapping();
    if (!$entity_mapping || !isset($entity_mapping['action'])) {
      return;
    }

    $action = NULL;
    switch ($entity_mapping['action']) {
      case EntityManagerInterface::ACTION_CREATE:
        $action = 'create';
        break;

      case EntityManagerInterface::ACTION_UPDATE:
        $action = 'update';
        break;

      case EntityManagerInterface::ACTION_DELETE:
        $action = 'delete';
        break;

      default:
        return;
    }

    $settings = $event->getSync()->getOperationsSettings();
    if (empty($settings['export_entity']["{$action}_entities"])) {
      $event->setEntityMapping([
        'action' => EntityManagerInterface::ACTION_SKIP,
      ]);
    }
  }

}
