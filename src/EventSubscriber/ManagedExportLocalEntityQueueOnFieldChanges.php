<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Event\QueueOperationEvent;
use Drupal\entity_sync\MachineName\Internal\Action\Type as ActionType;
use Drupal\entity_sync\StateManagerInterface;
// External libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cancels queueing export operations depending on values of fields.
 */
class ManagedExportLocalEntityQueueOnFieldChanges implements
  EventSubscriberInterface {

  /**
   * The Entity Synchronization state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * Constructs a new ManagedExportLocalEntityQueueOnFieldChanges object.
   *
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Synchronization export field manager.
   */
  public function __construct(
    StateManagerInterface $state_manager
  ) {
    $this->stateManager = $state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_QUEUE => ['cancelQueueing', 0],
    ];
    return $events;
  }

  /**
   * Cancels queueing exports depending on values of fields.
   *
   * State-managed exports may define to queue an entity export depending on the
   * value of a field of the entity being exported.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   *
   * @see entity_sync.schema.yml::entity_sync.operation.export_entity
   */
  public function cancelQueueing(QueueOperationEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    $settings = $this->onFieldChangesSettings($event->getSync());
    if ($settings === NULL) {
      throw new InvalidConfigurationException(
        'The "on_field_changes" configuration property cannot be empty.'
      );
    }
    $local_entity = $event->getContext()['local_entity'];

    foreach ($settings as $changed_field) {
      $changed_value = $local_entity->get($changed_field['field_name'])->value;
      if (!in_array($changed_value, $changed_field['changed_values'], TRUE)) {
        $event->cancel();
        return;
      }
    }
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to `export_entity` operations managed by Entity
   * Synchronization, and that their synchronization configuration contain the
   * properties required for the functionality provided by this subscriber.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(QueueOperationEvent $event) {
    if ($event->getOperation() !== ActionType::EXPORT_ENTITY) {
      return FALSE;
    }

    $sync = $event->getSync();
    $is_managed = $this->stateManager->isManaged(
      $sync->id(),
      ActionType::EXPORT_ENTITY
    );
    if (!$is_managed) {
      return FALSE;
    }

    if ($this->onFieldChangesSettings($event->getSync()) === NULL) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns the `on_field_changes` settings for the given synchronization.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization or operation type configuration entity for the
   *   operation being run.
   *
   * @return array|null
   *   The settings, or `NULL` if not defined.
   */
  protected function onFieldChangesSettings(SyncInterface $sync) {
    $settings = $sync->getOperationsSettings()[ActionType::EXPORT_ENTITY];
    return $settings['state']['on_field_changes'] ?? NULL;
  }

}
