<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Event\TerminateOperationEvent;
use Drupal\entity_sync\Export\EntityManagerInterface;
use Drupal\entity_sync\Import\FieldManagerInterface;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
// Third-party libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Updates the local entity after the export local entity terminate event.
 */
class ManagedExportLocalEntityTerminate implements EventSubscriberInterface {

  /**
   * The Entity Sync import field manager.
   *
   * @var \Drupal\entity_sync\Import\FieldManagerInterface
   */
  protected $fieldManager;

  /**
   * Constructs a new ManagedExportLocalEntityTerminate object.
   *
   * @param \Drupal\entity_sync\Import\FieldManagerInterface $field_manager
   *   The import field manager.
   */
  public function __construct(FieldManagerInterface $field_manager) {
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_TERMINATE => ['updateLocalEntity', 0],
    ];
    return $events;
  }

  /**
   * Updates the sync ID/changed/composite fields on the local entity.
   *
   * The `changed` property is updated for both new and updated remote entities.
   *
   * For single-cardinality sync fields, the `id` property is only updated when
   * new remote entities are created. We do check however that the field does
   * not already have value - normally it shouldn't, but in rare configurations
   * we may be creating a new remote entity as a result of a local entity update
   * in which case we may have a remote ID already set. In such cases the new ID
   * would replace the old one - we don't do that.
   *
   * For multiple-cardinality sync composite fields, if the ID exists we update
   * its `changed` property; otherwise, a new ID/changed pair is added.
   *
   * @param \Drupal\entity_sync\Event\TerminateOperationEvent $event
   *   The terminate operation event.
   * phpcs:disable
   * @I Do not set the sync fields if the entity was not created or updated
   *    type     : bug
   *    priority : high
   *    labels   : export
   *    notes    : In cases such as when the sync has entity creates or updates
   *               disabled we never make a request to the remote and therefore
   *               don't have a response available. The error thrown here in
   *               that case might block other terminate subscribers from being
   *               run.
   * phpcs:enable
   */
  public function updateLocalEntity(TerminateOperationEvent $event) {
    $sync = $event->getSync();
    $local_entity = $event->getContext()['local_entity'];

    // First, handle the case where we have a composite field.
    if ($sync->getLocalEntitySettings()['remote_composite_field'] ?? NULL) {
      $this->setRemoteCompositeField($local_entity, $event, $sync);
      $event->setSaveLocalEntity(TRUE);
      return;
    }

    $data = $event->getData();

    $actions = [
      EntityManagerInterface::ACTION_CREATE,
      EntityManagerInterface::ACTION_UPDATE,
    ];
    if (!in_array($data['action'], $actions, TRUE)) {
      return;
    }

    $local_entity_changed = FALSE;

    // We only set the remote changed field if it is defined in the
    // synchronization configuration. There are cases where it is not, such as
    // when we send the entity to a remote resource when created or updated but
    // we do not track an association.
    $remote_resource_settings = $sync->getRemoteResourceSettings();
    if ($remote_resource_settings['changed_field'] ?? NULL) {
      $changed_field_changed = $this->fieldManager->setRemoteChangedField(
        $data['response'],
        $local_entity,
        $sync
      );
      $local_entity_changed = $local_entity_changed ?: $changed_field_changed;
    }

    // Similarly, we only set the remote ID field if it is defined in the
    // synchronization configuration. If it is, by default we only set it if we
    // are creating a new remote ID; when are updating an existing one we should
    // already have the association correctly stored in the field, in the most
    // common import/export flows. Custom flows might have to create custom
    // subscribers to define the desired behavior.
    $is_create = $data['action'] === EntityManagerInterface::ACTION_CREATE;
    if ($is_create && ($remote_resource_settings['id_field'] ?? NULL)) {
      $id_field_changed = $this->fieldManager->setRemoteIdField(
        $data['response'],
        $local_entity,
        $sync,
        // Only if it does not already have a value.
        FALSE
      );
      $local_entity_changed = $local_entity_changed ?: $id_field_changed;
    }

    if ($local_entity_changed) {
      $event->setSaveLocalEntity(TRUE);
    }
  }

  /**
   * Sets the ID/changed properties on the remote composite field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The updated local entity.
   * @param \Drupal\entity_sync\Event\TerminateOperationEvent $event
   *   The terminate operation event.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type of the operation that was run.
   */
  protected function setRemoteCompositeField(
    ContentEntityInterface $local_entity,
    TerminateOperationEvent $event,
    SyncInterface $sync
  ) {
    $data = $event->getData();
    // If we have a single-dimensional array, convert it to two dimensional so
    // that we can handle it the same way.
    if (isset($data['action'])) {
      $data = [$data];
    }

    $actions = [
      EntityManagerInterface::ACTION_CREATE,
      EntityManagerInterface::ACTION_UPDATE,
    ];
    foreach ($data as $export) {
      if (!in_array($export['action'], $actions, TRUE)) {
        continue;
      }

      $this->fieldManager->setRemoteCompositeField(
        $export['response'],
        $local_entity,
        $sync,
        // If there is already a field item for this remote entity, only update
        // the `changed` property.
        FALSE
      );
    }
  }

}
