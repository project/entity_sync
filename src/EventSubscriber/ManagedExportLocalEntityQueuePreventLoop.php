<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Event\QueueOperationEvent;
use Drupal\entity_sync\MachineName\Internal\Action\Type as ActionType;
use Drupal\entity_sync\StateManagerInterface;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
// External libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cancels queueing export operations caused by linked import operations.
 */
class ManagedExportLocalEntityQueuePreventLoop implements
  EventSubscriberInterface {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The Entity Synchronization state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * Constructs a new ManagedExportLocalEntityQueuePreventLoops object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The entity field manager.
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Synchronization export field manager.
   */
  public function __construct(
    EntityFieldManagerInterface $field_manager,
    StateManagerInterface $state_manager
  ) {
    $this->fieldManager = $field_manager;
    $this->stateManager = $state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_QUEUE => ['cancelQueueing', 0],
    ];
    return $events;
  }

  /**
   * Cancels queueing export operations caused by linked import operations.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   */
  public function cancelQueueing(QueueOperationEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    $context = $event->getContext();
    $entity_settings = $event->getSync()->getLocalEntitySettings();

    // We must have either a sync changed or a composite sync field. If not, it
    // is a configuration error.
    $is_composite = FALSE;
    $field_name = $entity_settings['remote_changed_field'] ?? NULL;
    if ($field_name === NULL) {
      $is_composite = TRUE;
      $field_name = $entity_settings['remote_composite_field'] ?? NULL;
    }

    if (!$field_name) {
      throw new InvalidConfigurationException(
        'Export operations managed by Entity Synchronization that have linked, enabled, import operations must provide either a sync changed field or a sync composite field.'
      );
    }

    // Entity update.
    if (isset($context['original_entity'])) {
      $this->cancelQueueingUpdate(
        $event,
        $context['original_entity'],
        $context['local_entity'],
        $field_name,
        $is_composite
      );
    }
    // Entity creation.
    // Currently, we do not support importing entity deletions for
    // synchronizations managed by Entity Synchronization i.e. we wouldn't be
    // here as a result of deleting an entity due to an import; no need to check
    // for that.
    else {
      $this->cancelQueueingCreation(
        $event,
        $context['local_entity'],
        $field_name
      );
    }
  }

  /**
   * Cancels queueing update exports caused by linked imports.
   *
   * If we are coming from an entity update, we may be here because an entity
   * import triggered an entity update hook. In that case, we don't want to
   * export the entity because that would cause a loop i.e. export the entity to
   * the remote which may cause the entity to be made available in the recently
   * modified list of entities, causing an import again which in turn will
   * trigger a `hook_entity_update` etc.
   *
   * If we are here as a result of a change that happened within Drupal e.g. an
   * entity edited via the UI, the remote changed field would not be changed.
   * While, if we are here as a result of an import, the original remote changed
   * field would be a timestamp earlier than the new remote changed field.
   *
   * We therefore check for that, and if the original remote changed field is
   * earlier than the updated remote changed field then we do not export the
   * entity.
   *
   * This will also prevent triggering another export as a result of saving the
   * entity in order to update the remote changed field when getting the
   * response from the create/update export operation.
   *
   * It does not cover though the case that we import changes that have already
   * been imported where the remote changed field will be the same before and
   * after the import. That still remains to be resolved and it relies on
   * finding a way to let the method that saves the entity provide custom data
   * to `hook_entity_insert`/`hook_entity_update`.
   * See https://www.drupal.org/project/drupal/issues/3158180
   *
   * In most cases, though, the above case will still be covered by Entity
   * Synchronization detecting that no fields were changed - see
   * (`Drupal\entity_sync\EventSubscriber\ManagedExportLocalEntityQueueUpdate`)
   * and the export will be cancelled.
   *
   * Note that we may not always have a remote changed field; that can be, for
   * example, when we only export an entity type while imports are disabled. We
   * therefore skip the check if imports are disabled - see `$this::applies()`.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   * @param \Drupal\Core\Entity\ContentEntityInterface $original_entity
   *   The original entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The updated entity.
   * @param string $field_name
   *   The machine name of the field that holds the value of the remote changed
   *   field.
   * @param bool $is_composite
   *   `TRUE` if we have a sync composite field, `FALSE` if we have a sync
   *   changed field.
   * phpcs:disable
   * @I Test skipping exports that are triggered by imports
   *    type     : task
   *    priority : high
   *    labels   : export, testing
   * @I Review case of imports+exports without a remote changed field
   *    type     : bug
   *    priority : high
   *    labels   : export
   * phpcs:enable
   */
  protected function cancelQueueingUpdate(
    QueueOperationEvent $event,
    ContentEntityInterface $original_entity,
    ContentEntityInterface $local_entity,
    string $field_name,
    bool $is_composite
  ) {
    $field_cardinality = $this->getFieldCardinality(
      $local_entity,
      $field_name
    );

    $cancel = FALSE;
    if ($is_composite && $field_cardinality === 1) {
      $cancel = $this->shouldCancelQueueingUpdateForSingleCompositeField(
        $original_entity,
        $local_entity,
        $field_name
      );
    }
    elseif ($is_composite) {
      $cancel = $this->shouldCancelQueueingUpdateForMultipleCompositeField(
        $event,
        $original_entity,
        $local_entity,
        $field_name
      );
    }
    elseif ($field_cardinality === 1) {
      $cancel = $this->shouldCancelQueueingUpdateForSingleChangedField(
        $original_entity,
        $local_entity,
        $field_name
      );
    }
    else {
      throw new \RuntimeException(
        'Multiple cardinality in the sync changed field is supported only when using a sync composite field.'
      );
    }

    if ($cancel === FALSE) {
      return;
    }

    // Otherwise, we must have a `TRUE` i.e. cancel the queueing of the export.
    $event->cancel();
  }

  /**
   * Cancels queueing creation exports caused by linked imports.
   *
   * If we are coming here from an entity creation, we want to prevent exporting
   * entities that were created as a result of an import. In those cases, the
   * remote changed field of the new local entity will not be empty, while if
   * the entity was created within the application it will always be empty.
   * See `entity_sync_entity_bundle_field_info_alter()`.
   *
   * As with entity updates, we skip the checks if imports are disabled - see
   * `$this::applies()`.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The updated entity.
   * @param string $field_name
   *   The machine name of the field that holds the value of the remote changed
   *   field.
   * phpcs:disable
   * @I Test skipping exports that are triggered by imports
   *    type     : task
   *    priority : high
   *    labels   : export, testing
   * @I Review case of imports+exports without a remote changed field
   *    type     : bug
   *    priority : high
   *    labels   : export
   * phpcs:enable
   * phpcs:disable
   * @I Allow queueing exports of imported entities for different syncs
   *    type     : bug
   *    priority : high
   *    labels   : export
   *    notes    : There are legitimate cases where we still want to allow
   *               exports of new imported entities e.g. import from one
   *               remote resource and send to another.
   * phpcs:enable
   */
  protected function cancelQueueingCreation(
    QueueOperationEvent $event,
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    if (!$local_entity->get($field_name)->isEmpty()) {
      $event->cancel();
    }
  }

  /**
   * Returns the result for single-cardinality sync composite fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $original_entity
   *   The original entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The updated entity.
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return bool
   *   `TRUE` to cancel queueing the export, `FALSE` otherwise.
   */
  protected function shouldCancelQueueingUpdateForSingleCompositeField(
    ContentEntityInterface $original_entity,
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    $old_changed_field = $original_entity->get($field_name);
    $new_changed_field = $local_entity->get($field_name);

    $old_is_empty = $old_changed_field->isEmpty();
    $new_is_empty = $new_changed_field->isEmpty();
    // If the new value for the `changed` sync field property is empty, we are
    // here as a result of either:
    // - The old value is also empty and the entity was saved in Drupal.
    // - The old value is not empty and the sync field was reset; that normally
    //   never happens but it can be done intentionally to disassociate the
    //   Drupal from the remote entity.
    // In both cases we are not here as a result of an import or an entity save
    // at the end of an export; proceed with queueing.
    if ($new_is_empty) {
      return FALSE;
    }
    // If the old value is empty and the new value is not, or if the value of
    // the `changed` sync field property has changed we are here as a result
    // of an import, or as a result of updating the `changed` property at the
    // end of an export. Do not export the entity.
    if ($old_is_empty && !$new_is_empty) {
      return TRUE;
    }

    $old_changed = $old_changed_field->first()->getChanged();
    $new_changed = $new_changed_field->first()->getChanged();
    // The old value would normally never be bigger than the new value; if it
    // happens it is an error or the entity was intentionally reverted to a
    // previous version. We do not yet support the latter as a feature and have
    // never encountered it in a real world case, so we do not intervene until
    // we review what should be the default course of action in such cases.
    if ($old_changed < $new_changed) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns the result for multiple-cardinality sync composite fields.
   *
   * In the case that queueing the export is allowed, the method also saves the
   * IDs of the entities that are allowed to be exported in the stored context
   * of the queue item. This is because, since we may be exporting to multiple
   * remote entities, we may want to cancel the export of one of them - the one
   * that was just imported, for example - and queue exporting the rest.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   * @param \Drupal\Core\Entity\ContentEntityInterface $original_entity
   *   The original entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The updated entity.
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return bool
   *   `TRUE` to cancel queueing the export, `FALSE` otherwise.
   */
  protected function shouldCancelQueueingUpdateForMultipleCompositeField(
    QueueOperationEvent $event,
    ContentEntityInterface $original_entity,
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    $result = [];

    $reduce = function ($carry, $value) {
      $carry[$value['id']] = $value['changed'];
      return $carry;
    };
    $old_values = array_reduce(
      $original_entity->get($field_name)->getValue(),
      $reduce,
      []
    );
    $new_values = array_reduce(
      $local_entity->get($field_name)->getValue(),
      $reduce,
      []
    );

    foreach ($new_values as $id => $changed) {
      // For existing linked entities, if the `changed` sync field property has
      // changed we are here as a result of an import, or as a result of
      // updating the `changed` property at the end of an export. Do not export
      // the entity.
      if (isset($old_values[$id])) {
        $result[$id] = $old_values[$id] < $changed ? TRUE : FALSE;
        continue;
      }

      // If we have a new linked entity, we are here either as a result of an
      // import or as a result of exporting a new entity to the remote. Do not
      // export the entity.
      $result[$id] = TRUE;
    }

    // Keep only the remote IDs that we will be exporting to, and add them to
    // the stored context - if any. The stored context is stored in the queue
    // item and will be made available to the entity mapping subscriber when the
    // export operation is actual run. Entities that are not in the
    // `allowed_remote_entity_ids` array will not be exported.
    // @I Do not cancel exports if unlinking from all entities.
    //    type     : bug
    //    priority : normal
    //    labels   : export, sync-field
    //    notes    : As in `shouldCancelQueueingUpdateForSingleCompositeField()`
    //               we should not prevent queueing if the local entity is being
    //               unlinked from all remote entities. For that, we should
    //               compare the old and the new values before filtering the
    //               results.
    $result = array_filter($result, fn($r) => $r === FALSE);
    // If there are no linked entities set to `FALSE` i.e. not cancelling, then
    // either there are linked entities or all of them are cancelled. Cancel
    // queueing the operation.
    if (!$result) {
      return TRUE;
    }

    $event->addStoredContext([
      'allowed_remote_entity_ids' => array_keys($result),
    ]);
    return FALSE;
  }

  /**
   * Returns the result for single-cardinality sync changed fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $original_entity
   *   The original entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The updated entity.
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return bool
   *   `TRUE` to cancel queueing the export, `FALSE` otherwise.
   */
  protected function shouldCancelQueueingUpdateForSingleChangedField(
    ContentEntityInterface $original_entity,
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    // @I Test unlinking the entity with/without updating the changed field
    //    type     : bug
    //    priority : normal
    //    labels   : export, sync-field
    //    notes    : See `shouldCancelQueueingUpdateForSingleCompositeField()`.
    // @I Provide official interface/method for unlinking entities
    //    type     : feature
    //    priority : normal
    //    labels   : sync-field
    // @I Provide entity validation constraint for preventing unlinking
    //    type     : feature
    //    priority : normal
    //    labels   : sync-field
    $old_changed = (int) $original_entity->get($field_name)->value;
    $new_changed = (int) $local_entity->get($field_name)->value;

    // If the `changed` sync field property has changed we are here as a result
    // of an import, or as a result of updating the `changed` property at the
    // end of an export. Do not export the entity.
    if ($old_changed < $new_changed) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Returns the cardinality of the given field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The entity that the field belongs to.
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return int
   *   The cardinality of the field.
   */
  protected function getFieldCardinality(
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    $definitions = $this->fieldManager->getFieldStorageDefinitions(
      $local_entity->getEntityTypeId()
    );

    return $definitions[$field_name]->getCardinality();
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to `export_entity` operations managed by Entity
   * Synchronization, that is linked to import operations.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(QueueOperationEvent $event) {
    if ($event->getOperation() !== ActionType::EXPORT_ENTITY) {
      return FALSE;
    }

    $sync = $event->getSync();
    $is_managed = $this->stateManager->isManaged(
      $sync->id(),
      ActionType::EXPORT_ENTITY
    );
    if (!$is_managed) {
      return FALSE;
    }

    // Linked import operations must be enabled, otherwise no import/export
    // loops can occur.
    $settings = $sync->getOperationsSettings();
    $import_status = $settings[ActionType::IMPORT_LIST]['status'] ?? NULL;
    if (!$import_status) {
      $import_status = $settings[ActionType::IMPORT_ENTITY]['status'] ?? FALSE;
    }

    return $import_status;
  }

}
