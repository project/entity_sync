<?php

namespace Drupal\entity_sync\EventSubscriber;

use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\ListFiltersEvent;
use Drupal\entity_sync\StateManagerInterface;
use Drupal\entity_sync\Utility\DateTime;

use Drupal\Component\Datetime\TimeInterface;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Builds the filters for fetching a remote list of entities.
 *
 * phpcs:disable
 * @I Write tests for the managed import filters subscriber
 *    type     : task
 *    priority : high
 *    labels   : import, testing
 * phpcs:enable
 */
class ManagedImportRemoteListFilters implements EventSubscriberInterface {

  /**
   * The Entity Sync state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * The system time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new ManagedImportRemoteListFilters object.
   *
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Sync state manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The system time service.
   */
  public function __construct(
    StateManagerInterface $state_manager,
    TimeInterface $time
  ) {
    $this->stateManager = $state_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::REMOTE_LIST_FILTERS => ['buildFilters', 0],
    ];
    return $events;
  }

  /**
   * Builds the filters for importing a list of remote entities.
   *
   * The default state manager keeps track of the time the import was last
   * run and the end time filter that was used. We use that and we set the time
   * filters to import the entities that have changed from the time the last
   * import left off run to the time of the current run (request) - or to the
   * time defined by a maximum interval set in the context.
   *
   * This system can be used to implement a managed import flow where each
   * import (run via cron or otherwise) would pick up from where the previous
   * run left off and import only a small batch of entities. Otherwise, initial
   * imports in systems with hundreds of thousands or millions of entities can
   * be problematic. The flow can look like the following:
   * - On the first import, start by importing entities changed on datetime A or
   *   later; import maximum 1 day's worth of entities.
   * - On the next import, start by import entities where the first import
   *   stopped i.e. datetime A + 1 day, and import maximum 1 day's worth of
   *   entities.
   *
   * @param \Drupal\entity_sync\Import\Event\ListFiltersEvent $event
   *   The list filters event.
   */
  public function buildFilters(ListFiltersEvent $event) {
    $sync = $event->getSync();
    $context = $event->getContext();

    // Merge the state settings defined in context with those defined in
    // configuration. This might change after providing a generic mechanism for
    // overriding any configuration property at runtime, but for now we have to
    // do it this way for maintaining backwards compatibility.
    $state_settings = array_merge(
      $sync->getOperationsSettings()['import_list']['state'] ?? [],
      $context['state'] ?? []
    );

    if (!$this->applies($state_settings)) {
      return;
    }

    $filters = $event->getFilters();
    $sync_id = $sync->id();
    $has_time_filters = $state_settings['time_filters'] ?? TRUE;
    $last_run = $this->getLastRun($sync_id, $has_time_filters);

    // We set the time filters only if we don't have any defined. That allows
    // other subscribers or the caller of the import manager to override the
    // times even when the import is managed.
    if ($has_time_filters && !isset($filters['changed_start'])) {
      $filters['changed_start'] = $this->startTimeFilter($last_run, $context);
    }

    if ($has_time_filters && !isset($filters['changed_end'])) {
      $filters['changed_end'] = $this->endTimeFilter(
        $last_run,
        $context,
        $filters
      );
    }

    // The last run may have stopped before processing all entities available
    // for the tiven time range because of hitting the limit of entities to
    // process per run. In that case, it stores the offset from where the next
    // run (i.e. this run) should continue.
    // There is no default limit, and it is not carried over from the last
    // run. If no limit is given in the filters or in the options, this run
    // should fetch all remaining results.
    // The offset is still relevant even if we don't have time filters. In that
    // case, we may have other types of filtering by other means e.g. a range
    // based on a field such as the ID (externally, or as a future feature
    // here), or we may not have any filtering at all i.e. process all entities
    // but in batches.
    if (!isset($filters['offset'])) {
      $filters['offset'] = $last_run['next_offset'] ?? NULL;
    }

    $event->setFilters($filters);

    // Save the current run's state; we'll be using it when the import has
    // terminated to save the last run's state.
    $this->setCurrentRunState($sync_id, $filters);
  }

  /**
   * Calculates and returns the start time filter value.
   *
   * The start time is calculated as follows:
   * - If we have an end time on the operation's last run information, we use
   *   that so that we pick up from where the last run left off.
   * - If we don't have an end time we use the fallback start time, if provided
   *   in the state context. This is where the application explicitly tells us
   *   where to start.
   * - If we have neither an end time from the last run, nor a fallback start
   *   time, we don't set a start time. This should serve the cases where we
   *   import all entities, or all entities up to an end time.
   *
   * @param array $last_run
   *   An array containing the details of the last run.
   * @param array $context
   *   The context array.
   *
   * @return int
   *   The calculated start time filter value.
   *
   * @throws \InvalidArgumentException
   *   When an interval was defined in the state context but no fallback start
   *   time was given.
   */
  protected function startTimeFilter(
    array $last_run,
    array $context
  ) {
    // If the last run has left an offset to continue from, it did not continue
    // processing that time range. We therefore continue with the same start and
    // end times.
    if (($last_run['next_offset'] ?? NULL) !== NULL) {
      return $last_run['start_time'];
    }

    // If we are given a maximum interval we do need a start time as a fallback
    // when there is no end time for the last run; that can happen the very
    // first time that the import is run. The Unix epoch could be used i.e. 0 as
    // the default fallback start time, but we'd rather have the user to
    // explicitly define the earliest time that an entity was modified (or where
    // to start anyway).
    //
    // We do the validation in advance (a fallback start time wouldn't be needed
    // if we do have last run information) as otherwise we could have the same
    // configuration working when we have last run info and causing a runtime
    // error when we don't - let's chose safe behavior instead.
    $has_interval = !empty($context['state']['max_interval']);
    if ($has_interval && !isset($context['state']['fallback_start_time'])) {
      throw new \InvalidArgumentException(
        'A fallback start time for the time filters must be given when a maximum interval is defined.'
      );
    }

    // Use the last run's end time, if we have one.
    //
    // phpcs:disable
    // @I Eliminate overlap between start and end time
    //    type     : bug
    //    priority : low
    //    labels   : import, state-manager
    //    notes    : Using the end time of the last run as the start time of
    //               the next run may result in some entities being imported
    //               again depending on which operators (> or >=, < or <=) are
    //               used by the remote resource.
    // phpcs:enable
    if (!empty($last_run['end_time'])) {
      return $last_run['end_time'] + 1;
    }

    // If we do have a fallback start time, use that.
    // Otherwise, we don't have a fallback start time and we don't have an
    // interval (or we would have an exception thrown above). We must be in the
    // case where we import all entities changed at any moment up to the current
    // run time.
    return $context['state']['fallback_start_time'] ?? NULL;
  }

  /**
   * Calculates and returns the end time filter value.
   *
   * Required to be called after the start time filter is calculated.
   *
   * The end time is calculated as follows:
   * - If we don't have an interval given, we use the current request's time. We
   *   therefore throttle imports from one run to another importing as many
   *   entities as available.
   * - If we do have an interval given, we use the time defined by adding the
   *   interval to the start time (with the current request's time being the
   *   upper limit). We therefore throttle imports from one run to another
   *   importing entities in batches limited in number by the interval.
   *
   * @param array $last_run
   *   An array containing the details of the last run.
   * @param array $context
   *   The context array.
   * @param array $filters
   *   The filters array.
   *
   * @return int
   *   The calculated end time filter value.
   */
  protected function endTimeFilter(
    array $last_run,
    array $context,
    array $filters
  ) {
    // If the last run has left an offset to continue from, it did not continue
    // processing that time range. We therefore continue with the same start and
    // end times.
    if (($last_run['next_offset'] ?? NULL) !== NULL) {
      return $last_run['end_time'];
    }

    if (empty($context['state']['max_interval'])) {
      return $this->time->getRequestTime();
    }

    return DateTime::timeAfterTime(
      // If we are here we must have a start time; it would be the fallback
      // start time which must exist as otherwise an exception would had been
      // been thrown when calculating the start time.
      $filters['changed_start'],
      $context['state']['max_interval'],
      $this->time->getRequestTime()
    );
  }

  /**
   * Validates and returns the details of the last run.
   *
   * @param string $sync_id
   *   The ID of the entity synchronization that the operation belongs to.
   * @param bool $has_time_filters
   *   `TRUE` if time filters are enabled, `FALSE` otherwise.
   *
   * @return array
   *   An array containing the details of the last run.
   */
  protected function getLastRun($sync_id, $has_time_filters) {
    $last_run = $this->stateManager->getLastRun($sync_id, 'import_list');

    // The following validation only applies when we have time filters enabled.
    if (!$has_time_filters) {
      return $last_run;
    }

    // If we have an offset to continue from, but not a start time, something is
    // wrong. An end time is not required though, it may be an open-ended import
    // that hit the limit of entities to import.
    if (!isset($last_run['next_offset'])) {
      return $last_run;
    }
    if (isset($last_run['start_time'])) {
      return $last_run;
    }

    throw new \RuntimeException(
      'Next offset but no start time filters defined by the last run.'
    );
  }

  /**
   * Sets the current run state of the operation.
   *
   * @param string $sync_id
   *   The ID of the entity synchronization that the operation belongs to.
   * @param array $filters
   *   The filters containing the final values for the start and end time
   *   filters.
   */
  protected function setCurrentRunState($sync_id, array $filters) {
    $this->stateManager->setCurrentRun(
      $sync_id,
      'import_list',
      $this->time->getRequestTime(),
      $filters['changed_start'] ?? NULL,
      $filters['changed_end'] ?? NULL,
      $filters['limit'] ?? NULL,
      $filters['offset'] ?? NULL
    );
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to `import_list` operations managed by Entity
   * Synchronization. We do not need to check that the operation is a list
   * import because that is indirectly done by subscribing to an event that is
   * only available in list imports.
   *
   * @param array $state_settings
   *   An associative array containing the state settings for the
   *   synchronization.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(array $state_settings) {
    // Only proceed if the settings indicate that the import is managed by
    // Entity Synchronization.
    if (($state_settings['manager'] ?? NULL) !== 'entity_sync') {
      return FALSE;
    }

    return TRUE;
  }

}
