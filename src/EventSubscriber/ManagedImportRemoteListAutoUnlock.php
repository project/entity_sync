<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Event\PreInitiateOperationEvent;
use Drupal\entity_sync\StateManagerInterface;
// Drupal core.
use Drupal\Component\Datetime\TimeInterface;
// Third-party libraries.
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Unlocks the `import_list` operation if it is detected as stuck in lock state.
 *
 * phpcs:disable
 * @I Write tests for the managed import list auto-unlock subscriber
 *    type     : task
 *    priority : high
 *    labels   : import, testing
 * phpcs:enable
 */
class ManagedImportRemoteListAutoUnlock implements EventSubscriberInterface {

  /**
   * The logger channel for this module.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Entity Sync state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * The system time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new ManagedImportRemoteListUnlock object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel for this module.
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Sync state manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The system time service.
   */
  public function __construct(
    LoggerInterface $logger,
    StateManagerInterface $state_manager,
    TimeInterface $time
  ) {
    $this->logger = $logger;
    $this->stateManager = $state_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      // Normal priority, it just needs to run before the lock subscriber.
      Events::REMOTE_LIST_PRE_INITIATE => ['unlockOperation', 0],
    ];
    return $events;
  }

  /**
   * Unlocks th eoperation if it seems stuck in locked state.
   *
   * @param \Drupal\entity_sync\Event\PreInitiateOperationEvent $event
   *   The pre-initiate operation event.
   */
  public function unlockOperation(PreInitiateOperationEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    $sync = $event->getSync();
    $operation = $event->getOperation();
    $settings = $sync->getOperationsSettings()[$operation]['state'];
    if (($settings['auto_unlock'] ?? NULL) === NULL) {
      return;
    }

    $sync_id = $sync->id();

    // Nothing to do if the operation is not locked.
    if (!$this->stateManager->isLocked($sync_id, $operation)) {
      return;
    }

    $current_run = $this->stateManager->getCurrentRun(
      $sync_id,
      $operation
    );

    // The current run is unset at the end of the terminate event chain, because
    // this is when the operation is considered to have finished. However, after
    // that we run post-termination events and we do not unlock the operation
    // until the end of the post-termination chain. There is therefore a
    // possibility that an error occured after the the current run has been
    // unset but before the state is unlocked. Such possibility, and the
    // possibility that this subscriber runs after the current run is unset and
    // before the state is unlocked is extremely low though, since by default
    // the current run unset/state unlock subscriber run one immediately after
    // the other. Since we no longer have the time that the operation started,
    // the best thing we can do is to consider the run as completed and to
    // unlock the operation.
    if (!isset($current_run['run_time'])) {
      $this->stateManager->unlock($sync_id, $operation);
      $this->logger->warning(sprintf(
        'The "%s" operation for the synchronization with ID "%s" was automatically unlocked. The time of the last run is unknown.',
        $operation,
        $sync_id
      ));
      return;
    }

    $expiration_time = $current_run['run_time'] + $settings['auto_unlock'];
    if ($this->time->getRequestTime() <= $expiration_time) {
      return;
    }

    $this->stateManager->unlock($sync_id, $operation);
    $this->logger->warning(sprintf(
      'The "%s" operation for the synchronization with ID "%s" was automatically unlocked, %s seconds after the time of the last run.',
      $operation,
      $sync_id,
      $this->time->getRequestTime() - $current_run['run_time']
    ));
  }

  /**
   * Returns whether the subscriber applies to the operation of the given event.
   *
   * This subscriber is relevant only to operations managed by Entity
   * Synchronization. We do not need to check if the operation is `import_list`
   * since the event subscriber is only called for remote list imports.
   *
   * @param \Drupal\entity_sync\Event\PreInitiateOperationEvent $event
   *   The pre-initiate operation event.
   *
   * @return bool
   *   `TRUE` if the logic in this subscriber should be executed, `FALSE`
   *   otherwise.
   */
  protected function applies(PreInitiateOperationEvent $event) {
    $sync = $event->getSync();
    if (!$this->stateManager->isManaged($sync->id(), $event->getOperation())) {
      return FALSE;
    }

    return TRUE;
  }

}
