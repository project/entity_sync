<?php

namespace Drupal\entity_sync\EventSubscriber;

use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\EntityImportException;
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\RemoteEntityMappingEvent;
use Drupal\entity_sync\Import\ManagerInterface;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Builds the default remote entity mapping for import operations.
 */
class DefaultImportRemoteEntityMapping implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The field transformer plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $fieldTransformerManager;

  /**
   * Constructs a new DefaultImportRemoteEntityMapping object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $field_transformer_manager
   *   The field transformer plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    PluginManagerInterface $field_transformer_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldTransformerManager = $field_transformer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::REMOTE_ENTITY_MAPPING => ['buildEntityMapping', 0],
    ];
    return $events;
  }

  /**
   * Builds the default remote entity mapping.
   *
   * The default entity mapping is defined in the Synchronization or
   * Operation Type configuration entity.
   *
   * @param \Drupal\entity_sync\Import\Event\RemoteEntityMappingEvent $event
   *   The entity mapping event.
   */
  public function buildEntityMapping(RemoteEntityMappingEvent $event) {
    $sync = $event->getSync();
    $remote_id_field = $sync->getRemoteResourceSettings()['id_field'] ?? NULL;

    // Nothing to do if no remote ID fields are defined. Custom or contrib code
    // responsible for the imports will need to define an alternative subscriber
    // for providing the mapping.
    if ($remote_id_field === NULL) {
      return;
    }

    $entity_settings = $sync->getLocalEntitySettings();
    $local_id_field = $this->getLocalIdQueryField($entity_settings);
    if ($local_id_field === NULL) {
      return;
    }

    $entity_mapping = [];
    $remote_entity = $event->getRemoteEntity();
    $local_entity_ids = $this->entityTypeManager
      ->getStorage($entity_settings['type_id'])
      ->getQuery()
      // phpcs:disable
      // @I Review whether disabling access check is always safe
      //    type     : bug
      //    priority : high
      //    labels   : security
      // @I Review solutions from preventing anonymous entity ownership
      //    type     : bug
      //    priority : high
      //    labels   : security
      // phpcs:enable
      ->accessCheck(FALSE)
      ->condition(
        $local_id_field,
        $remote_entity->{$remote_id_field}
      )
      ->execute();

    if ($local_entity_ids) {
      $entity_mapping = $this->buildUpdateEntityMapping(
        $entity_settings,
        current($local_entity_ids)
      );
    }
    else {
      $entity_mapping = $this->buildCreateEntityMapping(
        $sync,
        $remote_entity,
        $entity_settings
      );
    }

    $event->setEntityMapping($entity_mapping);
  }

  /**
   * Returns the field/property to be queried for loading the local entity IDs.
   *
   * @param array $entity_settings
   *   The local entity settings defined in the synchronization configuration.
   *
   * @return string|null
   *   The field/property name, or `NULL` if there is no ID or composite sync
   *   field defined in the synchronization configuration.
   */
  protected function getLocalIdQueryField(array $entity_settings) {
    if (isset($entity_settings['remote_id_field'])) {
      return $entity_settings['remote_id_field'];
    }

    if (isset($entity_settings['remote_composite_field'])) {
      return $entity_settings['remote_composite_field'] . '.id';
    }
  }

  /**
   * Builds the entity mapping for the create action.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization entity that defines the operation being run.
   * @param object $remote_entity
   *   The remote entity being imported.
   * @param array $entity_settings
   *   The entity settings part of the synchronization object.
   *
   * @return array
   *   The entity mapping.
   */
  protected function buildCreateEntityMapping(
    SyncInterface $sync,
    object $remote_entity,
    array $entity_settings
  ) {
    $entity_mapping = [
      'action' => ManagerInterface::ACTION_CREATE,
      'entity_type_id' => $entity_settings['type_id'],
    ];

    // When creating a new entity, we need to determine the bundle. First, we
    // check if there is a field transformer configured for the bundle entity
    // property so that we support dynamic bundle determination and prioritize
    // it over fixed bundle definition. If not, we check if a fixed value is
    // defined in the local entity settings of the synchronization configuration
    // entity. It is still possible that we will not have a value e.g. if the
    // entity type is not bundleable.
    $bundle = $this->getBundleFromFieldTransformer(
      $sync,
      $remote_entity,
      $entity_mapping['entity_type_id'],
      $entity_settings
    );
    if ($bundle) {
      $entity_mapping['entity_bundle'] = $bundle;
    }
    elseif (isset($entity_settings['bundle'])) {
      $entity_mapping['entity_bundle'] = $entity_settings['bundle'];
    }

    return $entity_mapping;
  }

  /**
   * Builds the entity mapping for the update action.
   *
   * @param array $entity_settings
   *   The entity settings part of the synchronization object.
   * @param string|int $entity_id
   *   The ID of the local entity that was found to be associated with the given
   *   remote entity.
   *
   * @return array
   *   The entity mapping.
   */
  protected function buildUpdateEntityMapping(
    array $entity_settings,
    $entity_id
  ) {
    $entity_mapping = [
      'action' => ManagerInterface::ACTION_UPDATE,
      'entity_type_id' => $entity_settings['type_id'],
      'id' => $entity_id,
    ];

    if (isset($entity_settings['bundle'])) {
      $entity_mapping['entity_bundle'] = $entity_settings['bundle'];
    }

    return $entity_mapping;
  }

  /**
   * Determines the bundle from the bundle entity property field transformer.
   *
   * Note that if the field transformer is defined but it returns an invalid
   * value i.e. not a non-empty string, we do not raise an error. Drupal core
   * will raise an error for trying to create a bundleable entity without a
   * bundle defined.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization entity that defines the operation being run.
   * @param object $remote_entity
   *   The remote entity being imported.
   * @param string $entity_type_id
   *   The ID of the entity type that will be created.
   * @param array $entity_settings
   *   The local entity settings defined in the synchronization configuration.
   *
   * @return string|null
   *   The bundle of the entity that will be created as determined by the field
   *   transformer, or NULL if the entity is not bundleable or if the field
   *   mapping does not define a field transformer for the bundle entity
   *   property.
   */
  protected function getBundleFromFieldTransformer(
    SyncInterface $sync,
    object $remote_entity,
    string $entity_type_id,
    array $entity_settings
  ) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

    // No bundle to determine if the entity type is not bundleable. Exit early.
    if ($entity_type->getBundleEntityType() ? FALSE : TRUE) {
      return NULL;
    }

    $bundle_field_info = NULL;
    $bundle_key = $entity_type->getKey('bundle');
    $field_mapping = $sync->getFieldMapping();
    foreach ($field_mapping as $field_info) {
      if ($field_info['machine_name'] !== $bundle_key) {
        continue;
      }

      $bundle_field_info = $field_info;
    }

    // No field transformer found for the bundle property.
    if ($bundle_field_info === NULL) {
      return NULL;
    }

    try {
      return $this->fieldTransformerManager
        ->createInstance(
          $bundle_field_info['import']['transformer']['id'],
          $bundle_field_info['import']['transformer']['configuration'] ?? []
        )
        ->import(
          $remote_entity,
          NULL,
          $bundle_field_info,
          // Currently, we don't have the context available in the event.
          [],
          NULL,
          TRUE,
          FALSE
        );
    }
    catch (\Throwable $throwable) {
      throw new EntityImportException(sprintf(
        'A "%s" exception was thrown while determining the bundle for the new local entity that would be created as a result of the import. The error message was: %s. The field mapping was: %s',
        get_class($throwable),
        $throwable->getMessage(),
        json_encode($bundle_field_info)
      ));
    }
  }

}
