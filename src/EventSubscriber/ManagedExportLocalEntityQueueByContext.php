<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Event\QueueOperationEvent;
use Drupal\entity_sync\MachineName\Internal\Action\Type as ActionType;
use Drupal\entity_sync\StateManagerInterface;
// External libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cancels queueing export operations for disabled context IDs.
 */
class ManagedExportLocalEntityQueueByContext implements
  EventSubscriberInterface {

  /**
   * The Entity Synchronization state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * Constructs a new ManagedExportLocalEntityQueueByContext object.
   *
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Synchronization export field manager.
   */
  public function __construct(StateManagerInterface $state_manager) {
    $this->stateManager = $state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_QUEUE => ['cancelQueueing', 0],
    ];
    return $events;
  }

  /**
   * Cancels queueing exports based on the operation context.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   */
  public function cancelQueueing(QueueOperationEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    // For exports, when a synchronization is managed by Entity Synchronization
    // we support automatica exports upon entity insert/update/delete hooks. If
    // the context ID indicates that this is a different type of export, we
    // don't intervene - for example, it could be an export triggered by custom
    // code when a user submits a form in the UI. We don't have a say and we
    // don't cancel such operations, and we don't want developers to have to
    // create duplicate synchronizations just so that they can run them outside
    // of the expected contexts.
    $managed_context_ids = [
      'entity_insert',
      'entity_update',
      'entity_delete',
    ];
    $context_id = $event->getContext()['id'] ?? NULL;
    if ($context_id === NULL) {
      return;
    }
    if (!\in_array($context_id, $managed_context_ids, TRUE)) {
      return;
    }

    $settings = $event->getSync()->getOperationsSettings()[ActionType::EXPORT_ENTITY];
    $enabled_context_ids = $settings['state']['context_ids'] ?? NULL;

    // For backwards compatibility, if no context IDs are explicitly enabled we
    // consider entity creations and updates as enabled by default. We do not
    // enable entity deletions by default because existing custom event
    // subscribers might not be updated to handle entity deletions. Also,
    // exporting entity deletions are potentially destructive operations and its
    // better to have developers to explicitly enable them.
    if ($enabled_context_ids === NULL) {
      $enabled_context_ids = [
        'entity_insert',
        'entity_update',
      ];
    }

    if (!\in_array($context_id, $enabled_context_ids, TRUE)) {
      $event->cancel();
      return;
    }
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to `export_entity` operations managed by Entity
   * Synchronization.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(QueueOperationEvent $event) {
    if ($event->getOperation() !== ActionType::EXPORT_ENTITY) {
      return FALSE;
    }

    $is_managed = $this->stateManager->isManaged(
      $event->getSync()->id(),
      ActionType::EXPORT_ENTITY
    );
    if (!$is_managed) {
      return FALSE;
    }

    return TRUE;
  }

}
