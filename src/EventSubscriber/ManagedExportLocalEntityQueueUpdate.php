<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\FieldManagerInterface;
use Drupal\entity_sync\Event\QueueOperationEvent;
use Drupal\entity_sync\MachineName\Internal\Action\Type as ActionType;
use Drupal\entity_sync\StateManagerInterface;
// External libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Cancels queueing export operations if there are no exportable changed fields.
 */
class ManagedExportLocalEntityQueueUpdate implements EventSubscriberInterface {

  /**
   * The Entity Synchronization import field manager.
   *
   * @var \Drupal\entity_sync\Import\FieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The Entity Synchronization state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * Constructs a new ManagedExportLocalEntityQueueUpdate object.
   *
   * @param \Drupal\entity_sync\Import\FieldManagerInterface $field_manager
   *   The Entity Synchronization import field manager.
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Synchronization export field manager.
   */
  public function __construct(
    FieldManagerInterface $field_manager,
    StateManagerInterface $state_manager
  ) {
    $this->fieldManager = $field_manager;
    $this->stateManager = $state_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_QUEUE => ['cancelQueueing', 0],
    ];
    return $events;
  }

  /**
   * Cancels queueing exports if there are no exportable changed fields.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   */
  public function cancelQueueing(QueueOperationEvent $event) {
    if (!$this->applies($event)) {
      return;
    }

    $context = $event->getContext();

    // Cancel queueing the export if we don't have any changed fields that are
    // also set to be exported by the synchronization. The machine names of the
    // changed fields are always provided by the queue manager in the context -
    // even if that is an empty array i.e. no fields changed.
    if (!$context['changed_field_names']) {
      $event->cancel();
      return;
    }

    $exportable_field_names = $this->fieldManager
      ->getExportableChangedNames(
        $context['local_entity'],
        $context['original_entity'],
        $event->getSync()->getFieldMapping(),
        NULL,
        $context['changed_field_names']
      );
    if (!$exportable_field_names) {
      $event->cancel();
      return;
    }
  }

  /**
   * Returns whether our changes apply to the operation being run.
   *
   * This applies to `export_entity` operations managed by Entity
   * Synchronization, that are caused by entity updates.
   *
   * @param \Drupal\entity_sync\Event\QueueOperationEvent $event
   *   The queue operation event.
   *
   * @return bool
   *   Whether the changes in this subscriber should be applied.
   */
  protected function applies(QueueOperationEvent $event) {
    if ($event->getOperation() !== ActionType::EXPORT_ENTITY) {
      return FALSE;
    }

    $is_managed = $this->stateManager->isManaged(
      $event->getSync()->id(),
      ActionType::EXPORT_ENTITY
    );
    if (!$is_managed) {
      return FALSE;
    }

    // Applies to entity updates only. In such cases, `hook_entity_update`
    // calls the queue manager which makes available the original entity in the
    // context.
    if (!isset($event->getContext()['original_entity'])) {
      return FALSE;
    }

    return TRUE;
  }

}
