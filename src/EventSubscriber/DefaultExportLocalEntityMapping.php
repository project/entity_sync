<?php

namespace Drupal\entity_sync\EventSubscriber;

// Drupal modules.
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\Event\LocalEntityMappingEvent;
use Drupal\entity_sync\Export\EntityManagerInterface;
use Drupal\entity_sync\Field\SyncItemListInterface;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
// Third-party libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Builds the default remote ID mapping for export operations.
 */
class DefaultExportLocalEntityMapping implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * Constructs a new DefaultExportLocalEntityMapping object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager
   *   The entity field manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityFieldManagerInterface $field_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      Events::LOCAL_ENTITY_MAPPING => ['buildEntityMapping', 0],
    ];
    return $events;
  }

  /**
   * Builds the default local entity mapping.
   *
   * The default local entity mapping is defined in the synchronization
   * or operation type configuration object.
   *
   * @param \Drupal\entity_sync\Export\Event\LocalEntityMappingEvent $event
   *   The local entity mapping event.
   */
  public function buildEntityMapping(LocalEntityMappingEvent $event) {
    $sync = $event->getSync();

    // We can only provide the default mapping if we have a remote ID (or
    // composite) field. Otherwise, an alternative subscriber must be provided
    // by a custom/contrib module. If a remote ID or composite field is defined
    // in the configuration but it does not exist on the entity, we let an error
    // to be raised as that must be an error in development/configuration.
    $field_name = $this->getRemoteIdField($sync->getLocalEntitySettings());
    if ($field_name === NULL) {
      return;
    }

    $local_entity = $event->getLocalEntity();
    $entity_exists = !$this->isEntityDeleted($local_entity);
    $ids = $this->getRemoteEntityIds($local_entity, $field_name);

    // If we have IDs it's one update for each ID, if we don't it's one
    // creation.
    $entity_mapping = [];
    $remote_settings = $sync->getRemoteResourceSettings();
    // If we have IDs and the entity has not been deleted, we add an update
    // action for each ID.
    if ($ids && $entity_exists) {
      $entity_mapping = $this->buildExistingEntityMapping(
        $ids,
        $remote_settings,
        $event->getContext(),
        $this->getFieldCardinality($local_entity, $field_name),
        EntityManagerInterface::ACTION_UPDATE
      );
    }
    // If we have IDs and the entity has been deleted, we add a delete action
    // for each ID.
    elseif ($ids) {
      $entity_mapping = $this->buildExistingEntityMapping(
        $ids,
        $remote_settings,
        $event->getContext(),
        $this->getFieldCardinality($local_entity, $field_name),
        EntityManagerInterface::ACTION_DELETE
      );
    }
    // If we don't have IDs and the entity has not been deleted, we add a create
    // action.
    elseif ($entity_exists) {
      $entity_mapping = $this->buildCreateEntityMapping($remote_settings);
    }
    // Lastly, if we don't have IDs and the entity has been deleted, it means
    // that the entity was never created in the remote. Nothing to delete.
    else {
      $entity_mapping = $this->buildSkipMapping();
    }

    $event->setEntityMapping($entity_mapping);
  }

  /**
   * Returns the machine name of the remote ID or composite field.
   *
   * @param array $entity_settings
   *   The local entity settings of the synchronization.
   *
   * @return string|null
   *   The machine name of the sync ID (or composite) field, or `NULL` if no
   *   field is configured.
   */
  protected function getRemoteIdField(array $entity_settings) {
    if (isset($entity_settings['remote_id_field'])) {
      return $entity_settings['remote_id_field'];
    }

    if (isset($entity_settings['remote_composite_field'])) {
      return $entity_settings['remote_composite_field'];
    }

    return NULL;
  }

  /**
   * Returns the cardinality of the given field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The entity that the field belongs to.
   * @param string $field_name
   *   The machine name of the field.
   *
   * @return int
   *   The cardinality of the field.
   */
  protected function getFieldCardinality(
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    $definitions = $this->fieldManager->getFieldStorageDefinitions(
      $local_entity->getEntityTypeId()
    );

    return $definitions[$field_name]->getCardinality();
  }

  /**
   * Returns the IDs of the remote entities linked to the given local entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity being exported.
   * @param string $field_name
   *   The machine name of the sync ID/composite field.
   *
   * @return array
   *   An array containing the IDs of the remote entities.
   */
  protected function getRemoteEntityIds(
    ContentEntityInterface $local_entity,
    string $field_name
  ) {
    $field = $local_entity->get($field_name);
    if ($field->isEmpty()) {
      return [];
    }

    $ids = [];
    $is_composite = $field instanceof SyncItemListInterface;
    foreach ($field as $item) {
      if ($is_composite) {
        $ids[] = $item->getId();
      }
      else {
        $ids[] = $item->value;
      }
    }

    return $ids;
  }

  /**
   * Returns whether the entity has been deleted from the database.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The entity being exported.
   *
   * @return bool
   *   `TRUE` if the entity has been deleted from the database, `FALSE`
   *   otherwise.
   */
  protected function isEntityDeleted(ContentEntityInterface $local_entity) {
    // State-managed exports will export new entities after they are saved.
    // However, custom code could be running the export before saving it, or
    // even exporting an entity without ever saving it in Drupal - but still
    // relying on this event mapping provider to trigger a create action. In
    // such cases, the entity wouldn't exist in the database yet and we don't
    // want to confuse a new entity with a deleted one.
    if ($local_entity->isNew()) {
      return FALSE;
    }

    $id_property = $this->entityTypeManager
      ->getDefinition($local_entity->getEntityTypeId())
      ->getKey('id');
    $ids = $this->entityTypeManager
      ->getStorage($local_entity->getEntityTypeId())
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($id_property, $local_entity->id())
      ->execute();

    return $ids ? FALSE : TRUE;
  }

  /**
   * Builds the entity mapping for the create action.
   *
   * If there is no link entity, we create a new one. Even if there is support
   * for one local-to-multiple remote entities, we cannot how many to create -
   * unless we make this configurable. That would be an advanced feature that
   * would hardly ever be used though, so let's leave it for now.
   *
   * We therefore always create one new remote entity. An event subscriber
   * extending this one can easily return a two-dimensional array that will
   * map the local entity to multiple new entities in the remote.
   *
   * @param array $remote_resource_settings
   *   The remote resource settings of the synchronization.
   *
   * @return array
   *   The entity mapping.
   */
  protected function buildCreateEntityMapping(
    array $remote_resource_settings
  ) {
    return [
      'action' => EntityManagerInterface::ACTION_CREATE,
      'client' => $remote_resource_settings['client'] ?? NULL,
    ];
  }

  /**
   * Builds the entity mapping for the update/delete actions.
   *
   * If the cardinality of the sync ID/composite field is different to one, we
   * could have multiple linked remote entities. In that case we return a
   * two-dimensional array which the export manager will loop over and export
   * the local entity to all linked remote entities, one by one.
   *
   * @param array $ids
   *   The IDs of the linked remote entities to update.
   * @param array $remote_resource_settings
   *   The remote resource settings of the synchronization.
   * @param array $context
   *   The context array of the operation being run.
   * @param int $field_cardinality
   *   The cardinality of the sync ID/composite field.
   * @param int $action
   *   The action i.e. update or delete.
   *
   * @return array
   *   The entity mapping.
   */
  protected function buildExistingEntityMapping(
    array $ids,
    array $remote_resource_settings,
    array $context,
    int $field_cardinality,
    int $action
  ) {
    // If we are here without IDs, it must be a software error.
    if (count($ids) === 0) {
      throw new \RuntimeException(
        'Cannot build the entity mapping for exporting to existing remote entities without their IDs.'
      );
    }

    // If we are told to allow the export to specific entities only, respect
    // that.
    // @I Review whether `allowed_remote_entity_ids` should be an option
    //    type     : task
    //    priority : low
    //    labels   : architecture, context, export
    if (isset($context['allowed_remote_entity_ids'])) {
      $ids = array_intersect($ids, $context['allowed_remote_entity_ids']);
    }
    if (count($ids) === 0) {
      return [
        'action' => EntityManagerInterface::ACTION_SKIP,
      ];
    }

    $entity_mapping = [];
    foreach ($ids as $id) {
      $entity_mapping[] = [
        'action' => $action,
        'client' => $remote_resource_settings['client'] ?? NULL,
        'id' => $id,
      ];
    }

    // For maintaining bacward compatibility, we cannot return the mapping
    // derived from a single cardinality field in the same format
    // i.e. two-dimensional array. Event subscribers expect it as a flat array.
    return $field_cardinality === 1 ? $entity_mapping[0] : $entity_mapping;
  }

  /**
   * Builds the entity mapping for the skip action.
   *
   * @return array
   *   The entity mapping.
   */
  protected function buildSkipMapping() {
    return [
      'action' => EntityManagerInterface::ACTION_SKIP,
    ];
  }

}
