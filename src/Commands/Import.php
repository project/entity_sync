<?php

namespace Drupal\entity_sync\Commands;

use Drupal\entity_sync\Import\ManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Commands related to imports.
 *
 * phpcs:disable
 * @I Provide a command for importing a local entity
 *    type     : feature
 *    priority : normal
 *    labels   : drush, import
 * phpcs:enable
 */
class Import extends DrushCommands {

  /**
   * The Entity Sync import entity manager.
   *
   * @var \Drupal\entity_sync\Import\ManagerInterface
   */
  protected $manager;

  /**
   * Constructs a new Import object.
   *
   * @param \Drupal\entity_sync\Import\ManagerInterface $manager
   *   The Entity Sync import manager.
   */
  public function __construct(ManagerInterface $manager) {
    $this->manager = $manager;
  }

  /**
   * Imports a list of remote entities for the given synchronization.
   *
   * @param string $sync_id
   *   The ID of the synchronization for which to run the import.
   * @param array $command_options
   *   (optional) An array of options.
   *
   * @option changed-start
   *   The `changed_start` filter to pass to the import entity manager.
   * @option changed-end
   *   The `changed_end` filter to pass to the import entity manager.
   * @option limit
   *   The `limit` option to pass to the import entity manager.
   * @option options-context
   *   The `context` option to pass to the import entity manager.
   * @option options-client
   *   The `client` option to pass to the import entity manager.
   * @option parameters
   *   The `client[paramaters]` option to pass to the import entity
   *   manager. Deprecated; use `options-client` instead.
   *
   * @usage drush entity-sync:import-remote-list "my_sync_id"
   *   Import the list of remote entities defined in the `import_list` operation
   *   of the `my_sync_id` synchronization.
   * @usage drush entity-sync:import-remote-list "my_sync_id" --limit="10"
   *   Import a list of maximum 10 remote entities defined in the `import_list`
   *   operation of the `my_sync_id` synchronization.
   * @usage drush entity-sync:import-remote-list "my_sync_id" --options-context="key1=value1,key2[subkey1]=subvalue1"
   *   Import a list of remote entities defined in the `import_list` operation
   *   of the `my_sync_id` synchronization, passing custom context to the
   *    import manager.
   * @usage drush entity-sync:import-remote-list "my_sync_id" --options-client="key1=value1,key2[subkey1]=subvalue1"
   *   Import a list of remote entities defined in the `import_list` operation
   *   of the `my_sync_id` synchronization, passing custom options to the
   *   client that will be fetching the entities from the remote resource.
   * @usage drush entity-sync:import-remote-list "my_sync_id" --changed-start="1590105600000" --changed-end="1590623999000"
   *   Import a list of remote entities defined in the `import_list` operation
   *   of the `my_sync_id` synchronization; limit the entities to be imported to
   *   those changed between the given times. The times need to be given in Unix
   *   timestamp format, as expected by the import entity manager.
   *
   * @command entity-sync:import-remote-list
   *
   * @aliases esync-irl
   * phpcs:disable
   * @I Add created start/end filters when supported
   *    type     : improvement
   *    priority : low
   *    labels   : drush
   * @I Support passing state manager context
   *    type     : improvement
   *    priority : normal
   *    labels   : drush
   * phpcs:enable
   */
  public function importRemoteList(
    $sync_id,
    array $command_options = [
      'changed-start' => NULL,
      'changed-end' => NULL,
      'limit' => NULL,
      'options-context' => NULL,
      'options-client' => NULL,
      // @deprecated in entity_sync:4.0.0-beta3 and is removed from
      // entity_sync:6.0.0. Use the `options-client` option instead.
      'parameters' => NULL,
    ]
  ) {
    $filters = [];
    if (isset($command_options['changed-start'])) {
      $filters['changed_start'] = $command_options['changed-start'];
    }
    if (isset($command_options['changed-end'])) {
      $filters['changed_end'] = $command_options['changed-end'];
    }

    $this->validateOptions($command_options);

    // Prepare the options.
    $import_options = [];
    if (isset($command_options['limit'])) {
      $import_options['limit'] = (int) $command_options['limit'];
    }

    $options_to_convert = [
      'options-client' => 'client',
      'options-context' => 'context',
    ];
    foreach ($options_to_convert as $command_key => $import_key) {
      $this->convertAndAddOption(
        $command_key,
        $command_options,
        $import_key,
        $import_options
      );
    }

    // For maintaining backward compatibility we keep the parameters option;
    // it will be removed in a future major version.
    if (isset($command_options['parameters'])) {
      $import_options['client'] = [
        'parameters' => $this->keyValueStringToArray(
          $command_options['parameters']
        ),
      ];
    }

    $this->manager->importRemoteList(
      $sync_id,
      $filters,
      $import_options
    );
  }

  /**
   * Imports a remote entity as defined by the given synchronization.
   *
   * @param string $sync_id
   *   The ID of the synchronization for which to run the import.
   * @param string $remote_entity_id
   *   The ID of the remote entity to import.
   *
   * @usage drush entity-sync:import-remote-entity "my_sync_id" "1"
   *   Import the remote entity with ID "1" with the settings defined in the
   *   `import_entity` operation of the `my_sync_id` synchronization.
   *
   * @command entity-sync:import-remote-entity
   *
   * @aliases esync-ire
   */
  public function importRemoteEntity(
    $sync_id,
    $remote_entity_id
  ) {
    $this->manager->importRemoteEntityById(
      $sync_id,
      $remote_entity_id
    );
  }

  /**
   * Validates that the command options follow the expected rules.
   *
   * @param array $options
   *   The array containing the options passed to the command.
   *
   * @throws \InvalidArgumentException
   *   When both the "options-client" and "parameters" options are provided.
   */
  protected function validateOptions(array $options): void {
    if (isset($options['options-client']) && isset($options['parameters'])) {
      throw new \InvalidArgumentException(
        'Only one of the "options-client" and "parameters" options can be provided.'
      );
    }
  }

  /**
   * Converts and adds a command option input to manager import options.
   *
   * @param string $command_key
   *   The key of the command option.
   * @param array $command_options
   *   The array containing the options provided to the command.
   * @param string $import_key
   *   The key of the manager import option to convert to.
   * @param array $import_options
   *   The array containing the manager import options where the converted
   *   values will be added to.
   * phpcs:disable
   * @I Move to a utility class and test
   *    type     : task
   *    priority : low
   *    labels   : refactoring, testing
   * phpcs:enable
   */
  protected function convertAndAddOption(
    string $command_key,
    array $command_options,
    string $import_key,
    array &$import_options
  ) {
    if (!isset($command_options[$command_key])) {
      return;
    }

    $import_options[$import_key] = $this->keyValueStringToArray(
      $command_options[$command_key]
    );
  }

  /**
   * Converts a key/value string to an array.
   *
   * @param string $input
   *   The input string containing the key value pairs.
   *
   * @return array
   *   An associative array containing the key value pairs.
   * phpcs:disable
   * @I Move to a utility class and test
   *    type     : task
   *    priority : low
   *    labels   : refactoring, testing
   * @I Support strings that contain the delimiter
   *    type     : bug
   *    priority : low
   *    labels   : drush
   * phpcs:enable
   */
  protected function keyValueStringToArray(string $input): array {
    if (empty($input)) {
      return [];
    }

    $input = str_replace(',', '&', $input);

    parse_str($input, $output);
    return $output;
  }

}
