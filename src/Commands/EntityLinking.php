<?php

namespace Drupal\entity_sync\Commands;

use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Commands related to entity linking.
 *
 * Entity linking refers to connecting local and remote entities; it is used by
 * Entity Synchronization to automatically which remote entity to send the
 * changes to when a local entity is created or updated, and vice versa.
 *
 * This class provides commands related to sync field-based entity linking.
 */
class EntityLinking extends DrushCommands {

  /**
   * The Entity Synchronization configuration entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Entity object.
   *
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The Entity Synchronization configuration entity loader.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigEntityLoaderInterface $config_entity_loader,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->configEntityLoader = $config_entity_loader;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Unlinks a local entity from its remote for the given synchronization.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that defines the settings
   *   for linking the local to the remote entity.
   * @param string $entity_id
   *   The ID of the local entity to unlink.
   *
   * @usage drush entity-sync:unlink-local-entity "my_sync_id" 1
   *   Unlinks the local entity with ID "1" as defined in the `my_sync_id`
   *   synchronization - if linked using a sync ID or composite field.
   *
   * @command entity-sync:unlink-local-entity
   *
   * @aliases esync-ule
   */
  public function unlinkLocalEntity($sync_id, $entity_id) {
    $sync = $this->configEntityLoader->load($sync_id);
    if (!$sync) {
      throw new \InvalidArgumentException(
        sprintf(
          'No synchronization or operation type with ID "%s" was found.',
          $sync_id
        )
      );
    }

    $settings = $sync->getLocalEntitySettings();
    $storage = $this->entityTypeManager->getStorage($settings['type_id']);
    $entity = $storage->load($entity_id);
    if (!$entity) {
      throw new \InvalidArgumentException(
        sprintf(
          'No entity of type "%s" and ID "%s" was found.',
          $settings['type_id'],
          $entity_id
        )
      );
    }

    $sync_fields = array_filter([
      $settings['remote_id_field'] ?? FALSE,
      $settings['remote_changed_field'] ?? FALSE,
      $settings['remote_composite_field'] ?? FALSE,
    ]);
    if (count($sync_fields) === 0) {
      throw new \InvalidArgumentException(sprintf(
        'The synchronization or operation type with ID "%s" does not define sync field-based entity linking.',
        $sync_id
      ));
    }

    foreach ($sync_fields as $field_name) {
      $entity->set($field_name, NULL);
    }
    $storage->save($entity);
  }

}
