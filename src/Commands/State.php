<?php

namespace Drupal\entity_sync\Commands;

use Drupal\entity_sync\StateManagerInterface;
use Drupal\entity_sync\Utility\DateTime;
use Drush\Commands\DrushCommands;

/**
 * Commands related to state management.
 *
 * phpcs:disable
 * @I Check that the operation is managed in state commands
 *    type     : bug
 *    priority : low
 *    labels   : drush, state
 * phpcs:enable
 */
class State extends DrushCommands {

  /**
   * The Entity Sync state manager.
   *
   * @var \Drupal\entity_sync\StateManagerInterface
   */
  protected $stateManager;

  /**
   * Constructs a new State object.
   *
   * @param \Drupal\entity_sync\StateManagerInterface $state_manager
   *   The Entity Sync state manager.
   */
  public function __construct(StateManagerInterface $state_manager) {
    $this->stateManager = $state_manager;
  }

  /**
   * Sets last run values for the given synchronization and operation.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param string $operation
   *   The operation.
   * @param array $options
   *   (optional) An array of options.
   *
   * @option run-time
   *   The value to set for the run time.
   * @option start-time
   *   The value to set for the start time.
   * @option end-time
   *   The value to set for the end time.
   * @option limit
   *   The value to set for the limit.
   * @option offset
   *   The value to set for the offset.
   * @option next-offset
   *   The value to set for the  next offset.
   *
   * @usage drush entity-sync:set-last-run "my_sync_id" "import_list" end_time 1690335408
   *   Set the end time of the last run state for the `import_list` operation of
   *   the `my_sync_id` synchronization.
   *
   * @command entity-sync:state-set-last-run
   *
   * @aliases esync-sslr
   */
  public function setLastRun(
    $sync_id,
    $operation,
    array $options = [
      'run-time' => NULL,
      'start-time' => NULL,
      'end-time' => NULL,
      'limit' => NULL,
      'offset' => NULL,
      'next-offset' => NULL,
    ]
  ) {
    $option_keys = [
      'run-time',
      'start-time',
      'end-time',
      'limit',
      'offset',
      'next-offset',
    ];

    // Validate and format the given options.
    $values = [];
    foreach ($option_keys as $key) {
      // Supported options will have a `NULL` value if not provided. In such
      // cases we consider this as a request to not update these values
      // i.e. only update the values given. To request that a value is unset
      // i.e. set to `NULL`, the user needs to provide "null" as a string.
      if ($options[$key] === NULL) {
        continue;
      }

      if ($options[$key] !== 'null' && !DateTime::isTimestamp($options[$key])) {
        throw new \InvalidArgumentException(sprintf(
          'Option "%s" must be a positive integer number.',
          $options[$key]
        ));
      }

      if ($options[$key] === 'null') {
        $values[str_replace('-', '_', $key)] = NULL;
        continue;
      }

      $values[str_replace('-', '_', $key)] = (int) $options[$key];
    }

    if (count($values) === 0) {
      throw new \InvalidArgumentException(
        'At least one option must be provided.'
      );
    }

    // Merge with the existing values so that existing values for which a new
    // value is not provided are not removed.
    $last_run = $this->stateManager->getLastRun($sync_id, $operation);
    $last_run = array_merge($last_run, $values);

    $this->stateManager->setLastRun(
      $sync_id,
      $operation,
      $last_run['run_time'] ?? NULL,
      $last_run['start_time'] ?? NULL,
      $last_run['end_time'] ?? NULL,
      $last_run['limit'] ?? NULL,
      $last_run['offset'] ?? NULL,
      $last_run['next_offset'] ?? NULL
    );
  }

  /**
   * Unsets the last run state for the given synchronization and operation.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param string $operation
   *   The operation.
   *
   * @usage drush entity-sync:unset-last-run "my_sync_id" "import_list"
   *   Unset the last run state for the `import_list` operation of the
   *   `my_sync_id` synchronization.
   *
   * @command entity-sync:state-unset-last-run
   *
   * @aliases esync-sulr
   */
  public function unsetLastRun($sync_id, $operation) {
    $this->stateManager->unsetLastRun($sync_id, $operation);
  }

  /**
   * Locks the operation for the given synchronization and operation.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param string $operation
   *   The operation.
   *
   * @usage drush entity-sync:state-lock-operation "my_sync_id" "import_list"
   *   Lock the `import_list` operation of the `my_sync_id` synchronization.
   *
   * @command entity-sync:state-lock-operation
   *
   * @aliases esync-slo
   */
  public function lock($sync_id, $operation) {
    $this->stateManager->lock($sync_id, $operation);
  }

  /**
   * Unlocks the operation for the given synchronization and operation.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param string $operation
   *   The operation.
   *
   * @usage drush entity-sync:state-unlock-operation "my_sync_id" "import_list"
   *   Unlocks the `import_list` operation of the `my_sync_id` synchronization.
   *
   * @command entity-sync:state-unlock-operation
   *
   * @aliases esync-suo
   */
  public function unlock($sync_id, $operation) {
    $this->stateManager->unlock($sync_id, $operation);
  }

  /**
   * Deletes the state for the given synchronization and operation.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param string $operation
   *   The operation.
   *
   * @usage drush entity-sync:state-delete-operation "my_sync_id" "import_list"
   *   Deletes the state of the `import_list` operation of the `my_sync_id`
   *   synchronization.
   *
   * @command entity-sync:state-delete-operation
   *
   * @aliases esync-sdo
   */
  public function delete($sync_id, $operation) {
    $this->stateManager->delete($sync_id, $operation);
  }

}
