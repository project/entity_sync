<?php

namespace Drupal\entity_sync\Import;

use Drupal\entity_sync\Config\ManagerInterface as ConfigManagerInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\ComputedValueException;
use Drupal\entity_sync\Exception\FieldImportException;
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Exception\SkipFieldException;
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\FieldMappingEvent;
use Drupal\entity_sync\Utility\EntityValidation;
use Drupal\entity_sync\Utility\DateTime;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The default field manager.
 */
class FieldManager implements FieldManagerInterface {

  /**
   * The Entity Sync configuration manager.
   *
   * @var \Drupal\entity_sync\Config\ManagerInterface
   */
  protected $configManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The field transformer plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $fieldTransformerManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new FieldManager instance.
   *
   * @param \Drupal\entity_sync\Config\ManagerInterface $config_manager
   *   The Entity Sync configuration manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $field_transformer_manager
   *   The field transformer plugin manager.
   */
  public function __construct(
    ConfigManagerInterface $config_manager,
    EventDispatcherInterface $event_dispatcher,
    LoggerInterface $logger,
    PluginManagerInterface $field_transformer_manager
  ) {
    $this->configManager = $config_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger;
    $this->fieldTransformerManager = $field_transformer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function import(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $options = []
  ) {
    // Build the field mapping for the fields that will be imported.
    // Note that we proceed with the import even if we are given an empty field
    // mapping. There are cases where we may be importing just the ID field
    // e.g. a taxonomy term import from a CSV file where we use the name as the
    // ID and we do not import a description. In that case it would be redundant
    // to have declare the name as the ID field and in the field mapping.
    //
    // phpcs:disable
    // @I Validate the final field mapping
    //    type     : bug
    //    priority : normal
    //    labels   : mapping, validation
    // phpcs:enable
    $field_mapping = $this->fieldMapping($remote_entity, $local_entity, $sync);

    $this->doImport(
      $remote_entity,
      $local_entity,
      $sync,
      $field_mapping,
      $options['context'] ?? []
    );

    // Run Drupal core's entity validation, if requested. We do not do this on
    // the entity manager before saving the entity because we do not have the
    // field mapping available there.
    // phpcs:disable
    // @I Support entity validation when importing individual entities as well
    //    type     : improvement
    //    priority : normal
    //    labels   : validation
    // phpcs:enable
    $settings = $sync->getOperationsSettings();
    if ($settings['import_list']['validate_entities'] ?? FALSE) {
      $this->validateEntityFields(
        $remote_entity,
        $local_entity,
        $sync,
        $field_mapping
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteCompositeField(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    $force = TRUE
  ) {
    // The remote composite field is specifically meant for storing both the ID
    // and the changed field of the remote entity. We must therefore have both
    // of them in the remote resource settings, otherwise something is wrong.
    $remote_settings = $sync->getRemoteResourceSettings();
    $remote_id_field = $remote_settings['id_field'] ?? NULL;
    $remote_changed_field_config = $remote_settings['changed_field'] ?? NULL;
    if (!($remote_id_field || $remote_changed_field_config)) {
      throw new InvalidConfigurationException(
        'Both the ID and the changed field must be configured on the remote resource when using a remote composite field.'
      );
    }

    if (!isset($remote_entity->{$remote_id_field})) {
      throw new \RuntimeException(
        sprintf(
          'The non-existing remote entity field "%s" was requested to be mapped to the remote entity ID field on the local entity.',
          $remote_id_field
        )
      );
    }
    $remote_changed_field_name = $remote_changed_field_config['name'];
    if (!isset($remote_entity->{$remote_changed_field_name})) {
      throw new \RuntimeException(
        sprintf(
          'The non-existing remote entity field "%s" was requested to be mapped to the remote entity changed field on the local entity.',
          $remote_changed_field_name
        )
      );
    }

    $local_settings = $sync->getLocalEntitySettings();
    $composite_field_name = $local_settings['remote_composite_field'] ?? NULL;
    if (!$composite_field_name) {
      throw new InvalidConfigurationException(
        'A remote composite field must be defined in local entity settings.'
      );
    }

    $remote_changed_field_value = $this->getRemoteChangedFieldValue(
      $remote_entity,
      $remote_changed_field_config
    );
    $item_value = [
      'id' => $remote_entity->{$remote_id_field},
      'changed' => $remote_changed_field_value,
    ];

    $composite_field = $local_entity->get($composite_field_name);

    // If we have a multiple cardinality field, this means that we can import
    // multiple remote entities into the same local entity. We therefore append
    // the new ID/changed pair - if a field item does not exist yet for the
    // incoming ID, or we update the `changed` property of the existing
    // item. This way we can keep track of multiple remote entity associations
    // and the time that each has changed in the remote system.
    // If the cardinality of the field is not unlimited, we would not know what
    // to do when reaching the limit - unless we add settings to configure that
    // behavior e.g. fail, if `force` is set to `TRUE` override the last item,
    // or override the first item etc. This is probably a very rare case that we
    // have not encountered yet so we're not going to support it until it
    // actually comes up.
    // phpcs:disable
    // @I Support multiple cardinality in the remote ID field as well
    //    type     : improvement
    //    priority : low
    //    labels   : import, mapping
    // phpcs:enable
    $cardinality = $composite_field
      ->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    if ($cardinality !== 1) {
      $composite_field->appendOrUpdateItem($item_value);
      return;
    }

    if (!$force && !$composite_field->isEmpty()) {
      $composite_field
        ->first()
        ->setChanged($item_value['changed']);
      return;
    }

    $composite_field->setValue($item_value);
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteIdField(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    $force = TRUE
  ) {
    // The entity mapping method supported by default is using a remote ID
    // field. We set it here if we are importing a new entity, or if we are
    // exporting a local entity for the first time. However, if there's no
    // remote ID field defined we must have custom subscribers being responsible
    // for providing a system for entity mapping. We don't do anything in that
    // case.
    //
    // If a remote ID field is configured, however, we do expect it to exist in
    // the remote entity - otherwise something's wrong.
    $remote_id_field = $sync->getRemoteResourceSettings()['id_field'] ?? NULL;
    if (!$remote_id_field) {
      return FALSE;
    }

    if (!isset($remote_entity->{$remote_id_field})) {
      throw new \RuntimeException(
        sprintf(
          'The non-existing remote entity field "%s" was requested to be mapped to the remote entity ID field on the local entity.',
          $remote_id_field
        )
      );
    }

    $local_id_field = $sync->getLocalEntitySettings()['remote_id_field'] ?? NULL;
    if (!$local_id_field) {
      throw new InvalidConfigurationException(
        'A remote ID field must be defined in local entity settings if there is an ID field in the remote resource settings.'
      );
    }
    if (!$force && !$local_entity->get($local_id_field)->isEmpty()) {
      return FALSE;
    }

    $local_entity->set(
      $local_id_field,
      $remote_entity->{$remote_id_field}
    );
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteChangedField(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync
  ) {
    // The changed fields may not exist when the list import is always a full
    // import i.e. not based on fetching recently created or updated list of
    // remote entities. We therefore simply do not anything if the changed field
    // is not defined. If it is defined in the configuration though but it does
    // not exist in the remote entity, we throw an exception.
    //
    // phpcs:disable
    // @I Validate that the changed field is defined wherever appropriate
    //    type     : improvement
    //    priority : normal
    //    labels   : config, validation
    //    notes    : Validate when the sync configuration is imported.
    // @I Validate that both changed fields are defined or not
    //    type     : improvement
    //    priority : normal
    //    labels   : config, validation
    //    notes    : Validate when the sync configuration is imported.
    // phpcs:enable
    $field_config = $sync->getRemoteResourceSettings()['changed_field'] ?? NULL;
    if (!$field_config) {
      return FALSE;
    }

    $field_name = $field_config['name'];
    if (!isset($remote_entity->{$field_name})) {
      throw new \RuntimeException(
        sprintf(
          'The non-existing remote entity field "%s" was requested to be mapped to the remote entity changed field on the local entity.',
          $field_name
        )
      );
    }

    $field_value = $this->getRemoteChangedFieldValue(
      $remote_entity,
      $field_config
    );

    $local_entity->set(
      $sync->getLocalEntitySettings()['remote_changed_field'] ?? NULL,
      $field_value
    );
    return TRUE;
  }

  /**
   * Builds and returns the field mapping for the given entities.
   *
   * The field mapping defines which local entity fields will be updated with
   * which values contained in the given remote entity. The default mapping is
   * defined in the synchronization to which the operation we are currently
   * executing belongs.
   *
   * An event is dispatched that allows subscribers to alter the default field
   * mapping.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   *
   * @return array
   *   The final field mapping.
   * phpcs:disable
   * @I Validate the final field mapping
   *    type     : bug
   *    priority : normal
   *    labels   : import, mapping, validation
   * phcs:enable
   */
  protected function fieldMapping(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync
  ) {
    $event = new FieldMappingEvent(
      $remote_entity,
      $local_entity,
      $sync
    );
    $this->eventDispatcher->dispatch($event, Events::FIELD_MAPPING);

    // Return the final mappings.
    return $event->getFieldMapping();
  }

  /**
   * Does the actual import of the fields for the given entities.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $field_mapping
   *   The field mapping.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @throws \Drupal\entity_sync\Exception\FieldImportException
   *   When an error occurs while importing a field.
   */
  protected function doImport(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $field_mapping,
    array $context
  ) {
    $context['computed_values'] = $this->calculateComputedValues(
      $remote_entity,
      $local_entity,
      $sync,
      $context
    );

    foreach ($field_mapping as $field_info) {
      $field_info = $this->configManager
        ->mergeImportFieldMappingDefaults($field_info);
      if (!$field_info['import']['status']) {
        continue;
      }

      try {
        $this->doImportField(
          $remote_entity,
          $local_entity,
          $field_info,
          $sync,
          $context
        );
      }
      // If we are instructed to skip the field, we do so but continue with the
      // rest.
      catch (SkipFieldException $exception) {
        if ($exception->getMessage()) {
          $this->logger->error($this->buildExceptionMessage(
            $remote_entity,
            $local_entity,
            $field_info,
            $sync,
            $exception
          ));
        }
        continue;
      }
      catch (\Throwable $throwable) {
        throw new FieldImportException(
          $this->buildExceptionMessage(
            $remote_entity,
            $local_entity,
            $field_info,
            $sync,
            $throwable
          )
        );
      }
    }

    // Update the remote composite field.
    // With the absence of automated tests, safest way to not break existing
    // code is to keep the rest of the code exactly as is if the
    // `remote_composite_field` is not defined.
    if (($sync->getLocalEntitySettings()['remote_composite_field'] ?? FALSE) !== FALSE) {
      try {
        $this->setRemoteCompositeField($remote_entity, $local_entity, $sync);
      }
      catch (\Throwable $throwable) {
        $this->throwSyncFieldException(
          $remote_entity,
          $local_entity,
          'remote_composite',
          $sync,
          $throwable
        );
      }
      return;
    }

    // Update the remote ID field.
    // phpcs:disable
    // @I Support not updating the remote ID field
    //    type     : improvement
    //    priority : low
    //    labels   : import
    // phpcs:enable
    try {
      $this->setRemoteIdField($remote_entity, $local_entity, $sync);
    }
    catch (\Throwable $throwable) {
      $this->throwSyncFieldException(
        $remote_entity,
        $local_entity,
        'remote_id',
        $sync,
        $throwable
      );
    }

    // Update the remote changed field. The remote changed field will be used in
    // `hook_entity_insert` and `hook_entity_update` to prevent triggering an
    // export of the local entity as a result of an import.
    try {
      $this->setRemoteChangedField($remote_entity, $local_entity, $sync);
    }
    catch (\Throwable $throwable) {
      $this->throwSyncFieldException(
        $remote_entity,
        $local_entity,
        'remote_changed',
        $sync,
        $throwable
      );
    }
  }

  /**
   * Performs the actual import of a remote field to a local field.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   */
  protected function doImportField(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    array $field_info,
    SyncInterface $sync,
    array $context
  ) {
    // If the field value should be converted and stored by a custom callback,
    // then invoke that.
    if ($field_info['import']['callback'] ?? []) {
      call_user_func(
        $field_info['import']['callback']['callable'],
        $remote_entity,
        $local_entity,
        $field_info,
        $field_info['import']['callback']['parameters'] ?? []
      );
    }
    // If the field value should be converted and stored by a plugin, then
    // invoke that.
    elseif ($field_info['import']['transformer'] ?? []) {
      $this->fieldTransformerManager
        ->createInstance(
          $field_info['import']['transformer']['id'],
          $field_info['import']['transformer']['configuration'] ?? []
        )
        ->import(
          $remote_entity,
          $local_entity,
          $field_info,
          $context
        );
    }
    // Else, we assume direct copy of the remote field value into the local
    // field.
    //
    // phpcs:disable
    // @I Fallback to the `copy_value` plugin
    //    type     : task
    //    priority : low
    //    labels   : field-handler, import
    // @I Add more details about the field mapping in the exception message
    //    type     : task
    //    priority : low
    //    labels   : error-handling, import
    // @I Provide the option to continue the entity import when a field fails
    //    type     : improvement
    //    priority : normal
    //    labels   : error-handling, import
    // @I Log warning or throw exception when the remote field does not exist
    //    type     : improvement
    //    priority : normal
    //    labels   : error-handling, import
    // @I Implement log levels e.g. error, warning, info, debug
    //    type     : feature
    //    priority : normal
    //    labels   : error-handling, import
    // @I Support configuration entities
    //    type     : feature
    //    priority : normal
    //    labels   : import
    //    notes    : Configuration entities do not have a function that checks
    //               whether a property exists and that case has to be handled
    //               differently.
    // phpcs:enable
    elseif (!$local_entity->hasField($field_info['machine_name'])) {
      throw new \RuntimeException(
        sprintf(
          'The non-existing local entity field "%s" was requested to be mapped to a remote field',
          $field_info['machine_name']
        )
      );
    }
    // We  copy the value when the field exists even if it is NULL. When that's
    // the case, that would result any existing field value to be unset - which
    // is legitimate case, a field can be empty.
    // More advanced configuration options will be provided in the future via
    // field mapping import modes that will allow to determine the desired
    // behavior in such cases.
    elseif (property_exists($remote_entity, $field_info['remote_name'])) {
      $local_entity->set(
        $field_info['machine_name'],
        $remote_entity->{$field_info['remote_name']}
      );
    }
  }

  /**
   * Calculates the computed values.
   *
   * Computed values are calculated similarly to how field values are; they
   * are just calculated in advance and are made available to the field
   * transformers. The most common use case is to calculate a value only once
   * and then make it available in the pipelines of multiple fields.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return array
   *   An associative array keyed by each value's key (`computed_name` property
   *   of the settings item) and containing the computed value.
   */
  protected function calculateComputedValues(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $context
  ) {
    $values = [];
    $context['computed_values'] = [];
    foreach (($sync->getComputedValuesSettings() ?? []) as $value_info) {
      $value_info = $this->configManager
        ->mergeImportComputedValueDefaults($value_info);
      if (!$value_info['import']['status']) {
        continue;
      }

      $key = $value_info['computed_name'];
      try {
        // Computed values in imports can only work with field transformers.
        // Callbacks and direct field copies would set the value on the local
        // entity - which we don't want to do here.
        $values[$key] = $this->fieldTransformerManager
          ->createInstance(
            $value_info['import']['transformer']['id'],
            $value_info['import']['transformer']['configuration'] ?? []
          )
          ->import(
            $remote_entity,
            $local_entity,
            $value_info,
            $context,
            NULL,
            TRUE,
            // Do not set the value on the local entity.
            FALSE
          );
      }
      // If we are instructed to skip the value, we do so but continue with the
      // rest. Since transformer plugins do not know whether they are
      // transforming a value for a field or for a computed value, we do not
      // have an exception specific to skipping computed values.
      catch (SkipFieldException $exception) {
        continue;
      }
      catch (\Throwable $throwable) {
        $this->throwComputedValueException(
          $remote_entity,
          $local_entity,
          $value_info,
          $sync,
          $throwable
        );
      }

      // Make the computed value available to subsequent computations.
      $context['computed_values'][$key] = $values[$key];
    }

    return $values;
  }

  /**
   * Runs the Drupal core entity validation.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the field mapping.
   * @param array $field_mapping
   *   The field mapping.
   */
  protected function validateEntityFields(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $field_mapping
  ) {
    $violations = $local_entity->validate();

    // Remove violations for fields that are not being imported.
    $violations = $violations->getByFields(array_map(
      function ($field_info) {
        return $field_info['machine_name'];
      },
      $field_mapping
    ));
    if ($violations->count() === 0) {
      return;
    }

    $field_name = NULL;
    $settings = $sync->getOperationsSettings()['import_list'] ?? [];
    $allowed_violations = $settings['validate_entities_settings']['allowed_violations'] ?? [];
    foreach ($violations as $violation) {
      [$field_name] = explode('.', $violation->getPropertyPath(), 2);
      $is_allowed = EntityValidation::isViolationAllowed(
        $field_name,
        $violation,
        $allowed_violations
      );
      if ($is_allowed) {
        $field_name = NULL;
        continue;
      }

      // Stop at the first violation found.
      // We currently stop the entity import by raising a field exception. We
      // therefore do not use all of the potential multiple violations that were
      // found.
      // phpcs:disable
      // @I Log all entity violations
      //    type     : improvement
      //    priority : low
      //    labels   : import, validation
      // phpcs:enable
      break;
    }

    // Return early if all found violations are allowed.
    if ($field_name === NULL) {
      return;
    }

    foreach ($field_mapping as $field_info) {
      if ($field_info['machine_name'] !== $field_name) {
        continue;
      }

      // phpcs:disable
      // @I Include the violation code in the message
      //    type     : improvement
      //    priority : low
      //    labels   : import, validation
      // phpcs:enable
      throw new FieldImportException(
        $this->buildExceptionMessage(
          $remote_entity,
          $local_entity,
          $field_info,
          $sync,
          new \Exception((string) $violation->getMessage())
        )
      );
    }
  }

  /**
   * Returns the change field value based on the format defined in settings.
   *
   * @param \stdClass $remote_entity
   *   The remote entity.
   * @param array $field_config
   *   The remote resource changed field settings defined in the synchronization
   *   configuration.
   *
   * @throws \RuntimeException
   *   When the value of the changed field in the remote entity cannot be
   *   converted based on the configured format.
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When an unsupported format is requested in the settings.
   */
  protected function getRemoteChangedFieldValue(
    object $remote_entity,
    array $field_config
  ) {
    // Prepare the value based on the configured format.
    $field_name = $field_config['name'];
    $field_value = NULL;
    if ($field_config['format'] === 'timestamp') {
      $field_value = $remote_entity->{$field_name};
      if (!DateTime::isTimestamp($field_value)) {
        throw new \RuntimeException(
          sprintf(
            'The remote entity field "%s" that was requested to be mapped to the remote entity changed field on the local entity was expected to be in Unix timestamp format, "%s" given.',
            $field_name,
            $field_value
          )
        );
      }

      return $field_value;
    }

    if ($field_config['format'] === 'string') {
      $field_value = strtotime($remote_entity->{$field_name});
      if ($field_value === FALSE) {
        throw new \RuntimeException(
          sprintf(
            'The remote entity field "%s" that was requested to be mapped to the remote entity changed field on the local entity was expected to be in textual datetime format supported by PHP\'s `strtotime`, "%s" given.',
            $field_name,
            $field_value
          )
        );
      }

      return $field_value;
    }

    if ($field_config['format'] === 'iso8601') {
      // Unlike the cases above, an exception will be thrown if the format is
      // incorrect so no need to check and do so here.
      return DateTime::iso8601ToTimestamp($remote_entity->{$field_name});
    }

    throw new InvalidConfigurationException(sprintf(
      'Unsupported remote changed field format "%s".',
      $field_config['format']
    ));
  }

  /**
   * Builds the message for errors that happen during field imports.
   *
   * All errors are caught by the field manager, including errors that happen in
   * field transformers, and `FieldImportException` exceptions are thrown. We
   * add additional context to the error messages so that we know when and how
   * the error occurred.
   *
   * The error may also just be logged instead of halting program execution if
   * it is an error in a field transformer with the failure mode set to
   * `log_and_skip`. Thus, we don't raise the error here, we just build the
   * message.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param \Throwable $throwable
   *   The exception that was thrown during field import.
   */
  protected function buildExceptionMessage(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    array $field_info,
    SyncInterface $sync,
    \Throwable $throwable
  ) {
    $id_field = $sync->getRemoteResourceSettings()['id_field'] ?? NULL;
    $entity_text = sprintf(
      'the local entity with ID "%s"',
      $local_entity->id()
    );
    return sprintf(
      'A "%s" exception was thrown while importing the "%s" field of the remote entity with ID "%s" into the "%s" field of %s. The error message was: %s. The field mapping was: %s',
      get_class($throwable),
      $field_info['remote_name'],
      $id_field ? ($remote_entity->{$id_field} ?? '') : '',
      $field_info['machine_name'],
      $local_entity->isNew() ? 'a new local entity' : $entity_text,
      $throwable->getMessage(),
      json_encode($field_info)
    );
  }

  /**
   * Throws a wrapper exception for exceptions thrown during sync field import.
   *
   * We do this so that we can recognize as `ImportFieldException` any exception
   * that may be thrown during field import runtime.
   *
   * Sync fields are special fields required by the module to correctly perform
   * its operations e.g. remote ID and remote changed fields.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param string $sync_field
   *   The sync field being imported. Supported values are:
   *   - `remote_id` for the remote ID field.
   *   - `remote_changed` for the remote changed field.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param \Throwable $throwable
   *   The exception that was thrown during field import.
   */
  protected function throwSyncFieldException(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    $sync_field,
    SyncInterface $sync,
    \Throwable $throwable
  ) {
    // We want to build the message in the same way that we do for other field
    // import exceptions; the only difference in the arguments is that it
    // expects the field info array in the format provided by the
    // synchronization configuration for standard fields. We therefore prepare
    // that info for the given sync field.
    $field_info = [];
    $local_settings = $sync->getLocalEntitySettings();
    $remote_settings = $sync->getRemoteResourceSettings();
    switch ($sync_field) {
      case 'remote_id':
        $field_info['machine_name'] = $local_settings['remote_id_field'] ?? NULL;
        $field_info['remote_name'] = $remote_settings['id_field'] ?? NULL;
        break;

      case 'remote_changed':
        $field_info['machine_name'] = $local_settings['remote_changed_field'] ?? NULL;
        $field_info['remote_name'] = $remote_settings['changed_field']['name'] ?? NULL;
        break;

      case 'remote_composite':
        $field_info['machine_name'] = $local_settings['remote_composite_field'] ?? NULL;
        $field_info['remote_name'] = $remote_settings['id_field']['name'] ?? NULL;
        break;

      default:
        throw new \InvalidArgumentException(
          sprintf(
            'The "remote_id", "remote_changed" and "remote_composite" sync fields are supported, "%s" given while throwing a field import exception.',
            $sync_field
          )
        );
    }

    throw new FieldImportException(
      $this->buildExceptionMessage(
        $remote_entity,
        $local_entity,
        $field_info,
        $sync,
        $throwable
      )
    );
  }

  /**
   * Throws a wrapper exception for exceptions thrown during value calculations.
   *
   * We do this so that we can recognize as a `ComputedValueException` any
   * exception that may be thrown during calculating computed values.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param array $value_info
   *   The computed value info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param \Throwable $throwable
   *   The exception that was thrown during computed value calculation.
   *
   * @throws \Drupal\entity_sync\Exception\ComputedValueException
   *   Always.
   */
  protected function throwComputedValueException(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    array $value_info,
    SyncInterface $sync,
    \Throwable $throwable
  ) {
    $id_field = $sync->getRemoteResourceSettings()['id_field'] ?? NULL;
    $entity_text = sprintf(
      'the local entity with ID "%s"',
      $local_entity->id()
    );

    throw new ComputedValueException(sprintf(
      '"%s" exception was thrown while calculating the "%s" computed value for the remote entity with ID "%s" while being imported to %s. The error message was: %s. The field mapping was: %s',
      get_class($throwable),
      $value_info['computed_name'],
      $id_field ? ($remote_entity->{$id_field} ?? '') : '',
      $local_entity->isNew() ? 'a new local entity' : $entity_text,
      $throwable->getMessage(),
      json_encode($value_info)
    ));
  }

}
