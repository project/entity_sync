<?php

namespace Drupal\entity_sync\Import\Event;

use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\Core\Entity\EntityInterface;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the import local entity mapping event.
 *
 * Allows subscribers to define which remote entity a local entity should be
 * mapped to.
 */
class LocalEntityMappingEvent extends Event {

  /**
   * The entity mapping for the local entity being mapped.
   *
   * The mapping is an associative array that defines which remote entity the
   * local entity should be mapped to. Supported array elements:
   * - action: The action to be taken. Possible values are:
   *   - ManagerInterface::ACTION_SKIP: Do not import the entity.
   *   - ManagerInterface::ACTION_IMPORT: Import the remote entity.
   *   See \Drupal\entity_sync\Import\ManagerInterface.
   * - client: An associative array that contains the details of the client that
   *   will be used to fetch the remote entity. Supported elements are:
   *   - type: The type of the client; currently supported types are:
   *     - service
   *     - factory
   *   - service: The Drupal service that provides the client or the client
   *     factory.
   *   - parameters: An associative array containing parameters to pass to pass
   *     to the client factory - when the type is set to `factory`.
   * - id: The ID of the entity that will be imported.
   *
   * @var array
   */
  protected $entityMapping = [];

  /**
   * The local entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $localEntity;

  /**
   * The Synchronization or Operation Type configuration entity.
   *
   * @var \Drupal\entity_sync\Entity\SyncInterface
   */
  protected $sync;

  /**
   * Constructs a new LocalEntityMappingEvent object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $local_entity
   *   The local entity that's being mapped.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   */
  public function __construct(
    EntityInterface $local_entity,
    SyncInterface $sync
  ) {
    $this->localEntity = $local_entity;
    $this->sync = $sync;
  }

  /**
   * Gets the entity mapping array.
   *
   * @return array
   *   The entity mapping array.
   */
  public function getEntityMapping() {
    return $this->entityMapping;
  }

  /**
   * Sets the entity mapping.
   *
   * @param array $entity_mapping
   *   The entity mapping array.
   */
  public function setEntityMapping(array $entity_mapping) {
    $this->entityMapping = $entity_mapping;
  }

  /**
   * Gets the local entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The local entity.
   */
  public function getLocalEntity() {
    return $this->localEntity;
  }

  /**
   * Gets the Synchronization or Operation Type configuration entity.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface
   *   The Synchronization or Operation Type configuration entity.
   */
  public function getSync() {
    return $this->sync;
  }

}
