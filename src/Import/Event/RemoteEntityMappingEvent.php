<?php

namespace Drupal\entity_sync\Import\Event;

use Drupal\entity_sync\Entity\SyncInterface;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the import remote entity mapping event.
 *
 * Allows subscribers to define which local entity a remote entity should be
 * mapped to.
 */
class RemoteEntityMappingEvent extends Event {

  /**
   * The entity mapping for the remote entity being imported.
   *
   * The mapping is an associative array that defines which local entity the
   * remote entity should be mapped to. Supported array elements:
   * - action: The action to be taken. Possible values are:
   *   - ManagerInterface::ACTION_SKIP: Do not import the entity.
   *   - ManagerInterface::ACTION_CREATE: Create a new local entity.
   *   - ManagerInterface::ACTION_UPDATE: Update an existing local entity.
   *   See \Drupal\entity_sync\Import\ManagerInterface.
   * - entity_type_id: The type of the entity that will be created or updated.
   * - entity_bundle: The bundle of the entity, in the case of creating a new
   *   entity.
   * - id (optional): The ID of the local entity that will be updated. Required
   *   when the action is ManagerInterface::ACTION_UPDATE.
   *
   * @var array
   */
  protected $entityMapping = [];

  /**
   * The remote entity being imported.
   *
   * @var object
   */
  protected $remoteEntity;

  /**
   * The Synchronization or Operation Type configuration entity.
   *
   * @var \Drupal\entity_sync\Entity\SyncInterface
   */
  protected $sync;

  /**
   * Constructs a new RemoteEntityMappingEvent object.
   *
   * @param object $remote_entity
   *   The remote entity that's being mapped.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   */
  public function __construct($remote_entity, SyncInterface $sync) {
    $this->remoteEntity = $remote_entity;
    $this->sync = $sync;
  }

  /**
   * Gets the entity mapping array.
   *
   * @return array
   *   The entity mapping array.
   */
  public function getEntityMapping() {
    return $this->entityMapping;
  }

  /**
   * Sets the entity mapping.
   *
   * @param array $entity_mapping
   *   The entity mapping array.
   */
  public function setEntityMapping(array $entity_mapping) {
    $this->entityMapping = $entity_mapping;
  }

  /**
   * Gets the remote entity.
   *
   * @return object
   *   The remote entity.
   */
  public function getRemoteEntity() {
    return $this->remoteEntity;
  }

  /**
   * Gets the Synchronization or Operation Type configuration entity.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface
   *   The Synchronization or Operation Type configuration entity.
   */
  public function getSync() {
    return $this->sync;
  }

}
