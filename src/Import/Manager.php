<?php

namespace Drupal\entity_sync\Import;

use Drupal\entity_sync\Client\ClientFactory;
use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\EntityImportException;
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Import\Event\Events;
use Drupal\entity_sync\Import\Event\ListFiltersEvent;
use Drupal\entity_sync\Import\Event\LocalEntityMappingEvent;
use Drupal\entity_sync\Import\Event\RemoteEntityMappingEvent;
use Drupal\entity_sync\EntityManagerBase;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The default import manager.
 *
 * phpcs:disable
 * @I Rename to EntityManager
 *    type     : task
 *    priority : low
 *    labels   : coding-standards
 * phpcs:enable
 */
class Manager extends EntityManagerBase implements ManagerInterface {

  /**
   * The client factory.
   *
   * @var \Drupal\entity_sync\Client\ClientFactory
   */
  protected $clientFactory;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The config entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Entity Sync import field manager.
   *
   * @var \Drupal\entity_sync\Import\FieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new Manager instance.
   *
   * @param \Drupal\entity_sync\Client\ClientFactory $client_factory
   *   The client factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\entity_sync\Import\FieldManagerInterface $field_manager
   *   The Entity Sync import field manager.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger to pass to the client.
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The config entity loader.
   * phpcs:disable
   * @I Use \Psr\Log\LoggerInterface
   * phpcs:enable
   */
  public function __construct(
    ClientFactory $client_factory,
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EventDispatcherInterface $event_dispatcher,
    FieldManagerInterface $field_manager,
    LoggerChannelInterface $logger,
    ConfigEntityLoaderInterface $config_entity_loader
  ) {
    $this->logger = $logger;
    $this->eventDispatcher = $event_dispatcher;
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->fieldManager = $field_manager;
    $this->clientFactory = $client_factory;
    $this->configEntityLoader = $config_entity_loader;
  }

  /**
   * {@inheritdoc}
   */
  public function importRemoteList(
    $sync_id,
    array $filters = [],
    array $options = []
  ) {
    // Load the sync.
    // phpcs:disable
    // @I Validate the sync/operation configuration
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the provider supports the `import_list` operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // phpcs:enable
    $sync = $this->configEntityLoader->load($sync_id);

    // Make sure the operation is enabled and supported by the provider.
    // phpcs:disable
    // @I Consider throwing an exception if unsupported operations are run
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, error-handling
    // phpcs:enable
    if (!$this->operationSupported($sync, 'import_list')) {
      $this->logger->error(
        sprintf(
          'The synchronization with ID "%s" and/or its provider do not support the `import_list` operation.',
          $sync_id
        )
      );
      return;
    }

    // phpcs:disable
    // @I Consider always adding the filters/options to the context
    //    type     : improvement
    //    priority : normal
    //    labels   : context, import, operation
    // phpcs:enable
    $context = $options['context'] ?? [];

    // Notify subscribers that the operation is about to be initiated.
    // phpcs:disable
    // @I Write tests for operation cancellations
    //    type     : task
    //    priority : high
    //    labels   : import, testing
    // phpcs:enable
    $cancel = $this->preInitiate(
      Events::REMOTE_LIST_PRE_INITIATE,
      'import_list',
      $context,
      $sync
    );
    if ($cancel) {
      return;
    }

    // Run the operation.
    // We do this in a `try/finally` structure so that we can still dispatch the
    // post-terminate event. Subscribers may still need to run whether the
    // operation was successfull or not. For example, even if a managed
    // operation failed we unlock it so that the next one is allowed to run.
    // At the end, the error/exception is still thrown so that the caller can
    // handle it as required.
    // phpcs:disable
    // @I Test that post-terminate event is dispatched in case of errors
    //    type     : task
    //    priority : normal
    //    labels   : event, import, testing
    // phpcs:enable
    try {
      $this->doImportRemoteList($sync, $filters, $options, $context);
    }
    finally {
      // Notify subscribers that the operation has terminated.
      $this->postTerminate(
        Events::REMOTE_LIST_POST_TERMINATE,
        'import_list',
        $context,
        $sync
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function importRemoteEntityById(
    $sync_id,
    $remote_entity_id,
    array $options = []
  ) {
    // Load the sync.
    // phpcs:disable
    // @I Validate the sync/operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the provider supports the `import_entity` operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // phpcs:enable
    $sync = $this->configEntityLoader->load($sync_id);

    // Make sure the operation is enabled and supported by the provider.
    if (!$this->operationSupported($sync, 'import_entity')) {
      $this->logger->error(
        sprintf(
          'The synchronization with ID "%s" and/or its provider do not support the `import_entity` operation.',
          $sync_id
        )
      );
      return;
    }

    // phpcs:disable
    // @I Support client configuration overrides as with list imports
    //    type     : improvement
    //    priority : normal
    //    labels   : import, operation, sync
    // phpcs:enable
    $remote_entity = $this->clientFactory
      ->get($sync->id())
      ->importEntity($remote_entity_id);

    // The client should be throwing an 404 Exception if no remote entity with
    // the given ID is found. However, let's throw an exception here to prevent
    // errors in the case the client does not do that.
    if (!$remote_entity) {
      throw new EntityImportException(
        sprintf(
          'No remote entity with ID "%s" was found.',
          $remote_entity_id
        )
      );
    }

    $this->wrapDoImportRemoteEntity($sync, $remote_entity, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function importRemoteEntity(
    $sync_id,
    $remote_entity,
    array $options = []
  ) {
    // Load the sync.
    // phpcs:disable
    // @I Validate the sync/operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the provider supports the `import_entity` operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // phpcs:enable
    $sync = $this->configEntityLoader->load($sync_id);

    // Make sure the operation is enabled and supported by the provider.
    if (!$this->operationSupported($sync, 'import_entity')) {
      $this->logger->error(
        sprintf(
          'The synchronization with ID "%s" and/or its provider do not support the `import_entity` operation.',
          $sync_id
        )
      );
      return;
    }

    $this->wrapDoImportRemoteEntity($sync, $remote_entity, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function importLocalEntity(
    $sync_id,
    ContentEntityInterface $local_entity,
    array $options = []
  ) {
    // Load the sync.
    // phpcs:disable
    // @I Validate the sync/operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the provider supports the `import_entity` operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // phpcs:enable
    $sync = $this->configEntityLoader->load($sync_id);

    // Make sure the operation is enabled and supported by the provider.
    if (!$this->operationSupported($sync, 'import_entity')) {
      $this->logger->error(
        sprintf(
          'The synchronization with ID "%s" and/or its provider do not support the `import_entity` operation.',
          $sync_id
        )
      );
      return;
    }

    $context = $options['context'] ?? [];

    // Notify subscribers that the operation is about to be initiated.
    // phpcs:disable
    // @I Write tests for operation cancellations
    //    type     : task
    //    priority : high
    //    labels   : import, testing
    // phpcs:enable
    $cancel = $this->preInitiate(
      Events::LOCAL_ENTITY_PRE_INITIATE,
      'import_entity',
      $context,
      $sync
    );
    if ($cancel) {
      return;
    }

    // Run the operation.
    // We do this in a `try/finally` structure so that we can still dispatch the
    // post-terminate event. Subscribers may still need to run whether the
    // operation was successfull or not. For example, even if a managed
    // operation failed we unlock it so that the next one is allowed to run.
    // At the end, the error/exception is still thrown so that the caller can
    // handle it as required.
    try {
      $this->doImportLocalEntity($sync, $local_entity, $options, $context);
    }
    finally {
      // Notify subscribers that the operation has terminated.
      $this->postTerminate(
        Events::LOCAL_ENTITY_POST_TERMINATE,
        'import_entity',
        $context,
        $sync
      );
    }
  }

  /**
   * Runs the actual remote entity import operation.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $filters
   *   An associative array of filters that determine which entities will be
   *   imported. For supported filters see
   *   \Drupal\entity_sync\Import\ManagerInterface::importRemoteList().
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see
   *   \Drupal\entity_sync\Import\ManagerInterface::importRemoteList().
   * @param array $context
   *   An associative array containing of context related to the circumstances
   *     of the operation. See
   *     \Drupal\entity_sync\Import\ManagerInterface::importRemoteList().
   */
  protected function doImportRemoteList(
    SyncInterface $sync,
    array $filters,
    array $options,
    array $context
  ) {
    // Initiate the operation.
    $this->initiate(
      Events::REMOTE_LIST_INITIATE,
      'import_list',
      $context,
      $sync
    );

    // Build the filters and the options that will be passed to the client for
    // fetching the list of entities.
    $filters = $this->remoteListFilters($filters, $context, $sync);
    $client_options = $options['client'] ?? [];
    $client_config = $options['client_config'] ?? [];
    // Keep the same default.
    $recursive_overrides = $options['client_config_recursive'] ?? FALSE;

    // Now, use the remote client to fetch the list of entities.
    $entities = $this->clientFactory
      ->get($sync->id(), $client_config, $recursive_overrides)
      ->importList($filters, $client_options);

    // Go through each entity and create or update the corresponding local
    // entity.
    if ($entities) {
      [$entity_count, $iterator_end] = $this->doubleIteratorApply(
        $entities,
        [$this, 'tryWrapDoImportRemoteEntity'],
        $filters['limit'] ?? ($options['limit'] ?? NULL),
        $sync,
        ['parent_operation' => 'import_remote_list'] + $options,
        'import_list'
      );
    }

    // Terminate the operation.
    // phpcs:disable
    // @I Write tests that the terminate event runs when there are no entities
    //    type     : task
    //    priority : low
    //    labels   : event, import, testing
    // @I Make local entities available to import remote list terminate event
    //    type     : improvement
    //    priority : low
    //    labels   : event, import
    // phpcs:enable
    $this->terminate(
      Events::REMOTE_LIST_TERMINATE,
      'import_list',
      $context,
      $sync,
      [
        'remote_entities' => $entities,
        'entity_count' => $entity_count,
        'iterator_end' => $iterator_end,
      ]
    );
  }

  /**
   * Runs the actual remote entity import operation with its pre/post events.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param object $remote_entity
   *   The remote entity.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see
   *   \Drupal\entity_sync\Import\ManagerInterface::importRemoteEntity().
   */
  protected function wrapDoImportRemoteEntity(
    SyncInterface $sync,
    $remote_entity,
    array $options
  ) {
    $remote_entity = $this->serializeRemoteEntity($sync, $remote_entity);

    $context = $options['context'] ?? [];
    $context['remote_entity'] = $remote_entity;

    // Notify subscribers that the operation is about to be initiated.
    // phpcs:disable
    // @I Write tests for operation cancellations
    //    type     : task
    //    priority : high
    //    labels   : import, testing
    // phpcs:enable
    $cancel = $this->preInitiate(
      Events::REMOTE_ENTITY_PRE_INITIATE,
      'import_entity',
      $context,
      $sync
    );
    if ($cancel) {
      return;
    }

    // Run the operation.
    // We do this in a `try/finally` structure so that we can still dispatch the
    // post-terminate event. Subscribers may still need to run whether the
    // operation was successfull or not. For example, even if a managed
    // operation failed we unlock it so that the next one is allowed to run.
    // At the end, the error/exception is still thrown so that the caller can
    // handle it as required.
    try {
      $data = $this->doImportRemoteEntity(
        $sync,
        $remote_entity,
        $options,
        $context
      );
    }
    finally {
      // Notify subscribers that the operation has terminated.
      $this->postTerminate(
        Events::REMOTE_ENTITY_POST_TERMINATE,
        'import_entity',
        $context,
        $sync,
        $data ?? []
      );
    }
  }

  /**
   * Runs the actual remote entity import operation.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param object $remote_entity
   *   The remote entity.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see
   *   \Drupal\entity_sync\Import\ManagerInterface::importRemoteEntity().
   * @param array $context
   *   An associative array containing of context related to the circumstances
   *     of the operation. See
   *     \Drupal\entity_sync\Import\ManagerInterface::importRemoteEntity().
   *
   * @return array|null
   *   An associative array containing data related to the operation. For
   *   supported array items see `$this::createOrUpdate()`.
   */
  protected function doImportRemoteEntity(
    SyncInterface $sync,
    $remote_entity,
    array $options,
    array $context
  ) {
    // Initiate the operation.
    $this->initiate(
      Events::REMOTE_ENTITY_INITIATE,
      'import_entity',
      $context,
      $sync
    );

    // Run the operation, create or update.
    $data = $this->createOrUpdate($remote_entity, $sync, $options);

    // Terminate the operation.
    // Add to the context the local entity that was imported.
    $this->terminate(
      Events::REMOTE_ENTITY_TERMINATE,
      'import_entity',
      $context,
      $sync,
      $data ?? []
    );

    return $data;
  }

  /**
   * Runs the actual local entity import operation.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see
   *   \Drupal\entity_sync\Import\ManagerInterface::importLocalEntity().
   * @param array $context
   *   An associative array containing of context related to the circumstances
   *     of the operation. See
   *     \Drupal\entity_sync\Import\ManagerInterface::importLocalEntity().
   */
  protected function doImportLocalEntity(
    SyncInterface $sync,
    ContentEntityInterface $local_entity,
    array $options,
    array $context
  ) {
    // Initiate the operation.
    $this->initiate(
      Events::LOCAL_ENTITY_INITIATE,
      'import_entity',
      $context,
      $sync
    );

    // Build the entity mapping for this local entity.
    $entity_mapping = $this->localEntityMapping($local_entity, $sync);
    if (!$entity_mapping) {
      return;
    }

    // Skip importing the remote entity if we are explicitly told to do so.
    if ($entity_mapping['action'] === ManagerInterface::ACTION_SKIP) {
      return;
    }
    elseif ($entity_mapping['action'] !== ManagerInterface::ACTION_IMPORT) {
      throw new \RuntimeException(
        sprintf(
          'Unsupported entity mapping action "%s"',
          $entity_mapping['action']
        )
      );
    }

    // Now, use the remote client to fetch the remote entity for this ID.
    $remote_entity = $this->clientFactory
      ->getByClientConfig($entity_mapping['client'])
      ->importEntity($entity_mapping['entity_id']);

    // Finally, update the entity.
    $this->wrapDoImportRemoteEntity(
      $sync,
      $remote_entity,
      ['parent_operation' => 'import_local_entity'] + $options
    );

    // Terminate the operation.
    // Add to the context the local entity that was imported.
    //
    // phpcs:disable
    // @I Pass local entity to all events via context
    //    type     : improvement
    //    priority : normal
    //    labels   : event, import
    //
    // @I Pass remote entity to terminate events via data
    //    type     : improvement
    //    priority : normal
    //    labels   : event, import
    //
    // @I Write tests that the entities are passed to events as required
    //    type     : task
    //    priority : low
    //    labels   : event, import, testing
    // phpcs:enable
    $this->terminate(
      Events::LOCAL_ENTITY_TERMINATE,
      'import_entity',
      $context + ['local_entity' => $local_entity],
      $sync
    );
  }

  /**
   * Imports the changes without halting execution if an exception is thrown.
   *
   * An error is logged instead; the caller may then continue with import the
   * next entity, if there is one.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param string $operation
   *   The operation that is doing the import; used for logging purposes.
   */
  protected function tryWrapDoImportRemoteEntity(
    $remote_entity,
    SyncInterface $sync,
    array $options,
    $operation
  ) {
    try {
      $this->wrapDoImportRemoteEntity($sync, $remote_entity, $options);
    }
    catch (\Exception $e) {
      // In the case of an error the entity here is unserialized. Serialize it
      // so that we properly log it's ID.
      $remote_entity = $this->serializeRemoteEntity($sync, $remote_entity);

      $id_field = $sync->getRemoteResourceSettings()['id_field'] ?? NULL;
      $this->logger->error(
        sprintf(
          'An "%s" exception was thrown while importing the remote entity with ID "%s" as part of the "%s" synchronization and the "%s" operation. The error message was: %s',
          get_class($e),
          $id_field ? ($remote_entity->{$id_field} ?? '') : '',
          $sync->id(),
          $operation,
          $e->getMessage()
        )
      );
    }
  }

  /**
   * Import the changes contained in the given remote entity to a local entity.
   *
   * If an associated local entity is identified, the local entity will be
   * updated. A new local entity will be created otherwise.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import.
   *
   * @return array|null
   *   An associative array containing data related to the operation. Currently:
   *   - action: The action (skip/create/update) that was selected by the entity
   *     mapping. Note that it is possible that the action was not actually
   *     executed if the related setting in the synchronization configuration is
   *     set to FALSE e.g. create_entities.
   *   - local_entity: The local entity that was created or updated as a result
   *     of the operation. Note that it can be NULL in the case mentioned above.
   */
  protected function createOrUpdate(
    $remote_entity,
    SyncInterface $sync,
    array $options
  ) {
    // The remote entity is expected in object format. We don't enforce that in
    // the function declaration because we want to throw an
    // `EntityImportException` exception.
    if (!is_object($remote_entity)) {
      throw new EntityImportException(
        sprintf(
          'Expecting the remote entity to be an object, %s given.',
          gettype($remote_entity)
        )
      );
    }

    // Build the entity mapping for this remote entity.
    $entity_mapping = $this->remoteEntityMapping($remote_entity, $sync);

    // If the entity mapping is empty we will not be updating or creating a
    // local entity; nothing to do. Still, event subscribers may require an
    // action so we set it to skip.
    if (!$entity_mapping) {
      return ['action' => ManagerInterface::ACTION_SKIP];
    }

    $data = ['action' => $entity_mapping['action']];

    // Skip updating the local entity if we are explicitly told to do so.
    if ($entity_mapping['action'] === ManagerInterface::ACTION_SKIP) {
      return $data;
    }
    elseif ($entity_mapping['action'] === ManagerInterface::ACTION_CREATE) {
      $data['local_entity'] = $this->create(
        $remote_entity,
        $sync,
        $entity_mapping,
        $options
      );
      return $data;
    }
    elseif ($entity_mapping['action'] === ManagerInterface::ACTION_UPDATE) {
      $data['local_entity'] = $this->update(
        $remote_entity,
        $sync,
        $entity_mapping,
        $options
      );
      return $data;
    }
    else {
      throw new \RuntimeException(
        sprintf(
          'Unsupported entity mapping action "%s"',
          $entity_mapping['action']
        )
      );
    }
  }

  /**
   * Import the changes from the given remote entity to a new local entity.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $entity_mapping
   *   An associative array containing information about the local entity being
   *   mapped to the given remote entity.
   *   See \Drupal\entity_sync\Event\RemoteEntityMapping::entityMapping.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity that was created, or NULL if the synchronization configuration
   *   is set to not create new entities.
   * phpcs:disable
   * @I Support entity creation validation
   *    type     : bug
   *    priority : normal
   *    labels   : import, validation
   * phpcs:enable
   */
  protected function create(
    object $remote_entity,
    SyncInterface $sync,
    array $entity_mapping,
    array $options
  ) {
    $create_values = [];

    // If the entity type has bundles, we need to be provided the bundle that
    // will be used to create the entity.
    $entity_type = $this->entityTypeManager
      ->getDefinition($entity_mapping['entity_type_id']);

    $is_bundleable = $entity_type->getBundleEntityType() ? TRUE : FALSE;
    if ($is_bundleable && empty($entity_mapping['entity_bundle'])) {
      throw new EntityImportException(
        sprintf(
          'A bundle needs to be provided for creating an entity of type "%s".',
          $entity_mapping['entity_type_id']
        )
      );
    }

    if ($is_bundleable) {
      $create_values = [
        $entity_type->getKey('bundle') => $entity_mapping['entity_bundle'],
      ];
    }

    $local_entity = $this->entityTypeManager
      ->getStorage($entity_mapping['entity_type_id'])
      ->create($create_values);

    return $this->doImportEntity(
      $remote_entity,
      $local_entity,
      $sync,
      $options
    );
  }

  /**
   * Import the changes from the given remote entity to the local entity.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $entity_mapping
   *   An associative array containing information about the local entity being
   *   mapped to the given remote entity.
   *   See \Drupal\entity_sync\Event\RemoteEntityMapping::entityMapping.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity that was updated.
   * phpcs:disable
   * @I Support entity update validation
   *    type     : bug
   *    priority : normal
   *    labels   : import, validation
   * @I Check if the changes have already been imported
   *    type     : improvement
   *    priority : normal
   *    labels   : import, validation
   * phpcs:enable
   */
  protected function update(
    object $remote_entity,
    SyncInterface $sync,
    array $entity_mapping,
    array $options
  ) {
    // Load the local entity that this remote entity is associated with.
    // phpcs:disable
    // @I Validate that the local entity is of the expected bundle
    //    type     : task
    //    priority : low
    //    labels   : import, validation
    //    notes    : The synchronization configuration should allow bypassing
    //               bundle validation.
    // phpcs:enable
    $local_entity = $this->entityTypeManager
      ->getStorage($entity_mapping['entity_type_id'])
      ->load($entity_mapping['id']);

    if (!$local_entity) {
      // phpcs:disable
      // @I Add more details about the remote entity in the exception message
      //    type     : task
      //    priority : low
      //    labels   : error-handling, import
      // phpcs:enable
      throw new \RuntimeException(
        sprintf(
          'A non-existing local entity of type "%s" and ID "%s" was requested to be mapped to a remote entity.',
          $entity_mapping['entity_type_id'],
          $entity_mapping['id']
        )
      );
    }

    return $this->doImportEntity(
      $remote_entity,
      $local_entity,
      $sync,
      $options
    );
  }

  /**
   * Performs the actual import of a remote entity to a local entity.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The entity that was created or updated.
   */
  protected function doImportEntity(
    object $remote_entity,
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $options
  ) {
    // phpcs:disable
    // @I Pass context to the field manager
    //    type     : improvement
    //    priority : normal
    //    labels   : field, import
    // phpcs:enable
    $this->fieldManager->import($remote_entity, $local_entity, $sync, $options);

    // If no errors occurred (the field manager would throw an exception),
    // proceed with saving the entity.
    //
    // phpcs:disable
    // @I Provide a mechanism to track whether an entity has changed
    //     type     : improvement
    //     priority : normal
    //     labels   : entity, import
    // phpcs:enable
    $local_entity->save();

    return $local_entity;
  }

  /**
   * Builds and returns the entity mapping for the given remote entity.
   *
   * The entity mapping defines if and which local entity will be updated with
   * the data contained in the given remote entity. The default mapping
   * identifies the local entity based on an entity field containing the remote
   * entity's ID.
   *
   * An event is dispatched that allows subscribers to map the remote entity to
   * a different local entity, or to decide to not import it at all.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   *
   * @return array
   *   The final entity mapping.
   * phpcs:disable
   * @I Validate the final entity mapping
   *    type     : bug
   *    priority : normal
   *    labels   : import, mapping, validation
   * phpcs:enable
   */
  protected function remoteEntityMapping(
    object $remote_entity,
    SyncInterface $sync
  ) {
    $event = new RemoteEntityMappingEvent($remote_entity, $sync);
    $this->eventDispatcher->dispatch($event, Events::REMOTE_ENTITY_MAPPING);

    // Return the final mapping.
    $entity_mapping = $event->getEntityMapping();
    if (!$entity_mapping) {
      return [];
    }

    // phpcs:disable
    // @I Provide defaults for settings not explicitly set
    //    type     : improvement
    //    priority : low
    //    labels   : config
    // @I Respect the `create/update_entities` setting of the running operation
    //    type     : bug
    //    priority : normal
    //    labels   : config, import
    //    notes    : We may be in an `import_list` or `import_entity` operation.
    // phpcs:enable
    $settings = $sync->getOperationsSettings();
    if ($entity_mapping['action'] === ManagerInterface::ACTION_CREATE) {
      if (empty($settings['import_list']['create_entities'])) {
        $entity_mapping['action'] = ManagerInterface::ACTION_SKIP;
      }
    }
    elseif ($entity_mapping['action'] === ManagerInterface::ACTION_UPDATE) {
      if (empty($settings['import_list']['update_entities'])) {
        $entity_mapping['action'] = ManagerInterface::ACTION_SKIP;
      }
    }

    return $entity_mapping;
  }

  /**
   * Builds and returns the remote ID for the given local entity.
   *
   * The local entity mapping defines if and which remote entity will be
   * imported for the given local entity. The default mapping identifies the
   * remote entity based on a local entity field containing the remote
   * entity's ID.
   *
   * An event is dispatched that allows subscribers to map the local entity to a
   * different remote entity, or to decide to not import it at all.
   *
   * @param \Drupal\core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   *
   * @return array
   *   The final entity mapping.
   * phpcs:disable
   * @I Validate the final local entity mapping
   *    type     : bug
   *    priority : normal
   *    labels   : import, mapping, validation
   * phpcs:enable
   */
  protected function localEntityMapping(
    ContentEntityInterface $local_entity,
    SyncInterface $sync
  ) {
    $event = new LocalEntityMappingEvent(
      $local_entity,
      $sync
    );
    $this->eventDispatcher->dispatch($event, Events::LOCAL_ENTITY_MAPPING);

    // Return the final mapping.
    return $event->getEntityMapping();
  }

  /**
   * Builds and returns the filters for importing a remote list of entities.
   *
   * An event is dispatched that allows subscribers to alter the filters that
   * determine which entities will be fetched from the remote resource.
   *
   * @param array $filters
   *   The current filters.
   * @param array $context
   *   The context of the operation we are currently executing.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   *
   * @return array
   *   The final filters.
   */
  protected function remoteListFilters(
    array $filters,
    array $context,
    SyncInterface $sync
  ) {
    $event = new ListFiltersEvent(
      $filters,
      $context,
      $sync
    );
    $this->eventDispatcher->dispatch($event, Events::REMOTE_LIST_FILTERS);

    // Return the final filters.
    return $event->getFilters();
  }

  /**
   * Apply a callback to all items within an iterator.
   *
   * The callback needs to accept the item as its first argument.
   *
   * If the items of the iterator are iterators themselves, the callback is
   * applied to the items in the inner iterator. This is used to support paging;
   * the outer iterator contains pages and each page is an iterator that
   * contains the items.
   *
   * If a limit is provided, applying the callback will simply stop when we
   * reach the limit; otherwise, all items contained in the iterator(s) will be
   * processed.
   *
   * The client adapter that provides the iterator is responsible for providing
   * it with the pointer in the position from where the import will start. This
   * will usually be the beginning of the list, but it could be otherwise.
   *
   * @param \Iterator $iterator
   *   The iterator that contains the items.
   * @param callable $callback
   *   The callback to apply to the items.
   * @param int $limit
   *   The maximum number of items to apply the callback to, or NULL for no
   *   limit.
   * @param mixed $args
   *   The arguments to pass to the callback after the item.
   *
   * @return array
   *   A numerical array containing the following values in the given order:
   *   - The number of processed entities.
   *   - TRUE if the iterator reach its end, FALSE if it stopped before reaching
   *     its end due to hitting the limit.
   * phpcs:disable
   * @I Review and implement logging strategy for `info` and `debug` levels
   *    type     : feature
   *    priority : normal
   *    labels   : logging
   * @I Write unit tests for the `doubleIteratorApply` function
   *    type     : task
   *    priority : high
   *    labels   : testing
   * @I Write test for limiting the number of entities to import
   *    type     : task
   *    priority : normal
   *    labels   : testing
   * phpcs:enable
   */
  protected function doubleIteratorApply(
    \Iterator $iterator,
    callable $callback,
    $limit = NULL,
    ...$args
  ) {
    // If the current iterator pointer is not valid the iterator is at its end
    // already i.e. the iterator is empty or it was passed with the pointer past
    // its end. Otherwise we may have entities and we should continue at the
    // next run where the limit might be different.
    if ($limit === 0) {
      return [0, !$iterator->valid()];
    }
    if (!$iterator->valid()) {
      return [0, TRUE];
    }

    $counter = 0;
    $iterator_end = FALSE;

    // We don't use a `foreach` to loop over the iterator because it rewinds. We
    // want to process entities from the current pointer. The client adapter may
    // be intentionally setting the pointer so that the import continues from
    // where the previous import stopped. This is the case, for example, with
    // state-managed imports that define a limit filter.
    // Also, we need to be detecting whether we have reached the end of the
    // iterator. Iterators are not necessarily countable and we can only know if
    // we reached the end after moving to the next pointer - which happens
    // beneath the scenes in `foreach` loops.
    do {
      $item = $iterator->current();

      // If the item is an iterator, we must have a page containing the
      // items. Recursively call this method to iterate over the items.
      if ($item instanceof \Iterator) {
        [$inner_counter, $inner_iterator_end] = call_user_func_array(
          [$this, 'doubleIteratorApply'],
          array_merge(
            [
              $item,
              $callback,
              $limit === NULL ? NULL : $limit - $counter,
            ],
            $args
          )
        );
        $counter += $inner_counter;
      }
      // Otherwise we have the item (remote entity) already.
      else {
        call_user_func_array($callback, array_merge([$item], $args));
        $counter++;
      }

      $iterator->next();
      $valid = $iterator->valid();
      if (!$valid) {
        $iterator_end = TRUE && ($inner_iterator_end ?? TRUE);
      }

      if ($counter === $limit) {
        break;
      }
    } while ($valid);

    return [$counter, $iterator_end];
  }

  /**
   * Serializes the remote entity object, if requested in configuration.
   *
   * PHP SDKs might provide model classes for objects and they return the
   * results in such format. Many features provided by Entity Synchronization
   * rely on the object being converted a standard PHP object i.e. `\stdClass`.
   * The client adapter might do the serialization if needed, but it is more
   * efficient to do it here to prevent having to loop through all results just
   * for serializing them.
   *
   * Also, if the results are provided as a double iterator, such as in cases of
   * browsing the results in pages where the actual result collection (e.g. API
   * requests) happen by the iterator, serialization at the client adapter level
   * is complicated.
   *
   * We therefore provide the option to do the serialization here.
   *
   * Currently supported serialization types are:
   * - Call a method on the object itself that returns the serialized object.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param object $remote_entity
   *   The remote entity as provided by the client adapter.
   *
   * @return object
   *   The serialized object.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When serialization is not properly defined in configuration.
   * @throws \RuntimeException
   *   When serialization via an object method is requested but the method does
   *   not exist on the remote entity object.
   * phpcs:disable
   * @I Support serialization in individual entity imports
   *    type     : improvement
   *    priority : normal
   *    labels   : import
   * phpcs:enable
   */
  protected function serializeRemoteEntity(
    SyncInterface $sync,
    object $remote_entity
  ) {
    // We currently only support serialization in list imports.
    $settings = $sync->getOperationsSettings();
    $settings = $settings['import_list']['serialization'] ?? NULL;
    if ($settings === NULL) {
      return $remote_entity;
    }

    // Only serialize if the option is explicitly set to `TRUE`. By default,
    // the client is expected to return the objects in the right format.
    if (($settings['status'] ?? FALSE) !== TRUE) {
      return $remote_entity;
    }

    $type = $settings['type'] ?? NULL;
    $method = $settings['method'] ?? NULL;

    $this->validateSerializationSettings(
      $remote_entity,
      $type,
      $method
    );

    if ($method) {
      $remote_data = $remote_entity->{$method}();
    }

    switch ($type) {
      case 'array_to_object':
        return (object) $remote_data;

      case 'json_decode':
        return json_decode($remote_data);

      case 'object_method':
        return $remote_data;
    }
  }

  /**
   * Validates the serialization settings.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param string|null $type
   *   The requested serialization type, or `NULL` if none given.
   * @param string|null $method
   *   The requested object method, or `NULL` if none given.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the given type/method combination is invalid.
   * @throws \RuntimeException
   *   When the requested method does not exist on the remote entity object.
   */
  protected function validateSerializationSettings(
    object $remote_entity,
    string $type = NULL,
    string $method = NULL
  ) {
    $supported_types = [
      'array_to_object',
      'json_decode',
      'object_method',
    ];
    if (!in_array($type, $supported_types)) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown serialization type "%s"',
        (string) $type
      ));
    }

    if ($type === 'object_method' && $method === NULL) {
      throw new InvalidConfigurationException(
        'Serialization object method not defined'
      );
    }

    if ($method !== NULL && !method_exists($remote_entity, $method)) {
      throw new \RuntimeException(
        'The serialization method does not exist on the remote entity object'
      );
    }
  }

}
