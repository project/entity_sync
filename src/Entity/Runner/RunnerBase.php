<?php

namespace Drupal\entity_sync\Entity\Runner;

use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\Entity\OperationInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

use Psr\Log\LoggerInterface;

/**
 * Base class for runnners.
 *
 * All operations have the same workflow by default. We provide here code that
 * wraps the operation execution and handles state transitions.
 */
abstract class RunnerBase implements RunnerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module's logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Runs the operation.
   *
   * The `run` method handles state transitions. Child classes must define the
   * actual execution of the operation by implementing this method.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation to run.
   * @param array $context
   *   An associative array containing additional context that the runner may
   *   need for running the operation. Supported options should be documented by
   *   the runner.
   */
  abstract protected function doRun(
    OperationInterface $operation,
    array $context
  );

  /**
   * Constructs a new RunnerBase object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function run(OperationInterface $operation, array $context = []) {
    $transition_id = 'complete';
    try {
      $this->doRun($operation, $context);
    }
    catch (\Throwable $throwable) {
      $transition_id = 'fail';
      $this->logger->error(
        'Operation with ID "@operation_id" failed to run: @throwable @message',
        [
          '@operation_id' => $operation->id(),
          '@throwable' => get_class($throwable),
          '@message' => $throwable->getMessage(),
        ]
      );
    }

    $operation
      ->get(OperationField::STATE)
      ->first()
      ->applyTransitionById($transition_id);
    $this->entityTypeManager
      ->getStorage('entity_sync_operation')
      ->save($operation);
  }

}
