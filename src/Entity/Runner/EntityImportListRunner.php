<?php

namespace Drupal\entity_sync\Entity\Runner;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Import\ManagerInterface as ImportManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Runner for operation types that imports lists of entities.
 *
 * phpcs:disable
 * @I Add runner for exporting lists of entities
 *    type     : improvement
 *    priority : normal
 *    labels   : runner
 * phpcs:enable
 */
class EntityImportListRunner extends RunnerBase {

  /**
   * The Entity Synchronization entity import manager.
   *
   * @var \Drupal\entity_sync\Import\ManagerInterface
   */
  protected $importManager;

  /**
   * Constructs a new EntityImportListRunner object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   * @param \Drupal\entity_sync\Import\ManagerInterface $import_manager
   *   The Entity Synchronization entity import manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    ImportManagerInterface $import_manager
  ) {
    parent::__construct($entity_type_manager, $logger);

    $this->importManager = $import_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Supported context options are:
   * - filters (array, optional) An array containing the filters to pass to the
   *   import manager.
   * - options (array) An associative array with options to pass to the import
   *   manager.
   *
   * @see \Drupal\entity_sync\Import\ManagerInterface::importRemoteList()
   */
  protected function doRun(
    OperationInterface $operation,
    array $context
  ): void {
    $this->importManager->importRemoteList(
      $operation->bundle(),
      $context['filters'] ?? [],
      $context['options'] ?? []
    );
  }

}
