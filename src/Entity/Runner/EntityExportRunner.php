<?php

namespace Drupal\entity_sync\Entity\Runner;

use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Export\EntityManagerInterface as ExportManagerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Runner for operation types that exports individual entities.
 *
 * phpcs:disable
 * @I Add runner for importing individual entities
 *    type     : improvement
 *    priority : normal
 *    labels   : runner
 * phpcs:enable
 */
class EntityExportRunner extends RunnerBase {

  /**
   * The Entity Synchronization entity export manager.
   *
   * @var \Drupal\entity_sync\Export\EntityManagerInterface
   */
  protected $exportManager;

  /**
   * Constructs a new EntityExportRunner object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger channel.
   * @param \Drupal\entity_sync\Export\EntityManagerInterface $export_manager
   *   The Entity Synchronization entity export manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    ExportManagerInterface $export_manager
  ) {
    parent::__construct($entity_type_manager, $logger);

    $this->exportManager = $export_manager;
  }

  /**
   * {@inheritdoc}
   *
   * Supported context options are:
   * - options (array) An associative array with options to pass to the export
   *   manager. The operation entity is passed in the context automatically.
   *
   * @see \Drupal\entity_sync\Export\ManagerInterface::exportLocalEntity()
   */
  protected function doRun(
    OperationInterface $operation,
    array $context
  ) {
    $options = $context['options'] ?? [];
    if (!isset($options['context'])) {
      $options['context'] = [];
    }
    $options['context']['operation_entity'] = $operation;

    $this->exportManager->exportLocalEntity(
      $operation->bundle(),
      $operation->get(OperationField::ENTITY)->entity,
      $options
    );
  }

}
