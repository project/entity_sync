<?php

namespace Drupal\entity_sync\Entity\Runner;

use Drupal\entity_sync\Entity\OperationInterface;

/**
 * Defines the interface for operation runners.
 *
 * An operation runner is an object that can run i.e. execute a given
 * operation. Before introducing the runners, an application would have to write
 * code for running the operation e.g. getting the import/export manager,
 * preparing its arguments, and calling the appropriate method. That code would
 * have to be repeated for different synchronizations even when their providers
 * and the circumstances that the operations would be run would be the same.
 *
 * Now, operation configurator plugins can implement the
 * `\Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface` to define
 * their runners. That can make it easier to automatically run operations of
 * different types, for example, from the UI using a state transition form
 * without having to write custom code for each operation type.
 */
interface RunnerInterface {

  /**
   * Runs the given operation.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation to run.
   * @param array $context
   *   Additional context that the runner may need for running the operation.
   */
  public function run(OperationInterface $operation, array $context = []);

}
