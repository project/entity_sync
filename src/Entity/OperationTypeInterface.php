<?php

namespace Drupal\entity_sync\Entity;

use Drupal\entity_sync\OperationConfigurator\PluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for synchronization operation types.
 *
 * We use a mix of config entities and plugins to define the behavior of
 * operations. The standard config entities (operation types) allow for standard
 * bundle functionality e.g. fields, certain configuration etc., while the
 * operator configurator plugins allow developers to create certain
 * configuration, fields and behavior determined in the code that can be used in
 * different bundles.
 */
interface OperationTypeInterface extends
  ConfigEntityInterface,
  EntityDescriptionInterface,
  SyncInterface {

  /**
   * Gets the ID of the operation configurator plugin.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId();

  /**
   * Sets the ID of the operation configurator plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return $this
   */
  public function setPluginId(string $plugin_id);

  /**
   * Gets the operation configurator plugin.
   *
   * @return \Drupal\entity_sync\OperationConfigurator\PluginInterface
   *   The plugin.
   */
  public function getPlugin();

  /**
   * Sets the operation configurator plugin.
   *
   * It also updates the plugin configuration stored in the operation type.
   *
   * @param \Drupal\entity_sync\OperationConfigurator\PluginInterface $plugin
   *   The plugin.
   */
  public function setPlugin(PluginInterface $plugin);

  /**
   * Gets the operation configurator plugin configuration.
   *
   * @return array
   *   The plugin configuration.
   * phpcs:disable
   * @I Should return the plugin config and not that of the operation type
   *    type     : bug
   *    priority : high
   *    labels   : backwards-breaking, config, operation-type
   *    notes    : Will need to address this in a major release as it breaks
   *               existing code.
   * phpcs:enable
   */
  public function getPluginConfiguration();

}
