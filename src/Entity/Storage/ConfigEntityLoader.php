<?php

namespace Drupal\entity_sync\Entity\Storage;

use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\MachineName\Entity\Type as EntityType;
use Drupal\entity_sync\OperationConfigurator\PluginManagerInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The default implementation of the configuration entity loader.
 */
class ConfigEntityLoader implements ConfigEntityLoaderInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The operation configurator plugin manager.
   *
   * @var \Drupal\entity_sync\OperationConfigurator\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new ConfigEntityLoader object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_sync\OperationConfigurator\PluginManagerInterface $plugin_manager
   *   The operation configurator plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    PluginManagerInterface $plugin_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   * phpcs:disable
   * @I Cache merged synchronization entities
   *    type     : task
   *    priority : normal
   *    labels   : performance, sync-inheritance
   *    notes    : The entity cache system would cache the entity as it exists
   *               in the database i.e. with missing settings. We should look
   *               what are the options for caching the final configuration
   *               entity that includes the inherited settings e.g. cache it in
   *               the core entity cache, statically cache it here etc.
   * phpcs:enable
   */
  public function load(string $sync_id) {
    $sync = $this->entityTypeManager
      ->getStorage(EntityType::SYNC)
      ->load($sync_id);
    if ($sync === NULL) {
      $sync = $this->entityTypeManager
        ->getStorage(EntityType::OPERATION_TYPE)
        ->load($sync_id);
    }

    if ($sync === NULL) {
      return NULL;
    }

    $sync = $this->mergeParentSync($sync);

    // The settings are set in the plugin configuration when the plugin is
    // instantiated. We therefore need to instantiate the plugin after we merge
    // the parent settings. To not have to instantiate the plugin twice, we
    // don't use the `loadWithPluginInstantiated` when loading the entity from
    // storage above.
    if ($sync instanceof OperationTypeInterface) {
      $sync->setPlugin(
        $this->pluginManager->createInstanceForOperationType($sync)
      );
    }

    return $sync;
  }

  /**
   * {@inheritdoc}
   * phpcs:disable
   * @I Improve algorith to make less storage queries
   *    type     : task
   *    priority : normal
   *    labels   : performance, storage, sync-inheritance
   * @I Rename to loadMultiple
   *    type     : task
   *    priority : normal
   *    labels   : storage
   *    notes    : We always return the operation types with the plugins
   *               instantiated, as we do in `load`. For consistency with the
   *               `load` method that uses the same name with the entity
   *               storage, we should use just `loadMultiple`.
   * phpcs:enable
   */
  public function loadMultipleWithPluginInstantiated(array $sync_ids = NULL) {
    $sync_ids = $this->entityTypeManager
      ->getStorage(EntityType::SYNC)
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    $operation_type_ids = $this->entityTypeManager
      ->getStorage(EntityType::OPERATION_TYPE)
      ->getQuery()
      ->accessCheck(FALSE)
      ->execute();

    // There's no way to load the entities with the inherited settings
    // merged. We have to load and merge one by one.
    $all_syncs = [];
    foreach (array_merge($operation_type_ids, $sync_ids) as $id) {
      $all_syncs[$id] = $this->load($id);
    }

    return $all_syncs;
  }

  /**
   * Merges the given synchronization with its parent, if it has one.
   *
   * Each synchronization may only have one immediate parent; its immediate
   * parent can have a parent as well i.e. there can be multiple inheritance
   * levels.
   *
   * For this method to work, the patch provided by the MRs on the following
   * issue is required: https://www.drupal.org/project/drupal/issues/3392775
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization for which to merge the parent settings.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface
   *   The synchronization with the inherited settings merged.
   */
  protected function mergeParentSync(SyncInterface $sync) {
    $parent_sync_id = $sync->getParentSyncSettings()['id'] ?? NULL;
    if ($parent_sync_id === NULL) {
      return $sync;
    }

    // Load the parent with its own inherited settings merged.
    $parent_sync = $this->load($parent_sync_id);

    $sync->setLabels(NestedArray::mergeDeepArray(
      [
        $parent_sync->getLabels() ?? [],
        $sync->getLabels() ?? [],
      ],
      FALSE,
      FALSE
    ));
    $sync->setLocalEntitySettings(NestedArray::mergeDeepArray(
      [
        $parent_sync->getLocalEntitySettings() ?? [],
        $sync->getLocalEntitySettings() ?? [],
      ],
      FALSE,
      FALSE
    ));
    $sync->setRemoteResourceSettings(NestedArray::mergeDeepArray(
      [
        $parent_sync->getRemoteResourceSettings() ?? [],
        $sync->getRemoteResourceSettings() ?? [],
      ],
      FALSE,
      FALSE
    ));
    $sync->setOperationsSettings(NestedArray::mergeDeepArray(
      [
        $parent_sync->getOperationsSettings() ?? [],
        $sync->getOperationsSettings() ?? [],
      ],
      FALSE,
      FALSE
    ));
    $sync->setFieldMapping(NestedArray::mergeDeepArray(
      [
        $parent_sync->getFieldMapping() ?? [],
        $sync->getFieldMapping() ?? [],
      ],
      FALSE,
      FALSE
    ));

    return $sync;
  }

}
