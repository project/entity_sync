<?php

namespace Drupal\entity_sync\Entity\Storage;

/**
 * The interface for the configuration entity loader.
 *
 * We are transitioning to using Operation Type configuration entities for
 * determining the behavior of import/export operation. For now, until the
 * architecture finalizes and we are able to have Synchronization entities
 * contain Operation Type entities, we want to provide backwards compatibility
 * and support passing either a Synchronization or an Operation Type to the
 * import/export manager services.
 *
 * This configuration entity loader service facilitates that, as this is
 * something that is required in a number of different places in the code.
 */
interface ConfigEntityLoaderInterface {

  /**
   * Returns the Synchronization or Operation Type entity with the given ID.
   *
   * It first checks for a Synchronization with the given ID and it returns that
   * if it exists. Otherwise, it checks for an Operation Type. The Operation
   * Type should be returned with the configurator plugin instantiated.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type configuration entity to
   *   load.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface|null
   *   The Synchronization or Operation Type configuration entity, if found;
   *   NULL otherwise.
   */
  public function load(string $sync_id);

  /**
   * Returns the Synchronization or Operation Type entities with the given IDs.
   *
   * Operation Types are returned with their configurator plugin instantiated.
   *
   * All entities are returned if no IDs are provided.
   *
   * Currently, it is not supported to have Operation Types that have the same
   * ID with Synchronization entities. When this happens, Operation Types that
   * have IDs that match IDs of Synchronization entities are not included in the
   * results.
   *
   * @param array $sync_ids
   *   An array of entity IDs, or NULL to load all entities.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface[]
   *   An array of entity objects indexed by their IDs. Returns an empty array
   *   if no matching entities are found.
   */
  public function loadMultipleWithPluginInstantiated(array $sync_ids = NULL);

}
