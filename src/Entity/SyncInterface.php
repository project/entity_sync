<?php

namespace Drupal\entity_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for synchronizations.
 */
interface SyncInterface extends
  ConfigEntityInterface,
  EntityDescriptionInterface {

  /**
   * Returns whether the syncrhonization is internal.
   *
   * Should default to `FALSE`, if not defined in the configuration.
   *
   * @return bool
   *   `TRUE` if the synchronization is internal, `FALSE` otherwise.
   *
   * @see config/schema/entity_sync.schema.yml
   */
  public function isInternal();

  /**
   * Returns the parent synchronization settings.
   *
   * A parent synchronization defines configuration shared by multiple child
   * synchronizations; it can also be used to link import to export operations
   * to take advantage of features such as breaking import/export loops.
   *
   * @return array|null
   *   The parent synchronization settings, or `NULL` if no settings have been
   *   defined.
   */
  public function getParentSyncSettings();

  /**
   * Sets the parent synchronization settings.
   *
   * @param array|null $settings
   *   The parent synchronization settings, or `NULL` to remove them.
   *
   * @return $this
   */
  public function setParentSyncSettings(array $settings = NULL);

  /**
   * Returns the labels.
   *
   * Labels (tags) can be used, for example, for applying event subscribers to
   * multiple synchronizations.
   *
   * @return array|null
   *   The labels, or `NULL` if no labels have been defined.
   */
  public function getLabels();

  /**
   * Sets the labels.
   *
   * @param array|null $labels
   *   The labels, or `NULL` to remove them.
   *
   * @return $this
   */
  public function setLabels(array $labels = NULL);

  /**
   * Returns the settings related to the local entity.
   *
   * @return array|null
   *   The local entity settings, or `NULL` if no settings have been defined.
   */
  public function getLocalEntitySettings();

  /**
   * Sets the settings related to the local entity.
   *
   * @param array|null $settings
   *   The local entity settings, or to remove them.
   *
   * @return $this
   */
  public function setLocalEntitySettings(array $settings = NULL);

  /**
   * Returns the settings related to the remote resource.
   *
   * @return array|null
   *   The remote resource settings, or `NULL` if no settings have been defined.
   */
  public function getRemoteResourceSettings();

  /**
   * Sets the settings related to the remote resource.
   *
   * @param array|null $settings
   *   The remote resource settings, or `NULL` to remove them.
   *
   * @return $this
   */
  public function setRemoteResourceSettings(array $settings = NULL);

  /**
   * Returns the settings related to the operations.
   *
   * @return array|null
   *   The operations settings, or `NULL` if no settings have been defined.
   */
  public function getOperationsSettings();

  /**
   * Sets the settings related to the operations.
   *
   * @param array|null $settings
   *   The operations settings, or `NULL` to remove them.
   *
   * @return $this
   */
  public function setOperationsSettings(array $settings = NULL);

  /**
   * Returns the field mapping settings.
   *
   * @return array|null
   *   The field mapping settings, or `NULL` if no settings have been defined.
   */
  public function getFieldMapping();

  /**
   * Sets the field mapping settings.
   *
   * @param array|null $settings
   *   The field mapping settings, or `NULL` to remove them.
   *
   * @return $this
   */
  public function setFieldMapping(array $settings = NULL);

  /**
   * Returns the computed values settings.
   *
   * @return array|null
   *   The computed values settings, or `NULL` if no settings have been
   *   defined.
   */
  public function getComputedValuesSettings();

  /**
   * Sets the computed values settings.
   *
   * @param array|null $settings
   *   The computed values settings, or `NULL` to remove them.
   *
   * @return $this
   */
  public function setComputedValuesSettings(array $settings = NULL);

}
