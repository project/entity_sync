<?php

namespace Drupal\entity_sync\Entity\Controller;

use Drupal\Core\Entity\Controller\EntityController;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;

/**
 * Provides the add-page callback for the operation entity.
 *
 * Changes compared to the parent include:
 * - Displays a message when there is no bundle available. See
 *   https://www.drupal.org/project/drupal/issues/3256948.
 * - Optimizes the logic knowing that the operation entity has a bundle config
 *   entity.
 * - Removes links to bundles that have entity creation via the UI disabled in
 *   its configuration.
 */
class Operation extends EntityController {

  /**
   * {@inheritdoc}
   */
  public function addPage($entity_type_id) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    $bundle_entity_type_id = $entity_type->getBundleEntityType();
    $bundle_entity_type = $this->entityTypeManager
      ->getDefinition($bundle_entity_type_id);
    $bundles = $this->entityTypeManager
      ->getStorage($bundle_entity_type_id)
      ->loadMultiple();

    $build = [
      '#theme' => 'entity_add_list',
      '#bundles' => [],
      '#cache' => [
        'tags' => $bundle_entity_type->getListCacheTags(),
      ],
    ];

    $this->filterBundles($entity_type_id, $bundles, $build);

    $form_route_name = 'entity.' . $entity_type_id . '.add_form';

    // Redirect if there's only one bundle available.
    $bundle_count = count($bundles);
    if ($bundle_count === 1) {
      return $this->redirect(
        $form_route_name,
        [$bundle_entity_type_id => key($bundles)]
      );
    }
    // Show a message when there are no bundles.
    elseif ($bundle_count === 0) {
      $build['#add_bundle_message'] = $this->buildAddBundleMessage(
        $bundle_entity_type
      );
      return $build;
    }

    // Prepare the list of bundles for the template.
    foreach ($bundles as $bundle_id => $bundle) {
      $build['#bundles'][$bundle_id] = [
        'label' => $bundle->label(),
        'description' => $bundle->getDescription() ?? '',
        'add_link' => Link::createFromRoute(
          $bundle->label(),
          $form_route_name,
          [$bundle_entity_type_id => $bundle_id]
        ),
      ];
    }

    return $build;
  }

  /**
   * Filters out bundles that should not be listed.
   *
   * @param string $entity_type_id
   *   The ID of the entity type.
   * @param array $bundles
   *   An array containing all bundle entities.
   * @param array $build
   *   The page render array.
   */
  protected function filterBundles(
    string $entity_type_id,
    array &$bundles,
    array &$build
  ) {
    // Filter out the bundles the user doesn't have access to.
    $access_handler = $this->entityTypeManager
      ->getAccessControlHandler($entity_type_id);
    foreach ($bundles as $bundle_id => $bundle) {
      $access = $access_handler->createAccess($bundle_id, NULL, [], TRUE);
      if (!$access->isAllowed()) {
        unset($bundles[$bundle_id]);
      }
      $this->renderer->addCacheableDependency($build, $access);
    }

    // Filter out the bundles that have UI-creation disabled.
    $bundles = array_filter(
      $bundles,
      function ($bundle) {
        $config = $bundle->getPluginConfiguration();
        return $config['ui']['allow_create'] ?? TRUE;
      }
    );
  }

  /**
   * Builds the message displayed when there is no bundle available.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $bundle_entity_type
   *   The entity type providing the bundles for the entity.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The translated message.
   */
  protected function buildAddBundleMessage(EntityTypeInterface $bundle_entity_type) {
    $bundle_entity_type_id = $bundle_entity_type->id();
    $singular_label = $bundle_entity_type->getSingularLabel();
    $plural_label = $bundle_entity_type->getPluralLabel();

    $link_route_name = NULL;
    if ($bundle_entity_type->hasLinkTemplate('add-page')) {
      $link_route_name = "entity.{$bundle_entity_type_id}.add_page";
    }
    else {
      $link_route_name = "entity.{$bundle_entity_type_id}.add_form";
    }

    $link_text = $this->t(
      'Add a new @entity_type.',
      ['@entity_type' => $singular_label]
    );
    $link = Link::createFromRoute($link_text, $link_route_name)->toString();

    return $this->t(
      'There are no @entity_type yet, or there is none that allows creating
       entities via the UI. @add_link',
      [
        '@entity_type' => $plural_label,
        '@add_link' => $link,
      ]
    );
  }

}
