<?php

namespace Drupal\entity_sync\Entity\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * The default list builder for operation types.
 *
 * phpcs:disable
 * @I Display information about the plugin
 *    type     : improvement
 *    priority : low
 *    labels   : ux
 * phpcs:enable
 */
class OperationTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('ID');
    $header['description'] = $this->t('Description');
    $header['status'] = $this->t('Status');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['description'] = $entity->getDescription();

    $status = $this->t('Disabled');
    if ($entity->get('status') === TRUE) {
      $status = $this->t('Enabled');
    }
    $row['status'] = $status;

    return $row + parent::buildRow($entity);
  }

}
