<?php

namespace Drupal\entity_sync\Entity;

use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Defines the interface for synchronization operations.
 *
 * An operation is a specific execution of an import or export. This allows to
 * keep track of information such as the user that executed the operation, the
 * affected entity(ies), the time of execution, the status and the result.
 */
interface OperationInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityOwnerInterface {
}
