<?php

namespace Drupal\entity_sync\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the default Synchronization config entity class.
 *
 * @ConfigEntityType(
 *   id = "entity_sync",
 *   label = @Translation("Entity synchronization"),
 *   label_collection = @Translation("Entity synchronizations"),
 *   label_singular = @Translation("entity synchronization"),
 *   label_plural = @Translation("entity synchronizations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count entity synchronization",
 *     plural = "@count entity synchronizations",
 *   ),
 *   config_prefix = "sync",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "is_internal",
 *     "parent_sync",
 *     "labels",
 *     "local_entity",
 *     "remote_resource",
 *     "operations",
 *     "field_mapping"
 *   }
 * )
 */
class Sync extends ConfigEntityBase implements SyncInterface {

  use SyncTrait;

  /**
   * The synchronization ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The synchronization label.
   *
   * @var string
   */
  protected $label;

  /**
   * The synchronization description.
   *
   * @var string
   */
  protected $description;

  /**
   * Marks a synchronization as internal.
   *
   * If not defined, defaults to `FALSE`.
   *
   * @var bool
   *
   * @see \Drupal\entity_sync\Entity\SyncInterface::isInternal()
   */
  protected $is_internal;

  /**
   * The parent synchronization settings.
   *
   * @var array
   */
  protected $parent_sync;

  /**
   * The labels.
   *
   * @var array
   */
  protected $labels;

  /**
   * The local entity settings.
   *
   * @var array
   */
  protected $local_entity;

  /**
   * The remote resource settings.
   *
   * @var array
   */
  protected $remote_resource;

  /**
   * The operations settings.
   *
   * @var array
   */
  protected $operations;

  /**
   * The field mapping settings.
   *
   * @var array
   */
  protected $field_mapping;

  /**
   * The computed values settings.
   *
   * @var array
   */
  protected $computed_values;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return $this->is_internal ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentSyncSettings() {
    return $this->parent_sync;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentSyncSettings(array $settings = NULL) {
    $this->parent_sync = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabels() {
    return $this->labels;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabels(array $labels = NULL) {
    $this->labels = $labels;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalEntitySettings() {
    return $this->local_entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocalEntitySettings(array $settings = NULL) {
    $this->local_entity = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteResourceSettings() {
    return $this->remote_resource;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteResourceSettings(array $settings = NULL) {
    $this->remote_resource = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperationsSettings() {
    return $this->operations;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperationsSettings(array $settings = NULL) {
    $this->operations = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapping() {
    return $this->field_mapping;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMapping(array $settings = NULL) {
    $this->field_mapping = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getComputedValuesSettings() {
    return $this->computed_values;
  }

  /**
   * {@inheritdoc}
   */
  public function setComputedValuesSettings(array $settings = NULL) {
    $this->computed_values = $settings;
    return $this;
  }

}
