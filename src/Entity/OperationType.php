<?php

namespace Drupal\entity_sync\Entity;

use Drupal\entity_sync\OperationConfigurator\PluginInterface;
use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * The default implementation of the Operation Type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "entity_sync_operation_type",
 *   label = @Translation("Sync operation type"),
 *   label_singular = @Translation("sync operation type"),
 *   label_plural = @Translation("sync operation types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sync operation type",
 *     plural = "@count sync operation types"
 *   ),
 *   admin_permission = "administer entity_sync_operation_type",
 *   config_prefix = "operation_type",
 *   bundle_of = "entity_sync_operation",
 *   handlers = {
 *     "list_builder" = "Drupal\entity_sync\Entity\Controller\OperationTypeListBuilder",
 *     "storage" = "Drupal\entity_sync\Entity\Storage\OperationType",
 *     "form" = {
 *       "add" = "Drupal\entity_sync\Entity\Form\OperationType",
 *       "edit" = "Drupal\entity_sync\Entity\Form\OperationType",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "bundle" = "plugin_id",
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   },
 *   links = {
 *     "add-page" = "/admin/sync/config/operation-types/add",
 *     "add-form" = "/admin/sync/config/operation-types/add/{plugin_id}",
 *     "edit-form" = "/admin/sync/config/operation-types/{entity_sync_operation_type}/edit",
 *     "collection" = "/admin/sync/config/operation-types",
 *   },
 * )
 */
class OperationType extends ConfigEntityBundleBase implements
  OperationTypeInterface {

  use SyncTrait;

  /**
   * The machine name of the operation type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the operation type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the operation type.
   *
   * @var string
   */
  protected $description;

  /**
   * The operation configurator plugin ID for the operation type.
   *
   * @var string
   */
  protected $plugin_id;

  /**
   * The operation configurator plugin configuration for the operation type.
   *
   * @var array
   */
  protected $plugin_config = [];

  /**
   * The operation configurator plugin.
   *
   * @var \Drupal\entity_sync\OperationConfigurator\PluginInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId() {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId(string $plugin_id) {
    $this->plugin_id = $plugin_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin(PluginInterface $plugin) {
    $this->plugin = $plugin;
    $this->plugin_config = $plugin->getConfiguration();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration() {
    return $this->plugin_config;
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return $this->plugin_config['is_internal'] ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getParentSyncSettings() {
    return $this->plugin_config['parent_sync'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setParentSyncSettings(array $settings = NULL) {
    $this->plugin_config['parent_sync'] = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabels() {
    return $this->plugin_config['labels'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabels(array $labels = NULL) {
    $this->plugin_config['labels'] = $labels;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLocalEntitySettings() {
    return $this->plugin_config['local_entity'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLocalEntitySettings(array $settings = NULL) {
    $this->plugin_config['local_entity'] = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteResourceSettings() {
    return $this->plugin_config['remote_resource'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setRemoteResourceSettings(array $settings = NULL) {
    $this->plugin_config['remote_resource'] = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperationsSettings() {
    return $this->plugin_config['operations'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setOperationsSettings(array $settings = NULL) {
    $this->plugin_config['operations'] = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldMapping() {
    return $this->plugin_config['field_mapping'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldMapping(array $settings = NULL) {
    $this->plugin_config['field_mapping'] = $settings;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getComputedValuesSettings() {
    return $this->plugin_config['computed_values'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setComputedValuesSettings(array $settings = NULL) {
    $this->plugin_config['computed_values'] = $settings;
    return $this;
  }

}
