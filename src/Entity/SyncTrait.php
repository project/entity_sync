<?php

namespace Drupal\entity_sync\Entity;

/**
 * Trait that provides methods for synchronization configuration entities.
 */
trait SyncTrait {

  /**
   * Calculates dependencies and stores them in the dependency property.
   *
   * @return $this
   *
   * @see \Drupal\Core\Config\Entity\ConfigEntityInterface::calculateDependencies()
   * phpcs:disable
   * @I Add field dependencies from field mapping
   *    type     : task
   *    priority : normal
   *    labels   : config, field
   * @I Add field dependencies from remote ID/changed/composite fields
   *    type     : task
   *    priority : normal
   *    labels   : config, field
   * phpcs:enable
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    // Add the dependencies of the configurator plugin.
    $this->addDependencies($this->getPlugin()->calculateDependencies());

    // Add the dependencies of the local entity type/bundle.
    $settings = $this->getLocalEntitySettings();
    if (!isset($settings['type_id'])) {
      return $this;
    }

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $entity_type = $entity_type_manager->getDefinition($settings['type_id']);

    if (isset($settings['bundle'])) {
      $bundle_entity = $entity_type_manager
        ->getStorage($entity_type->getBundleEntityType())
        ->load($settings['bundle']);
      $this->addDependency('config', $bundle_entity->getConfigDependencyName());
    }
    else {
      $this->addDependency('module', $entity_type->getProvider());
    }

    return $this;
  }

}
