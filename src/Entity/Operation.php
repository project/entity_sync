<?php

namespace Drupal\entity_sync\Entity;

use Drupal\entity_sync\Import\ManagerInterface as ImportManagerInterface;
use Drupal\user\EntityOwnerTrait;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The default implementation for the Operation content entity.
 *
 * @ContentEntityType(
 *   id = "entity_sync_operation",
 *   label = @Translation("Sync operation"),
 *   label_collection = @Translation("Sync operations"),
 *   label_singular = @Translation("sync operation"),
 *   label_plural = @Translation("sync operations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count sync operation",
 *     plural = "@count sync operations",
 *   ),
 *   bundle_label = @Translation("Sync operation type"),
 *   bundle_entity_type = "entity_sync_operation_type",
 *   field_ui_base_route = "entity.entity_sync_operation_type.edit_form",
 *   handlers = {
 *     "list_builder" = "Drupal\entity_sync\Entity\Controller\OperationListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\entity_sync\Entity\Form\Operation",
 *       "edit" = "Drupal\entity_sync\Entity\Form\Operation",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "entity_sync_operation",
 *   admin_permission = "administer entity_sync_operation",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *   },
 *   links = {
 *     "canonical" = "/admin/sync/operations/{entity_sync_operation}",
 *     "add-page" = "/admin/sync/operations/add",
 *     "add-form" = "/admin/sync/operations/add/{entity_sync_operation_type}",
 *     "edit-form" = "/admin/sync/operations/{entity_sync_operation}/edit",
 *   },
 * )
 */
class Operation extends ContentEntityBase implements OperationInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    // Alterations on already defined fields.
    $fields['id']->setDescription(
      new TranslatableMarkup('The ID of the operation.')
    );
    $fields['uuid']->setDescription(
      new TranslatableMarkup('The UUID of the operation.')
    );

    $fields['uid']
      ->setLabel(new TranslatableMarkup('Created by'))
      ->setDescription(
        new TranslatableMarkup('The user that created the operation.')
      )
      ->setDisplayOptions(
        'view',
        [
          'label' => 'above',
          'type' => 'author',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    $fields['type']
      ->setLabel(new TranslatableMarkup('Type'))
      ->setDisplayConfigurable('view', TRUE);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(new TranslatableMarkup('State'))
      ->setDescription(new TranslatableMarkup('The state of the operation.'))
      ->setRequired(TRUE)
      ->setSetting(
        'workflow_callback',
        [__CLASS__, 'getWorkflowId']
      )
      ->setDisplayConfigurable('view', TRUE);

    // Other fields.
    // phpcs:disable
    // @I Review whether this should be a bundle field
    //    type     : bug
    //    priority : normal
    //    labels   : action, field, operation
    //    notes    : List operations do not have a specific action as that could
    //               be different for each entity in the list.
    //
    // @I Optimize the length of the `action` database column
    //    type     : improvement
    //    priority : low
    //    labels   : database, performance
    // phpcs:enable
    $fields['action'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(new TranslatableMarkup('Action'))
      ->setDescription(new TranslatableMarkup(
        'The action that was taken on the target local or remote entity.'
      ))
      ->setSetting('unsigned', TRUE)
      ->setSetting(
        'allowed_values',
        [
          // These are the same (values/labels) for both imports and exports,
          // even though they are defined separately in both entity managers. We
          // therefore use the constants from the import entity managers to
          // avoid further complexity.
          ImportManagerInterface::ACTION_CREATE => t('Create'),
          ImportManagerInterface::ACTION_UPDATE => t('Update'),
          ImportManagerInterface::ACTION_SKIP => t('Skip'),
        ]
      );

    // At last, timestamps.
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup(
        'The time when the operation entity was created.'
      ))
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time when the operation entity was last edited.'
      ))
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Returns the ID of the workflow for the state field.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation.
   *
   * @return string
   *   The workflow ID.
   */
  public static function getWorkflowId(OperationInterface $operation) {
    $operation_type = \Drupal::service('entity_type.manager')
      ->getStorage('entity_sync_operation_type')
      ->load($operation->bundle());

    return \Drupal::service('plugin.manager.entity_sync_operation_configurator')
      ->createInstanceForOperationType($operation_type)
      ->getPluginDefinition()['workflow_id'];
  }

}
