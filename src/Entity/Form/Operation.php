<?php

namespace Drupal\entity_sync\Entity\Form;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;

use Drupal\Component\Datetime\TimeInterface;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default form controller for operation forms.
 */
class Operation extends ContentEntityForm {

  /**
   * The operation storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $entity_repository,
      $entity_type_bundle_info,
      $time
    );

    $this->storage = $entity_type_manager
      ->getStorage('entity_sync_operation_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Usually, operations should not be updated manually after they have been
   * run. The configurator plugin defines in which states the operation should
   * be uneditable.
   *
   * phpcs:disable
   * @I Consider making fields uneditable instead of the whole edited
   *    type     : improvement
   *    priority : low
   *    labels   : operation, ux
   *    notes    : We may have custom fields that we would want to edit after
                   the operation has run, such as metadata.
   * phpcs:enable
   */
  public function form(array $form, FormStateInterface $form_state) {
    $operation = $this->getEntity();
    if ($operation->isNew()) {
      return $this->buildCreateForm($operation, $form, $form_state);
    }

    return $this->buildEditForm($operation, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $operation = $this->getEntity();
    $status = $operation->save();

    $bundle = $operation->bundle();
    $logger_context = [
      '@type' => $bundle,
      '%label' => $operation->label(),
      'link' => $operation->toLink($this->t('View'))->toString(),
    ];
    $translation_context = [
      '@type' => $this->storage->load($bundle)->label(),
      '%label' => $operation->toLink()->toString(),
    ];

    if ($status === SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t(
        '@type: operation %label has been updated.',
        $translation_context
      ));
      $this->logger('entity_sync')->notice(
        '@type: operation %label has been updated.',
        $logger_context
      );
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addStatus($this->t(
        '@type: operation %label has been added.',
        $translation_context
      ));
      $this->logger('entity_sync')->notice(
        '@type: operation %label has been added.',
        $logger_context
      );
    }

    $form_state->setRedirect('view.entity_sync_operations.page_1');
  }

  /**
   * {@inheritdoc}
   *
   * If the operation is uncreatable or uneditable, deny access to the submit
   * button. We do not alter other action buttons.
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    // Let's not recalculate whether the operation is creatable/editable here.
    if (empty($form['uncreatable']) && empty($form['uneditable'])) {
      return parent::actions($form, $form_state);
    }

    $element = parent::actions($form, $form_state);
    if (!isset($element['submit'])) {
      return;
    }

    $element['submit']['#access'] = FALSE;
    return $element;
  }

  /**
   * Builds and returns the form for new entities.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation entity.
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form render array.
   */
  protected function buildCreateForm(
    OperationInterface $operation,
    array $form,
    FormStateInterface $form_state
  ) {
    if ($this->isCreatable()) {
      return parent::form($form, $form_state);
    }

    $operation_type = $this->storage->load($operation->bundle());

    $form['uncreatable'] = [
      '#markup' => $this->t(
        '"@operation_type" operations cannot be created via the UI.',
        ['@operation_type' => $operation_type->label()]
      ),
    ];
    return $form;
  }

  /**
   * Builds and returns the form for existing entities.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation entity.
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form render array.
   */
  protected function buildEditForm(
    OperationInterface $operation,
    array $form,
    FormStateInterface $form_state
  ) {
    if ($this->isEditable()) {
      return parent::form($form, $form_state);
    }

    $operation_type = $this->storage->load($operation->bundle());
    $ui_config = $operation_type->getPluginConfiguration()['ui'] ?? [];
    if (($ui_config['allow_update'] ?? TRUE) === FALSE) {
      $form['uneditable'] = [
        '#markup' => $this->t(
          '"@operation_type" operations cannot be edited via the UI.',
          ['@operation_type' => $operation_type->label()]
        ),
      ];
      return $form;
    }

    $state_item = $operation->get(OperationField::STATE)->first();
    $form['uneditable'] = [
      '#markup' => $this->t(
        '"@operation_type" operations cannot be edited when they are on the
         "@state" state.',
        [
          '@operation_type' => $operation_type->label(),
          '@state' => $state_item->getLabel(),
        ]
      ),
    ];
    return $form;
  }

  /**
   * Returns whether the operation is allowed to be created via the UI.
   *
   * @return bool
   *   TRUE if the operation is creatable, FALSE otherwise.
   */
  protected function isCreatable() {
    $operation = $this->getEntity();
    $operation_type = $this->storage->load($operation->bundle());
    $ui_config = $operation_type->getPluginConfiguration()['ui'] ?? [];

    return $ui_config['allow_create'] ?? TRUE;
  }

  /**
   * Returns whether the operation is allowed to be edited via the UI.
   *
   * @return bool
   *   TRUE if the operation is editable, FALSE otherwise.
   */
  protected function isEditable() {
    $operation = $this->getEntity();
    $operation_type = $this->storage->load($operation->bundle());
    $ui_config = $operation_type->getPluginConfiguration()['ui'] ?? [];

    if (($ui_config['allow_update'] ?? TRUE) === FALSE) {
      return FALSE;
    }

    if (empty($ui_config['uneditable_states'])) {
      return TRUE;
    }

    $uneditable_states = $ui_config['uneditable_states'];
    $state_item = $operation->get(OperationField::STATE)->first();
    if (!in_array($state_item->getId(), $uneditable_states)) {
      return TRUE;
    }

    return FALSE;
  }

}
