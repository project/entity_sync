<?php

namespace Drupal\entity_sync\Entity\Form;

use Drupal\entity_sync\Entity\OperationTypeInterface;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default form controller for operation type forms.
 */
class OperationType extends EntityForm {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The operation configurator plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new OperationType object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The operation configurator plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    PluginManagerInterface $plugin_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_sync_operation_configurator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $operation_type = $this->getEntity();

    // Operation type form elements.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $operation_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $operation_type->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$operation_type->isNew(),
      '#required' => TRUE,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t(
        'A brief description of the operation type. It will be displayed on the
         <em>Add operation</em> page.'
      ),
      '#default_value' => $operation_type->getDescription(),
    ];

    // Plugin form elements.
    $plugin = $this->getPlugin();
    // This is just informational for the user. We get the plugin ID from the
    // URL. See `getEntityFromRouteMatch()`.
    $form['configurator'] = [
      '#type' => 'radios',
      '#title' => $this->t('Configurator'),
      '#options' => [
        $plugin->getPluginId() => $plugin->label(),
      ],
      '#default_value' => $plugin->getPluginId(),
      '#required' => TRUE,
    ];
    // Add the plugin configuration form elements.
    $form = $plugin->buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->getPlugin()->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $plugin = $this->getPlugin();
    $plugin->submitConfigurationForm($form, $form_state);
    $this->getEntity()->setPlugin($plugin);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $operation_type = $this->getEntity();
    $operation_type->set('type', trim($operation_type->id()));
    $operation_type->set('label', trim($operation_type->label()));

    $status = $operation_type->save();
    $message_context = [
      '%label' => $operation_type->label(),
    ];
    $logger_context = [
      '%label' => $operation_type->label(),
      'link' => $operation_type->toLink($this->t('Edit'), 'edit-form')->toString(),
    ];

    if ($status === SAVED_UPDATED) {
      $this->messenger()->addMessage($this->t(
        'Operation type %label has been updated.',
        $message_context
      ));
      $this->logger('entity_sync')->notice(
        'Operation type %label has been updated.',
        $logger_context
      );
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addMessage($this->t(
        'Operation type %label has been added.',
        $message_context
      ));
      $this->logger('entity_sync')->notice(
        'Operation type %label has been added.',
        $logger_context
      );
    }

    $form_state->setRedirect('entity.entity_sync_operation_type.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(
    RouteMatchInterface $route_match,
    $entity_type_id
  ) {
    $operation_type = parent::getEntityFromRouteMatch(
      $route_match,
      $entity_type_id
    );

    if ($operation_type->isNew()) {
      $operation_type->setPluginId($route_match->getRawParameter('plugin_id'));
    }

    return $operation_type;
  }

  /**
   * Determines if an operation type already exists for the given ID.
   *
   * @param string $id
   *   The operation type ID.
   *
   * @return bool
   *   TRUE if the operation type exists, FALSE otherwise.
   */
  public function exists($id) {
    $operation_type = $this->entityTypeManager
      ->getStorage('entity_sync_operation_type')
      ->load($id);
    return $operation_type ? TRUE : FALSE;
  }

  /**
   * Submit callback for creating fields for the operation type.
   *
   * The fields are defined by the operation configurator plugin.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function createFields(array $form, FormStateInterface $form_state) {
    $field_definitions = $this->getPlugin()->bundleFieldDefinitions();
    $operation_type = $this->getEntity();

    foreach ($field_definitions as $field_name => $field_definition) {
      $field_definition->setTargetEntityTypeId('entity_sync_operation');
      $field_definition->setTargetBundle($operation_type->id());
      $field_definition->setName($field_name);

      $this->createField($field_name, $field_definition, $operation_type);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#submit'] = array_merge(
      $actions['submit']['#submit'],
      ['::createFields']
    );

    return $actions;
  }

  /**
   * Creates the field for the given name and definition.
   *
   * The field storage and the field instance will be created only if they don't
   * exist. No errors are raised if they do.
   *
   * @param string $field_name
   *   The field machine name.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type to add the field to.
   * phpcs:disable
   * @I Detect collissions between fields provided by different plugins
   *    type     : bug
   *    priority : normal
   *    labels   : operation, plugin
   *    notes    : Maybe that should happen both here and when loading plugin
   *               definitions i.e. in the plugin manager so that errors can be
   *               displayed during cache rebuild before reaching here.
   * phpcs:enable
   */
  protected function createField(
    $field_name,
    FieldDefinitionInterface $field_definition,
    OperationTypeInterface $operation_type
  ) {
    $field_storage_config_storage = $this->entityTypeManager
      ->getStorage('field_storage_config');
    $field_config_storage = $this->entityTypeManager
      ->getStorage('field_config');

    // Create the field storage configuration, if it does not exist.
    $field_storage = $field_storage_config_storage->load(
      "entity_sync_operation.$field_name"
    );
    if (!$field_storage) {
      $field_storage = $field_storage_config_storage->create([
        'field_name' => $field_name,
        'entity_type' => 'entity_sync_operation',
        'type' => $field_definition->getType(),
        'cardinality' => $field_definition->getCardinality(),
        'settings' => $field_definition->getSettings(),
        'translatable' => $field_definition->isTranslatable(),
      ]);
      $field_storage_config_storage->save($field_storage);
    }

    // Create the field configuration, if it does not exist.
    $bundle = $operation_type->id();
    $field_config = $field_config_storage->load(
      "entity_sync_operation.$bundle.$field_name"
    );
    if (!$field_config) {
      $field_config = $field_config_storage->create([
        'field_storage' => $field_storage,
        'bundle' => $bundle,
        'label' => $field_definition->getLabel(),
        'required' => $field_definition->isRequired(),
        'settings' => $field_definition->getSettings(),
        'translatable' => $field_definition->isTranslatable(),
        'default_value' => $field_definition->getDefaultValueLiteral(),
        'default_value_callback' => $field_definition->getDefaultValueCallback(),
      ]);
      $field_config_storage->save($field_config);
    }
  }

  /**
   * {@inheritdoc}
   *
   * With the introduction of the `plugin` form element, the parent method saves
   * any plugin-specific configuration to the `$plugin` property - which we need
   * it to store the plugin object.
   *
   * We therefore unset it here from the values copied to the entity. The plugin
   * should be responsible to handle its custom configuration properties in the
   * submit callback.
   */
  protected function copyFormValuesToEntity(
    EntityInterface $entity,
    array $form,
    FormStateInterface $form_state
  ) {
    $values = $form_state->getValues();

    unset($values['plugin']);

    foreach ($values as $key => $value) {
      $entity->set($key, $value);
    }
  }

  /**
   * Returns the configurable plugin for the operation type.
   *
   * @return \Drupal\entity_sync\OperationConfigurator\PluginInterface
   *   The configurable operation configurator plugin.
   */
  protected function getPlugin() {
    $operation_type = $this->getEntity();
    $plugin = $operation_type->getPlugin();
    if ($plugin) {
      return $plugin;
    }

    $plugin = $this->pluginManager->createInstance(
      $operation_type->getPluginId(),
      $operation_type->getPluginConfiguration() ?? []
    );
    $operation_type->setPlugin($plugin);

    return $plugin;
  }

}
