<?php

namespace Drupal\entity_sync\Field;

use Drupal\Core\Field\FieldItemInterface;

/**
 * Defines the interface for entity synchronization field items.
 *
 * For simple integrations, a string or integer field is sufficient for holding
 * the ID of the remote entity. When we have connected imports/exports, we also
 * need the changed (last modified) time of the remote entity - which is used
 * for breaking import/export loops. In that case, it is also possible to use
 * two fields, a string/integer field for the ID and a timestamp field for the
 * changed time of the remote entity.
 *
 * There are cases though where we need to synchronize a local entity with
 * multiple remote entities. In such cases it would be quite complicated and
 * error prone to correctly associate the changed time for each remote entity
 * ID, if they are stored in different fields e.g. we'd have to ensure that they
 * are always stored with the same delta.
 *
 * We therefore provide this field type that stores together the ID and the
 * changed time for each remote entity.
 */
interface SyncItemInterface extends FieldItemInterface {

  /**
   * Returns the value of the `id` property.
   *
   * @return string|int
   *   The remote entity ID.
   */
  public function getId();

  /**
   * Sets the value of the `id` property.
   *
   * @param string|int $id
   *   The value to set.
   *
   * @return $this
   */
  public function setId($id);

  /**
   * Returns the value of the `changed` property.
   *
   * @return int
   *   The changed time of the remote entity.
   */
  public function getChanged();

  /**
   * Sets the value of the `changed` property.
   *
   * @param int $time
   *   The value to set.
   *
   * @return $this
   */
  public function setChanged(int $time);

}
