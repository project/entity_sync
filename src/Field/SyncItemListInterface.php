<?php

namespace Drupal\entity_sync\Field;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Defines the interface for entity synchronization field item lists.
 */
interface SyncItemListInterface extends FieldItemListInterface {

  /**
   * Appends a new item, or updates the existing item for the given value.
   *
   * If there is no item in the list with the same ID as the given value, a new
   * item is created and appended to the list. Otherwise, the `changed` property
   * of the existing item is updated.
   *
   * @param mixed $value
   *   The value of the item to append.
   *
   * @return \Drupal\entity_sync\Field\SyncItemInterface
   *   The item that was appended.
   */
  public function appendOrUpdateItem(array $value);

}
