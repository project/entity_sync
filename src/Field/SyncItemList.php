<?php

namespace Drupal\entity_sync\Field;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines the default entity synchronization field item list.
 */
class SyncItemList extends FieldItemList implements SyncItemListInterface {

  /**
   * {@inheritdoc}
   */
  public function appendOrUpdateItem(array $value) {
    $matching_delta = NULL;
    foreach ($this->list as $delta => $item) {
      if ($item->getId() === $value['id']) {
        $matching_delta = $delta;
        break;
      }
    }

    // If we do not have a matching item, append a new one.
    if ($matching_delta === NULL) {
      return $this->appendItem($value);
    }

    // Otherwise, update the `changed` property of the existing item.
    return $this->list[$delta]->setChanged($value['changed']);
  }

}
