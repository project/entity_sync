<?php

namespace Drupal\entity_sync\Field;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Base class for implementations of entity synchronization field types.
 */
abstract class SyncItemBase extends FieldItemBase implements SyncItemInterface {

  /**
   * {@inheritdoc}
   *
   * The `id` property must be defined in the child class.
   */
  public static function propertyDefinitions(
    FieldStorageDefinitionInterface $field_definition
  ) {
    $properties['changed'] = DataDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   *
   * The `id` column must be defined in the child class.
   */
  public static function schema(
    FieldStorageDefinitionInterface $field_definition
  ) {
    return [
      'columns' => [
        'changed' => [
          'type' => 'int',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->id === NULL || $this->id === '' || $this->changed === NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(
    FieldDefinitionInterface $field_definition
  ) {
    return [
      'changed' => \Drupal::time()->getRequestTime() - mt_rand(0, 86400 * 365),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function setId($id) {
    return $this->set('id', $id);
  }

  /**
   * {@inheritdoc}
   */
  public function getChanged() {
    return $this->changed;
  }

  /**
   * {@inheritdoc}
   */
  public function setChanged(int $time) {
    return $this->set('changed', $time);
  }

}
