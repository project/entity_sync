<?php

namespace Drupal\entity_sync\Queue;

use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface;

/**
 * Facilitates implementing queue workers that run operations.
 */
trait RunOperationWorkerTrait {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Runs the operation with the given ID.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type.
   * @param int|string $operation_id
   *   The ID of the operation.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @throws \RuntimeException
   *   When no operation was found for the given data.
   */
  protected function runOperation(
    OperationTypeInterface $operation_type,
    $operation_id,
    array $context = []
  ) {
    $storage = $this->entityTypeManager->getStorage('entity_sync_operation');
    $operation = $storage->load($operation_id);
    if (!$operation) {
      throw new \RuntimeException(
        sprintf(
          'No "%s" operation with ID "%s" found to run.',
          $operation_type->id(),
          $operation_id
        )
      );
    }

    $state_item = $operation->get(OperationField::STATE)->first();
    $run_transition = $state_item->getWorkflow()->getRunTransition();
    $state_item->applyTransitionById($run_transition);
    $storage->save($operation);

    $operation_type
      ->getPlugin()
      ->runner()
      ->run($operation, ['options' => ['context' => $context]]);
  }

  /**
   * Validates that the data passed to the queue item are valid.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type.
   * @param array $data
   *   The data to validate. They should be the data stored in the queue item.
   *
   * @throws \InvalidArgumentException
   *   When invalid or inadequate data are given.
   */
  protected function validateOperationData(
    OperationTypeInterface $operation_type,
    array $data
  ) {
    // Operations that are managed by Entity Sync need to define a runner,
    // otherwise we wouldn't know how to run the operation.
    $plugin = $operation_type->getPlugin();
    if (!$plugin instanceof SupportsRunnerInterface) {
      throw new \RuntimeException(
        sprintf(
          'Operations of type "%s" cannot be managed by Entity Synchronization because the configurator plugin "%s" does not define a runner. You must either define a runner, or run the operation by other means.',
          $operation_type->id(),
          $plugin->getPluginId(),
          $operation_id
        )
      );
    }

    if (isset($data['operation_id'])) {
      return;
    }

    throw new \InvalidArgumentException(
      'The ID of the operation to run must be given.'
    );
  }

}
