<?php

namespace Drupal\entity_sync\Queue;

use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\InvalidConfigurationException;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for queue workers running synchronization operations.
 */
abstract class WorkerBase extends QueueWorkerBase implements
  ContainerFactoryPluginInterface {

  /**
   * The `throw` error handling mode.
   */
  const ERROR_MODE_THROW = 'throw';

  /**
   * The `log_and_skip` error handling mode.
   */
  const ERROR_MODE_LOG_AND_SKIP = 'log_and_skip';

  /**
   * The `log_and_throw` error handling mode.
   */
  const ERROR_MODE_LOG_AND_THROW = 'log_and_throw';

  /**
   * The `skip` error handling mode.
   */
  const ERROR_MODE_SKIP = 'skip';

  /**
   * The Entity Synchronization configuration entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * This module's logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new WorkerBase instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The Entity Synchronization configuration entity loader.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   This module's logger channel.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigEntityLoaderInterface $config_entity_loader,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->configEntityLoader = $config_entity_loader;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_sync.config_entity_loader'),
      $container->get('logger.channel.entity_sync')
    );
  }

  /**
   * Processes a single queue item.
   *
   * Errors should be raised as they occur. The handling of the errors will be
   * done by the caller `processItem` method automatically taking into account
   * the synchronization's settings.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type entity that defines the operation
   *   being run.
   * @param array $data
   *   An array containing the data that was passed to
   *   \Drupal\Core\Queue\QueueInterface::createItem() when the item was queued.
   */
  abstract protected function doProcessItem(SyncInterface $sync, array $data);

  /**
   * Returns the type of action that is being processed in this queue.
   *
   * This is the "operation" in the old terminology.
   *
   * Each queue provided by Entity Synchronization only processes a specific
   * action type. This is so that queue processing can and that different action
   * types can be scheduled to be processed in different times/frequency using
   * e.g. Ultimate Cron.
   *
   * The queue worker needs to know the action type that we are running so that
   * it load the appropriate error handling mode from the synchronization. The
   * action type may (when we have an Operation Type) or may not be known
   * (otherwise) from the synchronizatoin. Each child class should therefore
   * declare the action type it processes by implementing this method.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type entity that defines the operation
   *   being run.
   * @param array $data
   *   An array containing the data that was passed to
   *   \Drupal\Core\Queue\QueueInterface::createItem() when the item was queued.
   *
   * @return string
   *   The action type.
   */
  abstract protected function getActionType(SyncInterface $sync, array $data);

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $this->preValidateData($data);

    // Load the error handling mode from the synchronization with falling back
    // to the default if it is not defined. If for some reason the sync is not
    // found, such as if it is removed before all of its queue items are
    // processed, raise an error following the default error handling behavior.
    $sync = $this->configEntityLoader->load($data['sync_id']);
    if (!$sync) {
      throw new \InvalidArgumentException(sprintf(
        'No Synchronization or Operation Type found with ID "%s".',
        $data['sync_id']
      ));
    }

    try {
      $this->validateData($sync, $data);
      $this->doProcessItem($sync, $data);
    }
    catch (\Throwable $throwable) {
      $action_type = $this->getActionType($sync, $data);
      $settings = $sync->getOperationsSettings()[$action_type];
      $this->handleThrowable(
        $throwable,
        $data,
        $settings['queue_error_handling'] ?? self::ERROR_MODE_THROW
      );
    }
  }

  /**
   * Validates that the data passed to the queue item are valid.
   *
   * This validates the data structure and data items that are common to all
   * queues and that have to be validated before knowing the error handling mode
   * of the synchronization.
   *
   * In most cases additional validation should be added by overriding the
   * `validateData` method so that any errors are handling automatically
   * respecting to the error handling mode.
   *
   * @param mixed $data
   *   The data to validate. They should be the data stored in the queue item.
   *
   * @throws \InvalidArgumentException
   *   When the data are invalid.
   */
  protected function preValidateData($data) {
    if (!is_array($data)) {
      throw new \InvalidArgumentException(
        sprintf(
          'Queue item data should be an array, %s given.',
          gettype($data)
        )
      );
    }

    if (empty($data['sync_id'])) {
      throw new \InvalidArgumentException(
        'The ID of the Synchronization or Operation Type that defines the operation must be given.'
      );
    }
  }

  /**
   * Validates that the data passed to the queue item are valid.
   *
   * No validation by default. It should be overridden by child classes if
   * custom validation is required for the data specific to the related queue.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type entity that defines the operation
   *   being run.
   * @param array $data
   *   The data to validate. They should be the data stored in the queue item.
   *
   * @throws \InvalidArgumentException
   *   When invalid or inadequate data are given.
   */
  protected function validateData(SyncInterface $sync, array $data) {
  }

  /**
   * Handles errors with the given error handling mode.
   *
   * See `config/schema/entity_sync.schema.yml` for an explanation of
   * supported error handling modes.
   *
   * @param \Throwable $throwable
   *   The error that was thrown.
   * @param array $data
   *   The data stored in the queue item.
   * @param string $handling
   *   The error handling mode.
   *
   * @throw \RuntimeException
   *   When an unknown error handling mode is requested.
   */
  protected function handleThrowable(
    \Throwable $throwable,
    array $data,
    string $handling
  ) {
    switch ($handling) {
      case NULL:
      case self::ERROR_MODE_THROW:
        $this->handleThrowableWithThrow($throwable);
        break;

      case self::ERROR_MODE_LOG_AND_SKIP:
        $this->handleThrowableWithLogAndSkip($throwable, $data);
        break;

      case self::ERROR_MODE_LOG_AND_THROW:
        $this->handleThrowableWithLogAndThrow($throwable, $data);

      case self::ERROR_MODE_SKIP:
        $this->handleThrowableWithSkip();
        break;

      default:
        throw new InvalidConfigurationException(sprintf(
          'Unknown error handling type "%s"',
          $handling
        ));
    }
  }

  /**
   * Handles errors with the `throw` error handling mode.
   *
   * Errors are thrown; their messages are not logged.
   *
   * @param \Throwable $throwable
   *   The error that was thrown.
   *
   * @throws \Throwable
   *   The error that was originally thrown.
   */
  protected function handleThrowableWithThrow(\Throwable $throwable) {
    throw $throwable;
  }

  /**
   * Handles errors with the `log_and_skip` error handling mode.
   *
   * Errors are not thrown; their messages are logged.
   *
   * @param \Throwable $throwable
   *   The error that was thrown.
   * @param array $data
   *   The data stored in the queue item.
   */
  protected function handleThrowableWithLogAndSkip(
    \Throwable $throwable,
    array $data
  ) {
    $this->logThrowable($throwable, $data);
  }

  /**
   * Handles errors with the `log_and_throw` error handling mode.
   *
   * Errors are thrown; their messages are logged.
   *
   * @param \Throwable $throwable
   *   The error that was thrown.
   * @param array $data
   *   The data stored in the queue item.
   *
   * @throws \Throwable
   *   The error that was originally thrown.
   */
  protected function handleThrowableWithLogAndThrow(
    \Throwable $throwable,
    array $data
  ) {
    $this->logThrowable($throwable, $data);
    throw $throwable;
  }

  /**
   * Handles errors with the `skip` error handling mode.
   *
   * Errors are not thrown, their messages are not logged.
   */
  protected function handleThrowableWithSkip() {
  }

  /**
   * Logs the given error for the given data.
   *
   * Queue workers can override this method to prepare a message that is
   * specific to the queue they handle.
   *
   * @param \Throwable $throwable
   *   The error that was thrown.
   * @param array $data
   *   The data stored in the queue item.
   */
  protected function logThrowable(\Throwable $throwable, array $data) {
    $this->logger->error(
      'A "@throwable_type" error was thrown while executing a queued operation with data: . The error message was: @throwable_message',
      [
        '@throwable_type' => get_class($throwable),
        '@throwable_message' => $throwable->getMessage(),
        '@data' => '<pre>' . print_r($data, TRUE) . '</pre>',
      ]
    );
  }

}
