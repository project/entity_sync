<?php

namespace Drupal\entity_sync\Queue;

use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides the interface for the queue manager.
 *
 * The queue manager is responsible for queueing import/export operations for
 * entities or lists of entities.
 */
interface ManagerInterface {

  /**
   * Queues all local entity export operations for the given entity.
   *
   * This method will detect all synchronizations that define the
   * `export_entity` operation for the given entity type and will queue the
   * operation for each one of them - where applicable.
   *
   * Each synchronization can define the ID of the queue to put the exports in
   * in the `queue_id` configuration property. Currently, this queue manager
   * supports queueing items in the default Drupal queue for the queue named
   * `entity_sync_export_local_entity`. This is usually the database queue
   * (`queue.database` service), but it might be overridden by defining the
   * `queue_service_entity_sync_export_local_entity` - see
   * `\Drupal\Core\Queue\QueueFactory::get()`. Synchronizations with this queue
   * ID or without any queue ID will be queued within execution of this method.
   *
   * The other supported queue ID is `delayed_executor`. Because the delayed
   * executor processes its queue items at a kernel terminate request event, and
   * this method may also be called as part of the same delayed executor queue,
   * we delegate it to the caller to put the items in the queue or to actually
   * run the operations if we are already within the delayed executor.
   *
   * This probably can be improved to have a more consistent API, but it needs a
   * bit more work and testing, at the very least.
   *
   * The caller of this method should therefore, currently, be ignoring the
   * items in the response array that have the `default` queue ID - because
   * these are already queue, and take care of queueing items that have the
   * `delayed_executor` queue ID.
   *
   * Items that their queueing cancelled for whatever reason are not included in
   * the response array at all.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to queue the export.
   * @param array $context
   *   The queueing context. Not to be confused with the execution context
   *   i.e. the context of the operation at the moment of its execution.
   *   Context items used by the Entity Sync module are:
   *   - id: (string) A identifier of the context. Currently used IDs are:
   *     - `entity_insert`: The operation is being queued as part of a
   *       `hook_entity_insert`.
   *     - `entity_update`: The operation is being queued as part of a
   *       `hook_entity_update`.
   *   - original_entity: (\Drupal\Core\Entity\ContentEntityInterface) The
   *     original entity before the changes, if we are queueing an export
   *     because of a local entity update. It will be used to detect which
   *     fields have changed so that only those are sent to the remote
   *     resource. Currently, if the operation is managed by Entity
   *     Synchronization, no export will be scheduled if there are no changed
   *     fields. See
   *     `Drupal\entity_sync\EventSubscriber::ManagedExportLocalEntityQueueUpdate`
   *   Third-party modules should be placing their custom context in a
   *   sub-array keyed by the module name to prevent conflicts with context
   *   items used by the Entity Sync module. The `id` top-level context item
   *   should be used but with a module-specific prefix to prevent conflicts
   *   there as well.
   *
   * @return array
   *   An array containing information about all synchronizations that were or
   *   should be queued queued. Each array item is keyed by the ID of the
   *   synchronization, and contains a numerical array with the following items:
   *   - The ID of the queue. Currently supported queue IDs are:
   *     - default: The queue for the `entity_sync_export_local_entity` queue.
   *     - delayed_executor: A queue that runs operations at kernel terminate
   *       even of the current request.
   *   - The stored context for the operation. The stored context is context
   *     that can be serialized and stored in the queue for later execution, as
   *     compared to the runtime context that might be different.
   */
  public function exportLocalEntityForAllSyncs(
    ContentEntityInterface $entity,
    array $context = []
  );

  /**
   * Queues the local entity export operation for the given synchronization.
   *
   * For additional information regarding the supported queue IDs and the
   * behavior for each, see comments on `$this::exportLocalEntityForAllSyncs()`.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization that defines the export operation to queue.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to queue the export.
   * @param array $context
   *   The queueing context. Not to be confused with the execution context
   *   i.e. the context of the operation at the moment of its execution.
   *   Context items used by the Entity Sync module are:
   *   - id: (string) A identifier of the context. Currently used IDs are:
   *     - `entity_insert`: The operation is being queued as part of a
   *       `hook_entity_insert`.
   *     - `entity_update`: The operation is being queued as part of a
   *       `hook_entity_update`.
   *     - `entity_delete`: The operation is being queued as part of a
   *       `hook_entity_delete`.
   *   - original_entity: (\Drupal\Core\Entity\ContentEntityInterface) The
   *     original entity before the changes, if we are queueing an export
   *     because of a local entity update. It will be used to detect which
   *     fields have changed so that only those are sent to the remote
   *     resource. Currently, if the operation is managed by Entity
   *     Synchronization, no export will be scheduled if there are no changed
   *     fields. See
   *     `Drupal\entity_sync\EventSubscriber::ManagedExportLocalEntityQueueUpdate`.
   *   Third-party modules should be placing their custom context in a
   *   sub-array keyed by the module name to prevent conflicts with context
   *   items used by the Entity Sync module. The `id` top-level context item
   *   should be used but with a module-specific prefix to prevent conflicts
   *   there as well.
   *
   * @return array|null
   *   Returns `NULL` if queueing the synchronization was cancelled. Otherwise,
   *   returns a numerical array that contains following items:
   *   - The ID of the queue. Currently supported queue IDs are:
   *     - default: The queue for the `entity_sync_export_local_entity` queue.
   *     - delayed_executor: A queue that runs operations at kernel terminate
   *       even of the current request.
   *   - The stored context for the operation. The stored context is context
   *     that can be serialized and stored in the queue for later execution, as
   *     compared to the runtime context that might be different.
   */
  public function exportLocalEntityForSync(
    SyncInterface $sync,
    ContentEntityInterface $entity,
    array $context = []
  );

}
