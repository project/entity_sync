<?php

namespace Drupal\entity_sync\Queue;

// Drupal modules.
use Drupal\entity_sync\Config\ManagerInterface as ConfigManagerInterface;
use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Event\QueueOperationEvent;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\FieldManagerInterface;
use Drupal\entity_sync\MachineName\Entity\Type as EntityType;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\MachineName\Internal\Action\Type as ActionType;
use Drupal\entity_sync\MachineName\Queue\Name as Queue;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
// External libraries.
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The default implementation of the queue manager.
 */
class Manager implements ManagerInterface {

  /**
   * The Entity Synchronization configuration manager.
   *
   * @var \Drupal\entity_sync\Config\ManagerInterface
   */
  protected $configManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Entity Synchronization export field manager.
   *
   * @var \Drupal\entity_sync\Export\FieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructs a new Manager object.
   *
   * @param \Drupal\entity_sync\Config\ManagerInterface $config_manager
   *   The Entity Synchronization configuration manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\entity_sync\Export\FieldManagerInterface $field_manager
   *   The Entity Synchronization export field manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory.
   */
  public function __construct(
    ConfigManagerInterface $config_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EventDispatcherInterface $event_dispatcher,
    FieldManagerInterface $field_manager,
    LoggerInterface $logger,
    QueueFactory $queue_factory
  ) {
    $this->configManager = $config_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->fieldManager = $field_manager;
    $this->logger = $logger;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function exportLocalEntityForAllSyncs(
    ContentEntityInterface $entity,
    array $context = []
  ) {
    // Get all syncs that define exports for the given entity's type.
    // phpcs:disable
    // @I Support caching syncs per entity type
    //    type     : improvement
    //    priority : normal
    //    labels   : export, performance
    //
    // @I Support filtering by bundle
    //    type     : task
    //    priority : low
    //    labels   : config, export, performance
    //    notes    : The entity bundle is optional in the synchronization
    //               configuration; however, if it is defined, it should be
    //               interpreted as if the export should only proceed if the
    //               bundle of the entity being exported is of the configured
    //               type. We skip the export in `exportLocalEntityForSync()`,
    //               however, we should filter out non-eligible syncs here to
    //               improve performance.
    // phpcs:enable
    $syncs = $this->configManager->getSyncs([
      'status' => TRUE,
      'local_entity' => [
        'type_id' => $entity->getEntityTypeId(),
      ],
      'operation' => [
        'id' => ActionType::EXPORT_ENTITY,
        'status' => TRUE,
      ],
    ]);
    if (!$syncs) {
      return [];
    }

    // Sort the synchronizations so that they are queued in the requested
    // way. If no queue weights are defined, they will be loaded and queued in
    // alphabetical order.
    $syncs = $this->sortSynchronizations($syncs);

    // The most common case is to call this method in a insert/update hook. At
    // that point the entity has already been saved and the entity's `isNew()`
    // method will return `FALSE`. We therefore expect to be given the original
    // entity and use that to detect whether we are responding to a local entity
    // creation or update i.e. whether we should be detecting field changes or
    // not.
    //
    // phpcs:disable
    // @I Validate the original entity passed through context
    //    type     : bug
    //    priority : low
    //    labels   : export, validation
    // @I Enable/disable field comparison via options
    //    type     : improvement
    //    priority : normal
    //    labels   : export
    // phpcs:enable
    //
    // If it is an update, detect whether we have changed fields. If the
    // operation is managed by Entity Sync for a given synchronization and there
    // are no exportable changed fields, we will be skipping the export.
    //
    // To be efficient and calculate the changed fields only when needed, we
    // could be doing this in the event subscriber that determines whether
    // queueing should be cancelled for each synchronization. However, in order
    // to not calculate the changed fields multiple times when we have many
    // synchronizations we calculate them here and we pass them to the
    // subscriber in the context.
    //
    // Moreover, we do so whether synchronizations are managed by Entity
    // Synchronization or not; providers of unmanaged synchronizations may have
    // their own custom management mechanism that might utilize this
    // information.
    //
    // phpcs:disable
    // @I Support forcing an export even if unchanged via config export mode
    //    type     : improvement
    //    priority : low
    //    labels   : config, export
    // phpcs:enable
    if (isset($context['original_entity'])) {
      $context['changed_field_names'] = $this->fieldManager->getChangedNames(
        $entity,
        $context['original_entity']
      );
    }

    $queue = [];
    foreach ($syncs as $sync) {
      $queue[$sync->id()] = $this->exportLocalEntityForSync(
        $sync,
        $entity,
        $context
      );
    }

    return $queue;
  }

  /**
   * {@inheritdoc}
   * phpcs:disable
   * @I Validate sync and operation status before queueing exports
   *    type     : bug
   *    priority : high
   *    labels   : export, validation
   *    notes    : When we queue exports via `exportLocalEntityForAllSyncs()`
   *               only enabled syncs are loaded, however, since we publicly
   *               expose this method validation should be added here.
   * phpcs:enable
   */
  public function exportLocalEntityForSync(
    SyncInterface $sync,
    ContentEntityInterface $entity,
    array $context = []
  ) {
    // Operation types might inherit export settings from parent
    // synchronizations used for defining shared settings, or for linking
    // import/export operation types. We therefore explicitly check that the
    // action type of the operation type is `export_entity`.
    // phpcs:disable
    // @I Raise an error when operation type of different action type is given
    //    type     : bug
    //    priority : low
    //    labels   : error-handling, export
    // phpcs:enable
    if ($sync instanceof OperationTypeInterface) {
      $action_type = $sync->getPluginConfiguration()['action_type'] ?? NULL;
      if ($action_type !== ActionType::EXPORT_ENTITY) {
        return NULL;
      }
    }

    // Do not proceed if queueing the exports is explicitly disabled.
    $settings = $sync->getOperationsSettings();
    if (($settings[ActionType::EXPORT_ENTITY]['queue'] ?? TRUE) === FALSE) {
      return NULL;
    }

    // Do not proceed if the entity is of a bundle different to the one defined
    // in the synchronization - if defined.
    $bundle = $sync->getLocalEntitySettings()['bundle'] ?? NULL;
    if ($bundle !== NULL && $bundle !== $entity->bundle()) {
      return NULL;
    }

    // If we don't have the changed fields provided by
    // `$this::exportLocalEntityForAllSyncs()`, calculate them here and make
    // them available in the context.
    $is_update = isset($context['original_entity']);
    if ($is_update && !isset($context['changed_field_names'])) {
      $context['changed_field_names'] = $this->fieldManager->getChangedNames(
        $entity,
        $context['original_entity']
      );
    }

    // Give the opportunity to event subscribers to cancel queueing the item.
    // Usually, we cancel items that shouldn't be exported at the time that the
    // operation is run via a pre-initiate event subscriber. However, there are
    // cases where we want to prevent queueing exports in the first place, such
    // as when entities are created or updated in large numbers but not all of
    // them should be exported. Cancelling before queueing helps to avoid
    // creating too many unnecessary queue items and operation entities.
    // Also, exports managed by Entity Synchronization are cancelled at this
    // level in the following cases:
    // - When the export is a result of an entity update, if there no exportable
    //   fields were changed as part of the update.
    // - When the export is a result of an entity creation/update that was in
    //   turn a result of a linked import.
    [$result, $stored_context] = $this->shouldCancelQueueing(
      $sync,
      $entity,
      $context
    );
    if ($result === TRUE) {
      return NULL;
    }

    $queue_id = $settings[ActionType::EXPORT_ENTITY]['queue_id'] ?? 'default';
    if (!in_array($queue_id, ['default', 'delayed_executor'])) {
      throw new \RuntimeException(sprintf(
        'Unsupported queue ID "%s".',
        $queue_id
      ));
    }

    // Currently, putting entity delete operations in the Drupal queue is not
    // supported because the entity will not exist when the queue item will be
    // processed. We therefore force queueing it in the delayed executor.
    // We haven't figured out yet what's the best way to define a different
    // queue for different context IDs i.e. entity update vs entity delete etc.
    // It would probably be through options, however options are not passed to
    // this method, context is. It needs a bit more thought.
    if (($context['id'] ?? NULL) === 'entity_delete') {
      $queue_id = 'delayed_executor';
    }

    if ($queue_id === 'delayed_executor') {
      return [$queue_id, $stored_context];
    }

    // Queue the export.
    // phpcs:disable
    // @I Support exporting the diff between specific revisions
    //    type     : improvement
    //    priority : normal
    //    labels   : export
    //    notes    : If the entity being exported is revisioned, we can store
    //               in the queue item the specific revisions and export the
    //               fields changed between those revisions.
    // phpcs:enable
    $this->createQueueItem(
      $sync,
      $entity,
      Queue::EXPORT_LOCAL_ENTITY,
      $stored_context
    );

    return [$queue_id, $stored_context];
  }

  /**
   * Sorts the synchronizations being queued.
   *
   * The synchronizations are sorted based on the `queue_weight` setting on the
   * `export_entity` operation. The weight defaults to 0 for synchronizations
   * that the setting is not set.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface[] $syncs
   *   The synchronizations being queued.
   *
   * @return \Drupal\entity_sync\Entity\SyncInterface[]
   *   The sorted synchronizations.
   */
  protected function sortSynchronizations(array $syncs) {
    uasort(
      $syncs,
      function ($a, $b) {
        $a_weight = $a->getOperationsSettings()[ActionType::EXPORT_ENTITY]['queue_weight'] ?? 0;
        $b_weight = $b->getOperationsSettings()[ActionType::EXPORT_ENTITY]['queue_weight'] ?? 0;

        if ($a_weight === $b_weight) {
          return 0;
        }

        return ($a_weight < $b_weight) ? -1 : 1;
      }
    );

    return $syncs;
  }

  /**
   * Gives the opportunity to subscribers to respond to and/or cancel queueing.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type that defines the operation that
   *   we are queueing.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being queued for export.
   * @param array $context
   *   An associative array containing the queueing context. See
   *   `\Drupal\entity_sync\Queue\ManagerInterface::exportLocalEntityForAllSyncs()`.
   *
   * @return array
   *   A numerical array containing the following items:
   *   - `TRUE` when queueing the operation should be cancelled, `FALSE`
   *     otherwise.
   *   - An array containing any context elements that should be stored in the
   *     queue item data.
   */
  protected function shouldCancelQueueing(
    SyncInterface $sync,
    ContentEntityInterface $entity,
    array $context
  ) {
    $stored_context = [];
    if (isset($context['id'])) {
      $stored_context['id'] = $context['id'];
    }
    $event = new QueueOperationEvent(
      ActionType::EXPORT_ENTITY,
      $context + ['local_entity' => $entity],
      $sync,
      $stored_context
    );
    $this->eventDispatcher->dispatch($event, Events::LOCAL_ENTITY_QUEUE);

    [$cancel, $messages] = $event->getCancellations();
    if (!$cancel) {
      return [FALSE, $event->getStoredContext()];
    }

    foreach ($messages as $message) {
      $this->logger->warning(
        sprintf(
          'Queueing the "%s" operation for the synchronization with ID "%s" and for the entity with ID "%s" was cancelled with message: %s',
          ActionType::EXPORT_ENTITY,
          $sync->id(),
          $entity->id(),
          $message
        )
      );
    }

    return [TRUE, $event->getStoredContext()];
  }

  /**
   * Creates a queue item for the given data.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type entity that defines the operation
   *   that will be queued.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that the operation relates to.
   * @param string $queue
   *   The queue to add the item to.
   * @param array $stored_context
   *   An associative array containing any context elements to store in the
   *   queue item data.
   */
  protected function createQueueItem(
    SyncInterface $sync,
    ContentEntityInterface $entity,
    string $queue,
    array $stored_context
  ) {
    if ($sync->getEntityTypeId() === EntityType::OPERATION_TYPE) {
      $this->queueFactory
        ->get($queue)
        ->createItem(
          $this->buildOperationQueueItem($sync, $entity, $stored_context)
        );
      return;
    }

    $this->queueFactory
      ->get($queue)
      ->createItem(
        $this->buildSynchronizationQueueItem($sync, $entity, $stored_context)
      );
  }

  /**
   * Creates a queue item for the given operation type and entity.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The Operation Type entity that defines the operation that will be queued.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that the operation relates to.
   * @param array $stored_context
   *   An associative array containing any context elements to store in the
   *   queue item data.
   *
   * @return array
   *   An associative array containing the data that the queue item requires for
   *   its processing.
   */
  protected function buildOperationQueueItem(
    OperationTypeInterface $operation_type,
    ContentEntityInterface $entity,
    array $stored_context
  ) {
    $storage = $this->entityTypeManager->getStorage(EntityType::OPERATION);
    $operation = $storage->create([
      OperationField::TYPE => $operation_type->id(),
      OperationField::ENTITY => $entity,
    ]);

    if (!$operation->hasField(OperationField::ENTITY)) {
      throw new \RuntimeException(sprintf(
        'Operations of type "%s" do not have a field for storing the related entity.',
        $operation_type->id()
      ));
    }

    $storage->save($operation);

    return [
      'sync_id' => $operation_type->id(),
      'operation_id' => $operation->id(),
      'context' => $stored_context,
    ];
  }

  /**
   * Creates a queue item for the given synchronization and entity.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization entity that defines the operation that will be
   *   queued.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity that the operation relates to.
   * @param array $stored_context
   *   An associative array containing any context elements to store in the
   *   queue item data.
   *
   * @return array
   *   An associative array containing the data that the queue item requires for
   *   its processing.
   */
  protected function buildSynchronizationQueueItem(
    SyncInterface $sync,
    ContentEntityInterface $entity,
    array $stored_context
  ) {
    return [
      'sync_id' => $sync->id(),
      'entity_type_id' => $entity->getEntityTypeId(),
      'entity_id' => $entity->id(),
      'context' => $stored_context,
    ];
  }

}
