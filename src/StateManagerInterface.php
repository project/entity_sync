<?php

namespace Drupal\entity_sync;

/**
 * Defines the interface for Entity Sync state managers.
 *
 * The Entity Sync state manager keeps track of the state of a synchronization
 * operation. Currently, it tracks when an operation was last run.
 *
 * phpcs:disable
 * @I Add status tracking to operation states that can be used as a lock
 *    type     : feature
 *    priority : normal
 *    labels   : operation, state
 *
 * @I Review the design of the state manager
 *    type     : task
 *    priority : low
 *    labels   : architecture
 *    notes    : Currently the state manager is only needed to manage import
 *               remote list operations. Review the design so that it allows to
 *               be used by other operations in the future allowing the
 *               operations to store custom data.
 *
 * @I Move the state manager to the `\Drupal\entity_sync\State` namespace
 *    type     : task
 *    priority : low
 *    labels   : structure
 * phpcs:enable
 */
interface StateManagerInterface {

  /**
   * Indicates that an operation is locked.
   */
  const LOCKED = 1;

  /**
   * Indicates that an operation is unlocked.
   */
  const UNLOCKED = 0;

  /**
   * Gets the state of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   *
   * @return array
   *   An associative array containing the properties of the state. Currently
   *   supported properties are:
   *   - last_run: The time that the operation was last run in Unix-timestamp
   *     format.
   */
  public function get($sync_id, $operation);

  /**
   * Returns whether the operation is managed.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   *
   * @return bool
   *   TRUE if the operation is managed, FALSE otherwise.
   */
  public function isManaged($sync_id, $operation);

  /**
   * Returns whether the operation is locked.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   *
   * @return bool
   *   TRUE if the operation is locked, FALSE otherwise.
   */
  public function isLocked($sync_id, $operation);

  /**
   * Locks the given operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   */
  public function lock($sync_id, $operation);

  /**
   * Unlocks the given operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   */
  public function unlock($sync_id, $operation);

  /**
   * Gets the state of the last run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   *
   * @return array
   *   An associative array containing information related to the last run of
   *   the operation.
   *   - (int) run_time
   *     The time that the operation was last run in Unix-timestamp format.
   *   - (int) start_time
   *     The start time of the filters' time span that was used to fetch the
   *     list of remote entities.
   *   - (int) end_time
   *     The end time of the filters' time span that was used to fetch the list
   *     of remote entities. It can be used on the next run so that the import
   *     can continue from where it left off.
   *   - (int) limit
   *     The limit given to the last run i.e. maximum number of entities to
   *     process.
   *   - (int) offset
   *     The offset that was used to fetch the list of remote entities.
   *   - (int) next_offset
   *     The offset that the next run will need to use in order to continue from
   *     where the last run stopped.
   *   - (array) data
   *     An associative array containing custom data required by additional
   *     features provided by the Entity Synchronization module or other
   *     providers.
   */
  public function getLastRun($sync_id, $operation);

  /**
   * Sets the state of the last run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   * @param int $run_time
   *   The time that the operation was last run in Unix-timestamp format.
   * @param int|null $start_time
   *   The start time of the filters' time span that was used to fetch the list
   *   of remote entities.
   * @param int|null $end_time
   *   The end time of the filters' time span that was used to fetch the list of
   *   remote entities.
   * @param int|null $limit
   *   The limit given to the last run i.e. maximum number of entities to
   *   process.
   * @param int|null $offset
   *   The offset that was used to fetch the list of remote entities.
   * @param int|null $next_offset
   *   The offset that the next run will need to use in order to continue from
   *   where the last run stopped. `NULL` should be given if the last run
   *   processed all entities.
   * @param array $data
   *   An associative array containing custom data required by additional
   *   features provided by the Entity Synchronization module or other
   *   providers. Note that this is meant to store scalar values that represent
   *   the state of the operation; if you extend this scope to store other data
   *   be careful to store values that can be cleanly serialized for the purpose
   *   of storing them into the database.
   */
  public function setLastRun(
    $sync_id,
    $operation,
    $run_time,
    $start_time = NULL,
    $end_time = NULL,
    $limit = NULL,
    $offset = NULL,
    $next_offset = NULL,
    array $data = NULL
  );

  /**
   * Gets the state of the requested item of the last run of the operation.
   *
   * For available items see `$this::getLastRun()`.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   * @param string $key
   *   The key of the item to get.
   *
   * @return mixed
   *   The value of the item, or `NULL` if the item is not defined.
   */
  public function getLastRunItem($sync_id, $operation, $key);

  /**
   * Sets the state of the requested item of the last run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   * @param int $key
   *   The key of the item to set.
   * @param mixed $item_value
   *   The value of the item.
   */
  public function setLastRunItem($sync_id, $operation, $key, $item_value);

  /**
   * Unsets the state of the last run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   */
  public function unsetLastRun($sync_id, $operation);

  /**
   * Gets the state of the current run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   *
   * @return array
   *   An associative array containing information related to the current run of
   *   the operation.
   *   - (int) run_time
   *     The time that the operation started running in Unix-timestamp format.
   *   - (int) start_time
   *     The start time of the filters' time span that was used to fetch the
   *     list of remote entities.
   *   - (int) end_time
   *     The end time of the filters' time span that was used to fetch the list
   *     of remote entities.
   *   - (int) limit
   *     The limit given to the current run i.e. maximum number of entities to
   *     process.
   *   - (int) offset
   *     The offset that was used to fetch the list of remote entities.
   *   - (array) data
   *     An associative array containing custom data required by additional
   *     features provided by the Entity Synchronization module or other
   *     providers.
   */
  public function getCurrentRun($sync_id, $operation);

  /**
   * Sets the state of the current run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   * @param int $run_time
   *   The time that the operation was last run in Unix-timestamp format.
   * @param int|null $start_time
   *   The start time of the filters' time span that was used to fetch the list
   *   of remote entities.
   * @param int|null $end_time
   *   The end time of the filters' time span that was used to fetch the list of
   *   remote entities.
   * @param int|null $limit
   *   The limit given to the current run i.e. maximum number of entities to
   *   process.
   * @param int|null $offset
   *   The offset that was used to fetch the list of remote entities.
   * @param array $data
   *   An associative array containing custom data required by additional
   *   features provided by the Entity Synchronization module or other
   *   providers. Note that this is meant to store scalar values that represent
   *   the state of the operation; if you extend this scope to store other data
   *   be careful to store values that can be cleanly serialized for the purpose
   *   of storing them into the database.
   */
  public function setCurrentRun(
    $sync_id,
    $operation,
    $run_time,
    $start_time = NULL,
    $end_time = NULL,
    $limit = NULL,
    $offset = NULL,
    array $data = NULL
  );

  /**
   * Gets the state of the requested item of the current run of the operation.
   *
   * For available items see `$this::getCurrentRun()`.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   * @param string $key
   *   The key of the item to get.
   *
   * @return mixed
   *   The value of the item, or `NULL` if the item is not defined.
   */
  public function getCurrentRunItem($sync_id, $operation, $key);

  /**
   * Sets the state of the requested item of the current run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   * @param int $key
   *   The key of the item to set.
   * @param mixed $item_value
   *   The value of the item.
   */
  public function setCurrentRunItem($sync_id, $operation, $key, $item_value);

  /**
   * Unsets the state of the current run of the operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   */
  public function unsetCurrentRun($sync_id, $operation);

  /**
   * Deletes the state of the given synchronization operation.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   * @param string $operation
   *   The name of the the operation.
   */
  public function delete($sync_id, $operation);

}
