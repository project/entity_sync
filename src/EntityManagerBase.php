<?php

namespace Drupal\entity_sync;

use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Event\InitiateOperationEvent;
use Drupal\entity_sync\Event\PostTerminateOperationEvent;
use Drupal\entity_sync\Event\PreInitiateOperationEvent;
use Drupal\entity_sync\Event\TerminateOperationEvent;

use Drupal\entity_sync\Import\Event\Events as ImportEvents;
use Drupal\entity_sync\Export\Event\Events as ExportEvents;

/**
 * Base class for the import/export entity managers.
 */
class EntityManagerBase {

  /**
   * Checks that the given operation is enabled for the given synchronization.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param string $operation
   *   The operation to check.
   *
   * @return bool
   *   TRUE if the operation is enabled and supported, FALSE otherwise.
   */
  protected function operationSupported(SyncInterface $sync, $operation) {
    // phpcs:disable
    // @I Check that the provider supports the corresponding method as well
    //    type     : bug
    //    priority : normal
    //    labels   : operation, validation
    // phpcs:enable
    $settings = $sync->getOperationsSettings();
    if (empty($settings[$operation]['status'])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Dispatches an event before an operation is initiated.
   *
   * The `PreInitiateOperationEvent` event allows subscribers to set the
   * operation to be cancelled. This method collects any cancellations, logs
   * their messages, and returns a boolean indicating whether the operation
   * should be cancelled or not so that the caller can act accordingly.
   *
   * @param string $event_name
   *   The name of the event to dispatch. It must be a name for a
   *   `PreInitiateOperationEvent` event.
   * @param string $operation
   *   The name of the operation that will be initiated.
   * @param array $context
   *   The context of the operation we are currently executing.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $data
   *   Custom data related to the operation.
   *
   * @return bool
   *   Whether the operation should be cancelled or not.
   */
  protected function preInitiate(
    $event_name,
    $operation,
    array $context,
    SyncInterface $sync,
    array $data = []
  ) {
    $event = new PreInitiateOperationEvent(
      $operation,
      $context,
      $sync,
      $data
    );
    $this->eventDispatcher->dispatch($event, $event_name);

    $cancel = $messages = NULL;
    [$cancel, $messages] = $event->getCancellations();
    if (!$cancel) {
      return FALSE;
    }

    // Do not log cancellation messages if we are told so.
    $settings = $sync->getOperationsSettings()[$operation] ?? [];
    if (($settings['log_cancellations'] ?? TRUE) === FALSE) {
      return TRUE;
    }

    foreach ($messages as $message) {
      $this->logger->warning(
        sprintf(
          'The %s operation for the synchronization with ID "%s" was cancelled with message: %s',
          $operation,
          $sync->id(),
          $message
        )
      );
    }

    return TRUE;
  }

  /**
   * Dispatches an event when an operation is being initiated.
   *
   * @param string $event_name
   *   The name of the event to dispatch. It must be a name for a
   *   `InitiateOperationEvent` event.
   * @param string $operation
   *   The name of the operation being initiated.
   * @param array $context
   *   The context of the operation we are currently executing.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $data
   *   Custom data related to the operation.
   */
  protected function initiate(
    $event_name,
    $operation,
    array $context,
    SyncInterface $sync,
    array $data = []
  ) {
    $event = new InitiateOperationEvent(
      $operation,
      $context,
      $sync,
      $data
    );
    $this->eventDispatcher->dispatch($event, $event_name);
  }

  /**
   * Dispatches an event when an operation is being terminated.
   *
   * @param string $event_name
   *   The name of the event to dispatch. It must be a name for a
   *   `TerminateOperationEvent` event.
   * @param string $operation
   *   The name of the operation being terminated.
   * @param array $context
   *   The context of the operation we are currently executing.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $data
   *   Custom data related to the operation.
   */
  protected function terminate(
    $event_name,
    $operation,
    array $context,
    SyncInterface $sync,
    array $data = []
  ) {
    $event = new TerminateOperationEvent(
      $operation,
      $context,
      $sync,
      $data
    );
    $this->eventDispatcher->dispatch($event, $event_name);

    // If we are instructed by event subsribers to save the local entity, do
    // so.
    // See \Drupal\entity_sync\Event::$saveLocalEntity.
    // See \Drupal\entity_sync\EntityManagerBase::terminateGetLocalEntity().
    if (!$event->getSaveLocalEntity()) {
      return;
    }
    $local_entity = $this->terminateGetLocalEntity($event, $event_name);
    if (!$local_entity) {
      return;
    }

    $local_entity->save();
  }

  /**
   * Dispatches an event after an operation terminated.
   *
   * @param string $event_name
   *   The name of the event to dispatch. It must be a name for a
   *   `PostTerminateOperationEvent` event.
   * @param string $operation
   *   The name of the operation that terminated.
   * @param array $context
   *   The context of the operation we are currently executing.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $data
   *   Custom data related to the operation.
   * phpcs:disable
   * @I Inform post-terminate subscribers whether the operation succeeded
   *    type     : improvement
   *    priority : normal
   *    labels   : event, import, operation
   * phpcs:enable
   */
  protected function postTerminate(
    $event_name,
    $operation,
    array $context,
    SyncInterface $sync,
    array $data = []
  ) {
    $event = new PostTerminateOperationEvent(
      $operation,
      $context,
      $sync,
      $data
    );
    $this->eventDispatcher->dispatch($event, $event_name);
  }

  /**
   * Gets the local entity for the given terminate event.
   *
   * It retrieves the local entity from the operation context or result data
   * depending on the event type.
   *
   * Right now it applies to import operations only, even though it could be
   * implemented for export operations as well.
   *
   * It does not apply to list operations e.g. import a remote list of entities,
   * because currently we don't have the local entities available in the list
   * terminate event.
   *
   * @param \Drupal\entity_sync\Event\TerminateOperationEvent $event
   *   The terminate event.
   * @param string $event_name
   *   The event name.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The local entity or NULL if none available for the given event type.
   * phpcs:disable
   * @I Support saving the local entity for export terminate events
   *    type     : feature
   *    priority : normal
   *    labels   : event, export
   * @I Support saving the local entities for import list terminate events
   *    type     : feature
   *    priority : low
   *    labels   : event, import
   * phpcs:enable
   */
  protected function terminateGetLocalEntity(
    TerminateOperationEvent $event,
    string $event_name
  ) {
    switch ($event_name) {
      case ImportEvents::REMOTE_ENTITY_TERMINATE:
        return $event->getData()['local_entity'];

      case ImportEvents::LOCAL_ENTITY_TERMINATE:
      case ExportEvents::LOCAL_ENTITY_TERMINATE:
        return $event->getContext()['local_entity'];
    }
  }

}
