<?php

namespace Drupal\entity_sync\OperationConfigurator;

use Drupal\entity_sync\Annotation\EntitySyncOperationConfigurator as PluginAnnotation;
use Drupal\entity_sync\Entity\OperationTypeInterface;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The plugin manager for operation configurator plugins.
 *
 * @see \Drupal\entity_sync\Annotation\EntitySyncOperationConfigurator
 * @see \Drupal\entity_sync\OperationConfigurator\PluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager implements
  PluginManagerInterface {

  /**
   * Constructs a new PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/EntitySync/OperationConfigurator',
      $namespaces,
      $module_handler,
      PluginInterface::class,
      PluginAnnotation::class
    );

    $this->alterInfo('entity_sync_operation_configurator_info');
    $this->setCacheBackend(
      $cache_backend,
      'entity_sync_operation_configurator',
      ['entity_sync_operation_configurator_plugins']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createInstanceForOperationType(
    OperationTypeInterface $operation_type
  ) {
    return $this->createInstance(
      $operation_type->getPluginId(),
      $operation_type->getPluginConfiguration() ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getAllLabels() {
    $plugins = [];

    foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
      $plugins[$plugin_id] = $plugin_definition['label'];
    }
    asort($plugins);

    return $plugins;
  }

}
