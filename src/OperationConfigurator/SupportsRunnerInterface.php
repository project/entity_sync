<?php

namespace Drupal\entity_sync\OperationConfigurator;

/**
 * The interface for plugins that define a runner.
 *
 * @see \Drupal\entity_sync\Entity\Runner\RunnerInterface
 */
interface SupportsRunnerInterface {

  /**
   * Returns the runner for operations of types that use this plugins.
   *
   * @return \Drupal\entity_sync\Entity\Runner\RunnerInterface
   *   The operation runner.
   */
  public function runner();

}
