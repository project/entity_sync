<?php

namespace Drupal\entity_sync\OperationConfigurator;

use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\Component\Plugin\PluginManagerInterface as ComponentPluginManagerInterface;

/**
 * Defines the interface for the operation configuration plugin manager.
 */
interface PluginManagerInterface extends ComponentPluginManagerInterface {

  /**
   * Creates an instance based on the plugin info of the given operation type.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type.
   *
   * @return \Drupal\entity_sync\OperationConfiguration\PluginInterface
   *   The plugin.
   */
  public function createInstanceForOperationType(
    OperationTypeInterface $operation_type
  );

  /**
   * Gets all plugin labels.
   *
   * @return array
   *   Returns an array of all operation configurator plugin labels keyed by
   *   plugin ID.
   */
  public function getAllLabels();

}
