<?php

namespace Drupal\entity_sync\OperationConfigurator\FieldTrait;

use Drupal\entity_sync\Field\BundleFieldDefinition;

/**
 * Trait for plugins that require a file field.
 *
 * Machine names for fields must be unique across all different bundles. Custom
 * or contrib modules outside of the official Entity Synchronization module(s)
 * should always be prefixing their fields (see
 * `\Drupal\entity_sync\OperationConfigurator::bundleFieldDefinition()`), making
 * it more difficult to reuse such fields.
 *
 * The best way to promote sharing field storages that - in general - is good
 * for performance is to provide here some commonly used fields with reasonable
 * storage settings. We do that by providing traits that custom/contrib
 * configurator plugins can use to define their field.
 *
 * This trait provides file fields using either the public or the private file
 * system. Most other settings can be configured per bundle in the UI after the
 * field storage is created.
 */
trait FileTrait {

  /**
   * Provides a field definition for a private file field.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single field configured to use the default private file system.
   */
  protected function privateFileBundleFieldDefinitions() {
    return $this->fileBundleFieldDefinitions('private_file', 'private');
  }

  /**
   * Provides a field definition for a public file field.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single field configured to use the default public file system.
   */
  protected function publicFileBundleFieldDefinitions() {
    return $this->fileBundleFieldDefinitions('public_file', 'public');
  }

  /**
   * Provides a field definition for a file field.
   *
   * @param string $field_name
   *   The machine name of the field to define.
   * @param string $uri_scheme
   *   The URI scheme to use.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single field configured based on the given machine name and URI scheme.
   */
  protected function fileBundleFieldDefinitions(
    string $field_name,
    string $uri_scheme
  ) {
    $fields = [];

    $fields[$field_name] = BundleFieldDefinition::create('file')
      ->setLabel($this->t('File'))
      ->setDescription($this->t('The file.'))
      ->setCardinality(1)
      ->setSetting('uri_scheme', $uri_scheme)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
