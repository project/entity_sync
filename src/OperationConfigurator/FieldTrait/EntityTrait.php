<?php

namespace Drupal\entity_sync\OperationConfigurator\FieldTrait;

use Drupal\entity_sync\Field\BundleFieldDefinition;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;

/**
 * Trait for plugins the operations of which relate to a single entity.
 *
 * The plugin provides a field for referencing the entity that the operation
 * relates to e.g. a new entity created during an import operation or an
 * existing entity updated during an import or exported during an export
 * operation.
 *
 * The field could be used by different plugins to reference entities of
 * different type. Therefore we use a `dynamic_entity_reference` field type to
 * facilitate that.
 */
trait EntityTrait {

  /**
   * Provides a field definition for referencing an entity.
   *
   * phpcs:disable
   * @I Make the field uneditable after the operation has run
   *    type     : bug
   *    priority : normal
   *    labels   : operation, ux
   * phpcs:enable
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single field for referencing a Drupal entity.
   */
  protected function buildEntityBundleFieldDefinitions() {
    $fields = [];

    $fields[OperationField::ENTITY] = BundleFieldDefinition::create('dynamic_entity_reference')
      ->setLabel($this->t('The entity'))
      ->setDescription($this->t('The entity that the operation relates to.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
