<?php

namespace Drupal\entity_sync\OperationConfigurator\FieldTrait;

use Drupal\entity_sync\Field\BundleFieldDefinition;

/**
 * Trait for plugins that require a string field.
 *
 * Machine names for fields must be unique across all different bundles. Custom
 * or contrib modules outside of the official Entity Synchronization module(s)
 * should always be prefixing their fields (see
 * `\Drupal\entity_sync\OperationConfigurator::bundleFieldDefinition()`), making
 * it more difficult to reuse such fields.
 *
 * The best way to promote sharing field storages that - in general - is good
 * for performance is to provide here some commonly used fields with reasonable
 * storage settings. We do that by providing traits that custom/contrib
 * configurator plugins can use to define their field.
 *
 * This trait provides string fields with default settings.
 */
trait StringTrait {

  /**
   * Provides a field definition for a string.
   *
   * Non-storage settings, such as the field label, can be customized per
   * bundle.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single string field with the default storage settings provided by the
   *   field type.
   */
  protected function stringBundleFieldDefinitions() {
    $fields = [];

    $fields['string'] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('String'))
      ->setDescription($this->t('The string (plain text).'))
      ->setCardinality(1)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
