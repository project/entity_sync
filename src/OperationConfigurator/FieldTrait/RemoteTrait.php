<?php

namespace Drupal\entity_sync\OperationConfigurator\FieldTrait;

use Drupal\entity_sync\Field\BundleFieldDefinition;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;

/**
 * Trait for plugins the operations of which relate to a remote operation.
 *
 * Some remote systems track the operations on their side as well. This trait
 * facilitates integration with such systems by providing fields for storing the
 * remote ID and the remote state of the operation.
 *
 * This can be the case, for example, for asynchronous operations.
 *
 * @see \Drupal\entity_sync_async\OperationConfigurator\EntityAsyncBase
 */
trait RemoteTrait {

  /**
   * Provides a field definition for storing the remote ID of the operation.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single field for the remote ID of the operation.
   */
  protected function buildRemoteIdBundleFieldDefinitions() {
    $fields = [];

    $fields[OperationField::REMOTE_ID] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Remote ID'))
      ->setDescription($this->t(
        'The ID of the operation in the remote system.'
      ))
      // Normally this is programmatically populated.
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Provides a field definition for storing the remote state of the operation.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name, containing a
   *   single field for the remote state of the operation.
   */
  protected function buildRemoteStateBundleFieldDefinitions() {
    $fields = [];

    $fields[OperationField::REMOTE_STATE] = BundleFieldDefinition::create('string')
      ->setLabel($this->t('Remote status'))
      ->setDescription($this->t(
        'The status of the operation in the remote system.'
      ))
      // Normally this is programmatically populated.
      ->setRequired(TRUE)
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
