<?php

namespace Drupal\entity_sync\OperationConfigurator;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase as CorePluginBase;

/**
 * Default base class for operation configurator plugin implementations.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface {

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    if (!isset($plugin_definition['action_types'])) {
      throw new InvalidConfigurationException(sprintf(
        'No supported action types defined by plugin with ID "%s".',
        $plugin_id
      ));
    }

    // Always include the default configuration.
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => [$this->pluginDefinition['provider']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // The type of action (e.g. `import_list` or `export_entity` etc.) that
      // operations of types configured by this plugin should perform when run.
      'action_type' => NULL,
      // Synchronization settings.
      'local_entity' => [],
      'remote_resource' => [],
      'operations' => [],
      'field_mapping' => [],
      // Settings that determine UI-related features.
      'ui' => [
        // Enable/disable creating/updating operations via the UI.
        'allow_create' => TRUE,
        'allow_update' => TRUE,
        // Selected transitions will not be executable manually via the state
        // transition form. This can be used for transitions that are only meant
        // to be executed programmatically.
        'disallowed_transitions' => [
          'complete',
          'fail',
        ],
        // Operations that are in one of the selected states will get the
        // default form disabled.
        'uneditable_states' => [
          'running',
          'completed',
          'failed',
        ],
      ],
      // Plugin-specific configuration.
      'plugin' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['#tree'] = TRUE;

    // phpcs:disable
    // @I Disable action type element if there are existing operations
    //    type     : bug
    //    priority : normal
    //    labels   : action, configurator
    // phpcs:enable
    $allowed_types = $this->pluginDefinition['action_types'];
    $allowed_types = array_intersect_key(
      [
        'import_list' => $this->t('Import a list of entities'),
        'import_entity' => $this->t('Import a single entity'),
        'export_list' => $this->t('Export a list of entities'),
        'export_entity' => $this->t('Export a single entity'),
      ],
      array_combine($allowed_types, $allowed_types)
    );
    $form['action_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Action type'),
      '#description' => $this->t(
        'The type of action that will be taken for operations of this type e.g.
         import or export an entity or a list of entities. It must not be
         changed after operations have been created for this type.'
      ),
      '#options' => $allowed_types,
      '#default_value' => $this->configuration['action_type'],
      '#required' => TRUE,
    ];

    $form = $this->buildUiConfigurationForm($form, $form_state);

    // Plugin-specific configuration.
    // Plugins are responsible for building this part of the form.
    $form['plugin'] = [
      '#type' => 'container',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * Override this function if you need to do something specific to the
   * submitted data before it is saved as configuration on the plugin. The data
   * gets saved on the plugin in the operation type form.
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    $this->configuration['action_type'] = $form_state->getValue('action_type');
    $this->submitUiConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Builds the form elements for UI-related settings.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  protected function buildUiConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form['ui'] = [
      '#type' => 'details',
      '#title' => $this->t('UI settings'),
      '#open' => FALSE,
    ];

    $form = $this->buildUiDisallowedTransitionsConfigurationForm(
      $form,
      $form_state
    );
    $form = $this->buildUiUneditableStatesConfigurationForm(
      $form,
      $form_state
    );

    return $form;
  }

  /**
   * Builds the form element for the disallowed transitions.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  protected function buildUiDisallowedTransitionsConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    // For now, we intentionally not inject the workflow manager in the
    // constructor. We will be adding this as a lazy service so that it is not
    // injected for every plugin instantiated.
    $transitions = \Drupal::service('plugin.manager.workflow')
      ->createInstance($this->getPluginDefinition()['workflow_id'])
      ->getTransitions();
    $options = array_map(
      function ($transition) {
        return $transition->getLabel();
      },
      $transitions
    );

    $default_value = [];
    if (!empty($this->configuration['ui']['disallowed_transitions'])) {
      $default_value = $this->configuration['ui']['disallowed_transitions'];
    }
    $form['ui']['disallowed_transitions'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Disallowed transitions'),
      '#description' => $this->t(
        'Selected transitions will not be available in the state transition form.
         This feature can be used to prevent manual executions via the UI of
         transitions that should only be executed programmatically.'
      ),
      '#options' => $options,
      '#default_value' => $default_value,
    ];

    return $form;
  }

  /**
   * Builds the form element for the uneditable states.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated form render array.
   */
  protected function buildUiUneditableStatesConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    // For now, we intentionally not inject the workflow manager in the
    // constructor. We will be adding this as a lazy service so that it is not
    // injected for every plugin instantiated.
    $states = \Drupal::service('plugin.manager.workflow')
      ->createInstance($this->getPluginDefinition()['workflow_id'])
      ->getStates();
    $options = array_map(
      function ($state) {
        return $state->getLabel();
      },
      $states
    );

    $default_value = [];
    if (!empty($this->configuration['ui']['uneditable_states'])) {
      $default_value = $this->configuration['ui']['uneditable_states'];
    }
    $form['ui']['uneditable_states'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Uneditable states'),
      '#description' => $this->t(
        'Operations that are in one of the selected states will not be editable
         via the default form. This feature can be used to prevent operations
         from being edited after they have been executed. Only disabling
         specific fields is not supported via this configuration form; it
         requires custom code.'
      ),
      '#options' => $options,
      '#default_value' => $default_value,
    ];

    return $form;
  }

  /**
   * Saves submitted data for UI-related settings.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitUiConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
    // Avoid errors for plugins that were last saved before these settings were
    // introduced.
    if (!isset($this->configuration['ui'])) {
      $this->configuration['ui'] = [];
    }

    $this->configuration['ui']['disallowed_transitions'] = array_values(
      array_filter($form_state->getValue('ui')['disallowed_transitions'])
    );
    $this->configuration['ui']['uneditable_states'] = array_values(
      array_filter($form_state->getValue('ui')['uneditable_states'])
    );
  }

}
