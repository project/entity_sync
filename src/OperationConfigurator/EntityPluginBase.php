<?php

namespace Drupal\entity_sync\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait;

/**
 * Base class for plugins the operations of which relate to a single entity.
 *
 * Abstract plugin classes are provided for easily creating simple plugin
 * implementations with minimal code. Using traits directly is recommended for
 * complex implementations.
 *
 * @see \Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait
 */
abstract class EntityPluginBase extends PluginBase {

  use EntityTrait;

  /**
   * {@inheritdoc}
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();

    // Add the dynamic entity reference field.
    $fields += $this->buildEntityBundleFieldDefinitions();

    return $fields;
  }

}
