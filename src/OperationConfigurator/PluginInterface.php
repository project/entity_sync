<?php

namespace Drupal\entity_sync\OperationConfigurator;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides the interface for operation configurator plugins.
 *
 * Operation configurator plugins are responsible for configuring the behavior
 * of operation types.
 */
interface PluginInterface extends
  ConfigurableInterface,
  DependentPluginInterface,
  PluginFormInterface,
  PluginInspectionInterface {

  /**
   * Gets the plugin label.
   *
   * @return string
   *   The operation configurator label.
   */
  public function label();

  /**
   * Provides the field definitions for operation types that use this plugin.
   *
   * As with all Drupal entities, field machine names must always be unique
   * across all operation types. Currently, there is no programmatic detection
   * of conflicts between fields provided by different plugins.
   *
   * It is therefore recommended to leave unprefixed field machine names for the
   * exclusive use of official Entity Synchronization modules. Use an
   * appropriate prefix for fields provided by other contrib or custom modules,
   * such as the name of the module or the plugin ID.
   *
   * @return \Drupal\entity_sync\Field\BundleFieldDefinition[]
   *   An array of bundle field definitions, keyed by field name.
   */
  public function bundleFieldDefinitions();

}
