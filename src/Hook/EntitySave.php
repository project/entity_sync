<?php

namespace Drupal\entity_sync\Hook;

// Drupal modules.
use Drupal\entity_sync\DelayedExecutor\Executor as DelayedExecutor;
use Drupal\entity_sync\Queue\ManagerInterface as QueueManagerInterface;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Holds methods implementing entity save/delete hooks.
 */
class EntitySave {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The delayed executor.
   *
   * @var \Drupal\entity_sync\DelayedExecutor
   */
  protected $delayedExecutor;

  /**
   * The entity export manager.
   *
   * @var \Drupal\entity_sync\Export\EntityManagerInterface
   */
  protected $queueManager;

  /**
   * Constructs a new EntitySave object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\entity_sync\DelayedExecutor\Executor $delayed_executor
   *   The Entity Synchronization delayed executor.
   * @param \Drupal\entity_sync\Queue\ManagerInterface $queue_manager
   *   The Entity Synchronization queue manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    DelayedExecutor $delayed_executor,
    QueueManagerInterface $queue_manager
  ) {
    $this->configFactory = $config_factory;
    $this->delayedExecutor = $delayed_executor;
    $this->queueManager = $queue_manager;
  }

  /**
   * Processes or schedules entity export queueing according to configuration.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being exported.
   * @param array $context
   *   The context of the entity export operation.
   *
   * @see entity_sync.schema.yml::entity_sync.settings
   */
  public function exportLocalEntityForAllSyncs(
    ContentEntityInterface $entity,
    array $context
  ) {
    if (!isset($context['id'])) {
      throw new \InvalidArgumentException('The context ID must be provided');
    }

    $delay = FALSE;
    $config = $this->configFactory->get('entity_sync.settings');
    $property = "state_manager.export_entity.{$context['id']}.delayed_executor";
    if ($config !== NULL && (($config->get($property) ?? FALSE) === TRUE)) {
      $delay = TRUE;
    }

    if ($delay === FALSE) {
      $this->process($entity, $context);
      return;
    }

    $this->delayedExecutor->addQueueExportLocalEntityForAllSync(
      $entity,
      $context
    );
  }

  /**
   * Processes entity export queueing according to configuration.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity being exported.
   * @param array $context
   *   The context of the entity export operation.
   *
   * @see entity_sync.schema.yml::entity_sync.settings
   */
  protected function process(
    ContentEntityInterface $entity,
    array $context
  ) {
    $queue = $this->queueManager->exportLocalEntityForAllSyncs(
      $entity,
      $context
    );

    foreach ($queue as $sync_id => $queue_item) {
      // Queueing cancelled.
      if ($queue_item === NULL) {
        continue;
      }

      // Queueing for all other queues are handled by the queue manager.
      if ($queue_item[0] !== 'delayed_executor') {
        continue;
      }

      $this->delayedExecutor->addExportLocalEntity(
        $sync_id,
        $entity,
        $queue_item[1]
      );
    }
  }

}
