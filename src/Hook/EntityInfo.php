<?php

namespace Drupal\entity_sync\Hook;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Contains methods implementing hooks related to entity information.
 */
class EntityInfo {

  /**
   * The operation configurator plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $configuratorPluginManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new EntityInfo object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $configurator_plugin_manager
   *   The operation configurator plugin manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    PluginManagerInterface $configurator_plugin_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configuratorPluginManager = $configurator_plugin_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @see hook_entity_bundle_info()
   */
  public function bundleInfo() {
    $is_translatable = $this->entityTypeManager
      ->getDefinition('entity_sync_operation_type')
      ->isTranslatable();

    $bundles = array_reduce(
      $this->configuratorPluginManager->getDefinitions(),
      function ($bundles, $definition) use ($is_translatable) {
        $bundles[$definition['id']] = [
          'label' => $definition['label'],
          'description' => $definition['description'] ?? '',
          'translatable' => $is_translatable,
          'provider' => $definition['provider'],
        ];
        return $bundles;
      },
      []
    );

    return ['entity_sync_operation_type' => $bundles];
  }

}
