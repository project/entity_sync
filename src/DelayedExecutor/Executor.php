<?php

namespace Drupal\entity_sync\DelayedExecutor;

// Drupal modules.
use Drupal\entity_sync\Export\EntityManagerInterface as ExportManagerInterface;
use Drupal\entity_sync\Queue\ManagerInterface as QueueManagerInterface;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
// Third-party libraries.
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Runs tasks and operations at the end of requests.
 *
 * For queueing tasks in the delayed executor to have any meaningful effect, the
 * web server must support `fastcgi_finish_request`. Otherwise the response will
 * not be streamed to the browser until the tasks have been executed, providing
 * no performance improvements.
 *
 * @link https://symfony.com/doc/current/components/http_kernel.html#8-the-kernel-terminate-event
 */
class Executor implements EventSubscriberInterface {

  /**
   * A numerical array containing the items to execute.
   *
   * Each array item is an associative array with the following items:
   * - type (string, required): The type of the task. Supported tasks are:
   *   - exportLocalEntity: exports an individual entity.
   *   - queueExportLocalEntityForAllSyncs: queues the export of an individual
   *     entity for all relevant synchronizations.
   * - arguments (array, required): The arguments to pass to the method that
   *   will execute the task.
   *
   * @var array
   */
  protected $items = [];

  /**
   * Constructs a new Executor object.
   *
   * @param \Drupal\entity_sync\Export\EntityManagerInterface $export_manager
   *   The Entity Synchronization entity export manager.
   * @param \Drupal\entity_sync\Queue\ManagerInterface $queue_manager
   *   The Entity Synchronization queue manager.
   */
  public function __construct(
    ExportManagerInterface $export_manager,
    QueueManagerInterface $queue_manager
  ) {
    $this->exportManager = $export_manager;
    $this->queueManager = $queue_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::TERMINATE][] = ['execute'];
    return $events;
  }

  /**
   * Executes all items in the internal queue.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The kernel terminate event.
   *
   * @throws \InvalidArgumentException
   *   When an unsupported item type is provided.
   * phpcs:disable
   * @I Support error handling modes
   *    type     : improvement
   *    priority : normal
   *    labels   : delayed-executor
   * phpcs:enable
   */
  public function execute(TerminateEvent $event) {
    $supported_types = [
      'exportLocalEntity',
      'queueExportLocalEntityForAllSyncs',
    ];
    foreach ($this->items as $item) {
      if (!in_array($item['type'], $supported_types, TRUE)) {
        $this->logger->error(sprintf(
          'Unknown queue item type "%s".',
          $item['type'])
        );
        continue;
      }

      $this->{$item['type']}(...$item['arguments']);
    }
  }

  /**
   * Adds a task for queueing an individual export for all relevant syncs.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $context
   *   The context of the operation.
   */
  public function addQueueExportLocalEntityForAllSync(
    ContentEntityInterface $entity,
    array $context = []
  ) {
    $this->items[] = [
      'type' => 'queueExportLocalEntityForAllSyncs',
      'arguments' => [
        $entity,
        $context,
      ],
    ];
  }

  /**
   * Adds a task for exporting an individual entity for the given sync.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $context
   *   The context of the operation.
   */
  public function addExportLocalEntity(
    string $sync_id,
    ContentEntityInterface $entity,
    array $context = []
  ) {
    $this->items[] = [
      'type' => 'exportLocalEntity',
      'arguments' => [
        $sync_id,
        $entity,
        $context,
      ],
    ];
  }

  /**
   * Queues the export of the given entity for all relevant syncs.
   *
   * Note that the type of the queue that will execute the actual export is
   * defined by the synchronization. If that is the Drupal queue, then a queue
   * item will be created and it will processed upon queue run. If the queue
   * type is the delayed executor, the export will be processed here.
   *
   * We could create another event subscriber that will hold the queued
   * operations and run them just after this one, but there's not much point in
   * doing that so we run the operations here.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $context
   *   The context of the operation.
   */
  protected function queueExportLocalEntityForAllSyncs(
    ContentEntityInterface $entity,
    array $context = []
  ) {
    $queue = $this->queueManager->exportLocalEntityForAllSyncs(
      $entity,
      $context
    );

    foreach ($queue as $sync_id => $queue_item) {
      // Queueing cancelled.
      if ($queue_item === NULL) {
        continue;
      }

      // Queueing for all other queues are handled by the queue manager.
      if ($queue_item[0] !== 'delayed_executor') {
        continue;
      }

      $options = ['context' => $queue_item[1]];
      $this->exportLocalEntity(
        $sync_id,
        $entity,
        $options
      );
    }
  }

  /**
   * Exports an individual entity for the given sync.
   *
   * @param string $sync_id
   *   The ID of the synchronization.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity.
   * @param array $context
   *   The context of the operation.
   */
  protected function exportLocalEntity(
    string $sync_id,
    ContentEntityInterface $entity,
    array $context
  ) {

    $this->exportManager->exportLocalEntity(
      $sync_id,
      $entity,
      ['context' => $context]
    );
  }

}
