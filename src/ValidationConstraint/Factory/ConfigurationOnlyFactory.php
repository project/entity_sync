<?php

namespace Drupal\entity_sync\ValidationConstraint\Factory;

use Drupal\Component\Plugin\Factory\DefaultFactory;

/**
 * Plugin factory which passes only the configuration to the plugin class.
 *
 * We use a lightweight version of Drupal plugins in order to benefit from
 * discovery etc. but all validation constraint plugins just extend Symfony
 * validation constraint classes. Those receive any options as an array in the
 * first argument. Since Symfony version 6, there are additional arguments and
 * passing the plugin ID and the plugin definition breaks them. We therefore
 * only pass the plugin configuration as the first argument, which contains the
 * constraint options array.
 */
class ConfigurationOnlyFactory extends DefaultFactory {

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->discovery->getDefinition($plugin_id);
    $plugin_class = static::getPluginClass(
      $plugin_id,
      $plugin_definition,
      $this->interface
    );
    return new $plugin_class($configuration);
  }

}
