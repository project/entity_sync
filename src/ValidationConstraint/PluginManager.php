<?php

namespace Drupal\entity_sync\ValidationConstraint;

use Drupal\entity_sync\Annotation\EntitySyncValidationConstraint as PluginAnnotation;
use Drupal\entity_sync\ValidationConstraint\Factory\ConfigurationOnlyFactory;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The plugin manager for validation constraint plugins.
 *
 * @see \Drupal\entity_sync\Annotation\EntitySyncValidationConstraint
 * @see \Drupal\entity_sync\Validation\PluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager {

  /**
   * Constructs a new PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/EntitySync/ValidationConstraint',
      $namespaces,
      $module_handler,
      NULL,
      PluginAnnotation::class
    );

    $this->alterInfo('entity_sync_validation_constraint_info');
    $this->setCacheBackend(
      $cache_backend,
      'entity_sync_validation_constraint',
      ['entity_sync_validation_constraint_plugins']
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFactory() {
    if (!$this->factory) {
      $this->factory = new ConfigurationOnlyFactory(
        $this,
        $this->pluginInterface
      );
    }

    return $this->factory;
  }

}
