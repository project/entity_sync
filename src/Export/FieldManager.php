<?php

namespace Drupal\entity_sync\Export;

use Drupal\entity_sync\Config\ManagerInterface as ConfigManagerInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Exception\ComputedValueException;
use Drupal\entity_sync\Exception\FieldExportException;
use Drupal\entity_sync\Exception\SkipFieldException;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\Event\FieldMappingEvent;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Field\FieldItemInterface;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The default field manager.
 */
class FieldManager implements FieldManagerInterface {

  /**
   * The Entity Sync configuration manager.
   *
   * @var \Drupal\entity_sync\Config\ManagerInterface
   */
  protected $configManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The field transformer plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $fieldTransformerManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new FieldManager instance.
   *
   * @param \Drupal\entity_sync\Config\ManagerInterface $config_manager
   *   The Entity Sync configuration manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $field_transformer_manager
   *   The field transformer plugin manager.
   */
  public function __construct(
    ConfigManagerInterface $config_manager,
    EventDispatcherInterface $event_dispatcher,
    LoggerInterface $logger,
    PluginManagerInterface $field_transformer_manager
  ) {
    $this->configManager = $config_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->logger = $logger;
    $this->fieldTransformerManager = $field_transformer_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function export(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    SyncInterface $sync,
    array $options = []
  ) {
    // Build the field mapping for the fields that will be exported.
    // phpcs:disable
    // @I Validate the final field mapping
    //    type     : bug
    //    priority : normal
    //    labels   : export, mapping, validation
    // phpcs:enable
    $field_mapping = $this->fieldMapping(
      $local_entity,
      $remote_entity_id,
      $sync
    );

    // If the field mapping is empty we will not be exporting any fields.
    if (!$field_mapping) {
      return [];
    }

    return $this->doExport(
      $local_entity,
      $remote_entity_id,
      $sync,
      $field_mapping,
      $options['context'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getExportableChangedNames(
    ContentEntityInterface $changed_entity,
    ContentEntityInterface $original_entity,
    array $field_mapping,
    array $names_filter = NULL,
    array $changed_names = NULL
  ) {
    if ($names_filter === [] || $changed_names === []) {
      return [];
    }

    $exportable_names = $this->getExportableNames($field_mapping);

    if ($changed_names === NULL) {
      $changed_names = $this->getChangedNames(
        $changed_entity,
        $original_entity,
        $names_filter
      );
    }

    if ($names_filter) {
      return array_intersect(
        $exportable_names,
        $changed_names,
        $names_filter
      );
    }

    return array_intersect(
      $exportable_names,
      $changed_names
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedNames(
    ContentEntityInterface $changed_entity,
    ContentEntityInterface $original_entity,
    array $names_filter = NULL
  ) {
    $changed_names = [];

    foreach ($changed_entity->getFields() as $field_name => $field) {
      if ($names_filter !== NULL && !in_array($field_name, $names_filter)) {
        continue;
      }

      $original_field = $original_entity->get($field_name);
      // In some cases, an item list might contain an item with a value that is
      // considered empty, and it is compared with the other list that is empty.
      // The `equals` method will not consider those as equal. Until that is
      // fixed in core, check for emptiness here.
      if ($field->isEmpty() && $original_field->isEmpty()) {
        continue;
      }
      $unchanged = $field->equals($original_field);
      if ($unchanged) {
        continue;
      }

      $changed_names[] = $field_name;
    }

    return $changed_names;
  }

  /**
   * {@inheritdoc}
   */
  public function getExportableNames(array $field_mapping) {
    $exportable_names = [];

    foreach ($field_mapping as $field_info) {
      // phpcs:disable
      // @I Merge default export field mapping when configuration is loaded
      //    type     : improvement
      //    priority : normal
      //    labels   : config, export
      // phpcs:enable
      $field_info = $this->configManager->mergeExportFieldMappingDefaults(
        $field_info
      );
      if (!$field_info['export']['status']) {
        continue;
      }

      // We may not always have a local field. For example, the field
      // transformer or callback may prepares a value for a remote field from
      // multiple local fields, from a computed field, from another resource or
      // using an algorithm.
      // phpcs:disable
      // @I Add options for determining export behavior for computed fields
      //    type     : improvement
      //    priority : normal
      //    labels   : config, export
      //    notes    : Normally, an entity will be exported only if the module
      //               detects that at least one exportable field has been
      //               changed. Add options for always exporting, always
      //               skipping, for depending on other fields, or for custom
      //               logic.
      // phpcs:enable
      if (!array_key_exists('machine_name', $field_info)) {
        continue;
      }

      $exportable_names[] = $field_info['machine_name'];
    }

    return $exportable_names;
  }

  /**
   * Builds and returns the field mapping for the given entities.
   *
   * The field mapping defines which remote entity fields will be updated with
   * which values contained in the given local entity. The default mapping is
   * defined in the synchronization to which the operation we are currently
   * executing belongs.
   *
   * An event is dispatched that allows subscribers to alter the default field
   * mapping.
   *
   * @param \Drupal\core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   *
   * @return array
   *   The final field mapping.
   * phpcs:disable
   * @I Validate the final field mapping
   *    type     : bug
   *    priority : normal
   *    labels   : export, mapping, validation
   * phpcs:enable
   */
  protected function fieldMapping(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    SyncInterface $sync
  ) {
    $event = new FieldMappingEvent(
      $local_entity,
      $remote_entity_id,
      $sync
    );
    $this->eventDispatcher->dispatch($event, Events::FIELD_MAPPING);

    // Return the final mappings.
    return $event->getFieldMapping();
  }

  /**
   * Does the actual export of the fields for the given entities.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $field_mapping
   *   The field mapping.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return array
   *   An associative array containing the remote fields, keyed by the field
   *   name and containing the field value.
   *
   * @throws \Drupal\entity_sync\Exception\FieldExportException
   *   When an error occurs while exporting a field.
   */
  protected function doExport(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    SyncInterface $sync,
    array $field_mapping,
    array $context
  ) {
    $context['computed_values'] = $this->calculateComputedValues(
      $local_entity,
      $remote_entity_id,
      $sync,
      $context
    );

    $fields = [];
    foreach ($field_mapping as $field_info) {
      $field_info = $this->configManager
        ->mergeExportFieldMappingDefaults($field_info);
      if (!$field_info['export']['status']) {
        continue;
      }
      if ($field_info['export']['skip'] === TRUE) {
        continue;
      }

      try {
        $fields[$field_info['remote_name']] = $this->doExportField(
          $local_entity,
          $remote_entity_id,
          $field_info,
          $sync,
          $context
        );
      }
      // If we are instructed to skip the field, we do so but continue with the
      // rest.
      catch (SkipFieldException $exception) {
        continue;
      }
      catch (\Throwable $throwable) {
        $this->throwExportFieldException(
          $local_entity,
          $remote_entity_id,
          $field_info,
          $sync,
          $throwable
        );
      }
    }

    return $fields;
  }

  /**
   * Performs the actual export of a local field to a remote field.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return string|array|null
   *   The exported value for the field, or NULL if the field is empty.
   */
  protected function doExportField(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    SyncInterface $sync,
    array $context
  ) {
    // If the field value should be converted and exported by a custom callback,
    // then invoke that.
    if ($field_info['export']['callback'] ?? []) {
      return call_user_func(
        $field_info['export']['callback']['callable'],
        $local_entity,
        $remote_entity_id,
        $field_info,
        $field_info['export']['callback']['parameters'] ?? []
      );
    }
    // If the field value should be converted by a plugin, invoke that.
    elseif ($field_info['export']['transformer'] ?? []) {
      return $this->fieldTransformerManager
        ->createInstance(
          $field_info['export']['transformer']['id'],
          $field_info['export']['transformer']['configuration'] ?? []
        )
        ->export(
          $local_entity,
          $remote_entity_id,
          $field_info,
          $context
        );
    }
    // Else, we assume direct copy of the remote field value into the local
    // field.
    //
    // phpcs:disable
    // @I Fallback to the `copy_value` plugin
    //    type     : task
    //    priority : low
    //    labels   : export, field-handler
    // @I Add more details about the field mapping in the exception message
    //    type     : task
    //    priority : low
    //    labels   : error-handling, export
    // @I Provide the option to continue the entity export when a field fails
    //    type     : improvement
    //    priority : normal
    //    labels   : error-handling, export
    // @I Implement log levels e.g. error, warning, info, debug
    //    type     : feature
    //    priority : normal
    //    labels   : error-handling, export
    // @I Support configuration entities
    //    type     : feature
    //    priority : normal
    //    labels   : export
    //    notes    : Configuration entities do not have a function that checks
    //               whether a property exists and that case has to be handled
    //               differently.
    // phpcs:enable
    elseif (!$local_entity->hasField($field_info['machine_name'])) {
      throw new \RuntimeException(
        sprintf(
          'The non-existing local entity field "%s" was requested to be mapped to a remote field',
          $field_info['machine_name']
        )
      );
    }

    // We  copy the value when the field exists even if it is NULL.
    $field = $local_entity->get($field_info['machine_name']);

    if ($field->isEmpty()) {
      return;
    }

    $value = [];

    foreach ($field->filterEmptyItems() as $index => $field_item) {
      // The field item value is stored as an array of properties. However, the
      // majority of the fields have only one main property and the majority of
      // the remote resources expect that value directly. We therefore return
      // that value directly by default; if a remote resource expects the value
      // as an array, that can be handled by a callback.
      //
      // phpcs:disable
      // @I Provide export field callback that returns the value in array format
      //    type     : improvement
      //    priority : low
      //    labels   : export, field
      // @I Consider moving the no-callback logic to a default callback
      //    type     : improvement
      //    priority : low
      //    labels   : export, field
      // phpcs:enable
      $main_property_name = $this->getFieldMainPropertyName($field_item);
      $value[$index] = $field_item->getValue()[$main_property_name];
    }

    // If a field is a single-cardinality field we export a single value. If a
    // field is a multiple-cardinality field we export an array of values - even
    // when there is only one field item.
    $cardinality = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();
    return $cardinality === 1 ? current($value) : $value;
  }

  /**
   * Calculates the computed values.
   *
   * Computed values are calculated similarly to how field values are; they
   * are just calculated in advance and are made available to the field
   * transformers. The most common use case is to calculate a value only once
   * and then make it available in the pipelines of multiple fields.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return array
   *   An associative array keyed by each value's key (`computed_name` property
   *   of the settings item) and containing the computed value.
   */
  protected function calculateComputedValues(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    SyncInterface $sync,
    array $context
  ) {
    $values = [];
    $context['computed_values'] = [];
    foreach (($sync->getComputedValuesSettings() ?? []) as $value_info) {
      $value_info = $this->configManager
        ->mergeExportComputedValueDefaults($value_info);
      if (!$value_info['export']['status']) {
        continue;
      }

      $key = $value_info['computed_name'];
      try {
        $values[$key] = $this->doExportField(
          $local_entity,
          $remote_entity_id,
          $value_info,
          $sync,
          $context
        );
      }
      // If we are instructed to skip the value, we do so but continue with the
      // rest. Since transformer plugins do not know whether they are
      // transforming a value for a field or for a computed value, we do not
      // have an exception specific to skipping computed values.
      catch (SkipFieldException $exception) {
        continue;
      }
      catch (\Throwable $throwable) {
        $this->throwComputedValueException(
          $local_entity,
          $remote_entity_id,
          $value_info,
          $sync,
          $throwable
        );
      }

      // Make the computed value available to subsequent computations.
      $context['computed_values'][$key] = $values[$key];
    }

    return $values;
  }

  /**
   * Throws a wrapper exception for exceptions thrown during field export.
   *
   * We do this so that we can recognize as `FieldExportException` any exception
   * that may be thrown during field export runtime.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param \Throwable $throwable
   *   The exception that was thrown during field export.
   */
  protected function throwExportFieldException(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    SyncInterface $sync,
    \Throwable $throwable
  ) {
    $local_entity_text = 'a new local entity';
    if (!$local_entity->isNew()) {
      $local_entity_text = sprintf(
        'the local entity with ID "%s"',
        $local_entity->id()
      );
    }
    $remote_entity_text = 'a new remote entity';
    if ($remote_entity_id !== NULL) {
      $remote_entity_text = sprintf(
        'the remote entity with ID "%s"',
        $remote_entity_id
      );
    }

    throw new FieldExportException(
      sprintf(
        '"%s" exception was thrown while exporting the "%s" field of the local entity with ID "%s" into the "%s" field of %s. The error message was: %s. The field mapping was: %s',
        get_class($throwable),
        $field_info['machine_name'] ?? '',
        $local_entity_text,
        $field_info['remote_name'],
        $remote_entity_text,
        $throwable->getMessage(),
        json_encode($field_info)
      )
    );
  }

  /**
   * Throws a wrapper exception for exceptions thrown during value calculations.
   *
   * We do this so that we can recognize as a `ComputedValueException` any
   * exception that may be thrown during calculating computed values.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The associated local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param array $value_info
   *   The computed value info.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The Synchronization or Operation Type configuration entity that defines
   *   the operation we are currently executing.
   * @param \Throwable $throwable
   *   The exception that was thrown during computed value calculation.
   *
   * @throws \Drupal\entity_sync\Exception\ComputedValueException
   *   Always.
   */
  protected function throwComputedValueException(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $value_info,
    SyncInterface $sync,
    \Throwable $throwable
  ) {
    $local_entity_text = 'a new local entity';
    if (!$local_entity->isNew()) {
      $local_entity_text = sprintf(
        'the local entity with ID "%s"',
        $local_entity->id()
      );
    }
    $remote_entity_text = 'a new remote entity';
    if ($remote_entity_id !== NULL) {
      $remote_entity_text = sprintf(
        'the remote entity with ID "%s"',
        $remote_entity_id
      );
    }

    throw new ComputedValueException(
      sprintf(
        '"%s" exception was thrown while calculating the "%s" computed value for %s while being exported to %s. The error message was: %s. The field mapping was: %s',
        get_class($throwable),
        $value_info['computed_name'] ?? '',
        $local_entity_text,
        $remote_entity_text,
        $throwable->getMessage(),
        json_encode($value_info)
      )
    );
  }

  /**
   * Returns the main property name for the given field item.
   *
   * This method is made available just so that we can bypass limitations of
   * mocking static methods by mocking this class.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $field_item
   *   The field item.
   *
   * @return string
   *   The name of the field item's main property.
   */
  protected function getFieldMainPropertyName(FieldItemInterface $field_item) {
    return $field_item::mainPropertyName();
  }

}
