<?php

namespace Drupal\entity_sync\Export;

// Drupal modules.
use Drupal\entity_sync\Client\ClientFactory;
use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Export\Event\Events;
use Drupal\entity_sync\Export\Event\LocalEntityMappingEvent;
use Drupal\entity_sync\EntityManagerBase;
use Drupal\entity_sync\Queue\ManagerInterface as QueueManagerInterface;
// Drupal core.
use Drupal\Core\Entity\ContentEntityInterface;
// External libraries.
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The default export manager.
 */
class EntityManager extends EntityManagerBase implements
  EntityManagerInterface {

  /**
   * The client factory.
   *
   * @var \Drupal\entity_sync\Client\ClientFactory
   */
  protected $clientFactory;

  /**
   * The config entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * The Entity Sync export field manager.
   *
   * @var \Drupal\entity_sync\Export\FieldManagerInterface
   */
  protected $fieldManager;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Entity Synchronization queue manager.
   *
   * @var \Drupal\entity_sync\Queue\ManagerInterface
   */
  protected $queueManager;

  /**
   * Constructs a new EntityManager instance.
   *
   * @param \Drupal\entity_sync\Client\ClientFactory $client_factory
   *   The client factory.
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The config entity loader.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\entity_sync\Export\FieldManagerInterface $field_manager
   *   The Entity Sync export field manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\entity_sync\Queue\ManagerInterface $queue_manager
   *   The Entity Synchronization queue manager.
   */
  public function __construct(
    ClientFactory $client_factory,
    ConfigEntityLoaderInterface $config_entity_loader,
    EventDispatcherInterface $event_dispatcher,
    FieldManagerInterface $field_manager,
    LoggerInterface $logger,
    QueueManagerInterface $queue_manager
  ) {
    $this->clientFactory = $client_factory;
    $this->configEntityLoader = $config_entity_loader;
    $this->eventDispatcher = $event_dispatcher;
    $this->fieldManager = $field_manager;
    $this->logger = $logger;
    $this->queueManager = $queue_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function exportLocalListByEntities(
    $sync_id,
    array $local_entities,
    array $options = []
  ) {
    // Load the sync.
    // phpcs:disable
    // @I Validate the sync/operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the provider supports the `export_list` operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the given local entities are of the correct type
    //    type     : bug
    //    priority : normal
    //    labels   : export, operation, sync, validation
    // phpcs:enable
    $sync = $this->configEntityLoader->load($sync_id);

    // Make sure the operation is enabled and supported by the provider.
    if (!$this->operationSupported($sync, 'export_list')) {
      $this->logger->error(
        sprintf(
          'The synchronization with ID "%s" and/or its provider do not support the `export_list` operation.',
          $sync_id
        )
      );
      return;
    }

    $context = $options['context'] ?? [];

    // Notify subscribers that the operation is about to be initiated.
    // phpcs:disable
    // @I Write tests for operation cancellations
    //    type     : task
    //    priority : high
    //    labels   : export, testing
    // phpcs:enable
    $cancel = $this->preInitiate(
      Events::LOCAL_LIST_PRE_INITIATE,
      'export_list',
      ['local_entities' => $local_entities] + $context,
      $sync
    );
    if ($cancel) {
      return;
    }

    // Run the operation.
    // We do this in a `try/finally` structure so that we can still dispatch the
    // post-terminate event. Subscribers may still need to run whether the
    // operation was successfull or not. For example, even if a managed
    // operation failed we unlock it so that the next one is allowed to run.
    // At the end, the error/exception is still thrown so that the caller can
    // handle it as required.
    try {
      $this->doExportLocalList($sync, $local_entities, $options, $context);
    }
    finally {
      // Notify subscribers that the operation has terminated.
      $this->postTerminate(
        Events::LOCAL_LIST_POST_TERMINATE,
        'export_list',
        ['local_entities' => $local_entities] + $context,
        $sync
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function exportLocalEntity(
    $sync_id,
    ContentEntityInterface $local_entity,
    array $options = []
  ) {
    // Load the sync.
    // phpcs:disable
    // @I Validate the sync/operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the provider supports the `export_entity` operation
    //    type     : bug
    //    priority : normal
    //    labels   : operation, sync, validation
    //    notes    : Review whether the validation should happen upon runtime
    //               i.e. here, or when the configuration is created/imported.
    // @I Validate that the given local entity is of the correct type
    //    type     : bug
    //    priority : normal
    //    labels   : export, operation, sync, validation
    // phpcs:enable
    $sync = $this->configEntityLoader->load($sync_id);

    // Make sure the operation is enabled and supported by the provider.
    if (!$this->operationSupported($sync, 'export_entity')) {
      $this->logger->error(
        sprintf(
          'The synchronization with ID "%s" and/or its provider do not support the `export_entity` operation.',
          $sync_id
        )
      );
      return;
    }

    $this->wrapDoExportLocalEntity($sync, $local_entity, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function queueExportLocalEntityAllSyncs(
    ContentEntityInterface $entity,
    array $context = []
  ) {
    @trigger_error(
      __METHOD__ . '() is deprecated in entity_sync:4.0.0-beta2 and is removed from entity_sync:6.0.0. Use the \Drupal\entity_sync\Queue\ManagerInterface::expportLocalEntityForAllSyncs() method instead.',
      E_USER_DEPRECATED
    );
    $this->queueManager->exportLocalEntityForAllSyncs($entity, $context);
  }

  /**
   * Runs the actual local entity export operation.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $local_entities
   *   The local entities.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalListByEntities().
   * @param array $context
   *   An associative array containing of context related to the circumstances
   *   of the operation. See
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalListByEntities().
   */
  protected function doExportLocalList(
    SyncInterface $sync,
    array $local_entities,
    array $options,
    array $context
  ) {
    // Initiate the operation.
    $this->initiate(
      Events::LOCAL_LIST_INITIATE,
      'export_list',
      ['local_entities' => $local_entities] + $context,
      $sync
    );

    // Go through each local entity and create or update the corresponding
    // remote entity.
    foreach ($local_entities as $local_entity) {
      $this->tryWrapDoExportLocalEntity(
        $local_entity,
        $sync,
        ['parent_operation' => 'export_local_list'] + $options,
        'export_list'
      );
    }

    // Terminate the operation.
    $this->terminate(
      Events::LOCAL_LIST_TERMINATE,
      'export_list',
      ['local_entities' => $local_entities] + $context,
      $sync
    );
  }

  /**
   * Runs the actual local entity export operation with its pre/post events.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see the operations that we may be running:
   *   - \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalListByEntities()
   *   - \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity()
   */
  protected function wrapDoExportLocalEntity(
    SyncInterface $sync,
    ContentEntityInterface $local_entity,
    array $options
  ) {
    $context = $options['context'] ?? [];
    $context['local_entity'] = $local_entity;

    // Notify subscribers that the operation is about to be initiated.
    // phpcs:disable
    // @I Write tests for operation cancellations
    //    type     : task
    //    priority : high
    //    labels   : import, testing
    // phpcs:enable
    $cancel = $this->preInitiate(
      Events::LOCAL_ENTITY_PRE_INITIATE,
      'export_entity',
      $context,
      $sync
    );
    if ($cancel) {
      return;
    }

    // Run the operation.
    // We do this in a `try/finally` structure so that we can still dispatch the
    // post-terminate event. Subscribers may still need to run whether the
    // operation was successfull or not. For example, even if a managed
    // operation failed we unlock it so that the next one is allowed to run.
    // At the end, the error/exception is still thrown so that the caller can
    // handle it as required.
    try {
      $this->doExportLocalEntity($sync, $local_entity, $options, $context);
    }
    finally {
      // Notify subscribers that the operation has terminated.
      $this->postTerminate(
        Events::LOCAL_ENTITY_POST_TERMINATE,
        'export_entity',
        $context,
        $sync
      );
    }
  }

  /**
   * Runs the actual local entity export operation.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity().
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation. See
   *   \Drupal\entity_sync\Export\ManagerInterface::exportLocalEntity().
   */
  protected function doExportLocalEntity(
    SyncInterface $sync,
    ContentEntityInterface $local_entity,
    array $options,
    array $context
  ) {
    // Initiate the operation.
    $this->initiate(
      Events::LOCAL_ENTITY_INITIATE,
      'export_entity',
      $context,
      $sync
    );

    // Export the entity.
    $data = $this->runAction($local_entity, $sync, $options);

    // Terminate the operation.
    // Add to the context the local entity that was exported.
    $this->terminate(
      Events::LOCAL_ENTITY_TERMINATE,
      'export_entity',
      $context,
      $sync,
      $data ?? []
    );
  }

  /**
   * Exports the entity without halting execution if an exception is thrown.
   *
   * An error is logged instead; the caller may then continue with exporting the
   * next entity, if there is one.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   import. For supported options see the operations that we may be running:
   *   - \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalListByEntities()
   *   - \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity()
   * @param string $operation
   *   The operation that is doing the import; used for logging purposes.
   */
  protected function tryWrapDoExportLocalEntity(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $options,
    $operation
  ) {
    try {
      $this->wrapDoExportLocalEntity($sync, $local_entity, $options);
    }
    catch (\Exception $e) {
      $this->logger->error(
        sprintf(
          'An "%s" exception was thrown while exporting the local entity with ID "%s" as part of the "%s" synchronization and the "%s" operation. The error message was: %s',
          get_class($e),
          $local_entity->id(),
          $sync->id(),
          $operation,
          $e->getMessage()
        )
      );
    }
  }

  /**
   * Export the changes contained in the given local entity to a remote entity.
   *
   * If an associated remote entity is identified, the remote entity will be
   * updated. A new remote entity will be created otherwise.
   *
   * If we have a two-dimensional array, we are exporting to multiple remote
   * entities. Unlike the event subscriber that are provided for setting the
   * default entity mapping and the subscriber that updates the sync fields at
   * the end of an export, we do not use the field cardinality to detect whether
   * we are exporting to multiple remote entities. This is because the code here
   * is agnostic to the mapping method i.e. mapping to multiple remotes might be
   * determined by logic that is not based on fields. We therefore accept an
   * array of entity mappings as a generic mechanism.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity().
   *
   * @return array
   *   An array containing information about the result.
   *
   * @see $this::createOrUpdate()
   */
  protected function runAction(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $options
  ) {
    // Build the entity mapping for this local entity.
    $entity_mapping = $this->localEntityMapping(
      $local_entity,
      $sync,
      $options['context'] ?? []
    );

    // If we have a flat array (single-dimensional), empty or not, we are
    // exporting to a single remote entity.
    if (empty($entity_mapping) || isset($entity_mapping['action'])) {
      return $this->createOrUpdate(
        $local_entity,
        $sync,
        $options,
        $entity_mapping
      );
    }

    // Otherwise we have a two-dimensional array; execute all exports one by one
    // and return the results in an array. For compatibility reasons, we cannot
    // return the results of a single export in the same format i.e.
    // two-dimensional array; event subscribers expect the results of a single
    // export as a flat array.
    $data = [];
    foreach ($entity_mapping as $item) {
      $data[] = $this->createOrUpdate(
        $local_entity,
        $sync,
        $options,
        $item
      );
    }

    return $data;
  }

  /**
   * Export the changes contained in the given local entity to a remote entity.
   *
   * If an associated remote entity is identified, the remote entity will be
   * updated. A new remote entity will be created otherwise.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity().
   * @param array $entity_mapping
   *   The entity mapping.
   *
   * @return array
   *   An array containing information about the result.
   * phpcs:disable
   * @I Document the expected result array for different action types
   *    type     : task
   *    priority : low
   *    labels   : documentation, export
   * phpcs:enable
   */
  protected function createOrUpdate(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $options,
    array $entity_mapping
  ) {
    // If the entity mapping is empty we will not be updating or creating a
    // remote entity; nothing to do. Still, event subscribers may require an
    // action so we set it to skip.
    if (!$entity_mapping) {
      return ['action' => EntityManagerInterface::ACTION_SKIP];
    }

    $data = ['action' => $entity_mapping['action']];

    // Skip exporting the local entity if we are explicitly told to do so.
    if ($entity_mapping['action'] === EntityManagerInterface::ACTION_SKIP) {
      return $data;
    }
    elseif ($entity_mapping['action'] === EntityManagerInterface::ACTION_CREATE) {
      $data['response'] = $this->create(
        $local_entity,
        $sync,
        $entity_mapping,
        $options
      );
      return $data;
    }
    elseif ($entity_mapping['action'] === EntityManagerInterface::ACTION_UPDATE) {
      $data['response'] = $this->update(
        $local_entity,
        $sync,
        $entity_mapping,
        $options
      );
      return $data;
    }
    elseif ($entity_mapping['action'] === EntityManagerInterface::ACTION_DELETE) {
      $data['response'] = $this->delete(
        $local_entity,
        $sync,
        $entity_mapping,
        $options
      );
      return $data;
    }
    else {
      throw new \RuntimeException(
        sprintf(
          'Unsupported entity mapping action "%s"',
          $entity_mapping['action']
        )
      );
    }
  }

  /**
   * Export the changes from the given local entity to a new remote entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $entity_mapping
   *   An associative array containing information about the remote entity being
   *   mapped to the given local entity.
   *   See \Drupal\entity_sync\Export\Event\LocalEntityMapping::entityMapping.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity().
   *
   * @return mixed|null
   *   The response from the client, or NULL if the operation was not run.
   */
  protected function create(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $entity_mapping,
    array $options
  ) {
    // Prepare the fields to create on the remote object.
    // phpcs:disable
    // @I Pass context to the field manager
    //    type     : improvement
    //    priority : normal
    //    labels   : field, export
    // phpcs:enable
    $remote_fields = $this->fieldManager->export(
      $local_entity,
      NULL,
      $sync,
      $options
    );

    $client_options = $options['client'] ?? [];
    if (!$client_options) {
      $client_options = $sync->getRemoteResourceSettings()['client']['options'] ?? [];
    }

    // Do the export i.e. call the client.
    return $this->clientFactory
      ->getByClientConfig($entity_mapping['client'])
      ->create($remote_fields, $client_options);
  }

  /**
   * Export the changes from the given local entity to the remote entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $entity_mapping
   *   An associative array containing information about the remote entity being
   *   mapped to the given local entity.
   *   See \Drupal\entity_sync\Export\Event\LocalEntityMapping::entityMapping.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity().
   *
   * @return mixed|null
   *   The response from the client, or NULL if the operation was not run.
   * phpcs:disable
   * @I Support entity update validation
   *    type     : bug
   *    priority : normal
   *    labels   : export, validation
   * @I Check if the changes have already been exported
   *    type     : improvement
   *    priority : normal
   *    labels   : export, validation
   * phpcs:enable
   */
  protected function update(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $entity_mapping,
    array $options
  ) {
    // Prepare the fields to update on the remote object.
    // phpcs:disable
    // @I Pass context to the field manager
    //    type     : improvement
    //    priority : normal
    //    labels   : field, export
    // phpcs:enable
    $remote_fields = $this->fieldManager->export(
      $local_entity,
      $entity_mapping['id'],
      $sync,
      $options
    );

    $client_options = $options['client'] ?? [];
    if (!$client_options) {
      $client_options = $sync->getRemoteResourceSettings()['client']['options'] ?? [];
    }

    // Do the export i.e. call the client.
    return $this->clientFactory
      ->getByClientConfig($entity_mapping['client'])
      ->update($entity_mapping['id'], $remote_fields, $client_options);
  }

  /**
   * Export the local entity changes as a deletion of a remote entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $entity_mapping
   *   An associative array containing information about the remote entity being
   *   mapped to the given local entity.
   *   See \Drupal\entity_sync\Export\Event\LocalEntityMapping::entityMapping.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. For supported options see
   *   \Drupal\entity_sync\Export\EntityManagerInterface::exportLocalEntity().
   *
   * @return mixed|null
   *   The response from the client, or NULL if the operation was not run.
   */
  protected function delete(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $entity_mapping,
    array $options
  ) {
    $client_options = $options['client'] ?? [];
    if (!$client_options) {
      $client_options = $sync->getRemoteResourceSettings()['client']['options'] ?? [];
    }

    // Do the export i.e. call the client.
    return $this->clientFactory
      ->getByClientConfig($entity_mapping['client'])
      ->delete($entity_mapping['id'], $client_options);
  }

  /**
   * Builds and returns the remote ID for the given local entity.
   *
   * The local entity mapping defines if and which remote entity this local
   * entity will be exported to. The default mapping identifies the remote
   * entity based on a local entity field containing the remote entity's ID.
   *
   * An event is dispatched that allows subscribers to map the local entity to a
   * different remote entity, or to decide to not export it at all.
   *
   * @param \Drupal\core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The configuration object for synchronization that defines the operation
   *   we are currently executing.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation. See
   *   \Drupal\entity_sync\Export\ManagerInterface::exportLocalEntity().
   *
   * @return array
   *   The final entity mapping.
   */
  protected function localEntityMapping(
    ContentEntityInterface $local_entity,
    SyncInterface $sync,
    array $context
  ) {
    $event = new LocalEntityMappingEvent(
      $local_entity,
      $sync,
      $context
    );
    $this->eventDispatcher->dispatch($event, Events::LOCAL_ENTITY_MAPPING);

    // Return the final mapping.
    return $event->getEntityMapping();
  }

}
