<?php

namespace Drupal\entity_sync\Export;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Defines the interface for the export manager.
 *
 * The export manager is responsible for all export operations, that is,
 * operations that export entities from the local to the remote resource.
 * Currently supported operations are:
 * - Export a local entity to the remote resource.
 */
interface EntityManagerInterface {

  /**
   * Export the local entity.
   */
  const ACTION_EXPORT = 0;

  /**
   * Create a new remote entity as a response to a local entity export.
   */
  const ACTION_CREATE = 1;

  /**
   * Update an existing remote entity as a response to a local entity export.
   */
  const ACTION_UPDATE = 2;

  /**
   * Do nothing as a response to a local entity export.
   */
  const ACTION_SKIP = 3;

  /**
   * Delete an existing remote entity as a response to a local entity export.
   */
  const ACTION_DELETE = 4;

  /**
   * Exports a list of entities to the remote resource.
   *
   * The behavior in relation to whether remote entities are created or updated
   * when local entities are exported as part of a list is the same with the
   * behavior when exporting an individual entity. See the `exportLocalEntity`
   * method for details.
   *
   * @param string $sync_id
   *   The ID of the entity sync.
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $local_entities
   *   The local entities.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. Supported options are:
   *   - context: An associative array of context related to the circumstances
   *     of the operation. It is passed to dispatched events and can help
   *     subscribers determine how to alter list filters and entity/field
   *     mappings and respond to initiate/terminate events.
   * phpcs:disable
   * @I Provide a method for exporting a list by filters
   *    type     : feature
   *    priority : low
   *    labels   : export
   *    notes    : To have a similar API with the import entity manager
   *               e.g. filtered by created/changed start/end times, to remove
   *               the need that the caller collects the entities, and to open
   *               the possibility of regularly exporting changed entities in
   *               one queue item instead of instantly and with one queue item
   *               per entity - wherever that might be appropriate and when we
   *               don't care about checking changed fields.
   * phpcs:enable
   */
  public function exportLocalListByEntities(
    $sync_id,
    array $local_entities,
    array $options = []
  );

  /**
   * Exports the given local entity to the remote resource.
   *
   * The most common use cases are to update the remote entity that is
   * associated to the given local entity, or to create a new remote entity if
   * there is no known association. However, subscribers to the entity mapping
   * event can determine that a different remote entity should be updated, or
   * that a new should be created even if there is a known association.
   *
   * With that in mind:
   *
   * Exporting local entities that already have remote entities associated with
   * them will result in the remote entities being updated.
   *
   * Exporting local entities that do not have remote entities associated with
   * them will result in new remote entities being created, subject to the
   * synchronization configuration.
   *
   * @param string $sync_id
   *   The ID of the entity sync.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $options
   *   An associative array of options that determine various aspects of the
   *   export. Supported options are:
   *   - context: An associative array of context related to the circumstances
   *     of the operation. It is passed to dispatched events and can help
   *     subscribers determine how to alter entity/field mappings and respond
   *     to initiate/terminate events.
   *   - client: An associative array of options to be passed to the client. The
   *     client should document the options it supports.
   *
   * @throws \Drupal\entity_sync\Exception\FieldExportException
   *   When an error occurs while exporting a field.
   */
  public function exportLocalEntity(
    $sync_id,
    ContentEntityInterface $local_entity,
    array $options = []
  );

  /**
   * Queues a local entity export operation for the given entity.
   *
   * This method will detect all synchronizations that define the
   * `export_entity` operation for the given entity type and will queue the
   * operation for each one of them.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which to queue the export.
   * @param array $context
   *   The queueing context. Not to be confused with the execution context
   *   i.e. the context of the operation at the moment of its execution.
   *   Context items used by the Entity Sync module are:
   *   - id: (string) A identifier of the context. Currently used IDs are:
   *     - `entity_insert`: The operation is being queued as part of a
   *       `hook_entity_insert`.
   *     - `entity_update`: The operation is being queued as part of a
   *       `hook_entity_update`.
   *   - original_entity: (\Drupal\Core\Entity\ContentEntityInterface) The
   *     original entity before the changes, if we are queueing an export
   *     because of a local entity update. It will be used to detect which
   *     fields have changed so that only those are sent to the remote
   *     resource. Currently, no export will be scheduled if there are no
   *     changed fields.
   *   Third-party modules should be placing their custom context in a
   *   sub-array keyed by the module name to prevent conflicts with context
   *   items used by the Entity Sync module. The `id` top-level context item
   *   should be used but with a module-specific prefix to prevent conflicts
   *   there as well.
   *
   * @deprecated in entity_sync:4.0.0-beta2 and is removed from
   *   entity_sync:6.0.0. Use the
   *   `\Drupal\entity_sync\Queue\ManagerInterface::exportLocalEntityForAllSyncs()`
   *   method instead.
   */
  public function queueExportLocalEntityAllSyncs(
    ContentEntityInterface $entity,
    array $context = []
  );

}
