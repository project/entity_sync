<?php

namespace Drupal\entity_sync;

use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;

/**
 * Defines the default state manager interface.
 */
class StateManager implements StateManagerInterface {

  /**
   * The Entity Synchronization configuration entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * The key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactoryInterface
   */
  protected $keyValueFactory;

  /**
   * Constructs a new StateManager object.
   *
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The Entity Synchronization configuration entity loader.
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   *   The key value factory.
   */
  public function __construct(
    ConfigEntityLoaderInterface $config_entity_loader,
    KeyValueFactoryInterface $key_value_factory
  ) {
    $this->configEntityLoader = $config_entity_loader;
    $this->keyValueFactory = $key_value_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function get($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    return $store->get($operation, []);
  }

  /**
   * {@inheritdoc}
   */
  public function isManaged($sync_id, $operation) {
    $sync = $this->configEntityLoader->load($sync_id);
    if (!$sync) {
      throw new \InvalidArgumentException(
        sprintf(
          'Unknown synchronization or operation type with ID "%s"',
          $sync_id
        )
      );
    }

    $manager = $sync->getOperationsSettings()[$operation]['state']['manager'] ?? NULL;
    if ($manager === 'entity_sync') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isLocked($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    return $value['locked'] ?? StateManagerInterface::UNLOCKED;
  }

  /**
   * {@inheritdoc}
   */
  public function lock($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);
    $value['locked'] = StateManagerInterface::LOCKED;

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function unlock($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);
    $value['locked'] = StateManagerInterface::UNLOCKED;

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getLastRun($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    return $value['last_run'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setLastRun(
    $sync_id,
    $operation,
    $run_time,
    $start_time = NULL,
    $end_time = NULL,
    $limit = NULL,
    $offset = NULL,
    $next_offset = NULL,
    array $data = NULL
  ) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);
    $value['last_run'] = [
      'run_time' => $run_time,
      'start_time' => $start_time,
      'end_time' => $end_time,
      'limit' => $limit,
      'offset' => $offset,
      'next_offset' => $next_offset,
      'data' => $data,
    ];

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function unsetLastRun($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);
    $value['last_run'] = [];

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getLastRunItem($sync_id, $operation, $key) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    return $value['last_run'][$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastRunItem(
    $sync_id,
    $operation,
    $key,
    $item_value
  ) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    if (!isset($value['last_run'])) {
      $value['last_run'] = [];
    }
    $value['last_run'][$key] = $item_value;

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentRun($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    return $value['current_run'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setCurrentRun(
    $sync_id,
    $operation,
    $run_time,
    $start_time = NULL,
    $end_time = NULL,
    $limit = NULL,
    $offset = NULL,
    array $data = NULL
  ) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);
    $value['current_run'] = [
      'run_time' => $run_time,
      'start_time' => $start_time,
      'end_time' => $end_time,
      'limit' => $limit,
      'offset' => $offset,
      'data' => $data,
    ];

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function unsetCurrentRun($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);
    $value['current_run'] = [];

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentRunItem($sync_id, $operation, $key) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    return $value['current_run'][$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setCurrentRunItem(
    $sync_id,
    $operation,
    $key,
    $item_value
  ) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $value = $store->get($operation, []);

    if (!isset($value['current_run'])) {
      $value['current_run'] = [];
    }
    $value['current_run'][$key] = $item_value;

    $store->set($operation, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function delete($sync_id, $operation) {
    $store = $this->keyValueFactory->get(
      $this->getCollectionName($sync_id)
    );
    $store->delete($operation);
  }

  /**
   * Gets the collection name for the given synchronization ID.
   *
   * We store operation states in collections based on their synchronization ID.
   *
   * @param string $sync_id
   *   The ID of the Synchronization or Operation Type that the operations
   *   belongs to.
   *
   * @return string
   *   The name of the collection.
   */
  protected function getCollectionName($sync_id) {
    return "entity_sync.state.$sync_id";
  }

}
