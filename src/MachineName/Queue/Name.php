<?php

namespace Drupal\entity_sync\MachineName\Queue;

/**
 * Holds machine names of queues.
 */
class Name {

  /**
   * Holds the name for the queue that exports individual local entities.
   */
  const EXPORT_LOCAL_ENTITY = 'entity_sync_export_local_entity';

  /**
   * Holds the name for the queue that imports a list of remote entities.
   */
  const IMPORT_LIST = 'entity_sync_import_list';

}
