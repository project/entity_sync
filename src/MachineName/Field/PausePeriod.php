<?php

namespace Drupal\entity_sync\MachineName\Field;

/**
 * Holds machine names of PausePeriod entity fields.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class PausePeriod {

  /**
   * Holds the machine name for the ID field.
   */
  const ID = 'id';

  /**
   * Holds the machine name for the UUID field.
   */
  const UUID = 'uuid';

  /**
   * Holds the machine name for the user ID field.
   *
   * The user is the creator of the pause period.
   */
  const USER_ID = 'uid';

  /**
   * Holds the machine name for the title field.
   */
  const TITLE = 'title';

  /**
   * Holds the machine name for the state field.
   *
   * Keeps track of the current state of the pause period. Currently, only a
   * simple default workflow is supported that includes the `pending`, `started`
   * and `ended` states. If there is a need custom workflows can be supported.
   */
  const STATE = 'state';

  /**
   * Holds the machine name for the operation types field.
   *
   * References the operation types that will be paused during the pause period.
   */
  const OPERATION_TYPES = 'operation_types';

  /**
   * Holds the machine name for the start field.
   */
  const START = 'start';

  /**
   * Holds the machine name for the end field.
   */
  const END = 'end';

  /**
   * Holds the machine name for the created field.
   */
  const CREATED = 'created';

  /**
   * Holds the machine name for the updated field.
   */
  const UPDATED = 'changed';

}
