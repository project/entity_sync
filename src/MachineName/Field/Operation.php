<?php

namespace Drupal\entity_sync\MachineName\Field;

/**
 * Holds machine names of Operation entity fields.
 *
 * @link https://github.com/krystalcode/drupal8-coding-standards/blob/master/Fields.md#field-name-constants
 */
class Operation {

  /**
   * Holds the machine name for the ID field.
   */
  const ID = 'id';

  /**
   * Holds the machine name for the UUID field.
   */
  const UUID = 'uuid';

  /**
   * Holds the machine name for the type field (bundle).
   */
  const TYPE = 'type';

  /**
   * Holds the machine name for the user ID field.
   *
   * The user is the creator of the operation.
   */
  const USER_ID = 'uid';

  /**
   * Holds the machine name for the action field.
   *
   * The action taken during the execution of the operation i.e. skipped,
   * created a new entity or updated an existing entity.
   */
  const ACTION = 'action';

  /**
   * Holds the machine name for the state field.
   *
   * Keeps track of the current state of the operation. This is the local
   * state. If there is a need for tracking a corresponding remote state, that
   * should be defined by the operation configurator plugin.
   */
  const STATE = 'state';

  /**
   * Holds the machine name for the created field.
   */
  const CREATED = 'created';

  /**
   * Holds the machine name for the updated field.
   */
  const UPDATED = 'changed';

  /**
   * Holds the reference to the entity related to the operation.
   *
   * @see \Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait
   */
  const ENTITY = 'entity';

  /**
   * Holds the machine name for the remote ID field.
   *
   * @see \Drupal\entity_sync\OperationConfigurator\FieldTrait\RemoteTrait
   */
  const REMOTE_ID = 'remote_id';

  /**
   * Holds the machine name for the remote state field.
   *
   * @see \Drupal\entity_sync\OperationConfigurator\FieldTrait\RemoteTrait
   */
  const REMOTE_STATE = 'remote_state';

}
