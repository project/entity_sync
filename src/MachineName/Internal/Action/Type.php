<?php

namespace Drupal\entity_sync\MachineName\Internal\Action;

/**
 * Holds IDs of operation action types.
 *
 * Action types indicate the type of an operation e.g. export an entity, import
 * a list of entities etc.
 */
class Type {

  /**
   * Holds the ID of the `export_entity` action type.
   *
   * It indicates that an action is referring to exporting an individual entity.
   */
  const EXPORT_ENTITY = 'export_entity';

  /**
   * Holds the ID of the `export_list` action type.
   *
   * It indicates that an action is referring to exporting a list of entities.
   */
  const EXPORT_LIST = 'export_list';

  /**
   * Holds the ID of the `import_entity` action type.
   *
   * It indicates that an action is referring to importing an individual entity.
   */
  const IMPORT_ENTITY = 'import_entity';

  /**
   * Holds the ID of the `import_entity` action type.
   *
   * It indicates that an action is referring to importing a list of entities.
   */
  const IMPORT_LIST = 'import_list';

}
