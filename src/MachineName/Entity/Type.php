<?php

namespace Drupal\entity_sync\MachineName\Entity;

/**
 * Holds machine names of entity types.
 */
class Type {

  /**
   * Holds the ID for the Operation entity type.
   */
  const OPERATION = 'entity_sync_operation';

  /**
   * Holds the ID for the Operation Type entity type.
   */
  const OPERATION_TYPE = 'entity_sync_operation_type';

  /**
   * Holds the ID for the Pause Period entity type.
   */
  const PAUSE_PERIOD = 'entity_sync_pause_period';

  /**
   * Holds the ID for the Session Type entity type.
   */
  const SESSION_TYPE = 'entity_sync_session_type';

  /**
   * Holds the ID for the Synchronization entity type.
   */
  const SYNC = 'entity_sync';

}
