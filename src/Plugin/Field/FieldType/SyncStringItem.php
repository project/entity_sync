<?php

namespace Drupal\entity_sync\Plugin\Field\FieldType;

// Drupal modules.
use Drupal\entity_sync\Field\SyncItemBase;
// Drupal core.
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * An entity synchronization field type for remote entity IDs that are strings.
 *
 * phpcs:disable
 * @FieldType(
 *   id = "entity_sync_string",
 *   label = @Translation("Entity synchronization (text, plain)"),
 *   description = @Translation("A field for holding entity synchronization information for remote entities with a plain text ID."),
 *   category = @Translation("Entity synchronization"),
 *   default_widget = "string_textfield",
 *   default_formatter = "string",
 *   list_class = "\Drupal\entity_sync\Field\SyncItemList",
 *   constraints = {
 *     "ComplexData" = {
 *       "changed" = {
 *         "Range" = {
 *           "min" = "-2147483648",
 *           "max" = "2147483648",
 *         }
 *       }
 *     }
 *   }
 * )
 * phpcs:enable
 */
class SyncStringItem extends SyncItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'case_sensitive' => FALSE,
      'max_length' => 255,
      'is_ascii' => FALSE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(
    FieldStorageDefinitionInterface $field_definition
  ) {
    $properties['id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      ->setSetting(
        'case_sensitive',
        $field_definition->getSetting('case_sensitive')
      )
      ->setRequired(TRUE);

    return $properties + parent::propertyDefinitions($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(
    FieldStorageDefinitionInterface $field_definition
  ) {
    $type = 'varchar';
    if ($field_definition->getSetting('is_ascii') === TRUE) {
      $type = 'varchar_ascii';
    }

    $columns = [
      'id' => [
        'type' => $type,
        'length' => (int) $field_definition->getSetting('max_length'),
        'binary' => $field_definition->getSetting('case_sensitive'),
      ],
    ] + parent::schema($field_definition)['columns'];

    return [
      'columns' => $columns,
      'indexes' => [
        'id' => ['id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $max_length = $this->getSetting('max_length');
    if (!$max_length) {
      return $constraints;
    }

    $constraints[] = \Drupal::typedDataManager()
      ->getValidationConstraintManager()
      ->create(
        'ComplexData',
        [
          'id' => [
            'Length' => [
              'max' => $max_length,
              'maxMessage' => $this->t(
                '%name: may not be longer than @max characters.',
                [
                  '%name' => $this->getFieldDefinition()->getLabel(),
                  '@max' => $max_length,
                ]
              ),
            ],
          ],
        ]
      );

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(
    FieldDefinitionInterface $field_definition
  ) {
    $values['id'] = StringItem::generateSampleValue($field_definition);
    return $values + parent::generateSampleValue($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(
    array &$form,
    FormStateInterface $form_state,
    $has_data
  ) {
    $element = [];

    $element['max_length'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum length'),
      '#default_value' => $this->getSetting('max_length'),
      '#required' => TRUE,
      '#description' => $this->t('The maximum length of the field in characters.'),
      '#min' => 1,
      '#disabled' => $has_data,
    ];

    return $element;
  }

}
