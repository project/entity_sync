<?php

namespace Drupal\entity_sync\Plugin\Field\FieldType;

// Drupal modules.
use Drupal\entity_sync\Field\SyncItemBase;
// Drupal core.
use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * An entity synchronization field type for when the remote entity ID is a UUID.
 *
 * phpcs:disable
 * @FieldType(
 *   id = "entity_sync_uuid",
 *   label = @Translation("Entity synchronization (UUID)"),
 *   description = @Translation("A field for holding entity synchronization information for remote entities with a UUID."),
 *   category = @Translation("Entity synchronization"),
 *   default_widget = "string_textfield",
 *   default_formatter = "string",
 *   list_class = "\Drupal\entity_sync\Field\SyncItemList",
 *   constraints = {
 *     "ComplexData" = {
 *       "id" = {
 *         "Length" = {
 *           "min" = "36",
 *           "max" = "36",
 *         }
 *       },
 *       "changed" = {
 *         "Range" = {
 *           "min" = "-2147483648",
 *           "max" = "2147483648",
 *         }
 *       }
 *     }
 *   }
 * )
 * phpcs:enable
 */
class SyncUuidItem extends SyncItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(
    FieldStorageDefinitionInterface $field_definition
  ) {
    $properties['id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('ID'))
      // UUIDs are never case sensitive.
      // See https://www.uuidtools.com/what-is-uuid
      ->setSetting('case_sensitive', FALSE)
      ->setRequired(TRUE);

    return $properties + parent::propertyDefinitions($field_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(
    FieldStorageDefinitionInterface $field_definition
  ) {
    $columns = [
      'id' => [
        'type' => 'char',
        'length' => 36,
        // UUIDs are never case sensitive.
        // See https://www.uuidtools.com/what-is-uuid
        'binary' => FALSE,
      ],
    ] + parent::schema($field_definition)['columns'];

    return [
      'columns' => $columns,
      'indexes' => [
        'id' => ['id'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['id'] = $random->word(mt_rand(36, 36));

    return $values + parent::generateSampleValue($field_definition);
  }

}
