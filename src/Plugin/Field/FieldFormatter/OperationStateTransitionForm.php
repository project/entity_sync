<?php

namespace Drupal\entity_sync\Plugin\Field\FieldFormatter;

use Drupal\entity_sync\StateMachine\Form\OperationStateTransitionForm as StateTransitionForm;
use Drupal\state_machine\Plugin\Field\FieldFormatter\StateTransitionFormFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormState;

/**
 * Formatter that renders the transition form for an operation's state field.
 *
 * @FieldFormatter(
 *   id = "entity_sync_operation_state_transition_form",
 *   label = @Translation("Entity synchronization operation transition form"),
 *   field_types = {
 *     "state",
 *   },
 * )
 */
class OperationStateTransitionForm extends StateTransitionFormFormatter {

  /**
   * {@inheritdoc}
   *
   * There are two changes compared to the parent method.
   *
   * 1. We use the `\Drupal\entity_sync\Entity\Form\OperationTransitionForm`
   * class to instantiate the form object instead of the
   * `\Drupal\state_machine\Form\StateTransitionForm` class. Our form takes care
   * of running the operation when the corresponding transition is triggered.
   * 2. We display the current state above the transition buttons.
   *
   * phpcs:disable
   * @I Find a way to make this customizable in the `state_machine` module
   *    type     : improvement
   *    priority : low
   *    labels   : operation, state, coding-standards
   * phpcs:enable
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $items->getEntity();
    // Do not show the form if the user isn't allowed to modify the entity.
    if (!$entity->access('update')) {
      return [];
    }
    /** @var \Drupal\state_machine\Form\StateTransitionFormInterface $form_object */
    $form_object = $this->classResolver->getInstanceFromDefinition(StateTransitionForm::class);
    $form_object->setEntity($entity);
    $form_object->setFieldName($items->getFieldDefinition()->getName());
    $form_state_additions = [];
    if ($this->supportsConfirmationForm()) {
      $form_state_additions += [
        // Store in the form state whether a confirmation is required before
        // applying the state transition.
        'require_confirmation' => (bool) $this->getSetting('require_confirmation'),
        'use_modal' => (bool) $this->getSetting('use_modal'),
      ];
    }
    $form_state = (new FormState())->setFormState($form_state_additions);
    // $elements needs a value for each delta. State fields can't be multivalue,
    // so it's safe to hardcode 0.
    $elements = [];
    $elements[0]['state'] = [
      '#markup' => '<div class="field--type-state--current-state">' . $items->first()->getLabel() . '</div>',
    ];
    $elements[0]['transition'] = $this->formBuilder->buildForm($form_object, $form_state);

    return $elements;
  }

}
