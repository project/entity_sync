<?php

namespace Drupal\entity_sync\Plugin\EntitySync\ValidationConstraint;

use Symfony\Component\Validator\Constraints\NotNullValidator as ValidatorBase;

/**
 * Validates the NotNull constraint.
 */
class NotNullValidator extends ValidatorBase {}
