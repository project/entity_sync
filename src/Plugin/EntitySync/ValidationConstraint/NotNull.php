<?php

namespace Drupal\entity_sync\Plugin\EntitySync\ValidationConstraint;

use Symfony\Component\Validator\Constraints\NotNull as ConstraintBase;

/**
 * Checks that the value is not NULL.
 *
 * @EntitySyncValidationConstraint(
 *   id = "NotNull"
 * )
 */
class NotNull extends ConstraintBase {

  /**
   * The action to take if validation returns errors.
   *
   * Currently supported values are: `fail` (default) and `skip`.
   *
   * @var string
   */
  public $onFailure = 'fail';

  /**
   * The error message.
   *
   * @var string
   */
  public $message = 'The value should not be null.';

}
