<?php

namespace Drupal\entity_sync\Plugin\EntitySync\ValidationConstraint;

use Symfony\Component\Validator\Constraints\NotBlankValidator as ValidatorBase;

/**
 * Validates the NotBlank constraint.
 */
class NotBlankValidator extends ValidatorBase {}
