<?php

namespace Drupal\entity_sync\Plugin\EntitySync\ValidationConstraint;

use Symfony\Component\Validator\Constraints\NotBlank as ConstraintBase;

/**
 * Checks that the value is not blank/empty.
 *
 * @EntitySyncValidationConstraint(
 *   id = "NotBlank"
 * )
 *
 * @see \Symfony\Component\Validator\Constraints\NotBlank
 */
class NotBlank extends ConstraintBase {

  /**
   * The action to take if validation returns errors.
   *
   * Currently supported values are: `fail` (default) and `skip`.
   *
   * @var string
   */
  public $onFailure = 'fail';

  /**
   * The error message.
   *
   * @var string
   */
  public $message = 'The value should not be blank.';

}
