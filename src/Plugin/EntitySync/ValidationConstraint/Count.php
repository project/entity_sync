<?php

namespace Drupal\entity_sync\Plugin\EntitySync\ValidationConstraint;

use Symfony\Component\Validator\Constraints\Count as ConstraintBase;

/**
 * Checks that the value meets the given count requirements.
 *
 * @EntitySyncValidationConstraint(
 *   id = "Count"
 * )
 */
class Count extends ConstraintBase {

  /**
   * The action to take if validation returns errors.
   *
   * Currently supported values are: `fail` (default) and `skip`.
   *
   * @var string
   */
  public $onFailure = 'fail';

  /**
   * The error message in the case of violating the minimum count.
   *
   * @var string
   */
  public $minMessage = 'The value should contain %limit element or more.|The value should contain %limit elements or more.';

  /**
   * The error message in the case of violating the maximum count.
   *
   * @var string
   */
  public $maxMessage = 'The value should contain %limit element or less.|The value should contain %limit elements or less.';

  /**
   * The error message in the case of violating the exact count.
   *
   * @var string
   */
  public $exactMessage = 'The value should contain exactly %limit element.|The value should contain exactly %limit elements.';

}
