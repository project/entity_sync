<?php

namespace Drupal\entity_sync\Plugin\EntitySync\ValidationConstraint;

use Symfony\Component\Validator\Constraints\CountValidator as ValidatorBase;

/**
 * Validates the Count constraint.
 */
class CountValidator extends ValidatorBase {}
