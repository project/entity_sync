<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Transformer that converts entity ID(s) to entity(ies).
 *
 * - If one ID is passed as a string/integer, an entity object will be returned.
 * - If an array of IDs is passed, an array of entity objects will be returned.
 *
 * This plugin follows the behavior of the entity storage loading methods
 * i.e. no errors will be raised if the requested entities do not exist.
 * - If a single entity is requested and it does not exist, NULL will be
 *   returned.
 * - If multiple entities are requested, entities that do not exist will not be
 *   part of the result array.
 *
 * Supported configuration properties:
 * - entity_type_id: (string, required) The type ID of the entities that will be
 *   loaded.
 *
 * @EntitySyncFieldTransformer(
 *   id = "ids_to_entities"
 * )
 */
class IdsToEntities extends PluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new IdsToEntities object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $constraint_manager
    );

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'entity_type_id',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $entity_ids,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($entity_ids);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $entity_ids,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($entity_ids);
  }

  /**
   * Returns the entity or entities with the given IDs.
   *
   * @param mixed $entity_ids
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($entity_ids) {
    if ($entity_ids === NULL) {
      return NULL;
    }

    if (is_array($entity_ids)) {
      return $this->entityTypeManager
        ->getStorage($this->configuration['entity_type_id'])
        ->loadMultiple($entity_ids);
    }

    return $this->entityTypeManager
      ->getStorage($this->configuration['entity_type_id'])
      ->load($entity_ids);
  }

}
