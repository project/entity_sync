<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that sets the field to a fixed value defined in configuration.
 *
 * No need to provide type casting here; YAML syntax should support all types
 * that may be needed.
 *
 * Supported configuration properties:
 * - value: (mixed, required) The value to set the field to.
 *
 * @EntitySyncFieldTransformer(
 *   id = "fixed_value"
 * )
 */
class FixedValue extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['value']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->configuration['value'];
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->configuration['value'];
  }

  /**
   * {@inheritdoc}
   *
   * There's no need to get the value from the field; we simply return the
   * configured value.
   */
  protected function getLocalEntityValue(
    ContentEntityInterface $local_entity,
    array $field_info
  ) {
    return NULL;
  }

}
