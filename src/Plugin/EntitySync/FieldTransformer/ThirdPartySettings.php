<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that sets the field to the third party settings of an entity.
 *
 * Retrieves one or more third party settings for a specific provider. It can
 * retrieve one setting as a scalar using the `setting` configuration property,
 * multiple settings as an array using the `settings` configuration property, or
 * all settings for the provider as an array if none of the `setting` and
 * `settings` configuration properties are defined.
 *
 * Currently, only makes sense for exports since configuration entities are not
 * supported yet as targets of operations.
 *
 * Supported configuration properties:
 * - provider: (string, required) The provider of the third party setting(s) to
 *   retrieve.
 * - setting: (string, optional) The setting to retrieve. When defined, only
 *   this setting will be returned as a scalar.
 * - settings: (array, optional) The settings to retrieve. When defined, the
 *   settings will be returned as an array.
 * - default_value: (mixed, optional, defaults to `NULL`) The value to use when
 *   the value a requested setting does not exist for the given provider.
 *
 * @EntitySyncFieldTransformer(
 *   id = "third_party_settings"
 * )
 */
class ThirdPartySettings extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'default_value' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $entity,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($entity === NULL) {
      return NULL;
    }

    if (!is_object($entity)) {
      throw new \RuntimeException(sprintf(
        'An object is expected to extract the third party settings from, "%s" given.',
        gettype($entity)
      ));
    }
    if (!$entity instanceof ThirdPartySettingsInterface) {
      throw new \RuntimeException(sprintf(
        '"%s" does not implement \Drupal\Core\Config\Entity\ThirdPartySettingsInterface.',
        get_class($entity)
      ));
    }

    if (isset($this->configuration['setting'])) {
      return $entity->getThirdPartySetting(
        $this->configuration['provider'],
        $this->configuration['setting'],
        $this->configuration['default_value']
      );
    }

    $settings = $entity->getThirdPartySettings($this->configuration['provider']);
    if (!isset($this->configuration['settings'])) {
      return $settings;
    }

    foreach ($this->configuration['settings'] as $setting) {
      if (!array_key_exists($setting, $settings)) {
        $settings[$setting] = $this->configuration['default_value'];
      }
    }

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'provider',
    ]);

    $has_setting = $this->configuration['setting'] ?? NULL;
    $has_settings = $this->configuration['settings'] ?? NULL;
    if ($has_setting && $has_settings) {
      throw new InvalidConfigurationException(
        'Only one of the "setting" and "settings" configuration properties can be provided.'
      );
    }
  }

}
