<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\file\FileInterface;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts a file entity to a file URL.
 *
 * Supported configuration properties:
 * - relative: (string, optional, defaults to `TRUE`) Whether the URL should be
 *   root-relative.
 *
 * Currently, only exports are supported. Support for imports should be added
 * here as well.
 *
 * @EntitySyncFieldTransformer(
 *   id = "file_url"
 * )
 *
 * @see \Drupal\file\FileInterface::createFileUrl()
 */
class FileUrl extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'relative' => TRUE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $file,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($file === NULL) {
      return NULL;
    }

    if (!$file instanceof FileInterface) {
      throw new \RuntimeException(sprintf(
        'The value being converted to a URL must implement "%s".',
        FileInterface::class
      ));
    }

    return $file->createFileUrl($this->configuration['relative']);
  }

}
