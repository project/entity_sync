<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Transformer that converts an entity to a URL.
 *
 * Supported configuration properties:
 * - link_template: (string, optional) The link template to use for generating
 *   the URL e.g. `canonical`, `edit-form` etc.
 * - url_options: (array, optional) The options to pass to the URL generator.
 *
 * Currently, only exports are supported. Support for imports should be added
 * here as well.
 *
 * @EntitySyncFieldTransformer(
 *   id = "entity_url"
 * )
 *
 * @see \Drupal\Core\Entity\EntityInterface::toUrl()
 */
class EntityUrl extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'link_template' => NULL,
      'url_options' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $entity,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($entity === NULL) {
      return NULL;
    }

    if (!$entity instanceof EntityInterface) {
      throw new \RuntimeException(sprintf(
        'The value being converted to a URL must implement "%s".',
        EntityInterface::class
      ));
    }

    return $entity->toUrl(
      $this->configuration['link_template'],
      $this->configuration['url_options']
    )->toString();
  }

}
