<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Transformer that converts an entity to the value of some of its fields.
 *
 * Supported configuration properties:
 * - field_name: (string, optional) The machine name of the field from which to
 *   extract the value.
 * - field_names: (array, optional) An array containing the machine names of the
 *   fields from which to extract the values.
 *
 * Either the `field_name` option or the `field_names` option must be provided.
 * - If the `field_name` option is provided, the transformed value will be
 *   directly the value of the field with the given machine name.
 * - If the `field_names` option is provided, the transformed value will be an
 *   associative array keyed by the machine name of the field and containing its
 *   value.
 *
 * The field/property/delta extraction options provided by the base field
 * transformer class can be used to determine the format of the value extracted
 * for each field.
 *
 * Currently, only exports are supported. Support for imports should be added
 * here as well.
 *
 * @EntitySyncFieldTransformer(
 *   id = "entity_field"
 * )
 *
 * @see \Drupal\entity_sync\FieldTransformer\PluginBase::extractFieldValue()
 */
class EntityField extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationOneOfProperties([
      'field_name',
      'field_names',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $entity,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($entity === NULL) {
      return NULL;
    }

    if (!$entity instanceof FieldableEntityInterface) {
      throw new \InvalidArgumentException(sprintf(
        'The value must implement "%s".',
        FieldableEntityInterface::class,
      ));
    }

    if (array_key_exists('field_name', $this->configuration)) {
      return $this->extractFieldValue(
        $entity,
        ['machine_name' => $this->configuration['field_name']]
      );
    }

    $value = [];
    foreach ($this->configuration['field_names'] as $field_name) {
      $value[$field_name] = $this->extractFieldValue(
        $entity,
        ['machine_name' => $field_name]
      );
    }

    return $value;
  }

}
