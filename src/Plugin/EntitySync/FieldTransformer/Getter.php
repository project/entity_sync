<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that returns a value from an object using a getter method.
 *
 * On exports, this is mostly meant to be used in a pipeline where this plugin
 * is given an object or entity as the value to transform. The `getter`
 * configuration option provided by and documented in the base plugin should be
 * used. There is no point in using this plugin outside of a pipeline i.e. as
 * the only and top-level field transformer; the `copy_value` plugin with the
 * `getter` configuration option should be used instead.
 *
 * Only exports are supported currently. Support for imports should be added
 * here as well.
 *
 * @EntitySyncFieldTransformer(
 *   id = "getter"
 * )
 */
class Getter extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['getter']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $object,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($object === NULL) {
      return NULL;
    }
    if (!method_exists($object, $this->configuration['getter'])) {
      throw new \RuntimeException(sprintf(
        'Method "%s" does not exist on the "%s" object being transformed.',
        $this->configuration['getter'],
        get_class($object)
      ));
    }

    return $object->{$this->configuration['getter']}();
  }

}
