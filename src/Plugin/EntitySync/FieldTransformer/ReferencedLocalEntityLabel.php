<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Transformer that gets the label of a referenced local entity.
 *
 * Right now, it applies to exporting local entity reference fields. Importing
 * from a remote field that references a local entity could be implemented here
 * as well.
 *
 * Also, it currently does not handle multiple values.
 *
 * @EntitySyncFieldTransformer(
 *   id = "referenced_local_entity_label"
 * )
 * phpcs:disable
 * @I Implement entity pipelines
 *    type     : feature
 *    priority : normal
 *    labels   : field-transformer
 *    notes    : A solution that could cover many cases would be to create a
 *               pipeline transformer plugin that passes along the referenced
 *               entity and then gives the opportunity to additional
 *               transformers to transform the value of a field belonging to the
 *               referenced entity, including references of references.
 * phpcs:enable
 */
class ReferencedLocalEntityLabel extends PluginBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $constraint_manager
    );

    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    if ($value === NULL) {
      return NULL;
    }

    $referenced_type_id = $this->entityFieldManager
      ->getFieldStorageDefinitions($local_entity->getEntityTypeId())
      [$field_info['machine_name']]
      ->getSetting('target_type');
    $referenced_entity = $this->entityTypeManager
      ->getStorage($referenced_type_id)
      ->load($value);

    // Handle dead references.
    // If an error should be raised, use post-transformation constraints.
    // phpcs:disable
    // @I Consider making handling dead references configurable
    //    type     : improvement
    //    priority : low
    //    labels   : field-transformer
    //    notes    : We may want to raise an error if we have a dead reference,
    //               but still differentiate from an empty value i.e. we cannot
    //               use post-transformation validator.
    // phpcs:enable
    if (!$referenced_entity) {
      return NULL;
    }

    return $referenced_entity->label();
  }

}
