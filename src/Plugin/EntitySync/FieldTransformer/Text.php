<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts the value as required by text fields.
 *
 * Text fields (`text` and `text_long`) require a format together with the
 * value. This plugin will set the format of the target text field to a fixed
 * value defined in configuration.
 *
 * Supported configuration properties:
 * - format: (string, required) The text format.
 *
 * @EntitySyncFieldTransformer(
 *   id = "text"
 * )
 * phpcs:disable
 * @I Move to the `entity_sync_text` submodule
 *    type     : task
 *    priority : low
 *    labels   : structure
 *    notes    : Provide an update function that enables the module.
 * phpcs:enable
 */
class Text extends CopyValue {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // By default, we make sure the value is converted to a string since the
      // target field `value` property is a string.
      'cast_to' => 'string',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['format']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    $value = parent::transformImportedValue(
      $value,
      $remote_entity,
      $local_entity,
      $field_info,
      $context
    );

    if ($value === NULL) {
      return NULL;
    }

    return [
      'value' => $value,
      'format' => $this->configuration['format'],
    ];
  }

}
