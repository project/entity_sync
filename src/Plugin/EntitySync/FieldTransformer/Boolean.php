<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts the value as required by boolean fields.
 *
 * Supported configuration properties:
 * - input: (array, required)
 *   - formats: (string, required) One or more formats that will be
 *     accepted. Supported formats are:
 *     - 1/0
 *       Will return TRUE/FALSE if the value is an integer or string 1/0.
 *     - true/false_boolean
 *       Will return TRUE/FALSE if the value is a boolean TRUE/FALSE.
 * - output: (array, optional)
 *   - format: (string, required) The format the value will be transformed
 *     to. Supported formats are:
 *     - true/false_boolean
 *       Will return boolean TRUE/FALSE.
 *     - true/false_lowercase
 *       Will return string true/false.
 *     - true/false_uppercase
 *       Will return string TRUE/FALSE.
 *     - true/false_uppercase_first
 *       Will return string True/False.
 *     - y/n_lowercase
 *       Will return string y/n.
 *     - y/n_uppercase
 *       Will return string Y/N.
 *     - yes/no_lowercase
 *       Will return string yes/no.
 *     - yes/no_uppercase
 *       Will return string YES/NO.
 *     - yes/no_uppercase_first
 *       Will return string Yes/No.
 *
 * In all cases, NULL will be returned when the value is not one of the values
 * accepted by at least one of the configured input format as valid. The
 * behavior in that case can be defined by constraints.
 *
 * Entity Synchronization is commonly used to synchronize data with external
 * systems for which the developer may not have control over. There are systems
 * that consider a value invalid if it is not exactly in the specified format
 * e.g. `y` instead of `Y`.
 *
 * We therefore want to provide a detailed list of input/output formats to cover
 * all cases. More formats that we want to eventually cover are:
 *
 * - true/false
 * - true/false_boolean
 * - true/false_string
 * - 1/0
 * - 1/0_integer
 * - 1/0_string
 *
 * @EntitySyncFieldTransformer(
 *   id = "boolean"
 * )
 * phpcs:disable
 * @I Add option for case sensitivity
 *    type     : improvement
 *    priority : low
 *    labels   : field-transformer
 * @I Add more supported input/output formats
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 * phpcs:enable
 */
class Boolean extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // We use boolean TRUE/FALSE as the default output format because that is
      // the most common case for importing into boolean fields.
      'output' => ['format' => 'true/false_boolean'],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['input']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the value casted to the configured type.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value) {
    $normalized_value = NULL;
    foreach ($this->configuration['input']['formats'] as $input_format) {
      $input_method = $this->inputFormatMethod($input_format);
      $normalized_value = $this->{$input_method}($value);
      if ($normalized_value !== NULL) {
        break;
      }
    }

    if ($normalized_value === NULL) {
      return;
    }

    $output_method = $this->outputFormatMethod(
      $this->configuration['output']['format']
    );
    return $this->{$output_method}($normalized_value);
  }

  /**
   * Returns the method corresponding to the given input format.
   *
   * @param string $format
   *   The format to get the method for.
   *
   * @return string
   *   The corresponding method.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the requested format is not supported.
   */
  protected function inputFormatMethod(string $format) {
    $map = [
      '1/0' => 'inputFormat10',
      'true/false_boolean' => 'inputFormatTrueFalseBoolean',
    ];
    if (!in_array($format, array_keys($map))) {
      throw new InvalidConfigurationException(sprintf(
        'Unsupported input format "%s"',
        $format
      ));
    }

    return $map[$format];
  }

  /**
   * Formats a value as per the `1/0` input format.
   *
   * @param mixed $value
   *   The value to format.
   *
   * @return bool|null
   *   TRUE if the value is integer or string 1, FALSE if it is integer or
   *   string 0, NULL otherwise.
   */
  protected function inputFormat10($value) {
    switch (TRUE) {
      case ($value === 1):
      case ($value === '1'):
        return TRUE;

      case ($value === 0):
      case ($value === '0'):
        return FALSE;

      default:
        return NULL;
    }
  }

  /**
   * Formats a value as per the `true/false_boolean` input format.
   *
   * @param mixed $value
   *   The value to format.
   *
   * @return bool|null
   *   TRUE if the value is boolean TRUE, FALSE if it is boolean FALSE, NULL
   *   otherwise.
   */
  protected function inputFormatTrueFalseBoolean($value) {
    if ($value === TRUE) {
      return TRUE;
    }

    if ($value === FALSE) {
      return FALSE;
    }

    return NULL;
  }

  /**
   * Returns the method corresponding to the given output format.
   *
   * @param string $format
   *   The format to get the method for.
   *
   * @return string
   *   The corresponding method.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the requested format is not supported.
   */
  protected function outputFormatMethod(string $format) {
    $map = [
      'true/false_boolean' => 'outputFormatTrueFalseBoolean',
      'true/false_lowercase' => 'outputFormatTrueFalseLowercase',
      'true/false_uppercase' => 'outputFormatTrueFalseUppercase',
      'true/false_uppercase_first' => 'outputFormatTrueFalseUppercaseFirst',
      'y/n_lowercase' => 'outputFormatYnLowercase',
      'y/n_uppercase' => 'outputFormatYnUppercase',
      'yes/no_lowercase' => 'outputFormatYesNoLowercase',
      'yes/no_uppercase' => 'outputFormatYesNoUppercase',
      'yes/no_uppercase_first' => 'outputFormatYesNoUppercaseFirst',
    ];
    if (!in_array($format, array_keys($map))) {
      throw new InvalidConfigurationException(sprintf(
        'Unsupported output format "%s"',
        $format
      ));
    }

    return $map[$format];
  }

  /**
   * Formats a value as per the `true/false_boolean` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return bool
   *   The normalized value as is since it is already in the expected boolean
   *   format.
   */
  protected function outputFormatTrueFalseBoolean(bool $normalized_value) {
    return $normalized_value;
  }

  /**
   * Formats a value as per the `true/false_lowercase` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'true' for TRUE and 'false' for FALSE.
   */
  protected function outputFormatTrueFalseLowercase(bool $normalized_value) {
    return $normalized_value ? 'true' : 'false';
  }

  /**
   * Formats a value as per the `true/false_uppercase` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'TRUE' for TRUE and 'FALSE' for FALSE.
   */
  protected function outputFormatTrueFalseUppercase(bool $normalized_value) {
    return $normalized_value ? 'TRUE' : 'FALSE';
  }

  /**
   * Formats a value as per the `true/false_uppercase_first` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'True' for TRUE and 'False' for FALSE.
   */
  protected function outputFormatTrueFalseUppercaseFirst(bool $normalized_value) {
    return $normalized_value ? 'True' : 'False';
  }

  /**
   * Formats a value as per the `y/n_lowercase` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'y' for TRUE and 'n' for FALSE.
   */
  protected function outputFormatYnLowercase(bool $normalized_value) {
    return $normalized_value ? 'y' : 'n';
  }

  /**
   * Formats a value as per the `y/n_uppercase` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'Y' for TRUE and 'N' for FALSE.
   */
  protected function outputFormatYnUppercase(bool $normalized_value) {
    return $normalized_value ? 'Y' : 'N';
  }

  /**
   * Formats a value as per the `yes/no_lowercase` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'yes' for TRUE and 'no' for FALSE.
   */
  protected function outputFormatYesNoLowercase(bool $normalized_value) {
    return $normalized_value ? 'yes' : 'no';
  }

  /**
   * Formats a value as per the `yes/no_uppercase` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'YES' for TRUE and 'NO' for FALSE.
   */
  protected function outputFormatYesNoUppercase(bool $normalized_value) {
    return $normalized_value ? 'YES' : 'NO';
  }

  /**
   * Formats a value as per the `yes/no_uppercase_first` output format.
   *
   * @param bool $normalized_value
   *   The value to format.
   *
   * @return string
   *   'Yes' for TRUE and 'No' for FALSE.
   */
  protected function outputFormatYesNoUppercaseFirst(bool $normalized_value) {
    return $normalized_value ? 'Yes' : 'No';
  }

}
