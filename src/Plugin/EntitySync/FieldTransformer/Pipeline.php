<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

// Drupal modules.
use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
// Drupal core.
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
// Third-party-libraries.
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Transformer that passes the value through multiple transformer plugins.
 *
 * Supported configuration properties:
 * - plugins: (array, required) An array containing the plugins that the value
 *   should be passed through. Each should contain the same properties that
 *   field transformers normally do i.e. `id` and `configuration`.
 * - mode: (string, optional, defaults to `default`) The mode that determines
 *   the way that the pipeline will pass the value to the plugins. Supported
 *   modes are:
 *   - default: The initial value will be passed on the first plugin in the
 *     sequence. The transformed value returned by each plugin will then be
 *     passed on to the next one.
 *   - array_map: The initial value must be an array. Each item of the initial
 *     array will be passed on to the complete sequence of plugins as the
 *     initial value in the `default` mode. The final results will be an array
 *     containing the transformed values for all items in the initial array. The
 *     keys of the initial array will be preserved.
 *
 * @EntitySyncFieldTransformer(
 *   id = "pipeline"
 * )
 */
class Pipeline extends PluginBase {

  /**
   * The field transformer plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new Pipeline object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The field transformer plugin manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager,
    PluginManagerInterface $plugin_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $constraint_manager
    );

    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint'),
      $container->get('plugin.manager.entity_sync_field_transformer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'mode' => 'default',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function processExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    $this->validate($value, 'pre_transformation');

    switch ($this->configuration['mode']) {
      case 'default':
        foreach ($this->configuration['plugins'] as $plugin) {
          $value = $this->pluginManager
            ->createInstance(
              $plugin['id'],
              $plugin['configuration'] ?? []
            )
            ->export(
              $local_entity,
              $remote_entity_id,
              $field_info,
              $context,
              $value,
              FALSE
            );
        }
        break;

      case 'array_map':
        $plugins = $this->configuration['plugins'];
        if ($value === NULL) {
          break;
        }
        $value = array_map(
          function ($item) use (
            $plugins,
            $local_entity,
            $remote_entity_id,
            $field_info,
            $context
          ) {
            foreach ($plugins as $plugin) {
              $item = $this->pluginManager
                ->createInstance(
                  $plugin['id'],
                  $plugin['configuration'] ?? []
                )
                ->export(
                  $local_entity,
                  $remote_entity_id,
                  $field_info,
                  $context,
                  $item,
                  FALSE
                );
            }
            return $item;
          },
          $value
        );
        break;

      default:
        throw new InvalidConfigurationException(sprintf(
          'Invalid pipeline mode "%s"',
          $this->configuration['mode']
        ));
    }

    $this->validate($value, 'post_transformation');

    // NULL in Drupal signifies an empty value i.e. the field has no
    // value. However, the system that we are exporting to may use a different
    // value for this. For example, CSV files commonly use an empty string for
    // no value. We therefore transform the Drupal's NULL value to the remote
    // system's corresponding value configured by the `null_value` configuration
    // setting.
    if ($value === NULL) {
      $value = $this->configuration['null_value'];
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  protected function processImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    // NULL in Drupal signifies an empty value i.e. the field has no
    // value. However, the system that we are exporting to may use a different
    // value for this. For example, CSV files commonly use an empty string for
    // no value. We therefore transform the remote system's NULL value,
    // configured by the `null_value` configuration setting, to Drupal's NULL
    // value.
    if ($value === $this->configuration['null_value']) {
      $value = NULL;
    }

    $this->validate($value, 'pre_transformation');

    switch ($this->configuration['mode']) {
      case 'default':
        foreach ($this->configuration['plugins'] as $plugin) {
          $value = $this->pluginManager
            ->createInstance(
              $plugin['id'],
              $plugin['configuration'] ?? []
            )
            ->import(
              $remote_entity,
              $local_entity,
              $field_info,
              $context,
              $value,
              FALSE,
              FALSE
            );
        }
        break;

      case 'array_map':
        $plugins = $this->configuration['plugins'];
        if ($value === NULL) {
          break;
        }
        $value = array_map(
          function ($item) use (
            $plugins,
            $remote_entity,
            $local_entity,
            $field_info,
            $context
          ) {
            foreach ($plugins as $plugin) {
              $item = $this->pluginManager
                ->createInstance(
                  $plugin['id'],
                  $plugin['configuration'] ?? []
                )
                ->import(
                  $remote_entity,
                  $local_entity,
                  $field_info,
                  $context,
                  $item,
                  FALSE,
                  FALSE
                );
            }
            return $item;
          },
          $value
        );
        break;

      default:
        throw new InvalidConfigurationException(sprintf(
          'Invalid pipeline mode "%s"',
          $this->configuration['mode']
        ));
    }

    $this->validate($value, 'post_transformation');

    return $value;
  }

}
