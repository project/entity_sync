<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that copies one or more computed values.
 *
 * Supported configuration properties:
 * - key: (string, optional) The key of the computed value to return as the
 *   transformed value.
 * - keys: (array, optional) An array containing the keys of the computed values
 *   to return in transformed value array.
 *
 * If one computed value is requested using the `key` configuration property,
 * the value is returned directly as the transformed value.
 *
 * If one or more computed values are requested using the `keys` configuration
 * property, an array containing all requested values is returned as the
 * transformed value.
 *
 * @EntitySyncFieldTransformer(
 *   id = "computed_value"
 * )
 */
class ComputedValue extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // This plugin always copies an already computed value; it never needs to
      // be provided an initial value.
      'getter' => '%null%',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationOneOfProperties([
      'key',
      'keys',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($context);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($context);
  }

  /**
   * Returns the computed value(s) as contained in the given context.
   *
   * @param array $context
   *   The context of the operation being run, as passed by the field manager.
   *
   * @return mixed
   *   The transformed value.
   *
   * @throws \RuntimeException
   *   When a requested computed value is not provided in the context.
   */
  protected function transformValue(array $context) {
    if (array_key_exists('key', $this->configuration)) {
      return $this->getValueForKey($this->configuration['key'], $context);
    }

    $value = [];
    foreach ($this->configuration['keys'] as $key) {
      $value[$key] = $this->getValueForKey($key, $context);
    }

    return $value;
  }

  /**
   * Returns the computed value for the given key.
   *
   * @param string $key
   *   The key of the computed value to return.
   * @param array $context
   *   The context of the operation being run, as passed by the field manager.
   *
   * @return mixed
   *   The computed value.
   *
   * @throws \RuntimeException
   *   When the computed value for the given key is not provided in the context.
   */
  protected function getValueForKey(string $key, array $context) {
    if (array_key_exists($key, $context['computed_values'])) {
      return $context['computed_values'][$key];
    }

    throw new \RuntimeException(sprintf(
      'Computed value "%s" is not defined, or calculating it was skipped intentionally or due to an error.',
      $key
    ));
  }

}
