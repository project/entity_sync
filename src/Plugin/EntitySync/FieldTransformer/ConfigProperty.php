<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Transformer that sets the field to a fixed value defined in configuration.
 *
 * No need to provide type casting here; YAML syntax should support all types
 * that may be needed.
 *
 * Supported configuration properties:
 * - config: (string, required) The configuration name to get the property
 *   from.
 * - property: (string, required) The property containing the value to set the
 *   field to.
 *
 * @EntitySyncFieldTransformer(
 *   id = "config_property"
 * )
 */
class ConfigProperty extends PluginBase {

  /**
   * Constructs a ConfigProperty object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $constraint_manager
    );

    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'config',
      'property',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue();
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue();
  }

  /**
   * {@inheritdoc}
   *
   * There's no need to get the value from the field; we directly load and
   * return the requested configuration property.
   */
  protected function getLocalEntityValue(
    ContentEntityInterface $local_entity,
    array $field_info
  ) {
    return NULL;
  }

  /**
   * Loads and returns the value of the given configuration property.
   *
   * @return mixed
   *   The configuration property value.
   */
  protected function transformValue() {
    return $this->configFactory
      ->get($this->configuration['config'])
      ->get($this->configuration['property']);
  }

}
