<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\entity_sync\Utility\DateTime;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts between Unix timestamp and ISO8601 formatted time.
 *
 * Since Unix timestamps are always integers while ISO8601 are always strings,
 * we do not need a configuration option to let the plugin the incoming/outgoing
 * format. We detect automatically and convert accordintly.
 *
 * @EntitySyncFieldTransformer(
 *   id = "timestamp_iso8601"
 * )
 */
class TimestampIso8601 extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the value converted to appropraite format.
   *
   * If the value is a timestamp, it is transformed to an ISO8601 string.
   * If the value is a string, it is transformed to an integer timestamp.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value) {
    if ($value === NULL) {
      return NULL;
    }

    // phpcs:disable
    // @I Support converting timestamps to ISO8601
    //    type     : improvement
    //    priority : normal
    //    labels   : field-transformer
    // phpcs:enable
    if (DateTime::isTimestamp($value)) {
      throw new \Exception(
        'Converting a timestamp to ISO8601 format is not supported yet.'
      );
    }

    return DateTime::iso8601ToTimestamp($value);
  }

}
