<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Transformer that selects entitys ID based on a field value.
 *
 * Currently supporting impors, it looks up for entities of the configured type
 * that have the incoming value being transformed as the value of the configured
 * field. For example, if the incoming value is the SKU of a product, return the
 * ID of the product that has that SKU in the corresponding field.
 *
 * More than one ID might be returned in the array if there are multiple
 * entities with the value in the field. If needed to ensure that the result is
 * a single entity, use the `Count` validation constraint in the
 * `post_transformation` validation group.
 *
 * Currently, the IDs are returned prepared for setting the value of an entity
 * reference field i.e. `[['target_id' => $id1], ['target_id' => $id2]]`. More
 * formats can be added here.
 *
 * IMPORTANT NOTICE:
 * If this plugin is used as the only transformer for the field, or if it is the
 * first in a top level pipeline, the incoming value will be the value of a
 * remote entity property. If the property does not exist though in the remote
 * entity object, the value will be `NULL`. This will cause all entities that do
 * not have a value for the field to be considered a match. To prevent this from
 * happening there are a couple of options depending on the details of the
 * import:
 * - If `NULL` is not an accepted input value i.e. if the remote entity property
 *   is set it would never contain a `NULL` value and you want to raise an
 *   error if the remote entity property is not set or is set and it is `NULL`,
 *   use a `NotNull` validation constraint in the `pre_transformation` group
 *   with the `onFailure` option set to `fail` or `skip` to cancel the entity
 *   import or skip the specific field only.
 * - If `NULL` is an accepted input value when the remote entity property is
 *   set, but not when the remote entity property is not set, there is no
 *   configuration option at the moment that will raise an error if the property
 *   is not set on the remote entity. You can create a custom plugin that
 *   inherits from this and override the `transformImportedValue` method to do
 *   such a check and throw a `FieldImportException` error.
 *
 * Supported configuration properties:
 * - entity_type_id: (string, required) The entity type ID of the entity that
 *   will be selected.
 * - field_name: (string, required) The name of the field that will be used to
 *   look up the entity.
 * - transform_null_value: (bool, optional) If the incoming value is `NULL`, the
 *   transformer would return the IDS of all entities that do not have a value
 *   set for the configured field. To prevent this from mistakenly happening,
 *   `NULL` will be returned by default. If you do want to get the IDs of all
 *   entities that do not have a value set for the field in such case, set this
 *   option to `TRUE`.
 *
 * @EntitySyncFieldTransformer(
 *   id = "local_entity_ids_by_field"
 * )
 * phpcs:disable
 * @I Allow configuring the format
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *    notes    : For example, return the IDs as they are, formatted as they
 *               currently are i.e. for setting them in entity reference fields,
 *               or load and return the entity objects.
 * @I Add a configuration option to fail if the remote property is not set
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *    notes    : Most likely this could be supported in the base plugin so that
 *               all transformers can benefit from it, but it needs to be
 *               properly reviewed in the context of consistency of behavior
 *               between imports and exports and in the context of
 *               pseudo-fields.
 * phpcs:enable
 */
class LocalEntityIdsByField extends PluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LocalEntityIdsByField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $constraint_manager
    );

    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'transform_null_value' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'entity_type_id',
      'field_name',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    if ($value === NULL && !$this->configuration['transform_null_value']) {
      return NULL;
    }

    $query = $this->entityTypeManager
      ->getStorage($this->configuration['entity_type_id'])
      ->getQuery()
      ->accessCheck(FALSE);

    if ($value === NULL) {
      $query->condition($this->configuration['field_name'], NULL, 'IS NULL');
    }
    else {
      $query->condition($this->configuration['field_name'], $value);
    }

    $entity_ids = $query->execute();
    if (!$entity_ids) {
      return NULL;
    }

    return array_map(
      function ($id) {
        return ['target_id' => $id];
      },
      $entity_ids
    );
  }

}
