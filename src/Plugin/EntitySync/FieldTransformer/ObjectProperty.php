<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that sets the field to the property value of an object.
 *
 * Supported configuration properties:
 * - object: (string, required) The type of object to get the property value
 *   from. Supported types:
 *   - remote_entity: The remote entity being imported.
 *   - value: The value of the remote entity property being imported.
 * - property: (string, required) The name of the property containing the value
 *   to set the field to.
 *
 * Note that the object (the remote entity or the property value) must already
 * be an object such as `stdClass`, it must have the property it is requested to
 * get the value from, and it must publicly expose the property. Use validation
 * constraints to handle cases where the object might not have the property,
 * otherwise the transformed value that will be imported will be NULL.
 *
 * @EntitySyncFieldTransformer(
 *   id = "object_property"
 * )
 */
class ObjectProperty extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'object',
      'property',
    ]);

    if (!in_array($this->configuration['object'], ['remote_entity', 'value'])) {
      throw new InvalidConfigurationException(sprintf(
        'Invalid object type "%s", `remote_entity` and `value` are supported.',
        $this->configuration['object']
      ));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    switch ($this->configuration['object']) {
      case 'remote_entity':
        if (!property_exists($remote_entity, $this->configuration['property'])) {
          return NULL;
        }

        return $remote_entity->{$this->configuration['property']};

      case 'value':
        if ($value === NULL) {
          return NULL;
        }
        if (!property_exists($value, $this->configuration['property'])) {
          return NULL;
        }

        return $value->{$this->configuration['property']};
    }
  }

}
