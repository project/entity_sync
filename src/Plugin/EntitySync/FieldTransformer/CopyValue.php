<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that copies the value being imported/exported.
 *
 * Supported configuration properties:
 * - cast_to: (string, optional) The type to cast to the value being
 *   transformed. Supported types are the ones that are supported by PHP
 *   type casting:
 *   - int/integer
 *   - bool/boolean
 *   - float/double/real
 *   - string
 *   - array
 *   - object
 *   - unset
 * - prefix: (string, optional) A prefix that will be prepended before the
 *   copied value. The string concatenation operator (`.`) is used i.e. this
 *   should currently only be used with string values or with values that will
 *   be converted to strings during concatenation.
 * - suffix: (string, optional) A suffix that will be appended after the
 *   copied value. The string concatenation operator (`.`) is used i.e. this
 *   should currently only be used with string values or with values that will
 *   be converted to strings during concatenation.
 *
 * The `array` value for the `cast_to` setting could be used to convert an
 * object to an array.
 *
 * @EntitySyncFieldTransformer(
 *   id = "copy_value"
 * )
 *
 * @link https://www.php.net/manual/en/language.types.type-juggling.php
 */
class CopyValue extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cast_to' => NULL,
      'prefix' => NULL,
      'suffix' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    if (!isset($this->configuration['cast_to'])) {
      return;
    }

    $supported_types = [
      'int',
      'integer',
      'bool',
      'boolean',
      'float',
      'double',
      'real',
      'string',
      'array',
      'object',
      'unset',
    ];
    if (in_array($this->configuration['cast_to'], $supported_types)) {
      return;
    }

    throw new InvalidConfigurationException(sprintf(
      'Requested to cast the value to an unsupported type "%s"',
      $this->configuration['cast_to']
    ));
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the value casted to the configured type.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value) {
    if ($value === NULL) {
      return NULL;
    }

    if (isset($this->configuration['cast_to'])) {
      $value = $this->castValue($value, $this->configuration['cast_to']);
    }

    if (isset($this->configuration['prefix'])) {
      $value = $this->configuration['prefix'] . $value;
    }

    if (isset($this->configuration['suffix'])) {
      $value = $value . $this->configuration['suffix'];
    }

    return $value;
  }

  /**
   * Returns the value casted to the given type.
   *
   * @param mixed $value
   *   The field value before transformation.
   * @param string $type
   *   The type to cast the value to.
   *
   * @return mixed
   *   The casted value.
   */
  protected function castValue($value, $type) {
    // No need to throw an exception if the type is unsupported; we validate the
    // type in the configuration validation method. Also, PHP will throw an
    // error as well.
    // Keep separate cases for types that are the same e.g. `int/integer`
    // etc. That behavior should be decided by PHP.
    switch ($type) {
      case 'int':
        return (int) $value;

      case 'integer':
        return (integer) $value;

      case 'bool':
        return (bool) $value;

      case 'boolean':
        return (boolean) $value;

      case 'float':
        return (float) $value;

      case 'double':
        return (double) $value;

      case 'real':
        return (float) $value;

      case 'string':
        return (string) $value;

      case 'array':
        return (array) $value;

      case 'object':
        return (object) $value;

      case 'unset':
        return NULL;
    }
  }

}
