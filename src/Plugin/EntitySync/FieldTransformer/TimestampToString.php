<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts a unix timestamp to a textual time/date value.
 *
 * Supported configuration properties:
 * - format: (string) The format to convert the timestamp to. Supported formats
 *   are all formats supported by PHP's `data` function.
 *
 * @EntitySyncFieldTransformer(
 *   id = "timestamp_to_string"
 * )
 */
class TimestampToString extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['format']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the value converted to the configured format.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value) {
    if ($value === NULL) {
      return NULL;
    }

    return date($this->configuration['format'], $value);
  }

}
