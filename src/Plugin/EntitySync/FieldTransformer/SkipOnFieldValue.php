<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\SkipFieldException;
use Drupal\entity_sync\FieldTransformer\PluginBase;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Transformer that skips a field when it has a configured value.
 *
 * Supported configuration properties:
 * - value: (string, required) The value that when found the field is
 *   skipped. Currently, the following special values are supported:
 *   - '%empty%': matched when the field is empty. This is determined by the
 *     `isEmpty()` method provided by the field API.
 *   - '%not_empty%': matched when the field is not empty. This is determined by
 *     the `isEmpty()` method provided by the field API.
 *
 * phpcs:disable
 * @I Consider adding support for '%null%' and '%not_null%'
 *     type     : improvement
 *     priority : low
 *     labels   : field-transformer
 *     notes    : Slightly different than `isEmpty()`.
 *
 * @I Support value loaded from the transformer configuration
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *    notes    : Care should be taken to consider how to extract the field value
 *    and provide the necessary options e.g. first()/getValue() etc.
 *
 * @I Support reverse mode i.e. when not having the given value
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *
 * @I Support field name different than the one from $field_info
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *
 * @I Support getting the entity from the value being transformed
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *
 * @I Support option for logging or not logging a skip message
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 *    notes    : The message would likely be generic, not configurable.
 *
 * @I Support getting the field from the remote entity
 *    type     : improvement
 *    priority : low
 *    labels   : field-transformer
 *    notes    : The message would likely be generic, not configurable.
 * phpcs:disable
 *
 * @EntitySyncFieldTransformer(
 *   id = "skip_on_field_value"
 * )
 */
class SkipOnFieldValue extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'value',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value, $local_entity, $field_info);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value, $local_entity, $field_info);
  }

  /**
   * Returns the value or throws an exception based on configuration.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   *
   * @throws \Drupal\entity_sync\Exception\SkipFieldException
   *   When the value being transformed matches the configured value.
   */
  protected function transformValue($value, $entity, array $field_info) {
    $field_name = $field_info['machine_name'];

    switch ($this->configuration['value']) {
      case '%empty%':
        if ($entity->get($field_name)->isEmpty()) {
          throw new SkipFieldException();
        }
        break;

      case '%not_empty%':
        if (!$entity->get($field_name)->isEmpty()) {
          throw new SkipFieldException();
        }
        break;
    }

    return $value;
  }

}
