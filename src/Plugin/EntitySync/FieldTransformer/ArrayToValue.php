<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that extracts an element from a given array.
 *
 * Supported configuration properties:
 * - array_type: (string, required) The type of the input array to expect.
 *   Supported array types are:
 *   - numerical
 *   - associative
 * - element_key: (string, optional) The key of the array element to return.
 *   Required when the array type is associative. If not given and the array
 *   type is numerical, the current key of the array is used as returned by
 *   PHP's `key` function.
 *
 * @EntitySyncFieldTransformer(
 *   id = "array_to_value"
 * )
 */
class ArrayToValue extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['array_type']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $array,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($array);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $array,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($array);
  }

  /**
   * Returns the value extracted from the array according to configuration.
   *
   * @param array|null $array
   *   The array to extract the value from.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($array) {
    if ($array === NULL) {
      return NULL;
    }

    if (!is_array($array)) {
      throw new \InvalidArgumentException(sprintf(
        'An array is required to extract a value from, %s given.',
        gettype($array)
      ));
    }

    // Nothing we can extract from if the array is empty.
    if (!$array) {
      return NULL;
    }

    if ($this->configuration['array_type'] === 'numerical') {
      return $array[($this->configuration['element_key'] ?? key($array))];
    }
    elseif ($this->configuration['array_type'] !== 'associative') {
      throw new InvalidConfigurationException(sprintf(
        'Unsupported array type "%s", supported options are `numerical` and `associative`.',
        $this->configuration['array_type']
      ));
    }

    if (!isset($this->configuration['element_key'])) {
      throw new InvalidConfigurationException(
        'The `element_key` configuration option must be defined.'
      );
    }

    return $array[$this->configuration['element_key']] ?? NULL;
  }

}
