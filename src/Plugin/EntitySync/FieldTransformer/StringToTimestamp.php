<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts a textual time/date value to a Unix timestamp.
 *
 * Supported formats for the original value are all values supported by PHP's
 * `strtotime` function.
 *
 * @EntitySyncFieldTransformer(
 *   id = "string_to_timestamp"
 * )
 */
class StringToTimestamp extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    if ($value === NULL) {
      return NULL;
    }

    // We used to validate the result here. Now this should be done with
    // validation constraints so that validation can be done either before or
    // after the value is transformed, and so that failure handlers are
    // supported as well i.e. what to do in the case of failure.
    return strtotime($value);
  }

}
