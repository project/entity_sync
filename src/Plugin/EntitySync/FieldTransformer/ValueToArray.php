<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that wraps a value in an array.
 *
 * The transformer gets a value of any type and it returns it as an element in a
 * new array.
 *
 * Supported configuration properties:
 * - array_type: (string, required) The type of the array to return. Supported
 *   array types are:
 *   - numerical
 *   - associative
 * - element_key: (string, optional) The key of the array element that the value
 *   will be copied in. Required when the array type is associative. If not
 *   given and the array type is numerical, then `0` is used as the element key.
 *
 * @EntitySyncFieldTransformer(
 *   id = "value_to_array"
 * )
 */
class ValueToArray extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['array_type']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the value wrapped in an array according to configuration.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value) {
    if ($value === NULL) {
      return NULL;
    }

    if ($this->configuration['array_type'] === 'numerical') {
      return [
        ($this->configuration['element_key'] ?? 0) => $value,
      ];
    }
    elseif ($this->configuration['array_type'] !== 'associative') {
      throw new InvalidConfigurationException(sprintf(
        'Unsupported array type "%s", supported options are `numerical` and `associative`.',
        $this->configuration['array_type']
      ));
    }

    if (!isset($this->configuration['element_key'])) {
      throw new InvalidConfigurationException(
        'The `element_key` configuration option must be defined.'
      );
    }

    return [
      $this->configuration['element_key'] => $value,
    ];
  }

}
