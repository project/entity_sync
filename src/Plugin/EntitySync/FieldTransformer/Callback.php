<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that applies one or more callbacks to the value.
 *
 * Supported configuration properties:
 * - callbacks: (array, required) A numerical array containing one or more
 *   items, each defining a callback. Each callback is an associative array
 *   containing the following items:
 *   - callback (string, required): The callback to be called for processing the
 *     value being transformed, in one of the formats supported by
 *     `call_user_func_array()`. The follow special formats are also supported:
 *     - $this::objectMethod: Will call the `objectMethod` on the value being
 *       transformed, which must be an object.
 *     - @service_id:serviceMethod: Will call the `serviceMethod` on the
 *       `service_id` service that must be registered in the Drupal container.
 *   - arguments (string, optional, defaults to `value`): Defines the arguments
 *     that will be passed to the callback. Supported values are:
 *     - value: Only the value being transformed will be passed to the callback.
 *     - all: All variables available will be passed to the callback. See
 *       `$this::transformImportedValue()`,  `$this::transformExportedValue()`.
 *     - custom: A custom sequence of arguments will be passed to the callback.
 *       The `custom_arguments` property must be provided in this case.
 *     - none: No arguments will be passed to the callback.
 *   - custom_arguments (array, optional): A numerical array containing the
 *     arguments to be passed to the callback in the case of using the `custom`
 *     value in the `arguments` option. The arguments may be any type/value
 *     supported by YAML, or any of the arguments available to the transforming
 *     method i.e. `transformImportedValue()` or `transformExportedValue()`. To
 *     request an argument of the transforming method, the name of the argument
 *     must be provided within '%' e.g. '%local_entity%'.
 *
 * If more than one callback is defined, they will be called one after the other
 * in the order provided. This is made possible as a convenience feature because
 * calling multiple time this plugin via a pipeline is syntactically cumbersome.
 *
 * The ability to pass all available variables as arguments to the callback is
 * provided for cases that writing a field transformer plugin is not
 * desirable. It is also the first step to deprecate direct callbacks in the
 * field mapping; they can be moved to a field transformer via this
 * plugin. However, these cases are discouraged and writing field transformers
 * should be the solution where possible. We therefore set the default setting
 * to providing only the value being transformed to the callback, since this
 * plugin is provided mostly for easily calling PHP functions e.g. strtolower
 * etc. for transforming the value. Many of such functions raise an error if
 * unexpected arguments are provided.
 *
 * @EntitySyncFieldTransformer(
 *   id = "callback"
 * )
 *
 * @link https://www.php.net/manual/en/function.call-user-func-array.php
 */
class Callback extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'callbacks',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue(
      $value,
      [
        '%value%' => $value,
        '%remote_entity%' => $remote_entity,
        '%local_entity%' => $local_entity,
        '%field_info%' => $field_info,
        '%context%' => $context,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue(
      $value,
      [
        '%value%' => $value,
        '%local_entity%' => $local_entity,
        '%remote_entity_id%' => $remote_entity_id,
        '%field_info%' => $field_info,
        '%context%' => $context,
      ]
    );
  }

  /**
   * Returns the value as transformed by the callbacks.
   *
   * @param mixed $value
   *   The value to transform.
   * @param array $arguments
   *   The arguments to pass to the callbacks.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value, array $arguments) {
    if ($value === NULL) {
      return NULL;
    }

    foreach ($this->configuration['callbacks'] as $callback) {
      if (($callback['arguments'] ?? 'value') === 'value') {
        $value = call_user_func_array(
          $this->getCallback($callback, $value),
          [$value]
        );
      }
      elseif ($callback['arguments'] === 'all') {
        $value = call_user_func_array(
          $this->getCallback($callback, $value),
          array_values($arguments)
        );
      }
      elseif ($callback['arguments'] === 'custom') {
        $value = call_user_func_array(
          $this->getCallback($callback, $value),
          $this->prepareCustomArguments($callback, $arguments)
        );
      }
      elseif ($callback['arguments'] === 'none') {
        $value = call_user_func_array(
          $this->getCallback($callback, $value),
          []
        );
      }
      else {
        throw new InvalidConfigurationException(sprintf(
          'Unsupported option "%s" provided for callback arguments, expected "value", "all", "custom" or "none".',
          $callback['arguments']
        ));
      }

      $arguments['%value%'] = $value;
    }

    return $value;
  }

  /**
   * Returns the callback as determined by the given callback configuration.
   *
   * @param array $callback
   *   The callback configuration.
   * @param mixed $value
   *   The value being transformed.
   *
   * @return mixed
   *   The callback.
   */
  protected function getCallback(array $callback, $value) {
    // Special formats that we support have a `::` separator. If that is not
    // present, the `callback` directly contains a callback that does not
    // require special handling.
    $callback_parts = explode('::', $callback['callback']);
    if (count($callback_parts) !== 2) {
      return $callback['callback'];
    }

    // Call a method on the value.
    if ($callback_parts[0] === '$this') {
      return [$value, $callback_parts[1]];
    }

    // Call a method on a service.
    if (strpos($callback_parts[0], '@') === 0) {
      return [
        \Drupal::service(substr($callback_parts[0], 1)),
        $callback_parts[1],
      ];
    }

    return $callback['callback'];
  }

  /**
   * Prepares the arguments for the custom arguments option.
   *
   * Custom arguments can be any types/values supported by the YAML, or any of
   * the arguments available to the transforming method. In the latter case, the
   * name of the argument must be provided within '%' e.g. '%local_entity%'.
   * Here we make the appropriate replacements.
   *
   * @param array $callback
   *   The configuration for the callback being processed.
   * @param array $arguments
   *   An associative array containing the arguments available to the
   *   transforming method, keyed by the name of the argument prefixed and
   *   suffixed with '%'.
   *
   * @return array
   *   An array containing the arguments to be passed to the callback.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the custom arguments for the callback are missing or are not
   *   provided in the expected format.
   */
  protected function prepareCustomArguments(
    array $callback,
    array $arguments
  ) {
    if (!array_key_exists('custom_arguments', $callback)) {
      throw new InvalidConfigurationException(
        'The custom arguments are not be defined.'
      );
    }
    if (!is_array($callback['custom_arguments'])) {
      throw new InvalidConfigurationException(
        'The custom arguments must be given as an array.'
      );
    }

    $function = function ($requested_argument) use ($arguments) {
      if (!is_string($requested_argument)) {
        return $requested_argument;
      }
      if (!array_key_exists($requested_argument, $arguments)) {
        return $requested_argument;
      }
      return $arguments[$requested_argument];
    };
    return array_map($function, $callback['custom_arguments']);
  }

}
