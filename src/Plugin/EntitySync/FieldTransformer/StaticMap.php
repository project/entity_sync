<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that converts a value according to a map.
 *
 * Supported configuration properties:
 * - map: (array, required) An associative array keyed by the value being
 *   transformed and containing the value that should be given after
 *   transformation.
 * - default_value: (mixed, optional, defaults to `NULL`) The value to use when
 *   the value being transformed is not found in the map.
 * - null_key: (string, optional) The value to return when a `NULL` value is
 *   passed to the transformer. The symfony YAML component used to parse YAML
 *   files does not accept `NULL` as a mapping node key. In such cases, set the
 *   `null_key` configuration option to a string e.g. 'null' and this plugin
 *   will use the corresponding value when `NULL` is passed to it for
 *   transformation.
 *
 * @EntitySyncFieldTransformer(
 *   id = "static_map"
 * )
 */
class StaticMap extends PluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'default_value' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['map']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the transformed value.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   * phpcs:disable
   * @I Remove unnecessary `null_key` configuration property
   *    type     : task
   *    priority : low
   *    labels   : config, field-transformer
   *    notes    : The `null_value` property provided by the base plugin should
   *               be used instead.
   * phpcs:enable
   */
  protected function transformValue($value) {
    $has_null_key = array_key_exists('null_key', $this->configuration);
    if ($value === NULL && $has_null_key) {
      return $this->configuration['map'][$this->configuration['null_key']];
    }

    if ($value === NULL || !isset($this->configuration['map'][$value])) {
      return $this->configuration['default_value'];
    }

    return $this->configuration['map'][$value];
  }

}
