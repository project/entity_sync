<?php

namespace Drupal\entity_sync\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Transformer that adds fixed values to an array.
 *
 * It can be used, for example, as part of a pipeline to add additional
 * properties required by a Drupal field such as adding the currency code to an
 * incoming price number when importing into a price field, after converting the
 * number to an array using `value_to_array`.
 *
 * Supported configuration properties:
 * - array_type: (string, required) The type of the input array to expect.
 *   Supported array types are:
 *   - numerical
 *   - associative
 * - values: (array, required) An associative array containing the values to add
 *   to the array.
 *
 * Currently, only assiative arrays are supported. Support for appending values
 * to numerical arrays should be added here though thus we add the `array_type`
 * option for forward-compatibility.
 *
 * @EntitySyncFieldTransformer(
 *   id = "array_add_fixed_values"
 * )
 * phpcs:disable
 * @I Support append fixed values to numerical arrays
 *    type     : improvement
 *    priority : normal
 *    labels   : field-transformer
 * phpcs:enable
 */
class ArrayAddFixedValues extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties(['array_type', 'values']);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $array,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    if ($array === NULL) {
      return NULL;
    }
    if (!is_array($array)) {
      throw new \InvalidArgumentException(sprintf(
        'An array is required to add values to it, %s given.',
        gettype($array)
      ));
    }

    if ($this->configuration['array_type'] === 'numerical') {
      throw new InvalidConfigurationException(sprintf(
        'Numerical arrays are not supported yet by "%s".',
        __CLASS__
      ));
    }
    elseif ($this->configuration['array_type'] !== 'associative') {
      throw new InvalidConfigurationException(sprintf(
        'Unsupported array type "%s", supported options are `numerical` and `associative`.',
        $this->configuration['array_type']
      ));
    }

    foreach ($this->configuration['values'] as $key => $value) {
      $array[$key] = $value;
    }

    return $array;
  }

}
