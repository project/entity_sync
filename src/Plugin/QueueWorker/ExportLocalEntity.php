<?php

namespace Drupal\entity_sync\Plugin\QueueWorker;

use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\entity_sync\Entity\SyncInterface;
// Rename it to prevent confusion with Drupal's entity manager service.
use Drupal\entity_sync\Export\EntityManagerInterface as ExportEntityManagerInterface;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\Queue\WorkerBase;
use Drupal\entity_sync\Queue\RunOperationWorkerTrait;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue worker for exporting a local entity.
 *
 * @QueueWorker(
 *  id = "entity_sync_export_local_entity",
 *  title = @Translation("Export local entity"),
 *  cron = {"time" = 60}
 * )
 */
class ExportLocalEntity extends WorkerBase {

  use RunOperationWorkerTrait;

  /**
   * The Entity Sync export entity manager service.
   *
   * @var \Drupal\entity_sync\Export\EntityManagerInterface
   */
  protected $manager;

  /**
   * Constructs a new ExportLocalEntity instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The Entity Synchronization configuration entity loader.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   This module's logger channel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_sync\Export\EntityManagerInterface $manager
   *   The Entity Sync export entity manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigEntityLoaderInterface $config_entity_loader,
    LoggerChannelInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
    ExportEntityManagerInterface $manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_entity_loader,
      $logger
    );

    $this->manager = $manager;

    // Injections required by traits.
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_sync.config_entity_loader'),
      $container->get('logger.channel.entity_sync'),
      $container->get('entity_type.manager'),
      $container->get('entity_sync.export.entity_manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Data should be an associative array with the following elements:
   * - sync_id: The ID of the Synchronization or Operation Type that defines
   *   the export operation to run.
   * - entity_type_id: The type ID of the entity being exported.
   * - entity_id: The ID of the entity being exported.
   *
   * @throws \InvalidArgumentException
   *   When invalid or inadequate data are passed to the queue worker.
   * @throws \RuntimeException
   *   When no entity or no operation was found for the given data.
   *
   * @see \Drupal\entity_sync\Export\EntityManager::exportLocalEntity()
   * phpcs:disable
   * @I Write tests for the export local entity queue worker
   *    type     : task
   *    priority : normal
   *    labels   : export, testing, queue
   * phpcs:enable
   */
  protected function doProcessItem(SyncInterface $sync, array $data) {
    if ($sync->getEntityTypeId() === 'entity_sync_operation_type') {
      $this->runOperation($sync, $data['operation_id'], $data['context'] ?? []);
      return;
    }

    $this->runSynchronization(
      $sync,
      $data['entity_type_id'],
      $data['entity_id'],
      $data['context'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getActionType(SyncInterface $sync, array $data) {
    return 'export_entity';
  }

  /**
   * {@inheritdoc}
   */
  protected function logThrowable(\Throwable $throwable, array $data) {
    $entity_id = NULL;

    if (isset($data['operation_id'])) {
      $entity_id = $this->entityTypeManager
        ->getStorage('entity_sync_operation')
        ->load($data['operation_id'])
        ->get(OperationField::ENTITY)
        ->first()
        ->getValue()['target_id'];
    }
    else {
      $entity_id = $data['entity_id'];
    }

    $this->logger->error(
      'A "@throwable_type" error was thrown while executing a queued export of the entity with ID "@entity_id" as part of the "@sync_id" synchronization. The error message was: @throwable_message',
      [
        '@throwable_type' => get_class($throwable),
        '@throwable_message' => $throwable->getMessage(),
        '@sync_id' => $data['sync_id'],
        '@entity_id' => $entity_id,
      ]
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function validateData(SyncInterface $sync, array $data) {
    if ($sync->getEntityTypeId() === 'entity_sync_operation_type') {
      $this->validateOperationData($sync, $data);
      return;
    }

    if (empty($data['entity_type_id'])) {
      throw new \InvalidArgumentException(
        'The type ID of the entity being exported must be given.'
      );
    }

    if (empty($data['entity_id'])) {
      throw new \InvalidArgumentException(
        'The ID of the entity being exported must be given.'
      );
    }
  }

  /**
   * Runs the synchronization operation for the given details.
   *
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization the defines the operation that will be run.
   * @param string $entity_type_id
   *   The ID of the operation.
   * @param int|string $entity_id
   *   The ID of the entity being exported.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @throws \RuntimeException
   *   When no entity was found for the given data.
   */
  protected function runSynchronization(
    SyncInterface $sync,
    string $entity_type_id,
    $entity_id,
    array $context = []
  ) {
    // phpcs:disable
    // @I Should we load the uncached entity?
    //    type     : bug
    //    priority : normal
    //    labels   : cache, export, queue
    // phpcs:enable
    $entity = $this->entityTypeManager
      ->getStorage($entity_type_id)
      ->load($entity_id);
    if (!$entity) {
      throw new \RuntimeException(
        sprintf(
          'No "%s" entity with ID "%s" found to export.',
          $entity_type_id,
          $entity_id,
        )
      );
    }

    $this->manager->exportLocalEntity(
      $sync->id(),
      $entity,
      empty($context) ? [] : ['context' => $context]
    );
  }

}
