<?php

namespace Drupal\entity_sync\Plugin\QueueWorker;

use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\entity_sync\Entity\SyncInterface;
use Drupal\entity_sync\Import\ManagerInterface;
use Drupal\entity_sync\Queue\WorkerBase;

use Drupal\Core\Logger\LoggerChannelInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue worker for importing a list of entities.
 *
 * @QueueWorker(
 *  id = "entity_sync_import_list",
 *  title = @Translation("Import List"),
 *  cron = {"time" = 60}
 * )
 */
class ImportList extends WorkerBase {

  /**
   * The Entity Sync import manager service.
   *
   * @var \Drupal\entity_sync\Import\ManagerInterface
   */
  protected $manager;

  /**
   * Constructs a new ImportList instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The Entity Synchronization configuration entity loader.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   This module's logger channel.
   * @param \Drupal\entity_sync\Import\ManagerInterface $manager
   *   The Entity Sync import manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ConfigEntityLoaderInterface $config_entity_loader,
    LoggerChannelInterface $logger,
    ManagerInterface $manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $config_entity_loader,
      $logger
    );

    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_sync.config_entity_loader'),
      $container->get('logger.channel.entity_sync'),
      $container->get('entity_sync.import.manager')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Data should be an associative array with the following elements:
   * - sync_id: The ID of the synchronization that defines the import operation
   *   to run.
   * - filters: (Optional) An array of filters to pass to the import entity
   *   manager.
   * - options: (Optional) An array of options to pass to the import entity
   *   manager.
   *
   * @see \Drupal\entity_sync\Import\Manager::importRemoteList()
   * phpcs:disable
   * @I Add an option to be notified when the import is run
   *    type     : feature
   *    priority : normal
   *    labels   : import, list
   * @I Add support for running operation entitites via queue
   *    type     : feature
   *    priority : normal
   *    labels   : import, list, queue
   * phpcs:enable
   */
  protected function doProcessItem(SyncInterface $sync, array $data) {
    $this->manager->importRemoteList(
      $data['sync_id'],
      $data['filters'] ?? [],
      $data['options'] ?? []
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getActionType(SyncInterface $sync, array $data) {
    return 'import_list';
  }

}
