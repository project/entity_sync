<?php

namespace Drupal\entity_sync\StateMachine;

use Drupal\state_machine\Plugin\Workflow\WorkflowInterface;

/**
 * Defines the interface for operation workflows.
 */
interface OperationWorkflowInterface extends WorkflowInterface {

  /**
   * Returns the ID of the transition that initiates running the operation.
   *
   * @return string
   *   The ID of the `run` transition.
   */
  public function getRunTransition();

}
