<?php

namespace Drupal\entity_sync\StateMachine;

use Drupal\state_machine\Guard\GuardFactoryInterface;
use Drupal\state_machine\Plugin\Workflow\Workflow as WorkflowBase;

/**
 * The plugin class for operation workflows.
 *
 * We add a property for defining which transition is the one that initiates
 * running the operation.
 */
class OperationWorkflow extends WorkflowBase implements
  OperationWorkflowInterface {

  /**
   * The transition that initiates running the operation.
   *
   * @var string
   */
  protected $runTransition = 'run';

  /**
   * Constructs a new OperationWorkflow object.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The workflow plugin_id.
   * @param mixed $plugin_definition
   *   The workflow plugin implementation definition.
   * @param \Drupal\state_machine\Guard\GuardFactoryInterface $guard_factory
   *   The guard factory.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    GuardFactoryInterface $guard_factory
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $guard_factory
    );

    if (isset($plugin_definition['run_transition'])) {
      $this->runTransition = $plugin_definition['run_transition'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRunTransition() {
    return $this->runTransition;
  }

}
