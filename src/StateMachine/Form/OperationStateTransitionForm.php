<?php

namespace Drupal\entity_sync\StateMachine\Form;

use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
use Drupal\entity_sync\OperationConfigurator\PluginInterface;
use Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface;
use Drupal\state_machine\Form\StateTransitionForm;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the transition form for the operation state field.
 */
class OperationStateTransitionForm extends StateTransitionForm {

  /**
   * Constructs a new OperationStateTransitionForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $button_keys = Element::children($form['actions']);
    if (!count($button_keys)) {
      return $form;
    }

    // phpcs:disable
    // @I Use config entity loader when inheriting UI settings is supported
    //    type     : improvement
    //    priority : normal
    //    labels   : sync-inheritance, ui
    // phpcs:enable
    $plugin = $this->entityTypeManager
      ->getStorage('entity_sync_operation_type')
      ->loadWithPluginInstantiated($this->getEntity()->bundle())
      ->getPlugin();

    $form = $this->disableDisallowedTransitions(
      $form,
      $plugin,
      $button_keys
    );
    $form = $this->disableRunTransition(
      $form,
      $plugin,
      $button_keys
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Do not do anything in the case that the submission was triggered by a
    // custom button that does not correspond to a transition.
    $triggering_element = $form_state->getTriggeringElement();
    if (!isset($triggering_element['#transition'])) {
      return parent::submitForm($form, $form_state);
    }

    $operation = $this->getEntity();
    $operation_type = $this->entityTypeManager
      ->getStorage('entity_sync_operation_type')
      ->loadWithPluginInstantiated($operation->bundle());

    $transition = $triggering_element['#transition'];
    $state_item = $operation->get(OperationField::STATE)->first();
    $run_transition = $state_item->getWorkflow()->getRunTransition();

    if ($transition->getId() !== $run_transition) {
      return parent::submitForm($form, $form_state);
    }

    // If the transition is the `run` transition, applying the transition above
    // will have moved the operation to the `running` state. We now actually run
    // the operation. The runner, defined by the configurator plugin, or a
    // post-terminate event subscriber are responsible for moving the operation
    // to its final state after it has been run e.g. completed or failed.
    $state_item->applyTransition($transition);
    $this->entityTypeManager
      ->getStorage('entity_sync_operation')
      ->save($operation);

    // We do not need to check whether the plugin provides a runner because in
    // that case the button should be disabled when building the form. If there
    // is custom manipulation of the form, let it error - something is wrong.
    $operation_type->getPlugin()->runner()->run($this->entity);
  }

  /**
   * Disables transitions that should not be run via the UI.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\entity_sync\OperationConfigurator\PluginInterface $plugin
   *   The configurator plugin for the form's operation entity.
   * @param array $button_keys
   *   An array containing the keys of the form's action buttons.
   *
   * @return array
   *   The updated form render array.
   *
   * @see \Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface
   */
  protected function disableDisallowedTransitions(
    array $form,
    PluginInterface $plugin,
    array $button_keys
  ) {
    $disallowed_transitions = [];
    $plugin_config = $plugin->getConfiguration();
    if (!empty($plugin_config['ui']['disallowed_transitions'])) {
      $disallowed_transitions = $plugin_config['ui']['disallowed_transitions'];
    }

    $actions = &$form['actions'];
    foreach ($button_keys as $button_key) {
      // We do not alter any custom action buttons that do not correspond to
      // transitions.
      if (!isset($actions[$button_key]['#transition'])) {
        continue;
      }

      $button_transition = $actions[$button_key]['#transition']->getId();
      if (in_array($button_transition, $disallowed_transitions)) {
        $actions[$button_key]['#access'] = FALSE;
        continue;
      }
    }

    return $form;
  }

  /**
   * Disables the `run` transition when appropriate.
   *
   * Plugins need to implement the
   * `\Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface` in
   * order for its operations to be run via the UI. Otherwise they may only be
   * run programmatically.
   *
   * @param array $form
   *   The form render array.
   * @param \Drupal\entity_sync\OperationConfigurator\PluginInterface $plugin
   *   The configurator plugin for the form's operation entity.
   *
   * @return array
   *   The updated form render array.
   *
   * @see \Drupal\entity_sync\OperationConfigurator\SupportsRunnerInterface
   */
  protected function disableRunTransition(
    array $form,
    PluginInterface $plugin
  ) {
    if ($plugin instanceof SupportsRunnerInterface) {
      return $form;
    }

    $run_transition = $this->getEntity()
      ->get(OperationField::STATE)
      ->first()
      ->getWorkflow()
      ->getRunTransition();
    if (!isset($form['actions'][$run_transition])) {
      return $form;
    }

    $form['actions'][$run_transition]['#disabled'] = TRUE;
    $form['actions']['notice'] = [
      '#markup' => '<p>' . $this->t(
        'Operations of this type may only be run programmatically.'
      ) . '</p>',
    ];

    return $form;
  }

}
