<?php

namespace Drupal\entity_sync\FieldTransformer;

use Drupal\entity_sync\Exception\InvalidConfigurationException;
use Drupal\entity_sync\Exception\SkipFieldException;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Plugin\PluginBase as CorePluginBase;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Default base class for field transformer plugin implementations.
 *
 * Supported configuration properties provided for all plugins extending this
 * class:
 * Applicable to both import and export configurations:
 * - null_value: (mixed, optional, defaults to NULL) NULL in Drupal signifies an
 *   empty value i.e. the field has no value. However, the system that we are
 *   exporting to may use a different value for this. For example, CSV files
 *   commonly use an empty string for no value. We therefore need to map the
 *   values that mean NULL on both systems. When this value is set:
 *   - When exporting a NULL value i.e. an empty Drupal field, we export this
 *     value instead.
 *   - When importing a remote value that equals this setting, NULL will be
 *     imported thus setting the field to have no value.
 * Applicable to export configurations only:
 * - getter: (string, optional) The name of a getter method to use when getting
 *   the value of the property/field on the local entity. If not defined, the
 *   `get` method provided by the Field API will be used. If defined, the method
 *   must exist on the local entity. Applies to exports only.
 *   Note that this only provides the initial value for the transformation chain
 *   and it will only have any effect when applied to the top level plugin i.e.
 *   when there is only one transformer or when added to the pipeline
 *   configuration.
 *   The following special values can also be used:
 *   - %null%: `NULL` will be used as the initial value without loading any
 *     field from the local entity.
 *   - %entity%: The local entity will be used as the initial value without
 *     loading any filed.
 * - auto_extract_property: (bool, optional, defaults to TRUE) Field items store
 *   their values as an array of properties. The majority of field types,
 *   however, only have one main property and we want that value to be sent to
 *   the remote resource directly in most cases. Or, some field types do have
 *   extra properties that are not essential and we do not want to send them to
 *   the remote resource. When this option is set to TRUE the field value will
 *   be passed to the transformer method as a numeric array containing directly
 *   the main property values. Applies only when using the default
 *   `getLocalEntityValue` for getting the field value from the entity. If a
 *   a getter method is defined this is bypassed because we don't know the
 *   format that the getter returns the value in.
 * - extract_property: (string, optional) Modifies the field value before
 *   transformation so that only the values for the given property are contained
 *   in the field value array. Applies only when using the default
 *   `getLocalEntityValue` for getting the field value from the entity. If a
 *   a getter method is defined this is bypassed because we don't know the
 *   format that the getter returns the value in.
 * - extract_properties: (string[], optional) Modifies the field value before
 *   transformation so that only the values for the given properties are
 *   contained in the field value array. Applies only when using the default
 *   `getLocalEntityValue` for getting the field value from the entity. If a
 *   a getter method is defined this is bypassed because we don't know the
 *   format that the getter returns the value in.
 * - auto_extract_delta: (bool, optional, defaults to TRUE) Field item lists
 *   contain their values in an array even when they are only meant to hold a
 *   single value. When this option is set to TRUE the field value will be
 *   passed to the transformer method directly i.e. without a wrapper array - if
 *   the field cardinality is 1. Applies only when using the default
 *   `getLocalEntityValue` for getting the field value from the entity. If a
 *   a getter method is defined this is bypassed because we don't know the
 *   format that the getter returns the value in.
 * - extract_delta: (int, optional) Modifies the field value
 *   before transformation so that only the value for the given delta is
 *   contained in the field value array. Applies only when using the default
 *   `getLocalEntityValue` for getting the field value from the entity. If a
 *   a getter method is defined this is bypassed because we don't know the
 *   format that the getter returns the value in.
 * - extract_deltas: (int[], optional) Modifies the field
 *   value before transformation so that only the value for the given delta is
 *   contained in the field value array. Applies only when using the default
 *   `getLocalEntityValue` for getting the field value from the entity. If a
 *   a getter method is defined this is bypassed because we don't know the
 *   format that the getter returns the value in.
 * Applicable to import configurations only:
 * - setter: (string, optional) The name of a setter method to use when setting
 *   the value of the property/field on the local entity. If not defined, the
 *   `set` method provided by the Field API will be used. If defined, the method
 *   must exist on the local entity. Applies to imports only.
 *
 * Note that default values (here or in inheriting classes) should be provided
 * in `defaultConfiguration` for properties that are optional - and only for
 * those. Setting default values for properties that are required nullifies the
 * validation and that can lead to mistakes and unintended behavior - there will
 * always be a value (the provided default) and validation will always pass even
 * when the configuration does not provide values for the properties that are
 * meant to be required.
 *
 * The `defaultConfiguration` method is meant to do just that - provide default
 * values for the properties. Which properties are supported should be indicated
 * in the class description, like above.
 *
 * Moreover, note that the `NULL` value has a special, but expected, meaning: it
 * means that a value does not exist. Plugins should return `NULL` as the
 * transformed value in all cases that should be interpreted as if the value
 * does not exist. For example, if an array of entities is loaded and the result
 * is an empty array, `NULL` should be returned instead of the empty array. See
 * the `local_entity_ids_by_field` plugin for an example.
 *
 * If the value to be transformed is already `NULL`, it should be kept the same
 * by plugins i.e. the first things they should do in their
 * `transformExportedValue` and `trasformImportedValue` methods is to check if
 * the value is `NULL` and directly return `NULL` if so. Otherwise, if the
 * plugin returns a different value, some features might not work as expected -
 * such as the following:
 * - The `null_value` configuration property will not be respected, if used.
 * - The `NotNull` validation constraint, commonly used to detect non-existing
 *   values on required fields, will fail to detect such cases.
 *
 * If it is needed to use (export or import) a different value when the result
 * of the transformation process is `NULL`, the `null_value` configuration
 * property should be used. This should be done at the top level plugin i.e.
 * either on the plugin being used individually or on the pipeline plugin if a
 * multiple plugins are used as part of a pipeline.
 *
 * There are a few exceptions to this rule:
 * - If custom logic is needed to determine the value; this should be done in a
 *   custom plugin.
 * - Plugins where the transformed value does not depend on the input. For
 *   example, the `config_property` and `fixed_value` plugins.
 * - The `pipeline` transformer; it does not immediately return `NULL` because
 *   it needs to hand over the value as is to the child plugins to do their
 *   processing, even if the initial value is `NULL`.
 * - The `static_map` plugin has, for legacy reasons, its own configuration
 *   property for handling `NULL` values; it therefore implements its own
 *   logic.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface {

  /**
   * The constraint plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $constraintManager;

  /**
   * The constraint validator.
   *
   * @var \Symfony\Component\Validator\Validator\ValidatorInterface
   */
  protected $constraintValidator;

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
    $this->validateConfiguration();

    $this->constraintValidator = $constraint_validator;
    $this->constraintManager = $constraint_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => [$this->pluginDefinition['provider']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'getter' => NULL,
      'setter' => NULL,
      'auto_extract_property' => TRUE,
      'extract_property' => NULL,
      'extract_properties' => [],
      'auto_extract_delta' => TRUE,
      'extract_delta' => NULL,
      'extract_deltas' => [],
      'null_value' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function export(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context,
    $original_value = NULL,
    bool $get_value = TRUE
  ) {
    if ($get_value === TRUE) {
      $original_value = $this->getLocalEntityValue($local_entity, $field_info);
    }

    return $this->processExportedValue(
      $original_value,
      $local_entity,
      $remote_entity_id,
      $field_info,
      $context
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context,
    $original_value = NULL,
    bool $get_value = TRUE,
    bool $set_value = TRUE
  ) {
    if ($get_value === TRUE) {
      $remote_name = $field_info['remote_name'] ?? NULL;
      $original_value = $remote_entity->{$remote_name} ?? NULL;
    }

    $transformed_value = $this->processImportedValue(
      $original_value,
      $remote_entity,
      $local_entity,
      $field_info,
      $context
    );

    if ($set_value && $local_entity === NULL) {
      throw new \InvalidArgumentException(
        'The local entity must be given in order to set the field value.'
      );
    }

    if ($set_value) {
      $this->setLocalEntityValue(
        $local_entity,
        $field_info,
        $transformed_value
      );
    }

    return $transformed_value;
  }

  /**
   * Validates the plugin configuration.
   *
   * This method is called in the constructor so that errors in the plugin's
   * configuration are caught.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the plugin is improperly configured.
   * phpcs:disable
   * @I Validate configuration upon import when supported by core
   *    type     : bug
   *    priority : low
   *    labels   : config, field-transformer, validation
   * @I Support validating configuration property types e.g. string, int etc.
   *    type     : bug
   *    priority : low
   *    labels   : config, field-transformer, validation
   * phpcs:enable
   */
  protected function validateConfiguration() {
    // Nothing to validate by default. Plugin implementations may define
    // additional configuration properties which they can validate by overriding
    // this method.
  }

  /**
   * Validates that the given properties are present in the configuration.
   *
   * @param array $keys
   *   The required configuration property keys.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When one or more required properties are missing.
   * phpcs:disable
   * @I Support requiring child properties
   *    type     : improvement
   *    priority : low
   *    labels   : config, field-transformer
   * phpcs:enable
   */
  protected function validateConfigurationRequiredProperties(array $keys) {
    $missing_keys = array_diff($keys, array_keys($this->configuration));
    if (!$missing_keys) {
      return;
    }

    throw new InvalidConfigurationException(sprintf(
      'The following configuration properties are required: %s',
      implode(', ', $missing_keys)
    ));
  }

  /**
   * Validates that one of the given properties are present in config.
   *
   * This validates that one, and only one, of the given properties is present
   * in the configuration. It should be used when a field transformer supports
   * two or more options that conflict with one another.
   *
   * @param array $keys
   *   The configuration property keys.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When none or more than one of the given properties exist in
   *   configuration.
   */
  protected function validateConfigurationOneOfProperties(array $keys) {
    $existing_keys = array_intersect($keys, array_keys($this->configuration));
    if (count($existing_keys) === 1) {
      return;
    }

    throw new InvalidConfigurationException(\sprintf(
      'Exactly one of the following configuration properties are required: %s',
      implode(', ', $existing_keys)
    ));
  }

  /**
   * Returns the local entity field value.
   *
   * If a getter method is defined in the plugin configuration, it uses
   * that. Otherwise it uses the `get` method provided by the
   * `FieldItemListInterface` API.
   *
   * Additionally, it extracts the main property value only, and the first value
   * only if we have a single cardinality field. This is the default behavior
   * because it is what we want in most cases. It can be changed in the plugin
   * configuration.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   *
   * @return mixed
   *   The field value.
   */
  protected function getLocalEntityValue(
    ContentEntityInterface $local_entity,
    array $field_info
  ) {
    if (!isset($this->configuration['getter'])) {
      return $this->extractFieldValue($local_entity, $field_info);
    }
    if ($this->configuration['getter'] === '%null%') {
      return NULL;
    }
    if ($this->configuration['getter'] === '%entity%') {
      return $local_entity;
    }

    // We cannot easily validate this in the `validateConfiguration` method
    // because we don't have the entity available there. We would need to have
    // the synchronization available, load the entity type, get the entity class
    // and find whether it implements the method. Even if that would be the
    // correct place to do such validation - which may not be if the entity has
    // a custom bundle class, it's not worth the complexity. Let's do it here
    // instead.
    if (!method_exists($local_entity, $this->configuration['getter'])) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown method "%s" on entity of type "%s" and bundle "%s".',
        $this->configuration['getter'],
        $local_entity->getEntityTypeId(),
        $local_entity->bundle()
      ));
    }

    return $local_entity->{$this->configuration['getter']}();
  }

  /**
   * Extracts the field value from the entity according to configuration.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   *
   * @return mixed|null
   *   The field value, or NULL if the field is empty.
   */
  protected function extractFieldValue(
    FieldableEntityInterface $local_entity,
    array $field_info
  ) {
    $field = $local_entity->get($field_info['machine_name']);
    if ($field->isEmpty()) {
      return NULL;
    }

    $field->filterEmptyItems();

    // Property extraction, if configured.
    if (isset($this->configuration['extract_property'])) {
      $value = $this->extractProperty(
        $field,
        $this->configuration['extract_property']
      );
    }
    elseif ($this->configuration['extract_properties']) {
      $value = $this->extractProperties(
        $field,
        $this->configuration['extract_properties']
      );
    }
    elseif ($this->configuration['auto_extract_property']) {
      $value = $this->extractMainProperty($field);
    }
    else {
      $value = $field->getValue();
    }

    // Delta extraction, if configured.
    if (isset($this->configuration['extract_delta'])) {
      $value = $this->extractDelta(
        $value,
        $this->configuration['extract_delta']
      );
    }
    elseif ($this->configuration['extract_deltas']) {
      $value = $this->extractDeltas(
        $value,
        $this->configuration['extract_deltas']
      );
    }
    elseif ($this->configuration['auto_extract_delta']) {
      $value = $this->autoExtractDelta($field, $value);
    }

    return $value;
  }

  /**
   * Extracts the value for the main property from the given field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field.
   *
   * @return mixed
   *   The value for the main property, or the complete field value if the field
   *   does not have a main property.
   */
  protected function extractMainProperty(FieldItemListInterface $field) {
    // The main property is the same for all field items because they are of the
    // same field type. Unfortunately, we can't get that from the field item
    // list unless we get the field definition and then load the field type
    // plugin - which is inefficient.
    $main_property_name = $field->first()::mainPropertyName();
    // There are field types that do not have a main property i.e. they have
    // multiple, equally important, properties.
    if ($main_property_name === NULL) {
      return $field->getValue();
    }

    return $this->extractProperty($field, $main_property_name);
  }

  /**
   * Extracts the value for the given property from the given field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field.
   * @param string $property_name
   *   The name of the property to extract the value for.
   *
   * @return mixed
   *   The value for the requested property.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the field does not contain a value for the requested property.
   */
  protected function extractProperty(
    FieldItemListInterface $field,
    string $property_name
  ) {
    $value = [];
    foreach ($field as $index => $field_item) {
      $item_value = $field_item->getValue();
      // For maintaining backwards compatibility on how values have been
      // extracted from the beginning, we first check for the property value as
      // contained in the field item value. If it doesn't exist there, we try
      // the property getter to cover computed properties as well.
      if (array_key_exists($property_name, $item_value)) {
        $value[$index] = $item_value[$property_name];
        continue;
      }

      $value[$index] = $field_item->get($property_name)->getValue();
    }

    return $value;
  }

  /**
   * Extracts the values for the given properties from the given field.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field.
   * @param array $requested_names
   *   The names of the properties to extract the values for.
   *
   * @return array
   *   An array containing the values for the requested properties.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the field does not contain a value for one or more of the requested
   *   properties.
   */
  protected function extractProperties(
    FieldItemListInterface $field,
    array $requested_names
  ) {
    $defined_names = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getPropertyNames();
    $undefined_names = array_diff($requested_names, $defined_names);
    if ($undefined_names) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown property or properties "%s"',
        implode(', ', $undefined_names)
      ));
    }

    $value = [];
    foreach ($field as $index => $field_item) {
      $value[$index] = array_filter(
        $field_item->getValue(),
        function ($property_value, $property_name) use ($requested_names) {
          if (in_array($property_name, $requested_names)) {
            return TRUE;
          }

          return FALSE;
        },
        ARRAY_FILTER_USE_BOTH
      );
    }

    return $value;
  }

  /**
   * Extracts the first delta for the given field value.
   *
   * After extracting one, more or all of the properties for a field, we will
   * still always have an array containing one or more values depending on the
   * cardinality of the field. Automatic delta extraction returns the first
   * value only if the field has cardinality 1 i.e. it will always have one
   * value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $field
   *   The field.
   * @param array $value
   *   The field value.
   *
   * @return array
   *   An array containing the values for the requested properties.
   *
   * @throws \Drupal\entity_sync\Exception\InvalidConfigurationException
   *   When the field does not contain a value for one or more of the requested
   *   properties.
   */
  protected function autoExtractDelta(
    FieldItemListInterface $field,
    array $value
  ) {
    $cardinality = $field->getFieldDefinition()
      ->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality !== 1) {
      return $value;
    }

    return $this->extractDelta($value, 0);
  }

  /**
   * Returns the value for the given delta of the field.
   *
   * @param array $value
   *   An array containing the values for all deltas in the field.
   * @param int $delta
   *   The delta to get the value for.
   *
   * @return mixed|null
   *   The value, or NULL if the field does not have a value for the given
   *   delta.
   */
  protected function extractDelta(
    array $value,
    int $delta
  ) {
    if (isset($value[$delta])) {
      return $value[$delta];
    }

    return NULL;
  }

  /**
   * Returns the values for the given deltas of the field.
   *
   * @param array $value
   *   An array containing the values for all deltas in the field.
   * @param array $deltas
   *   The deltas to get the values for.
   *
   * @return mixed
   *   An array with the values; it may be empty if the field value does not
   *   contain values for any of the requested deltas.
   */
  protected function extractDeltas(
    array $value,
    array $deltas
  ) {
    return array_filter(
      $value,
      function ($item_value, $delta) use ($deltas) {
        if (in_array($delta, $deltas)) {
          return TRUE;
        }

        return FALSE;
      },
      ARRAY_FILTER_USE_BOTH
    );
  }

  /**
   * Sets the local entity field to the given value.
   *
   * If a setter method is defined in the plugin configuration, it uses
   * that. Otherwise it uses the `set` method provided by the
   * `FieldItemListInterface` API.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param mixed $value
   *   The value to set the field to.
   */
  protected function setLocalEntityValue(
    ContentEntityInterface $local_entity,
    array $field_info,
    $value
  ) {
    if (!isset($this->configuration['setter'])) {
      $local_entity->set(
        $field_info['machine_name'],
        $value
      );
      return;
    }

    // We cannot easily validate this in the `validateConfiguration` method
    // because we don't have the entity available there. We would need to have
    // the synchronization available, load the entity type, get the entity class
    // and find whether it implements the method. Even that would be the correct
    // place to do such validation - which may not be if the entity has
    // a custom bundle class, it's not worth the complexity. Let's do it here
    // instead.
    if (!method_exists($local_entity, $this->configuration['setter'])) {
      throw new InvalidConfigurationException(sprintf(
        'Unknown method "%s" on entity of type "%s" and bundle "%s".',
        $this->configuration['setter'],
        $local_entity->getEntityTypeId(),
        $local_entity->bundle()
      ));
    }

    $local_entity->{$this->configuration['setter']}($value);
  }

  /**
   * Processes the value being exported.
   *
   * Processing includes:
   * - Pre-transformation validation.
   * - Transformation.
   * - Post-transformation validation.
   *
   * @param mixed $original_value
   *   The value to transform.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function processExportedValue(
    $original_value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    $this->validate($original_value, 'pre_transformation');

    $transformed_value = $this->transformExportedValue(
      $original_value,
      $local_entity,
      $remote_entity_id,
      $field_info,
      $context
    );

    $this->validate($transformed_value, 'post_transformation');

    // NULL in Drupal signifies an empty value i.e. the field has no
    // value. However, the system that we are exporting to may use a different
    // value for this. For example, CSV files commonly use an empty string for
    // no value. We therefore transform the Drupal's NULL value to the remote
    // system's corresponding value configured by the `null_value` configuration
    // setting.
    if ($transformed_value === NULL) {
      $transformed_value = $this->configuration['null_value'];
    }

    return $transformed_value;
  }

  /**
   * Processes the value being imported before setting it in the entity field.
   *
   * Processing includes:
   * - Pre-transformation validation.
   * - Transformation.
   * - Post-transformation validation.
   *
   * @param mixed $original_value
   *   The value to process.
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $local_entity
   *   The local entity, or NULL if not available.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function processImportedValue(
    $original_value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    // NULL in Drupal signifies an empty value i.e. the field has no
    // value. However, the system that we are exporting to may use a different
    // value for this. For example, CSV files commonly use an empty string for
    // no value. We therefore transform the remote system's NULL value,
    // configured by the `null_value` configuration setting, to Drupal's NULL
    // value.
    if ($original_value === $this->configuration['null_value']) {
      $original_value = NULL;
    }

    $this->validate($original_value, 'pre_transformation');

    $transformed_value = $this->transformImportedValue(
      $original_value,
      $remote_entity,
      $local_entity,
      $field_info,
      $context
    );

    $this->validate($transformed_value, 'post_transformation');

    return $transformed_value;
  }

  /**
   * Transforms the value being exported.
   *
   * The public API of the field transformer plugin requires the `export` method
   * to be implemented. This base class provides a default implementation of
   * that method that performs the following common actions so that plugin
   * implementations do not have to:
   * - Extracts the original value from the field.
   * - Validates the original value before transformation.
   * - Transforms the value calling this method.
   * - Validates the transformed value.
   *
   * With this, most plugin implementations will only need to implement this
   * method (`transformExportedValue`), and possibly the `getLocalEntityValue`
   * method if you need to customize the way the field value is set on the
   * entity.
   *
   * Note that this method (`transformExportedValue`) is not declared `abstract`
   * because plugin implementations could have a reason to override directly the
   * `export` method; in that case they may not need to implement
   * `transformExportedValue`.
   *
   * If you do have a reason to override the `export` method, make sure that you
   * take into account the actions listed above so that they are not ommitted.
   *
   * @param mixed $value
   *   The original value.
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    throw new \Exception('An implementation of the `transformExportedValue` method must be provided.');
  }

  /**
   * Transforms the value being imported.
   *
   * The public API of the field transformer plugin requires the `import` method
   * to be implemented. This base class provides a default implementation of
   * that method that performs the following common actions so that plugin
   * implementations do not have to:
   * - Validates the original value before transformation.
   * - Transforms the value calling this method.
   * - Validates the transformed value.
   * - Sets the field to the transformed value.
   *
   * With this, most plugin implementations will only need to implement this
   * method (`transformImportedValue`), and possibly the `setLocalEntityValue`
   * method if you need to customize the way the field value is set on the
   * entity.
   *
   * Note that this method (`transformImportedValue`) is not declared `abstract`
   * because plugin implementations could have a reason to override directly the
   * `import` method; in that case they may not need to implement
   * `transformImportedValue`.
   *
   * If you do have a reason to override the `import` method, make sure that you
   * take into account the actions listed above so that they are not ommitted.
   *
   * @param mixed $value
   *   The original value.
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $local_entity
   *   The local entity, or NULL if not available.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    throw new \Exception('An implementation of the `transformImportedValue` method must be provided.');
  }

  /**
   * Validates the given value against the constraints of the given group.
   *
   * The default behavior in the case that validation fails is to throw an
   * exception causing the import of the entire entity to fail. Support for
   * failure handler should be provided in the future.
   *
   * @param mixed $value
   *   The value to validate.
   * @param string $group
   *   The constraint group.
   *
   * @throws \RuntimeException
   *   When validation fails.
   * phpcs:disable
   * @I Collect all errors from all constraints before failing
   *    type     : improvement
   *    priority : normal
   *    labels   : field-transformer, validation
   * @I Support validation failure handlers
   *    type     : improvement
   *    priority : normal
   *    labels   : field-transformer, validation
   *    notes    : Possible failure handlers are fail, skip and nullify, all
   *               with a configuration option to log a message.
   * phpcs:enable
   */
  protected function validate($value, string $group) {
    if (empty($this->configuration['constraints'])) {
      return;
    }

    $constraints = $this->getConstraintsForGroup($group);
    if (!$constraints) {
      return;
    }

    foreach ($constraints as $constraint) {
      $errors = $this->constraintValidator->validate($value, $constraint);
      if (!$errors->count()) {
        continue;
      }

      // phpcs:disable
      // @I Move default constraint configuration to a base plugin
      //    type     : improvement
      //    priority : low
      //    labels   : validation
      // phpcs:enable
      $on_failure = 'fail';
      try {
        $on_failure = $constraint->onFailure;
      }
      // For backwards compatibility, do not break constraints defined in third
      // party modules that do not define the `onFailure` options.
      catch (InvalidOptionsException $exception) {
      }

      switch ($on_failure) {
        case 'fail':
          throw new \RuntimeException(
            $this->buildExceptionMessage($group, $errors)
          );

        case 'log_and_skip':
          throw new SkipFieldException(
              $this->buildExceptionMessage($group, $errors)
          );

        case 'skip':
          throw new SkipFieldException();

        default:
          throw new InvalidConfigurationException(sprintf(
            'Unsupported failure mode "%s".',
            $on_failure
          ));
      }

    }
  }

  /**
   * Returns the instantiated constraint plugins for the given group.
   *
   * @param string $group
   *   The constraint group.
   *
   * @return \Drupal\entity_sync\FieldTransformer\PluginInterface[]
   *   The constraints for the given group.
   */
  protected function getConstraintsForGroup(string $group) {
    $constraints = array_filter(
      $this->configuration['constraints'],
      function ($constraint) use ($group) {
        if (empty($constraint['groups'])) {
          return FALSE;
        }

        return in_array($group, $constraint['groups']);
      }
    );

    return array_map(
      function ($constraint) {
        return $this->constraintManager->createInstance(
          $constraint['id'],
          $constraint['configuration'] ?? []
        );
      },
      $constraints
    );
  }

  /**
   * Builds the exception message in the case we have validation errors.
   *
   * @param string $group
   *   The constraint group.
   * @param \Symfony\Component\Validator\ConstraintViolationListInterface $errors
   *   The constraint violation list.
   *
   * @return string
   *   The exception message.
   */
  protected function buildExceptionMessage(
    string $group,
    ConstraintViolationListInterface $errors
  ) {
    $messages = [];
    foreach ($errors as $index => $error) {
      $messages[] = (string) ++$index . ') ' . $error->getMessage();
    }

    return sprintf(
      'The following errors occurred while validating the value against the "%s" constraint group: "%s"',
      $group,
      implode(' ', $messages)
    );
  }

}
