<?php

namespace Drupal\entity_sync\FieldTransformer;

use Drupal\entity_sync\Annotation\EntitySyncFieldTransformer as PluginAnnotation;
use Drupal\entity_sync\FieldTransformer\Factory\ContainerAndValidatorFactory;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The plugin manager for field transformer plugins.
 *
 * @see \Drupal\entity_sync\Annotation\EntitySyncFieldTransformer
 * @see \Drupal\entity_sync\FieldTransformer\PluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager {

  /**
   * The dependency injection class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructs a new PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The dependency injection class resolver.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    ClassResolverInterface $class_resolver
  ) {
    parent::__construct(
      'Plugin/EntitySync/FieldTransformer',
      $namespaces,
      $module_handler,
      PluginInterface::class,
      PluginAnnotation::class
    );

    $this->alterInfo('entity_sync_field_transformer_info');
    $this->setCacheBackend(
      $cache_backend,
      'entity_sync_field_transformer',
      ['entity_sync_field_transformer_plugins']
    );

    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFactory() {
    if (!$this->factory) {
      $this->factory = new ContainerAndValidatorFactory(
        $this,
        $this->classResolver,
        $this->pluginInterface
      );
    }

    return $this->factory;
  }

}
