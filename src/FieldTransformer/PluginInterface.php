<?php

namespace Drupal\entity_sync\FieldTransformer;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides the interface for field transformer plugins.
 *
 * Field transformer plugins are responsible for converting field values between
 * local and remote entities.
 *
 * phpcs:disable
 * @I Support exporting values in field transformer plugins
 *    type     : feature
 *    priority : normal
 *    labels   : export, field-transformer
 * @I Support validating values in field transformer plugins
 *    type     : imrpovement
 *    priority : normal
 *    labels   : export, field-transformer, validation
 * phpcs:enable
 */
interface PluginInterface extends
  ConfigurableInterface,
  PluginInspectionInterface,
  DependentPluginInterface {

  /**
   * Transforms the remote field value and stores it to the local field.
   *
   * Implementations must store the value to the local entity field after
   * transforming the value of the remote entity field. This is not done by the
   * field manager because all values are potentially valid, including NULL, and
   * we want to keep open the possibility that the field handler aborts the
   * field import altogether i.e. do nothing and keep the current local field
   * value as is.
   *
   * @param object $remote_entity
   *   The remote entity.
   * @param \Drupal\Core\Entity\ContentEntityInterface|null $local_entity
   *   The local entity, or NULL if not available. The local entity might not
   *   available if running the transformation in a context different than the
   *   usual. The only known case used by this module is when the entity import
   *   manager runs the field transformer that determines the bundle of new
   *   entities at which point the entity has not been created yet.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Import\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   * @param mixed|null $original_value
   *   The value to transform. It may be given in the case of transformer being
   *   part of a pipeline, otherwise the field manager would not pass a value
   *   and the plugin should load the initial value from the remote entity.
   * @param bool $get_value
   *   When `TRUE` (default) the plugin should be loading the value from the
   *   remote entity before transforming it. `FALSE` is given by the pipeline
   *   plugin to child plugins so that they do not the load the value again and
   *   they process the value handed over to it.
   * @param bool $set_value
   *   When `FALSE` is given, the plugin should skip setting the value to the
   *   local entity. Used by the pipeline to avoid setting the value on the
   *   local entity until all transformers have run.
   */
  public function import(
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context,
    $original_value = NULL,
    bool $get_value = TRUE,
    bool $set_value = TRUE
  );

  /**
   * Transforms and returns the local entity field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $local_entity
   *   The local entity.
   * @param int|string|null $remote_entity_id
   *   The ID of the remote entity that will be updated, or NULL if we are
   *   creating a new one.
   * @param array $field_info
   *   The field info.
   *   See \Drupal\entity_sync\Export\Event\FieldMapping::fieldMapping.
   * @param array $context
   *   An associative array containing context related to the circumstances of
   *   the operation.
   * @param mixed|null $original_value
   *   The value to transform. It may be given in the case of transformer being
   *   part of a pipeline, otherwise the field manager would not pass a value
   *   and the plugin should load the initial value from the local entity.
   * @param bool $get_value
   *   When `TRUE` (default) the plugin should be loading the value from the
   *   locale entity before transforming it. `FALSE` is given by the pipeline
   *   plugin to child plugins so that they do not the load the value again and
   *   they process the value handed over to it.
   *
   * @return mixed
   *   The transformed value to export.
   */
  public function export(
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context,
    $original_value = NULL,
    bool $get_value = TRUE
  );

}
