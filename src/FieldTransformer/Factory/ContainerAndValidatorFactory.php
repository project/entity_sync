<?php

namespace Drupal\entity_sync\FieldTransformer\Factory;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\TypedData\Validation\ExecutionContextFactory;
use Drupal\Core\Validation\ConstraintValidatorFactory;
use Drupal\Core\Validation\DrupalTranslator;

use Symfony\Component\Validator\Mapping\Factory\BlackHoleMetadataFactory;
use Symfony\Component\Validator\Validator\RecursiveValidator;

/**
 * Plugin factory which passes a container and a validator to the create method.
 */
class ContainerAndValidatorFactory extends DefaultFactory {

  /**
   * The dependency injection class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructs a Drupal\Component\Plugin\Factory\DefaultFactory object.
   *
   * @param \Drupal\Component\Plugin\Discovery\DiscoveryInterface $discovery
   *   The plugin discovery.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The dependency injection class resolver.
   * @param string|null $plugin_interface
   *   (optional) The interface each plugin should implement.
   */
  public function __construct(
    DiscoveryInterface $discovery,
    ClassResolverInterface $class_resolver,
    $plugin_interface = NULL
  ) {
    parent::__construct($discovery, $plugin_interface);

    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    $plugin_definition = $this->discovery->getDefinition($plugin_id);
    $plugin_class = static::getPluginClass(
      $plugin_id,
      $plugin_definition,
      $this->interface
    );

    return $plugin_class::create(
      \Drupal::getContainer(),
      $this->createValidator(),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Returns a new validator instance.
   *
   * @return \Symfony\Component\Validator\Validator\ValidatorInterface
   *   The new validator instance.
   */
  protected function createValidator() {
    return new RecursiveValidator(
      // We use the execution context provided by the Typed Data validation
      // component so that we don't have to write our own. Even though it is
      // meant to be used by the core validators that validate constraints on
      // typed data objects, it doesn't seem to contain anything specific to
      // typed data. It just uses the Drupal translator instead of the Symfony
      // translator, which is something that we want here as well.
      new ExecutionContextFactory(new DrupalTranslator()),
      new BlackHoleMetadataFactory(),
      new ConstraintValidatorFactory($this->classResolver)
    );
  }

}
