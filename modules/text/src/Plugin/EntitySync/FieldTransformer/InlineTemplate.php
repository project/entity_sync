<?php

namespace Drupal\entity_sync_text\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\ContentEntityInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

/**
 * Transformer that renders a Twig template using the value as its variables.
 *
 * Supported configuration properties are:
 * - template: (string, requred) The inline template as text.
 *
 * @EntitySyncFieldTransformer(
 *   id = "inline_template"
 * )
 */
class InlineTemplate extends PluginBase {

  /**
   * The twig environment.
   *
   * @var \Twig\Environment
   */
  protected $twig;

  /**
   * Constructs a new InlineTemplate object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Symfony\Component\Validator\Validator\ValidatorInterface $constraint_validator
   *   The constraint validator.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $constraint_manager
   *   The constraint plugin manager.
   * @param \Twig\Environment $twig
   *   The twig environment.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ValidatorInterface $constraint_validator,
    PluginManagerInterface $constraint_manager,
    Environment $twig
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $constraint_manager
    );

    $this->twig = $twig;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    ValidatorInterface $constraint_validator,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $constraint_validator,
      $container->get('plugin.manager.entity_sync_validation_constraint'),
      $container->get('twig')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function validateConfiguration() {
    $this->validateConfigurationRequiredProperties([
      'template',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the rendered inline template for the given value.
   *
   * The value needs to be an array containing the variables to be passed to the
   * template.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed
   *   The transformed value.
   */
  protected function transformValue($value) {
    if ($value === NULL) {
      return NULL;
    }

    if (!is_array($value)) {
      throw new \InvalidArgumentException(sprintf(
        'The value must be an array, "%s" given.',
        gettype($value)
      ));
    }

    return $this->twig
      ->createTemplate($this->configuration['template'])
      ->render($value);
  }

}
