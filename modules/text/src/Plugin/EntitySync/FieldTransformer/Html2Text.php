<?php

namespace Drupal\entity_sync_text\Plugin\EntitySync\FieldTransformer;

use Drupal\entity_sync\FieldTransformer\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Html2Text\Html2Text as Html2TextConverter;

/**
 * Transformer that converts HTML to plain text.
 *
 * Supported configuration properties:
 * - conversion_options: (array, optional) An array of options that define
 *   certain aspects of the conversion.
 *
 * @EntitySyncFieldTransformer(
 *   id = "html2text"
 * )
 *
 * @see \Html2Text\Html2Text::__construct()
 */
class Html2Text extends PluginBase {

  /**
   * {@inheritdoc}
   */
  protected function transformImportedValue(
    $value,
    \stdClass $remote_entity,
    ?ContentEntityInterface $local_entity,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * {@inheritdoc}
   */
  protected function transformExportedValue(
    $value,
    ContentEntityInterface $local_entity,
    $remote_entity_id,
    array $field_info,
    array $context
  ) {
    return $this->transformValue($value);
  }

  /**
   * Returns the value transformed to plain text.
   *
   * @param mixed $value
   *   The value to transform.
   *
   * @return mixed|null
   *   The transformed value, or `NULL` when the value is `NULL`.
   */
  protected function transformValue($value) {
    if ($value === NULL) {
      return NULL;
    }

    $conversion_options = $this->configuration['conversion_options'] ?? [];
    return (new Html2TextConverter($value, $conversion_options))->getText();
  }

}
