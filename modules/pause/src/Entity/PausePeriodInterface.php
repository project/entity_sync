<?php

namespace Drupal\entity_sync_pause\Entity;

use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Defines the interface for pause periods.
 *
 * A pause period defines a time range during which operations of the
 * selected types will not be run.
 */
interface PausePeriodInterface extends
  ContentEntityInterface,
  EntityChangedInterface,
  EntityOwnerInterface {

  /**
   * Returns the state of the period.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The state field item.
   */
  public function getState();

  /**
   * Returns the start time of the period.
   *
   * @return int
   *   The start timestamp.
   */
  public function getStartTime();

  /**
   * Sets the start time of the period.
   *
   * @param int $timestamp
   *   The start timestamp.
   *
   * @return $this
   *   The pause period entity.
   */
  public function setStartTime($timestamp);

  /**
   * Returns the end time of the period.
   *
   * @return int
   *   The end timestamp.
   */
  public function getEndTime();

  /**
   * Sets the end time of the period.
   *
   * @param int $timestamp
   *   The end timestamp.
   *
   * @return $this
   *   The pause period entity.
   */
  public function setEndTime($timestamp);

  /**
   * Returns the time when the period was created.
   *
   * @return int
   *   The creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the time when the period was created.
   *
   * @param int $timestamp
   *   The creation timestamp.
   *
   * @return $this
   *   The pause period entity.
   */
  public function setCreatedTime($timestamp);

}
