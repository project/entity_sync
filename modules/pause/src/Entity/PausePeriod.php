<?php

namespace Drupal\entity_sync_pause\Entity;

use Drupal\entity_sync\MachineName\Field\PausePeriod as PausePeriodField;
use Drupal\user\EntityOwnerTrait;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * The default implementation for the PausePeriod content entity.
 *
 * @ContentEntityType(
 *   id = "entity_sync_pause_period",
 *   label = @Translation("Pause period"),
 *   label_collection = @Translation("Pause periods"),
 *   label_singular = @Translation("pause period"),
 *   label_plural = @Translation("pause periods"),
 *   label_count = @PluralTranslation(
 *     singular = "@count pause period",
 *     plural = "@count pause periods",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\entity_sync_pause\Entity\Controller\PausePeriodListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\entity_sync_pause\Entity\Form\PausePeriod",
 *       "edit" = "Drupal\entity_sync_pause\Entity\Form\PausePeriod",
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "entity_sync_pause_period",
 *   admin_permission = "administer entity_sync_pause_period",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *     "uid" = "uid",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/admin/sync/config/pause-periods/{entity_sync_pause_period}",
 *     "add-form" = "/admin/sync/config/pause-periods/add",
 *     "edit-form" = "/admin/sync/config/pause-periods/{entity_sync_pause_period}/edit",
 *     "collection" = "/admin/sync/config/pause-periods",
 *   },
 * )
 */
class PausePeriod extends ContentEntityBase implements
  PausePeriodInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get(PausePeriodField::STATE)->first();
  }

  /**
   * {@inheritdoc}
   */
  public function getStartTime() {
    return $this->get(PausePeriodField::START)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStartTime($timestamp) {
    $this->set(PausePeriodField::START, $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndTime() {
    return $this->get(PausePeriodField::END)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndTime($timestamp) {
    $this->set(PausePeriodField::END, $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get(PausePeriodField::CREATED)->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set(PausePeriodField::CREATED, $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    // Alterations on already defined fields.
    $fields[PausePeriodField::ID]->setDescription(
      new TranslatableMarkup('The ID of the pause period.')
    );
    $fields[PausePeriodField::UUID]->setDescription(
      new TranslatableMarkup('The UUID of the pause period.')
    );

    $fields[PausePeriodField::USER_ID]
      ->setLabel(new TranslatableMarkup('Created by'))
      ->setDescription(
        new TranslatableMarkup('The user that created the pause period.')
      )
      ->setDisplayOptions(
        'view',
        [
          'label' => 'above',
          'type' => 'author',
          'weight' => 4,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    // Many applications can have multiple synchronizations with multiple
    // systems, and potentially multiple different reasons for pausing one or
    // more synchronizations. Allow for a title explaining the pause period.
    $fields[PausePeriodField::TITLE] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setDescription(new TranslatableMarkup(
        'A title briefly describing the reason for the pause period.'
      ))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
        'view',
        [
          'label' => 'hidden',
          'type' => 'string',
          'weight' => 0,
        ]
      )
      ->setDisplayOptions(
        'form',
        [
          'type' => 'string_textfield',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('form', TRUE);

    // Start and end times.
    $fields[PausePeriodField::START] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Start time'))
      ->setDescription(new TranslatableMarkup(
        'The date and time when the pause period starts.'
      ))
      ->setRequired(TRUE)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'datetime_timestamp',
          'weight' => 1,
        ]
      )
      ->setDisplayOptions(
        'view',
        [
          'type' => 'timestamp',
          'weight' => 1,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields[PausePeriodField::END] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('End time'))
      ->setDescription(new TranslatableMarkup(
        'The date and time when the pause period ends.'
      ))
      ->setRequired(TRUE)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'datetime_timestamp',
          'weight' => 2,
        ]
      )
      ->setDisplayOptions(
        'view',
        [
          'type' => 'timestamp',
          'weight' => 2,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // The operation types to pause.
    $fields[PausePeriodField::OPERATION_TYPES] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Operation types'))
      ->setDescription(new TranslatableMarkup(
        'The operation types to pause during the pause period.'
      ))
      ->setSetting('target_type', 'entity_sync_operation_type')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setDisplayOptions(
        'form',
        [
          'type' => 'datetime_timestamp',
          'weight' => 3,
        ]
      )
      ->setDisplayOptions(
        'view',
        [
          'type' => 'entity_reference_label',
          'weight' => 3,
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // We need a simple state for the pause period that will be set to `active`
    // between the start and the end time. Synchronizations should be checking
    // the state to determine whether the operation should be run or not, and
    // not the start/end times.
    $fields[PausePeriodField::STATE] = BaseFieldDefinition::create('state')
      ->setLabel(new TranslatableMarkup('State'))
      ->setDescription(new TranslatableMarkup('The state of the pause period.'))
      ->setRequired(TRUE)
      ->setSetting('workflow', 'entity_sync_pause_period_default')
      ->setDisplayOptions(
        'view',
        [
          'type' => 'list_default',
          'weight' => 0,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    // At last, timestamps.
    $fields[PausePeriodField::CREATED] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup(
        'The time when the pause period entity was created.'
      ))
      ->setDisplayOptions(
        'view',
        [
          'type' => 'timestamp',
          'weight' => 5,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    $fields[PausePeriodField::CREATED] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup(
        'The time when the pause period entity was last edited.'
      ))
      ->setDisplayOptions(
        'view',
        [
          'type' => 'timestamp',
          'weight' => 6,
        ]
      )
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
