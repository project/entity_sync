<?php

namespace Drupal\entity_sync_pause\Entity\Controller;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default list builder for pause periods.
 *
 * @I Display the state of the pause period
 *    type     : improvement
 *    priority : normal
 *    labels   : ux
 * @I Display start and end times in one column
 *    type     : improvement
 *    priority : normal
 *    labels   : ux
 */
class PausePeriodListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The operation type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $typeStorage;

  /**
   * The bundle labels.
   *
   * We may have multiple operations of the same type listed. To avoid loading
   * the operation type entity every time - even if it's from the cache, we
   * cache here the operation type labels.
   *
   * @var array
   */
  protected $typeLabels = [];

  /**
   * Constructs a new PausePeriodListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityStorageInterface $type_storage
   *   The operation type storage.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    EntityStorageInterface $storage,
    EntityStorageInterface $type_storage,
    DateFormatterInterface $date_formatter
  ) {
    parent::__construct($entity_type, $storage);

    $this->typeStorage = $type_storage;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    $entity_type_manager = $container->get('entity_type.manager');

    return new static(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id()),
      $entity_type_manager->getStorage('entity_sync_operation_type'),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    $header['state'] = $this->t('State');
    $header['start'] = $this->t('Start time');
    $header['end'] = $this->t('End time');
    $header['operation_types'] = $this->t('Operation types');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $type_ids = array_column(
      $entity->get('operation_types')->getValue(),
      'target_id'
    );
    $types = [];
    foreach ($type_ids as $type_id) {
      if (!isset($this->typeLabels[$type_id])) {
        $this->typeLabels[$type_id] = $this->typeStorage->load($type_id)->label();
      }
      $types[] = $this->typeLabels[$type_id];
    }

    $row['id'] = $entity->id();
    $row['title'] = $entity->label();
    $row['state'] = $entity->getState()->getLabel();

    $row['start'] = $this->dateFormatter->format(
      $entity->getStartTime(),
      'short'
    );
    $row['end'] = $this->dateFormatter->format(
      $entity->getEndTime(),
      'short'
    );

    $row['operation_types'] = implode(', ', $types);

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View'),
        'weight' => 0,
        'url' => $entity->toUrl('canonical'),
      ];
    }

    return $operations;
  }

}
