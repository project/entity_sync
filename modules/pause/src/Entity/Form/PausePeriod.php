<?php

namespace Drupal\entity_sync_pause\Entity\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * The default form controller for pause period forms.
 */
class PausePeriod extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $period = $this->getEntity();
    $status = $period->save();

    $logger_context = [
      '%label' => $period->label(),
      'link' => $period->toLink($this->t('View'))->toString(),
    ];
    $translation_context = [
      '%label' => $period->toLink()->toString(),
    ];

    if ($status === SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t(
        'Pause period %label has been updated.',
        $translation_context
      ));
      $this->logger('entity_sync')->notice(
        'Pause period %label has been updated.',
        $logger_context
      );
    }
    elseif ($status === SAVED_NEW) {
      $this->messenger()->addStatus($this->t(
        'Pause period %label has been added.',
        $translation_context
      ));
      $this->logger('entity_sync')->notice(
        'Pause period %label has been added.',
        $logger_context
      );
    }

    $form_state->setRedirect('entity.entity_sync_pause_period.collection');
  }

}
