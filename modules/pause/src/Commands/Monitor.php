<?php

namespace Drupal\entity_sync_pause\Commands;

use Drupal\entity_sync_pause\Monitor\MonitorInterface;
use Drush\Commands\DrushCommands;

/**
 * Commands related to pause period monitoring.
 */
class Monitor extends DrushCommands {

  /**
   * The pause period monitor.
   *
   * @var \Drupal\entity_sync_pause\Monitor\MonitorInterface
   */
  protected $monitor;

  /**
   * Constructs a new Monitor object.
   *
   * @param \Drupal\entity_sync_pause\Monitor\MonitorInterface $monitor
   *   The pause period monitor.
   */
  public function __construct(MonitorInterface $monitor) {
    $this->monitor = $monitor;
  }

  /**
   * Runs monitoring on pause periods.
   *
   * @usage drush entity-sync-pause:monitor-run
   *   Runs the monitoring service for all pause periods.
   *
   * @command entity-sync-pause:monitor-run
   *
   * @aliases esync-pause-mrun
   *
   * @I Support monitoring specific pause periods only
   *    type     : improvement
   *    priority : low
   *    labels   : monitor
   *    notes    : That requires the corresponding feature to be added to the
   *               monitor service first.
   */
  public function run() {
    $this->monitor->run();
  }

}
