<?php

namespace Drupal\entity_sync_pause\Monitor;

/**
 * Provides the interface for the pause period monitor service.
 *
 * The monitor service is meant to be frequently run via cron. It observes pause
 * periods and starts/ends periods so that they are in the correct state when
 * operations are run.
 *
 * When a pause period has started, operations should be postponed or skipped
 * (depending on the pause mode). When a pause period has ended, operations
 * should resume normal execution.
 */
interface MonitorInterface {

  /**
   * Moves pause periods to the `started` or `ended` state, as needed.
   *
   * - If a pause period is still in `pending` state but its `start` time has
   *   arrived or passed already, it is transitioned to the `started` state.
   * - If a pause period is still in `pending` or `started` state but its `end`
   *   time has passed already, it is transitioned to the `ended` state.
   *
   * @I Support monitoring specific pause periods only
   *    type     : improvement
   *    priority : low
   *    labels   : monitor
   *    notes    : That might require to introduce the pause period configurator
   *               plugin first.
   */
  public function run();

}
