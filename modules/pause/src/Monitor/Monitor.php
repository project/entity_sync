<?php

namespace Drupal\entity_sync_pause\Monitor;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Default implementation of the pause period monitor service.
 */
class Monitor implements MonitorInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Drupal system time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Monitor object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The Drupal system time service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   *
   * @I Allow configuring advance time/early start per pause period
   *    type     : improvement
   *    priority : normal
   *    labels   : monitor
   *    notes    : Cron might not run exactly when a period starts, resulting in
   *               some operations being run while they should not. This might
   *               cause issues, for example, if the remote system does not
   *               properly handle API calls during downtime. There may be other
   *               reasons for requiring to ensure that all intended operations
   *               are paused during a pause period. We therefore should start
   *               pause periods at the last time that the monitor is run via
   *               cron before the actual start time of the period. That might
   *               require to introduce the pause period configurator plugin
   *               first.
   */
  public function run() {
    $now = $this->time->getRequestTime();
    $storage = $this->entityTypeManager->getStorage('entity_sync_pause_period');

    foreach ($this->getPausePeriods($storage) as $period) {
      $transition_id = 'start';
      if ($period->getEndTime() <= $now) {
        $transition_id = 'end';
      }

      $period->getState()->applyTransitionById($transition_id);
      $storage->save($period);
    }
  }

  /**
   * Returns all pause periods that need state transitions.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The pause period storage.
   *
   * @return \Drupal\entity_sync_pause\Entity\PausePeriodInterface[]
   *   The pause period entities.
   */
  protected function getPausePeriods(EntityStorageInterface $storage) {
    $query = $storage->getQuery()->accessCheck(FALSE);

    // Periods that are pending but the start time has arrived or passed.
    $started_condition = $query->andConditionGroup()
      ->condition('state', 'pending')
      ->condition('start', $now, '<=');
    // Periods that are started but the end time has passed.
    $ended_condition = $query->andConditionGroup()
      ->condition('state', ['pending', 'started'], 'IN')
      ->condition('end', $now, '<');

    $or_condition = $query->orConditionGroup()
      ->condition($started_condition)
      ->condition($ended_condition);
    $query->condition($or_condition);

    $period_ids = $query->execute();
    if (!$period_ids) {
      return [];
    }

    return $storage->loadMultiple($period_ids);
  }

}
