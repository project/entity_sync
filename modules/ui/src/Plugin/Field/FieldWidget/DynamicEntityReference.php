<?php

namespace Drupal\entity_sync_ui\Plugin\Field\FieldWidget;

use Drupal\dynamic_entity_reference\Plugin\Field\FieldWidget\DynamicEntityReferenceWidget as Base;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Widget for the dynamic entity reference field on operation entities.
 *
 * The widget provided by the Dynamic Entity Reference module allows selecting
 * the entity type. However, operations may only reference entities of a single
 * type that is defined by its bundle. This widget does and restricts the entity
 * type selection to improve usability and prevent errors.
 *
 * phpcs:disable
 * @FieldWidget(
 *   id = "entity_sync_dynamic_entity_reference",
 *   label = @Translation("Entity Synchronization autocomplete"),
 *   description = @Translation("An autocomplete for selecting the entity on Entity Synchronization operations."),
 *   field_types = {
 *     "dynamic_entity_reference"
 *   }
 * )
 * phpcs:enable
 */
class DynamicEntityReference extends Base {

  /**
   * The config entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->configEntityLoader = $container->get(
      'entity_sync.config_entity_loader'
    );
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Select the entity type.
    $operation = $items->getEntity();
    $entity_type_id = $this->configEntityLoader
      ->load($operation->bundle())
      ->getLocalEntitySettings()['type_id'];

    $element['target_type']['#default_value'] = $entity_type_id;
    $element['target_type']['#options'] = array_filter(
      $element['target_type']['#options'],
      function ($key) use ($entity_type_id) {
        return $key === $entity_type_id;
      },
      ARRAY_FILTER_USE_KEY
    );
    $element['target_type']['#disabled'] = TRUE;

    // The current title of the field ("Label") is not particularly descriptive.
    $element['target_id']['#title'] = $this->t('Entity');

    return $element;
  }

}
