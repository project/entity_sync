<?php

namespace Drupal\entity_sync_ui\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local task definitions for all entity types.
 */
class LocalTask extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Creates a new LocalTask object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    TranslationInterface $string_translation
  ) {
    $this->entityTypeManager = $entity_type_manager;

    // Injections required by traits.
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   *
   * We add a tab on all entity types that define content entities and that have
   * a view and/or edit tab. There may be cases that do not have a view/edit UI
   * exposed that are synchronized with other systems, but not sure if there is
   * a point in exposing synchronization information for entities that cannot be
   * viewed themselves in the UI. We may review that decision in the future.
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $derivatives = [];

    foreach ($this->entityTypeManager->getDefinitions() as $id => $type) {
      if (!$type->entityClassImplements(ContentEntityInterface::class)) {
        continue;
      }

      $view = $type->hasLinkTemplate('canonical');
      $edit = $type->hasLinkTemplate('edit-form');

      if (!($view || $edit)) {
        continue;
      }

      $derivatives["{$id}.sync_info"] = [
        'route_name' => "entity.{$id}.sync_info",
        'title' => $this->t('Sync'),
        'base_route' => "entity.{$id}." . ($view ? 'canonical' : 'edit_form'),
        'weight' => 90,
      ] + $base_plugin_definition;
    }

    return $derivatives;
  }

}
