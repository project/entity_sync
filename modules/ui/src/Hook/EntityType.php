<?php

namespace Drupal\entity_sync_ui\Hook;

/**
 * Provides methods implementing hooks related to entity types.
 */
class EntityType {

  /**
   * Alters entity types.
   *
   * We add a template link to all entities that have a view and/or edit
   * page. The template link contains the path to the page that renders the
   * synchronization information related to the entity.
   *
   * @param array $entity_types
   *   The entity types to alter.
   *
   * @see hook_entity_type_alter()
   */
  public function alterTypes(array &$entity_types) {
    foreach ($entity_types as $id => &$type) {

      $has_canonical = $type->hasLinkTemplate('canonical');
      $has_edit = $type->hasLinkTemplate('edit-form');

      if (!($has_edit || $has_canonical)) {
        continue;
      }

      $type->setLinkTemplate('sync-info', "/admin/sync/{$id}/{{$id}}");
    }
  }

}
