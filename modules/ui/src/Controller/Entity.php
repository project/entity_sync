<?php

namespace Drupal\entity_sync_ui\Controller;

use Drupal\entity_sync\Config\ManagerInterface as ConfigManagerInterface;
use Drupal\entity_sync\Entity\SyncInterface;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides pages with synchronization data related to individual entities.
 */
class Entity extends ControllerBase {

  /**
   * The Entity Synchronization configuration manager.
   *
   * @var \Drupal\entity_sync\Config\ManagerInterface
   */
  protected $configManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new Entity object.
   *
   * @param \Drupal\entity_sync\Config\ManagerInterface $config_manager
   *   The Entity Sync configuration manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigManagerInterface $config_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->configManager = $config_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_sync.config_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns the page that renders basic synchronization info for the entity.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   A RouteMatch object.
   *
   * @return array
   *   The render array for the page.
   */
  public function infoPage(RouteMatchInterface $route_match) {
    $entity = $this->getEntityFromRouteMatch($route_match);
    if (!$entity) {
      throw new \RuntimeException(
        'The sync information page can only be rendered on entity pages.'
      );
    }

    // Get all synchronizations for the entity type.
    $syncs = $this->configManager->getSyncs([
      'local_entity' => ['type_id' => $entity->getEntityTypeId()],
    ]);
    if (!$syncs) {
      return [
        '#markup' => $this->t(
          'There are no synchronizations for this entity type.'
        ),
      ];
    }

    $build = [];

    foreach ($syncs as $sync) {
      // Do not display internal syncs; they usually are parent syncs that are
      // setup for defining shared settings, but they do not actually run
      // operations.
      // phpcs:disable
      // @I Hide internal operation types from the UI
      //    type     : bug
      //    priority : low
      //    labels   : sync, ui
      //    notes    : Low priority for now since it is not clear in which cases
      //               should an operation type be marked as internal; usually
      //               synchronizations would be marked as internal and their
      //               child operation types would be exposed on the UI.
      // phpcs:enable
      if ($sync->isInternal()) {
        continue;
      }

      // If the synchronization is restricted to a bundle different than the
      // entity's bundle, it does not apply to the entity. Do not display the
      // synchronization.
      $bundle = $sync->getLocalEntitySettings()['bundle'] ?? NULL;
      if ($bundle && $bundle !== $entity->bundle()) {
        continue;
      }

      $build[$sync->id()] = $this->buildSynchronizationDetails($entity, $sync);
    }

    return $build;
  }

  /**
   * Retrieves the entity from route match.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity object, or NULL if not found in the given route.
   */
  protected function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    return $route_match->getParameter(
      $route_match->getRouteObject()->getOption('_entity_sync_entity_type_id')
    );
  }

  /**
   * Builds the render array for the given entity/synchronization pair.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param \Drupal\entity_sync\Entity\SyncInterface $sync
   *   The synchronization.
   */
  protected function buildSynchronizationDetails(
    EntityInterface $entity,
    SyncInterface $sync
  ) {
    $element = [
      '#type' => 'details',
      '#title' => $sync->label(),
      '#open' => $sync->get('status') ? TRUE : FALSE,
    ];

    $element['description'] = [
      '#markup' => '<p>' . $sync->get('description') . '</p>',
    ];

    // Status.
    $status = $this->t('Disabled');
    if ($sync->get('status')) {
      $status = $this->t('Enabled');
    }
    $element['status'] = [
      '#markup' => $this->t(
        '<strong>Status:</strong> @status',
        ['@status' => $status]
      ),
    ];

    // Linked entity ID.
    $ids = $this->getRemoteEntityIds($sync, $entity);
    if ($ids) {
      $message = $this->t(
        '<br><strong>Linked entity ID(s):</strong> @remote_id',
        ['@remote_id' => implode(', ', $ids)]
      );
    }
    else {
      $message = $this->t(
        '<br><strong>Linked entity ID(s):</strong> Not linked'
      );
    }
    $element['remote_id'] = ['#markup' => $message];
    return $element;
  }

  /**
   * Returns the remote entity IDs for the given sync and entity.
   *
   * @return array
   *   An array containing the IDs of the remote entity or entities linked by
   *   the given synchronization.
   */
  protected function getRemoteEntityIds(
    SyncInterface $sync,
    EntityInterface $entity
  ) {
    $entity_settings = $sync->getLocalEntitySettings();
    $is_composite = FALSE;
    $field_name = $entity_settings['remote_id_field'] ?? NULL;
    if ($field_name === NULL) {
      $is_composite = TRUE;
      $field_name = $entity_settings['remote_composite_field'] ?? NULL;
    }
    $id_field = $entity->get($field_name);

    if ($id_field->isEmpty()) {
      return [];
    }

    $property = $is_composite ? 'id' : 'value';
    return array_column($id_field->getValue(), $property);
  }

}
