<?php

namespace Drupal\entity_sync_ui\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Provides route alterations related to Entity Synchronization.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new RouteSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_manager) {
    $this->entityTypeManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->entityTypeManager->getDefinitions() as $id => $type) {
      $route = $this->getInfoRoute($type);
      if (!$route) {
        continue;
      }

      $collection->add("entity.{$id}.sync_info", $route);
    }
  }

  /**
   * Returns the route for the synchronization information page.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   *
   * @return \Symfony\Component\Routing\Route|null
   *   The route, if the template link is enabled on the given entity type.
   */
  protected function getInfoRoute(EntityTypeInterface $entity_type) {
    $sync_info = $entity_type->getLinkTemplate('sync-info');
    if (!$sync_info) {
      return;
    }

    $entity_type_id = $entity_type->id();
    $route = new Route($sync_info);
    $route
      ->addDefaults([
        '_controller' => '\Drupal\entity_sync_ui\Controller\Entity::infoPage',
        '_title' => 'Sync information',
      ])
      ->addRequirements([
        '_permission' => 'access entity_sync administration pages',
      ])
      ->setOption('_admin_route', TRUE)
      ->setOption('_entity_sync_entity_type_id', $entity_type_id)
      ->setOption(
        'parameters',
        [$entity_type_id => ['type' => 'entity:' . $entity_type_id]]
      );

    return $route;
  }

}
