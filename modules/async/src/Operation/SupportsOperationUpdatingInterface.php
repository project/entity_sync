<?php

namespace Drupal\entity_sync_async\Operation;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Entity\OperationTypeInterface;

/**
 * The interface for configurator plugins that support updating operations.
 */
interface SupportsOperationUpdatingInterface {

  /**
   * Returns the states that are considered as pending updates.
   *
   * The update manager should only fetch updates for operations that are
   * considered to be pending. We therefore need the configurator plugin of the
   * operation type to inform the update manager which states are considered
   * pending.
   *
   * @return array
   *   An associative array containing the pending workflow states. Array items
   *   should be.
   */
  public function operationUpdaterPendingStates();

  /**
   * Updates the given operation.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The type (bundle) of the operation.
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation to update.
   */
  public function updateOperation(
    OperationTypeInterface $operation_type,
    OperationInterface $operation
  );

}
