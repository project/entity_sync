<?php

namespace Drupal\entity_sync_async\Operation;

use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Entity\OperationTypeInterface;

/**
 * Provides the interface for the operation update manager.
 *
 * The operation update manager facilitates fetching updates for operations
 * from the remote system. It can be called in Cron hooks and in Drush
 * commands so that synchronization provider modules implementing asynchronous
 * operations do not have to implement an update system themselves.
 */
interface UpdateManagerInterface {

  /**
   * Updates the given operation.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation to fetch the update for.
   *
   * @throws \RuntimeException
   *   When the operation type does not support updating operations.
   * @throws \RuntimeException
   *   When the operation is not in a state considered to be pending updates.
   */
  public function update(OperationInterface $operation);

  /**
   * Updates operations for the given operation type.
   *
   * Only operations that are pending will be updated. See
   * \Drupal\entity_sync\Operation\SupportsOperationUpdatingInterface::operationUpdaterPendingStates().
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type to fetch the operation updates for.
   * @param array $options
   *   An associative array of options. Currently supported options are:
   *   - limit (int, optional, defaults to 50): The maximum number of
   *     operations to update.
   *   - access_check (bool, optional, defaults to TRUE): Whether to check
   *     access when querying for pending operations. It should be set to TRUE
   *     when calling from Cron or Drush e.g. for regularly updating pending
   *     operations; these are executed as the anonymous users and there could
   *     be permission issues that would prevent the operations from being
   *     loaded.
   *
   * @throws \RuntimeException
   *   When the operation type does not support updating operations.
   * @throws \RuntimeException
   *   When the given operation type does not properly define the operation
   *   pending states.
   */
  public function updateForOperationType(
    OperationTypeInterface $operation_type,
    array $options = []
  );

}
