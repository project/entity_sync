<?php

namespace Drupal\entity_sync_async\Operation;

// Drupal modules.
use Drupal\entity_sync\Entity\OperationInterface;
use Drupal\entity_sync\Entity\OperationTypeInterface;
use Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface;
use Drupal\entity_sync\MachineName\Entity\Type as EntityType;
use Drupal\entity_sync\MachineName\Field\Operation as OperationField;
// Drupal core.
use Drupal\Core\Entity\EntityTypeManagerInterface;
// External libraries.
use Psr\Log\LoggerInterface;

/**
 * The default operation update manager.
 */
class UpdateManager implements UpdateManagerInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The module's logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config entity loader.
   *
   * @var \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface
   */
  protected $configEntityLoader;

  /**
   * Constructs a new UpdateManager object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The module's logger.
   * @param \Drupal\entity_sync\Entity\Storage\ConfigEntityLoaderInterface $config_entity_loader
   *   The config entity loader.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    ConfigEntityLoaderInterface $config_entity_loader
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->configEntityLoader = $config_entity_loader;
  }

  /**
   * {@inheritdoc}
   */
  public function update(OperationInterface $operation) {
    $operation_type = $this->configEntityLoader->load($operation->bundle());

    $this->operationTypeSupportsUpdating($operation_type);

    $pending_states = $this->operationTypePendingStates($operation_type);
    $this->validatePendingState(
      $operation,
      $pending_states
    );

    $this->doUpdateOperation(
      $operation_type,
      $operation
    );
  }

  /**
   * {@inheritdoc}
   */
  public function updateForOperationType(
    OperationTypeInterface $operation_type,
    array $options = []
  ) {
    $this->operationTypeSupportsUpdating($operation_type);

    $options = array_merge(
      [
        'limit' => 50,
        'access_check' => TRUE,
      ],
      $options
    );

    $pending_states = $this->operationTypePendingStates($operation_type);

    $storage = $this->entityTypeManager->getStorage(EntityType::OPERATION);
    $operation_ids = $storage->getQuery()
      ->accessCheck($options['access_check'])
      ->condition('type', $operation_type->id(), '=')
      ->condition('state', $pending_states, 'IN')
      ->range(0, $options['limit'])
      ->sort('id', 'ASC')
      ->execute();
    if (!$operation_ids) {
      return;
    }

    $operations = $storage->loadMultiple($operation_ids);
    foreach ($operations as $operation) {
      // We catch and log errors so that we can proceed and update other
      // operations. Otherwise all operations could be prevented from
      // getting updated when a single operation fails for some reason.
      try {
        $this->doUpdateOperation($operation_type, $operation);
      }
      catch (\Throwable $throwable) {
        $this->logger->error(
          'An error occurred while updating the operation with ID "%operation_id". Error type: "%error_type". Error message: @message.',
          [
            '%operation_id' => $operation->id(),
            '%error_type' => get_class($throwable),
            '@message' => $throwable->getMessage(),
          ]
        );
      }
    }
  }

  /**
   * Updates the given operation.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type.
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation to update.
   */
  protected function doUpdateOperation(
    OperationTypeInterface $operation_type,
    OperationInterface $operation
  ) {
    $operation_type->getPlugin()->updateOperation($operation_type, $operation);
  }

  /**
   * Checks whether the given operation type supports updating operations.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type.
   *
   * @throws \RuntimeException
   *   When the given operation type does not support updating transactions.
   */
  protected function operationTypeSupportsUpdating(
    OperationTypeInterface $operation_type
  ) {
    $plugin = $operation_type->getPlugin();
    if ($plugin instanceof SupportsOperationUpdatingInterface) {
      return;
    }

    throw new \RuntimeException(sprintf(
      'The "%s" operation type does not support operation updating.',
      $operation_type->id()
    ));
  }

  /**
   * Returns the operation states that are considered pending.
   *
   * @param \Drupal\entity_sync\Entity\OperationTypeInterface $operation_type
   *   The operation type.
   *
   * @return array
   *   An array containing the workflow states that are considered pending.
   *
   * @throws \RuntimeException
   *   When the given operation type does not properly define the operation
   *   pending states.
   *
   * @see \Drupal\entity_sync_async\Updater\SupportsOperationUpdatingInterface::operationUpdaterPendingStates()
   */
  protected function operationTypePendingStates(
    OperationTypeInterface $operation_type
  ) {
    $states = $operation_type->getPlugin()->operationUpdaterPendingStates();

    if (!is_array($states)) {
      throw new \RuntimeException(sprintf(
        'No operation states defined for the operation updater for the "%s" operation type.',
        $operation_type->id()
      ));
    }

    return $states;
  }

  /**
   * Validates that the given operation is in a state considered as pending.
   *
   * @param \Drupal\entity_sync\Entity\OperationInterface $operation
   *   The operation to validate.
   * @param array $pending_states
   *   An array containing the state IDs that are considered to be pending.
   */
  protected function validatePendingState(
    OperationInterface $operation,
    array $pending_states
  ) {
    // We do not need to check whether the field is empty. The state field
    // should never be empty otherwise something is wrong; let an error be
    // raised.
    $state = $operation->get(OperationField::STATE)->first()->getId();
    if (in_array($state, $pending_states)) {
      return;
    }

    throw new \RuntimeException(sprintf(
      'Requested to fetch updates for the operation with ID "%s" of type "%s" that is in the "%s" state which is not considered to be pending updates by the operation type.',
      $operation->id(),
      $operation->bundle(),
      $state
    ));
  }

}
