<?php

namespace Drupal\entity_sync_async\OperationConfigurator;

use Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait;
use Drupal\entity_sync\OperationConfigurator\FieldTrait\RemoteTrait;
use Drupal\entity_sync\OperationConfigurator\PluginBase;

/**
 * Base class for plugins the operations of which relate to remote operation.
 *
 * Abstract plugin classes are provided for easily creating simple plugin
 * implementations with minimal code. Using traits directly is recommended for
 * complex implementations.
 *
 * @see \Drupal\entity_sync\OperationConfigurator\FieldTrait\EntityTrait
 * @see \Drupal\entity_sync\OperationConfigurator\FieldTrait\RemoteTrait
 */
abstract class EntityAsyncBase extends PluginBase {

  use EntityTrait;
  use RemoteTrait;

  /**
   * {@inheritdoc}
   *
   * A message is commonly given when collecting the result for an asynchronous
   * operation. The message may include whether the operation was successful or
   * not and details about any errors that may have occurred.
   *
   * We currently do not store such messages in the database because otherwise
   * it could contribute to the database getting really large. We may add in the
   * future a field for holding the message but accompanied by configuration
   * that allows enabling/disabling storing the message and cleaning up old
   * operations.
   */
  public function bundleFieldDefinitions() {
    $fields = parent::bundleFieldDefinitions();

    // Add the dynamic entity reference field.
    $fields += $this->buildEntityBundleFieldDefinitions();

    // Add the fields for storing the remote ID and state.
    $fields += $this->buildRemoteIdBundleFieldDefinitions();
    $fields += $this->buildRemoteStateBundleFieldDefinitions();

    return $fields;
  }

}
