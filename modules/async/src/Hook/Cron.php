<?php

namespace Drupal\entity_sync_async\Hook;

use Drupal\entity_sync\OperationConfigurator\PluginManagerInterface;
use Drupal\entity_sync_async\Operation\SupportsOperationUpdatingInterface;
use Drupal\entity_sync_async\Operation\UpdateManagerInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Holds methods implementing hooks related to cron.
 */
class Cron {

  /**
   * The operation type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $operationTypeStorage;

  /**
   * The operation update manager.
   *
   * @var \Drupal\entity_sync_async\Operation\UpdateManagerInterface
   */
  protected $operationUpdater;

  /**
   * The operation configurator plugin manager.
   *
   * @var \Drupal\entity_sync\OperationConfigurator\PluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a new Cron object.
   *
   * @param \Drupal\entity_sync\OperationConfigurator\PluginManagerInterface $plugin_manager
   *   The operation configurator plugin manager.
   * @param \Drupal\entity_sync_async\Operation\UpdateManagerInterface $operation_updater
   *   The operation update manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    PluginManagerInterface $plugin_manager,
    UpdateManagerInterface $operation_updater,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->pluginManager = $plugin_manager;
    $this->operationUpdater = $operation_updater;

    $this->operationTypeStorage = $entity_type_manager
      ->getStorage('entity_sync_operation_type');
  }

  /**
   * Updates pending transactions for supporting operation types.
   */
  public function run() {
    $types = $this->operationTypeStorage->loadMultipleWithPluginInstantiated();
    foreach ($types as $type) {
      if (!$type->getPlugin() instanceof SupportsOperationUpdatingInterface) {
        continue;
      }

      $this->operationUpdater->updateForOperationType(
        $type,
        ['access_check' => FALSE]
      );
    }
  }

}
