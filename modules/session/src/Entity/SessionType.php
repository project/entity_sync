<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Entity;

// Entity Synchronization.
use Drupal\entity_sync_session\SessionConfigurator\PluginInterface;
// Drupal core.
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * The default implementation of the SessionType entity.
 *
 * phpcs:disable
 * @ConfigEntityType(
 *   id = "entity_sync_session_type",
 *   label = @Translation("Entity Synchronization session type"),
 *   label_collection = @Translation("Entity Synchronization session types"),
 *   label_singular = @Translation("Entity Synchronization session type"),
 *   label_plural = @Translation("Entity Synchronization session types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count Entity Synchronization session type",
 *     plural = "@count Entity Synchronization session types",
 *   ),
 *   config_prefix = "session_type",
 *   handlers = {
 *     "storage" = "Drupal\entity_sync_session\Entity\Storage\SessionType",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "uuid",
 *     "label",
 *     "description",
 *     "plugin_id",
 *     "plugin_config",
 *   }
 * )
 *
 * @I Add handler such as list builder, storage, forms
 *    type     : feature
 *    priority : normal
 *    labels   : ux
 * phpcs:enable
 */
class SessionType extends ConfigEntityBase implements
  SessionTypeInterface {

  /**
   * The machine name of the session type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the session type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of the session type.
   *
   * @var string
   */
  protected $description;

  /**
   * The session configurator plugin ID for the session type.
   *
   * @var string
   */
  protected $plugin_id;

  /**
   * The session configurator plugin configuration for the session type.
   *
   * @var array
   */
  protected $plugin_config = [];

  /**
   * The session configurator plugin.
   *
   * @var \Drupal\entity_sync_session\SessionConfigurator\PluginInterface
   */
  protected $plugin;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId(): string {
    return $this->plugin_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setPluginId(string $plugin_id): SessionTypeInterface {
    $this->plugin_id = $plugin_id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin(): PluginInterface {
    return $this->plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function setPlugin(PluginInterface $plugin): SessionTypeInterface {
    $this->plugin = $plugin;
    $this->plugin_config = $plugin->getConfiguration();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfiguration(): array {
    return $this->plugin_config;
  }

}
