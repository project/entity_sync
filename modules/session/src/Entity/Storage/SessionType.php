<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Entity\Storage;

// Entity Synchronization.
use Drupal\entity_sync_session\SessionConfigurator\PluginManagerInterface;
// Drupal core.
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
// Third-party libraries.
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The default session type storage class.
 */
class SessionType extends ConfigEntityStorage {

  /**
   * Constructs an SessionType object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $memory_cache
   *   The memory cache backend.
   * @param \Drupal\entity_sync_session\SessionConfigurator\PluginManagerInterface $plugin_manager
   *   The session configurator plugin manager.
   */
  public function __construct(
    EntityTypeInterface $entity_type,
    ConfigFactoryInterface $config_factory,
    UuidInterface $uuid_service,
    LanguageManagerInterface $language_manager,
    MemoryCacheInterface $memory_cache,
    protected PluginManagerInterface $plugin_manager
  ) {
    parent::__construct(
      $entity_type,
      $config_factory,
      $uuid_service,
      $language_manager,
      $memory_cache
    );

    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(
    ContainerInterface $container,
    EntityTypeInterface $entity_type
  ) {
    return new static(
      $entity_type,
      $container->get('config.factory'),
      $container->get('uuid'),
      $container->get('language_manager'),
      $container->get('entity.memory_cache'),
      $container->get('plugin.manager.entity_sync_session_configurator')
    );
  }

  /**
   * Returns the session type with the given ID.
   *
   * When loading an session type using the `load` method, the plugin is not
   * instantiated and the plugin configuration stored in the session type
   * (`plugin_config` property) is not complete. This is because it only
   * contains the plugin configuration loaded from the configuration storage.
   *
   * When the plugin is actually instantiated, the plugin configuration is
   * merged with any configuration added by the plugin class, such as by the
   * `defaultConfiguration` method.
   *
   * We do not want to instantiate the plugin within the `SessionType` entity
   * class because that would require getting other services (the plugin
   * manager) within the entity class via the Drupal object i.e.
   * `\Drupal::services('plugin.manager.entity_sync_session_configurator')`
   * and that is not really a good practice. It also exceeds the scope of the
   * entity class.
   *
   * We therefore provide a method that facilitates instantiating the plugin
   * when loading an session type.
   *
   * @param string $id
   *   The ID of the session type to load.
   *
   * @return \Drupal\entity_sync_session\Entity\SessionTypeInterface|null
   *   The session type entity, or NULL if no matching entity is found.
   */
  public function loadWithPluginInstantiated(string $id) {
    $session_type = $this->load($id);
    if (!$session_type) {
      return;
    }

    $session_type->setPlugin(
      $this->pluginManager->createInstanceForSessionType($session_type)
    );
    return $session_type;
  }

  /**
   * Returns the session types with given IDs, or all if no IDs provided.
   *
   * @param array $ids
   *   An array of entity IDs, or NULL to load all entities.
   *
   * @return \Drupal\entity_sync_session\Entity\SessionType[]
   *   An array of entity objects indexed by their IDs. Returns an empty array
   *   if no matching entities are found.
   */
  public function loadMultipleWithPluginInstantiated(array $ids = NULL) {
    return array_map(
      function ($session_type) {
        return $session_type->setPlugin(
          $this->pluginManager->createInstanceForSessionType($session_type)
        );
      },
      $this->loadMultiple($ids)
    );
  }

}
