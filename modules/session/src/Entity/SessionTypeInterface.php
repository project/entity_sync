<?php

namespace Drupal\entity_sync_session\Entity;

// Entity Synchronization.
use Drupal\entity_sync_session\SessionConfigurator\PluginInterface;
// Drupal core.
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for session types.
 *
 * We use a mix of config entities and plugins to define the behavior of
 * sessions. The standard config entities (session types) allow for standard
 * bundle functionality e.g. fields, certain configuration etc., while the
 * operator configurator plugins allow developers to create certain
 * configuration, fields and behavior determined in the code that can be used in
 * different bundles.
 */
interface SessionTypeInterface extends
  ConfigEntityInterface,
  EntityDescriptionInterface {

  /**
   * Returns the ID of the session configurator plugin.
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId(): string;

  /**
   * Sets the ID of the session configurator plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return $this
   */
  public function setPluginId(string $plugin_id): SessionTypeInterface;

  /**
   * Returns the session configurator plugin.
   *
   * @return \Drupal\entity_sync_session\SessionConfigurator\PluginInterface
   *   The plugin.
   */
  public function getPlugin(): PluginInterface;

  /**
   * Sets the session configurator plugin.
   *
   * It also updates the plugin configuration stored in the session type.
   *
   * @param \Drupal\entity_sync_session\SessionConfigurator\PluginInterface $plugin
   *   The plugin.
   *
   * @return $this
   */
  public function setPlugin(PluginInterface $plugin): SessionTypeInterface;

  /**
   * Returns the session configurator plugin configuration.
   *
   * @return array
   *   The plugin configuration.
   */
  public function getPluginConfiguration(): array;

}
