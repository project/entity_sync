<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Commands;

use Drush\Commands\DrushCommands;
use KrystalCode\Api\Session\SessionStorageInterface;

/**
 * Commands related to session management.
 */
class Session extends DrushCommands {

  /**
   * Constructs a new Session object.
   *
   * @param \KrystalCode\Api\Session\SessionStorageInterface $sessionStorage
   *   The session storage.
   */
  public function __construct(
    protected SessionStorageInterface $sessionStorage) {
  }

  /**
   * Deletes the session with the given ID.
   *
   * @option string $session_type_id
   *   The type ID of the session. The session of the default type will be
   *   deleted if not provided.
   *
   * @usage drush entity-sync-session:delete-session --session-type-id "my_session"
   *   Deletes the session of type `my_session` from the storage. If no session
   *   type ID is provided, the session of the default type will be deleted.
   *
   * @command entity-sync-session:delete-session
   *
   * @aliases esync-session-ds
   */
  public function delete(array $options = [
    'session-type-id' => NULL,
  ]) {
    if ($options['session-type-id'] === NULL) {
      $this->sessionStorage->delete();
    }
    else {
      $this->sessionStorage->delete($options['session-type-id']);
    }
  }

}
