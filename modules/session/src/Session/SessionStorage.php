<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Session;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;

use KrystalCode\Api\Session\SessionInterface;
use KrystalCode\Api\Session\SessionStorageInterface;
use KrystalCode\Api\Session\SessionWithExpirationInterface;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * A session storage implementation that uses a key/value store.
 *
 * This currently works only with access token sessions. It can be made to work
 * with any sessions e.g. cookie session - given the right discriminator
 * resolver - but it will require the following changes.
 * - Define a `hasExpired` method on `\KrystalCode\Api\Session\SessionInterface`
 *   and call that here instead of fetching the access token and then calling
 *   its `hasExpired` method.
 * - Provide a way to determine the ignored attributes when serializing
 *   depending on the session type instead of hard-coding them or setting them
 *   for all session types whether they exist or not. See
 *   `Drupal\entity_sync_session\Session\SessionStorage::serialize()`.
 */
class SessionStorage implements SessionStorageInterface {

  /**
   * The key value collection used for storing the sessions.
   *
   * @var string
   */
  public const KEY_VALUE_COLLECTION = 'entity_sync.sessions';

  /**
   * The key value store.
   */
  protected KeyValueStoreInterface $store;

  /**
   * Static cache.
   *
   * Associative array keyed by the session type ID and containing the session,
   * or `FALSE` if we know that the session for a given type ID does not exist
   * in the store.
   */
  protected array $cache = [];

  /**
   * The serializer.
   */
  protected SerializerInterface $serializer;

  /**
   * Constructs a new SessionStorage object.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $keyValueFactory
   *   The key value store to use.
   * @param string $discriminatorResolverClass
   *   The class of the discriminator resolver to use for the session
   *   serializer/normalizer.
   */
  public function __construct(
    protected KeyValueFactoryInterface $keyValueFactory,
    protected string $discriminatorResolverClass
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function set(SessionInterface $session): void {
    $type_id = $session->getTypeId();
    $this->cache[$type_id] = $session;
    $this->store()->set($type_id, $this->serialize($this->cache[$type_id]));
  }

  /**
   * {@inheritdoc}
   */
  public function get(
    string $type_id = SessionInterface::SESSION_TYPE_ID_DEFAULT,
    bool $ignore_expired = TRUE
  ): SessionInterface|null {
    $session = $this->getFromCache($type_id);
    if ($session === NULL) {
      return NULL;
    }

    if ($ignore_expired === FALSE) {
      return $session;
    }

    // If the session has an expiration time, we only return it if it has not
    // expired yet.
    if (!$session instanceof SessionWithExpirationInterface) {
      return $session;
    }

    return $session->hasExpired() ? NULL : $session;
  }

  /**
   * {@inheritdoc}
   */
  public function exists(
    string $type_id = SessionInterface::SESSION_TYPE_ID_DEFAULT,
    bool $ignore_expired = TRUE
  ): bool {
    // We load the session object from cache/store; we will need to check
    // whether the session has expired and we'll need the object for that. We
    // could do further optimizations, depending on the underlying key/value
    // storage e.g. database or in-memory, but in most cases it's not worth it.
    // The rest of the logic is essentially the same with that in the `get`
    // method.
    return $this->get($type_id, $ignore_expired) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function delete(
    string $type_id = SessionInterface::SESSION_TYPE_ID_DEFAULT
  ): void {
    // From the interface it is not clear whether key value store
    // implementations would fail if they would be instructed to delete a
    // non-existing entry. We therefore check if the session exists first before
    // requesting to delete it.
    if ($this->store()->has($type_id)) {
      $this->store()->delete($type_id);
    }
    $this->cache[$type_id] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function count(bool $ignore_expired = TRUE): int {
    // We don't get the count from the static cache because we load sessions as
    // requested. There may therefore exist sessions in the store that have not
    // been loaded yet in the cache.
    //
    // Also, the interface requires us to not count expired sessions. We
    // therefore have to filter out expired sessions.
    return count(array_filter(
      $this->store()->getAll(),
      function ($session) use ($ignore_expired) {
        if ($ignore_expired === FALSE) {
          return TRUE;
        }

        if (!$session instanceof SessionWithExpirationInterface) {
          return TRUE;
        }

        return $session->hasExpired() ? FALSE : TRUE;
      }
    ));
  }

  /**
   * Loads and returns a session for the given type ID from the cache.
   *
   * If the session does not exist in the cache, it is loaded directly from the
   * store and stored in the cache before it is returned.
   *
   * @param string $type_id
   *   The session type ID.
   *
   * @return \KrystalCode\Api\Session\SessionInterface|null
   *   The session, or `NULL` if there is no session for the given type ID.
   */
  protected function getFromCache(string $type_id): SessionInterface|null {
    // If the session does not exist in the cache at all, we don't know whether
    // it exists in the store; load it.
    if (!isset($this->cache[$type_id])) {
      // If the cache does not exist in store, we set the cache entry to `FALSE`
      // indicating exactly that so that next time requested we don't have to
      // reload from the store.
      $this->cache[$type_id] = $this->getFromStore($type_id) ?? FALSE;
    }

    return ($this->cache[$type_id] === FALSE) ? NULL : $this->cache[$type_id];
  }

  /**
   * Loads and returns a session for the given type ID directly from the store.
   *
   * @param string $type_id
   *   The session type ID.
   *
   * @return \KrystalCode\Api\Session\SessionInterface|null
   *   The session, or `NULL` if there is no session for the given type ID.
   */
  protected function getFromStore(string $type_id): SessionInterface|null {
    $session = $this->store()->get($type_id);
    if ($session !== NULL) {
      $session = $this->deserialize($session);
    }

    return $session;
  }

  /**
   * Instantiates and returns the key value store.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   *   The key value store.
   */
  protected function store(): KeyValueStoreInterface {
    if (isset($this->store)) {
      return $this->store;
    }

    $this->store = $this->keyValueFactory->get(static::KEY_VALUE_COLLECTION);
    return $this->store;
  }

  /**
   * Instantiates and returns the session serializer.
   *
   * @return \Symfony\Component\Serializer\SerializerInterface
   *   The serializer.
   */
  protected function serializer(): SerializerInterface {
    if (isset($this->serializer)) {
      return $this->serializer;
    }

    $this->serializer = new Serializer(
      [
        new ObjectNormalizer(
          NULL,
          NULL,
          NULL,
          NULL,
          new $this->discriminatorResolverClass()
        ),
      ],
      [new JsonEncoder()]
    );

    return $this->serializer;
  }

  /**
   * Serializes the session object preparing it for storage.
   *
   * @param \KrystalCode\Api\Session\SessionInterface $session
   *   The session to serialize.
   *
   * @return string
   *   The serialized JSON string.
   */
  protected function serialize(SessionInterface $session): string {
    return $this->serializer()->serialize(
      $session,
      JsonEncoder::FORMAT,
      [
        AbstractNormalizer::IGNORED_ATTRIBUTES => [
          // We don't store the `expired` flag. It will not be valid when the
          // session is retrieved after expiration.
          'expired',
          // We don't store the `timeNow` timestamp because it is a static
          // property i.e. shared by all access tokens with the idea that it
          // always holds the request time. However, if we load it then from an
          // old session it results in new tokens created to believe that the
          // expiration time returned by the authorization server (`expires_in`)
          // is on top of the old session's start time thus calculating a wrong
          // expiration time.
          'timeNow',
        ],
      ]
    );
  }

  /**
   * Deserializes a JSON string back into a session object.
   *
   * @param string $serialized_session
   *   The serialized JSON string.
   *
   * @return \KrystalCode\Api\Session\SessionInterface
   *   The deserialized session.
   */
  protected function deserialize(string $serialized_session): SessionInterface {
    return $this->serializer()->deserialize(
      $serialized_session,
      SessionInterface::class,
      JsonEncoder::FORMAT
    );
  }

}
