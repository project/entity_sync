<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Session;

use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer as SymfonyObjectNormalizer;

/**
 * Normalizer for session objects.
 *
 * The access token object requires specific options in its constructor when
 * instantiating. Context builders were introduced in serializer v6.1 while
 * Drupal 9 still requires v4.4+. We therefore provide a custom normalizer
 * that can properly instantiate the access token object contained as a property
 * in the session object.
 */
class ObjectNormalizer extends SymfonyObjectNormalizer {

  /**
   * {@inheritdoc}
   */
  protected function instantiateObject(
    array &$data,
    $class,
    array &$context,
    \ReflectionClass $reflection_class,
    $allowed_attributes,
    string $format = NULL
  ) {
    if ($class !== AccessToken::class) {
      return parent::instantiateObject(
        $data,
        $class,
        $context,
        $reflection_class,
        $allowed_attributes,
        $format
      );
    }

    $parameters = [
      'access_token' => $data['token'],
      'resource_owner_id' => $data['resourceOwnerId'],
      'refresh_token' => $data['refreshToken'],
      'expires' => $data['expires'],
      'token_type' => $data['values']['token_type'],
    ];
    // Some providers do not return the scope with the access token.
    if (isset($data['values']['scope'])) {
      $parameters['scope'] = $data['values']['scope'];
    }

    return new $class($parameters);
  }

}
