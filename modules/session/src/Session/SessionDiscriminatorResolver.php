<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Session;

use Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping;
use Symfony\Component\Serializer\Mapping\ClassDiscriminatorResolverInterface;

/**
 * Provides the discriminator resolver required by the session serializer.
 *
 * The symfony serializer provides resolvers that use annotations or YAML files
 * to load the class mapping from. It becomes complicated to use either for
 * defining the class mapping because:
 * - The access token class is provided by the PHP SDK that is agnostic to
 *   session storage. Other session storages might not require
 *   serialization/deserialization and it is therefore not the right place to
 *   put annotations on the access token class directly.
 * - This module could provide a YAML file with the class mapping; however, it
 *   would not be very easy for other modules or applications to provide an
 *   alternative file via a service provider altering the session manager's
 *   constructor arguments.
 *
 * We therefore provide this custom resolver that is also easy to replace in the
 * session manager's constructor arguments via a service provider, if needed.
 */
class SessionDiscriminatorResolver implements
  ClassDiscriminatorResolverInterface {

  /**
   * {@inheritdoc}
   */
  public function getMappingForClass(
    string $class
  ): ClassDiscriminatorMapping|null {
    return $this->getMapping();
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingForMappedObject(
    $object
  ): ClassDiscriminatorMapping|null {
    return $this->getMapping();
  }

  /**
   * {@inheritdoc}
   */
  public function getTypeForMappedObject($object): ?string {
    return $this
      ->getMappingForMappedObject($object)
      ->getMappedObjectType($object);
  }

  /**
   * Returns the class mapping for the session serializer.
   *
   * @return \Symfony\Component\Serializer\Mapping\ClassDiscriminatorMapping
   *   The class mapping.
   */
  protected function getMapping(): ClassDiscriminatorMapping {
    return new ClassDiscriminatorMapping(
      'serializer_mapping',
      [
        'session:access_token' => 'KrystalCode\Api\Session\OAuth2\AccessTokenSession',
        'authentication:access_token' => 'League\OAuth2\Client\Token\AccessToken',
      ]
    );
  }

}
