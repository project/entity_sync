<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * The annotation object for the session configurator plugins.
 *
 * Plugin namespace: Plugin\EntitySync\SessionConfigurator.
 *
 * @Annotation
 */
class EntitySyncSessionConfigurator extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A short description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
