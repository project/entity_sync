<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator;

// Entity Synchronization.
use Drupal\entity_sync_session\Annotation\EntitySyncSessionConfigurator as PluginAnnotation;
use Drupal\entity_sync_session\Entity\SessionTypeInterface;
// Drupal core.
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * The plugin manager for session configurator plugins.
 *
 * @see \Drupal\entity_sync_session\Annotation\EntitySyncSessionConfigurator
 * @see \Drupal\entity_sync_session\SessionConfigurator\PluginInterface
 * @see plugin_api
 */
class PluginManager extends DefaultPluginManager implements
  PluginManagerInterface {

  /**
   * Constructs a new PluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/EntitySync/SessionConfigurator',
      $namespaces,
      $module_handler,
      PluginInterface::class,
      PluginAnnotation::class
    );

    $this->alterInfo('entity_sync_session_configurator_info');
    $this->setCacheBackend(
      $cache_backend,
      'entity_sync_session_configurator',
      ['entity_sync_session_configurator_plugins']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function createInstanceForSessionType(
    SessionTypeInterface $session_type
  ): PluginInterface {
    return $this->createInstance(
      $session_type->getPluginId(),
      $session_type->getPluginConfiguration() ?? []
    );
  }

}
