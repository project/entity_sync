<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator;

// Drupal core.
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\PluginFormInterface;
// Third-party libraries.
use KrystalCode\Api\Session\SessionManagerInterface;

/**
 * Provides the interface for session configurator plugins.
 *
 * Session configurator plugins are responsible for the following:
 * - Create and manage sessions.
 * - Provide the connection details that will be needed by the client adapter.
 */
interface PluginInterface extends
  ConfigurableInterface,
  DependentPluginInterface,
  PluginFormInterface,
  PluginInspectionInterface,
  SessionManagerInterface {

  /**
   * Returns the plugin label.
   *
   * @return string
   *   The session configurator label.
   */
  public function label(): string;

  /**
   * Returns the authentication configuration.
   *
   * @return array
   *   An array containing the authentication details. The format of the array
   *   depends on the authentication provider.
   */
  public function getAuthentication(): array;

  /**
   * Returns the connection configuration.
   *
   * @return array
   *   An array containing the connection details. The format of the array
   *   depends on the connection provider.
   */
  public function getConnection(): array;

}
