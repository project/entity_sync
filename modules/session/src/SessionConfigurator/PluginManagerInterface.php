<?php

namespace Drupal\entity_sync_session\SessionConfigurator;

// Entity Synchronization.
use Drupal\entity_sync_session\Entity\SessionTypeInterface;
// Drupal core.
use Drupal\Component\Plugin\PluginManagerInterface as ComponentPluginManagerInterface;

/**
 * Defines the interface for the session configuration plugin manager.
 */
interface PluginManagerInterface extends ComponentPluginManagerInterface {

  /**
   * Creates an instance based on the plugin info of the given session type.
   *
   * @param \Drupal\entity_sync_session\Entity\SessionTypeInterface $session_type
   *   The session type.
   *
   * @return \Drupal\entity_sync_session\SessionConfigurator\PluginInterface
   *   The plugin.
   */
  public function createInstanceForSessionType(
    SessionTypeInterface $session_type
  ): PluginInterface;

}
