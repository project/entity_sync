<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator\Oauth2;

// Entity Synchronization.
use Drupal\entity_sync_session\SessionConfigurator\PluginBase;
// Drupal core.
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
// Third-party libraries.
use KrystalCode\Api\Session\SessionInterface;
use KrystalCode\Api\Session\SessionManagerInterface;
use KrystalCode\Api\Session\SessionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for OAuth2 session configurator plugins.
 *
 * Supported configuration on top of what is supported by the base plugin:
 * - session_type_id (string, required): The ID used to store the session in the
 *   key value store.
 */
abstract class Base extends PluginBase implements
  ContainerFactoryPluginInterface {

  /**
   * Instantiates and returns the session manager.
   *
   * @return \KrystalCode\Api\Session\SessionManagerInterface
   *   The session manager.
   */
  abstract protected function sessionManager(): SessionManagerInterface;

  /**
   * The session manager.
   *
   * @var \KrystalCode\Api\Session\SessionManagerInterface
   */
  protected SessionManagerInterface|null $sessionManager = NULL;

  /**
   * Constructs a new Base object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \KrystalCode\Api\Session\SessionStorageInterface $sessionStorage
   *   The session storage.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected SessionStorageInterface $sessionStorage
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_sync_session.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setOptions(array $options): void {
    $this->sessionManager()->setOptions($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(): array {
    return $this->sessionManager()->getOptions();
  }

  /**
   * {@inheritdoc}
   */
  public function connect(): SessionInterface {
    return $this->sessionManager()->connect();
  }

}
