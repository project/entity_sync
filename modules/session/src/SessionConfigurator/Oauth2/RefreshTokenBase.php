<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator\Oauth2;

// Third-party libraries.
use KrystalCode\Api\Session\OAuth2\RefreshTokenSessionManager;
use KrystalCode\Api\Session\SessionManagerInterface;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Base class for plugins obtaining a token by refreshing an existing one.
 *
 * Authentication configuration structure:
 * - client_id (string, required): The client ID.
 * - client_secret (string, required): The client secret.
 * - url_authorize (string, required): The authorization URL.
 * - url_access_token (string, required): The token URL.
 * - url_resource_owner_details (string, required): The resource owner URL.
 * - grant (array, optional): An associative array containing settings related
 *   to the grant:
 *   - refresh_token (string, optional): The refresh token; if not provided, it
 *     will be loaded from the active or expired session in the storage. An
 *     error will be raised if that does not exist.
 *   - scope (string, optional): The scope.
 */
abstract class RefreshTokenBase extends Base {

  /**
   * {@inheritdoc}
   */
  protected function sessionManager(): SessionManagerInterface {
    if ($this->sessionManager !== NULL) {
      return $this->sessionManager;
    }

    $authentication = &$this->configuration['authentication'];
    $options = [
      'grant' => [],
      'type_id' => $this->configuration['session_type_id'],
    ];

    $refresh_token = $this->getRefreshToken();
    if ($refresh_token !== NULL) {
      $options['grant']['refresh_token'] = $refresh_token;
    }
    if (isset($authentication['scope'])) {
      $options['grant']['scope'] = $authentication['scope'];
    }

    $this->sessionManager = new RefreshTokenSessionManager(
      new GenericProvider([
        'clientId' => $authentication['client_id'],
        'clientSecret' => $authentication['client_secret'],
        'urlAuthorize' => $authentication['url_authorize'],
        'urlAccessToken' => $authentication['url_access_token'],
        'urlResourceOwnerDetails' => $authentication['url_resource_owner_details'],
      ]),
      $this->sessionStorage,
      $options
    );

    return $this->sessionManager;
  }

  /**
   * Returns the refresh token that will be used to get a new access token.
   *
   * By default, this method returns `NULL` which will cause the session manager
   * to get the refresh token from the stored session - active or expired.
   *
   * Child plugin implementations can override this method if needed, for
   * example, to load the refresh token from settings or configuration.
   *
   * @return string|null
   *   The refresh token, or `NULL` to load it from the session.
   */
  protected function getRefreshToken(): string|null {
    return NULL;
  }

}
