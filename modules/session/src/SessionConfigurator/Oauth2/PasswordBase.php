<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator\Oauth2;

// Third-party libraries.
use KrystalCode\Api\Session\OAuth2\PasswordSessionManager;
use KrystalCode\Api\Session\SessionManagerInterface;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Base class for plugins using OAuth2 Resource Owner Password Credentials.
 *
 * Authentication configuration structure:
 * - client_id (string, required): The client ID.
 * - client_secret (string, required): The client secret.
 * - url_authorize (string, required): The authorization URL.
 * - url_access_token (string, required): The token URL.
 * - url_resource_owner_details (string, required): The resource owner URL.
 * - grant (array, required): An associative array containing settings related
 *   to the grant:
 *   - username (string, required): The username.
 *   - password (string, required): The password.
 *   - scope (string, optional): The scope.
 */
abstract class PasswordBase extends Base {

  /**
   * {@inheritdoc}
   */
  protected function sessionManager(): SessionManagerInterface {
    if ($this->sessionManager !== NULL) {
      return $this->sessionManager;
    }

    $authentication = &$this->configuration['authentication'];
    $options = [
      'grant' => [
        'username' => $authentication['username'],
        'password' => $authentication['password'],
      ],
      'type_id' => $this->configuration['session_type_id'],
    ];
    if (isset($authentication['scope'])) {
      $options['grant']['scope'] = $authentication['scope'];
    }

    $this->sessionManager = new PasswordSessionManager(
      new GenericProvider([
        'clientId' => $authentication['client_id'],
        'clientSecret' => $authentication['client_secret'],
        'urlAuthorize' => $authentication['url_authorize'],
        'urlAccessToken' => $authentication['url_access_token'],
        'urlResourceOwnerDetails' => $authentication['url_resource_owner_details'],
      ]),
      $this->sessionStorage,
      $options
    );

    return $this->sessionManager;
  }

}
