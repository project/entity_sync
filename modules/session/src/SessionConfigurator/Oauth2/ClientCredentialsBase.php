<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator\Oauth2;

// Third-party libraries.
use KrystalCode\Api\Session\OAuth2\ClientCredentialsSessionManager;
use KrystalCode\Api\Session\SessionManagerInterface;
use League\OAuth2\Client\Provider\GenericProvider;

/**
 * Base class for plugins using OAuth2 Client Credentials.
 *
 * Authentication configuration structure:
 * - client_id (string, required): The client ID.
 * - client_secret (string, required): The client secret.
 * - url_authorize (string, required): The authorization URL.
 * - url_access_token (string, required): The token URL.
 * - url_resource_owner_details (string, required): The resource owner URL.
 * - grant (array, optional): An associative array containing settings related
 *   to the grant:
 *   - scope (string, optional): The scope.
 */
abstract class ClientCredentialsBase extends Base {

  /**
   * {@inheritdoc}
   */
  protected function sessionManager(): SessionManagerInterface {
    if ($this->sessionManager !== NULL) {
      return $this->sessionManager;
    }

    $authentication = &$this->configuration['authentication'];
    $options = [
      'grant' => [],
      'type_id' => $this->configuration['session_type_id'],
    ];
    if (isset($authentication['scope'])) {
      $options['grant']['scope'] = $authentication['scope'];
    }

    $this->sessionManager = new ClientCredentialsSessionManager(
      new GenericProvider([
        'clientId' => $authentication['client_id'],
        'clientSecret' => $authentication['client_secret'],
        'urlAuthorize' => $authentication['url_authorize'],
        'urlAccessToken' => $authentication['url_access_token'],
        'urlResourceOwnerDetails' => $authentication['url_resource_owner_details'],
      ]),
      $this->sessionStorage,
      $options
    );

    return $this->sessionManager;
  }

}
