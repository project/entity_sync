<?php

declare(strict_types=1);

namespace Drupal\entity_sync_session\SessionConfigurator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase as CorePluginBase;

/**
 * Default base class for session configurator plugin implementations.
 *
 * Supported configuration options:
 * - authentication (array, optional): The details required by the session
 *   manager for authenticating to the remote system and creating session.
 *   The supported properties depend on the remote system and the
 *   authentication/authorization method.
 * - connection (array, optional): The details required by the client adapter to
 *   exchange data with the remote system using the session provided by the
 *   session manager.
 * - duration (array, optional): The minimum session duration. Supported
 *   properties are:
 *   - interval (int, required): The minimum duration of the session.
 *   - limit (int, optional): The maximum duration of the session supported
 *     by the API provider, if known.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface {

  /**
   * Constructs a new PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Always include the default configuration.
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [
      'module' => [$this->pluginDefinition['provider']],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // Settings required for authenticating and creating sessions.
      'authentication' => [],
      // Settings required for creating connections in a session i.e. making API
      // calls.
      'connection' => [],
      // The minimum session duration.
      'duration' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   *
   * @I Add form element for configuring the default session duration
   *    type     : feature
   *    priority : low
   *    labels   : ux
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
  }

  /**
   * {@inheritdoc}
   *
   * Override this function if you need to do something specific to the
   * submitted data before it is saved as configuration on the plugin. The data
   * gets saved on the plugin in the session type form.
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $form_state
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthentication(): array {
    return $this->configuration['authentication'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConnection(): array {
    return $this->configuration['connection'];
  }

}
